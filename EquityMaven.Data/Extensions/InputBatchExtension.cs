﻿using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Extensions
{
    public static class InputBatchExtension
    {
        public static void CopyFrom(this InputBatch to, InputBatch from)
        {
            to.ActiveFrom = from.ActiveFrom;
            to.Description = from.Description;
            to.Created = from.Created;
            //to.Id = from.Id;
            to.isActive = from.isActive;
            to.LastUpdated = from.LastUpdated;
            to.BatchType = from.BatchType;
        }
    }
}

