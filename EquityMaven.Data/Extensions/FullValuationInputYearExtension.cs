﻿using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Extensions
{
    public static class FullValuationInputYearExtension
    {
        //public static void FullCopyFrom(this Entities.FullValuationInputYear to, IFullValuationInputYear from, FullValuationInput fv = null)
        //{
        //    //copy the generall fields
        //    to.CopyFrom(from, fv);
        //    to.FullValuationInput = fv;

        //    to.IncomeStatement_EBIT = from.IncomeStatement_EBIT;
        //    to.IncomeStatement_EBITDA = from.IncomeStatement_EBITDA;
        //    to.IncomeStatement_EarningsBeforeTax = from.IncomeStatement_EarningsBeforeTax;
        //    to.IncomeStatement_GrossProfit = from.IncomeStatement_GrossProfit;



        //    to.AssetsProjection_FixedAndIntangibleAssets = from.AssetsProjection_FixedAndIntangibleAssets;
        //    to.BalanceSheetAnalysis_AverageAccountsPayableDays = from.BalanceSheetAnalysis_AverageAccountsPayableDays;
        //    to.BalanceSheetAnalysis_AverageAccountsReceiveableDays = from.BalanceSheetAnalysis_AverageAccountsReceiveableDays;
        //    to.BalanceSheetAnalysis_AverageInventoryDays = from.BalanceSheetAnalysis_AverageInventoryDays;
        //    to.BalanceSheetAnalysis_CurrentRatio = from.BalanceSheetAnalysis_CurrentRatio;
        //    to.BalanceSheetAnalysis_TotalDebtOverEBITDA = from.BalanceSheetAnalysis_TotalDebtOverEBITDA;
        //    to.BalanceSheetEquityAndLiabilities_LongTermDebt = from.BalanceSheetEquityAndLiabilities_LongTermDebt;
        //    to.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = from.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities;
        //    to.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = from.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities;
        //    to.BalanceSheetEquityAndLiabilities_ShareholderLoans = from.BalanceSheetEquityAndLiabilities_ShareholderLoans;
        //    to.BalanceSheetEquityAndLiabilities_ShortTermDebt = from.BalanceSheetEquityAndLiabilities_ShortTermDebt;
        //    to.BalanceSheetTotalAssets_Calculated_CurrentLiabilities = from.BalanceSheetTotalAssets_Calculated_CurrentLiabilities;
        //    to.BalanceSheetTotalAssets_CurrentAssets = from.BalanceSheetTotalAssets_CurrentAssets;
        //    to.BalanceSheetTotalAssets_TotalAssets = from.BalanceSheetTotalAssets_TotalAssets;
          
        //    to.ExcessCash_ExcessCash = from.ExcessCash_ExcessCash;
        //    to.ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue = from.ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue;
        //    to.ExcessCash_RevenueTTM = from.ExcessCash_RevenueTTM;
        //    to.IncomeStatementAnalysis_Calculated_InterestCover = from.IncomeStatementAnalysis_Calculated_InterestCover;
        //    to.IncomeStatementAnalysis_EBITDAMargin = from.IncomeStatementAnalysis_EBITDAMargin;
        //    to.IncomeStatementAnalysis_EBITMargin = from.IncomeStatementAnalysis_EBITMargin;
     
        //    to.SustainableEBITDA_SustainableEBITDA = from.SustainableEBITDA_SustainableEBITDA;
        //    to.SustainableEBITDA_SustainableEBITDA_TTM = from.SustainableEBITDA_SustainableEBITDA_TTM;
        //    to.BalanceSheetTotalAssets_FixedAndIntangibleAssets = from.BalanceSheetTotalAssets_FixedAndIntangibleAssets;
        //    to.CapitalExpenditure_TTM = from.CapitalExpenditure_TTM;
            
        //}

        //public static void CopyFrom(this IFullValuationInputYear to, IFullValuationInputYear from, IFullValuationInput fv = null)
        //{
        //    to.FullValuationInputId = fv?.Id ?? from.FullValuationInputId;
        //    to.Id = from.Id;


        //    to.IncomeStatement_Revenue = from.IncomeStatement_Revenue;
        //    to.IncomeStatement_CostOfGoodsSold = from.IncomeStatement_CostOfGoodsSold;
        //    to.IncomeStatement_OperatingExpense = from.IncomeStatement_OperatingExpense;
        //    to.IncomeStatement_DepreciationAndAmortisationExpense = from.IncomeStatement_DepreciationAndAmortisationExpense;
        //    to.IncomeStatement_InterestExpense = from.IncomeStatement_InterestExpense;
        //    to.IncomeStatement_NonCashExpense = from.IncomeStatement_NonCashExpense;


        //    to.IncomeStatementAnalysis_PercentageRevenueGrowth = from.IncomeStatementAnalysis_PercentageRevenueGrowth;
        //    to.IncomeStatementAnalysis_PercentageGrossProfitMargin = from.IncomeStatementAnalysis_PercentageGrossProfitMargin;
        //    to.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = from.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue;
        //    to.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = from.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue;
        //    to.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = from.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue;




        //    to.BalanceSheetTotalAssets_AccountsReceiveable = from.BalanceSheetTotalAssets_AccountsReceiveable;
        //    to.BalanceSheetTotalAssets_Inventory = from.BalanceSheetTotalAssets_Inventory;
        //    to.BalanceSheetTotalAssets_OtherCurrentAssets = from.BalanceSheetTotalAssets_OtherCurrentAssets;
        //    to.BalanceSheetTotalAssets_CashAndCashEquivalents = from.BalanceSheetTotalAssets_CashAndCashEquivalents;
        //    to.BalanceSheetTotalAssets_IntangibleAssets = from.BalanceSheetTotalAssets_IntangibleAssets;
        //    to.BalanceSheetTotalAssets_FixedAssets = from.BalanceSheetTotalAssets_FixedAssets;


        //    to.BalanceSheetEquityAndLiabilities_AccountsPayable = from.BalanceSheetEquityAndLiabilities_AccountsPayable;
        //    //to.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = from.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities;
        //    //to.BalanceSheetEquityAndLiabilities_ShortTermDebt = from.BalanceSheetEquityAndLiabilities_ShortTermDebt;
        //    //to.BalanceSheetEquityAndLiabilities_LongTermDebt = from.BalanceSheetEquityAndLiabilities_LongTermDebt;
        //    //to.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = from.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities;
        //    //to.BalanceSheetEquityAndLiabilities_ShareholderLoans = from.BalanceSheetEquityAndLiabilities_ShareholderLoans;



        //    to.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = from.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue;
        //    to.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold;
        //    to.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold;
        //    to.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold;
        //    to.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold;


        //    to.CapitalExpenditure_CapitalExpenditure = from.CapitalExpenditure_CapitalExpenditure;
        //    to.CapitalExpenditure_PercentageForecastCapitalExpenditure = from.CapitalExpenditure_PercentageForecastCapitalExpenditure;

        //    to.SustainableEBITDA_NonRecurringExpenses = from.SustainableEBITDA_NonRecurringExpenses;
        //    to.SustainableEBITDA_NonRecurringIncome = from.SustainableEBITDA_NonRecurringIncome;

        //    to.ExcessCash_RequiredPercentageOperatingCash = from.ExcessCash_RequiredPercentageOperatingCash;


        //}



    }
}
