﻿using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Extensions
{
    public static class TRBCValueExtensions
    {

      

        public static void CopyFrom(this TRBCValue to, TRBCValue from)
        {
            to.AccPayableDaysAve = from.AccPayableDaysAve;
            to.AccRecDaysAve = from.AccRecDaysAve;
            to.Africa = from.Africa;
            to.Asia = from.Asia;
            to.AustraliaNewZealand = from.AustraliaNewZealand;
            to.CurrentRatio = from.CurrentRatio;
            to.EasternEuropeRussia = from.EasternEuropeRussia;
            to.EBITDAMargin = from.EBITDAMargin;
            to.EBITMargin = from.EBITMargin;
            to.GrossMargin = from.GrossMargin;
            to.InterestCover = from.InterestCover;
            to.InterestCoverAdjusted = from.InterestCoverAdjusted;
            to.InventoryDaysAve = from.InventoryDaysAve;
            to.MiddleEast = from.MiddleEast;
            to.NorthAmerica = from.NorthAmerica;
            to.SouthCentralAmerica = from.SouthCentralAmerica;
            to.SouthernEurope = from.SouthernEurope;
            to.TotalDebtEBITDA = from.TotalDebtEBITDA;
            to.TotalDebtMarketCap = from.TotalDebtMarketCap;
            to.UnleveredBeta = from.UnleveredBeta;
            to.WesternEurope = from.WesternEurope;
            to.World = from.World;
        }
    }
}
