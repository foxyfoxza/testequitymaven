﻿using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Extensions
{
    public static class CurrencyExtension
    {
        public static void CopyFrom(this Currency to, Currency from)
        {
            to.ISOCurrencySymbol = from.ISOCurrencySymbol;
            to.CurrencyEnglishName = from.CurrencyEnglishName;
        }
    }
}

