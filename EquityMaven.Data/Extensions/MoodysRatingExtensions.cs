﻿using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Extensions
{
    public static class MoodysRatingExtensions
    {
        
        public static void CopyFrom(this MoodysRating to, MoodysRating from)
        {
            to.Rating = from.Rating;
            to.DefaultSpread = from.DefaultSpread;
        }
    }
}
