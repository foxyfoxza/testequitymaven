﻿using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Extensions
{
    public static class PrelimEvaluationInputsExtensions
    {
        public static void CopyFrom(this IPrelimEvaluationInputs to, IPrelimEvaluationInputs from)
        {
            to.CompanyName = from.CompanyName;
            to.CurrentCashOnHand = from.CurrentCashOnHand;
            to.CurrentInterestBearingDebt = from.CurrentInterestBearingDebt;
            to.ExcessCash_CurrentCashOnHandAsAtLastMonthEnd = from.ExcessCash_CurrentCashOnHandAsAtLastMonthEnd;
            to.IncomeStatement_CostOfGoodsSold = from.IncomeStatement_CostOfGoodsSold;
            to.IncomeStatement_OperatingExpense = from.IncomeStatement_OperatingExpense;
            to.IncomeStatement_Revenue = from.IncomeStatement_Revenue;
            to.LastFinancialYearEnd = from.LastFinancialYearEnd;
            to.PercentageOperatingCashRequired = from.PercentageOperatingCashRequired;
            to.TRBC2012HierarchicalID = from.TRBC2012HierarchicalID;
        }

    }
}
