﻿
using EquityMaven.CommonEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data
{
     
    public partial class ClientInputsFinancial
    {
        private ClientInputsGeneral ClientInputsGeneral;
        public ClientInputsFinancial(FinancialYearType financialYearType, ClientInputsGeneral clientInputsGeneral)
        {
            this.FinancialYearType = financialYearType;
            this.ClientInputsGeneral = clientInputsGeneral;
        }
        public FinancialYearType FinancialYearType { get; private set; }
        public ClientInputsFinancial Prior { get; set; }
        public ClientInputsFinancial Next { get; set; }

        #region General

        public int MonthsSinceLastFinancialYearEnd { get { return ClientInputsGeneral.MonthsSinceLastFinancialYearEnd; } }
        public decimal CurrentCashOnHand { get { return ClientInputsGeneral.CurrentCashOnHand; } }

        #endregion

        #region Income Statement
        
        public decimal IncomeStatement_Revenue
        {
            get
            {
                return (FinancialYearType.IsActual()) ? _IncomeStatement_Revenue : Prior.IncomeStatement_Revenue * (1 + IncomeStatementAnalysis_PercentageRevenueGrowth);
            }
            set
            {
                _IncomeStatement_Revenue = value;
            }
        }
        private decimal _IncomeStatement_Revenue;

        /// <summary>
        /// Cost of goods sold
        /// </summary>
        public decimal IncomeStatement_CostOfGoodsSold
        {
            get
            {
                return (FinancialYearType.IsActual()) ? _IncomeStatement_CostOfGoods : IncomeStatement_Revenue - IncomeStatement_GrossProfit;
            }
            set
            {
                _IncomeStatement_CostOfGoods = value;
            }
        }
        private decimal _IncomeStatement_CostOfGoods;

        public decimal IncomeStatement_GrossProfit
        {
            get
            {
                return (FinancialYearType.IsActual()) ? IncomeStatement_Revenue - IncomeStatement_CostOfGoodsSold : IncomeStatement_Revenue * IncomeStatementAnalysis_PercentageGrossProfitMargin;
            }
        }

        /// <summary>
        /// Operating expenses(excluding depreciation & amortisation)
        /// </summary>
        public decimal IncomeStatement_OperatingExpense
        {
            get
            {
                return (FinancialYearType.IsActual()) ? _IncomeStatement_OperatingExpense : IncomeStatement_Revenue * IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue;
            }
            set
            {
                _IncomeStatement_OperatingExpense = value;
            }
        }
        private decimal _IncomeStatement_OperatingExpense;


        /// <summary>
        /// Earnings before interest, tax, depreciation, amortisation("EBITDA")
        /// </summary>
        public decimal IncomeStatement_EBITDA
        {
            get
            {
                return IncomeStatement_GrossProfit - IncomeStatement_OperatingExpense;
            }
        }




        /// <summary>
        /// Depreciation and amortisation expense
        /// </summary>
        public decimal IncomeStatement_DepreciationAndAmortisationExpense
        {
            get
            {
                return FinancialYearType.IsActual() ? _IncomeStatement_DepreciationAndAmortisationExpense : IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue * IncomeStatement_Revenue;
            }
            set
            {
                _IncomeStatement_DepreciationAndAmortisationExpense = value;
            }
        }
        private decimal _IncomeStatement_DepreciationAndAmortisationExpense;

        /// <summary>
        /// Earnings before interest and tax ("EBIT")
        /// </summary>
        public decimal IncomeStatement_EBIT { get { return IncomeStatement_EBITDA - IncomeStatement_DepreciationAndAmortisationExpense; } }

        public decimal IncomeStatement_InterestExpense { get; set; }
        public decimal IncomeStatement_EarningsBeforeTax { get { return IncomeStatement_EBIT - IncomeStatement_InterestExpense; } }

        /// <summary>
        /// Non-cash expenses included in Operating expenses above
        /// </summary>
        public decimal IncomeStatement_NonCashExpense
        {
            get
            {
                return FinancialYearType.IsActual() ? _IncomeStatement_NonCashExpense : IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue * IncomeStatement_Revenue;
            }
            set
            {
                _IncomeStatement_NonCashExpense = value;
            }
        }
        private decimal _IncomeStatement_NonCashExpense;
        public Decimal IncomeStatementAnalysis_PercentageRevenueGrowth
        {
            get
            {
                return (FinancialYearType.IsActual() && Prior != null) ? (IncomeStatement_Revenue / Prior.IncomeStatement_Revenue) - 1 : _IncomeStatementAnalysis_PercentageRevenueGrowth;
            }
            set
            {
                _IncomeStatementAnalysis_PercentageRevenueGrowth = value;
            }
        }
        private Decimal _IncomeStatementAnalysis_PercentageRevenueGrowth;

        public decimal IncomeStatementAnalysis_PercentageGrossProfitMargin
        {
            get
            {
                return (FinancialYearType.IsActual()) ? (IncomeStatement_GrossProfit / IncomeStatement_Revenue) : _IncomeStatementAnalysis_PercentageGrossProfitMargin;
            }
            set
            {
                _IncomeStatementAnalysis_PercentageGrossProfitMargin = value;
            }
        }
        private decimal _IncomeStatementAnalysis_PercentageGrossProfitMargin { get; set; }
        public decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue
        {
            get
            {
                return (FinancialYearType.IsActual()) ? IncomeStatement_OperatingExpense / IncomeStatement_Revenue : _IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue;
            }

            set
            {
                _IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = value;
            }
        }
        private decimal _IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue;


        public Decimal IncomeStatementAnalysis_EBITDAMargin { get { return IncomeStatement_EBITDA / IncomeStatement_Revenue; } }
        //private decimal _IncomeStatementAnalysis_EBITDAMargin;
        public Decimal IncomeStatementAnalysis_EBITMargin { get { return IncomeStatement_EBIT / IncomeStatement_Revenue; } }


        /// <summary>
        ///         Depreciation and amortisation as % of Revenue
        /// </summary>
        public Decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue
        {
            get
            {
                return FinancialYearType.IsActual() ? (IncomeStatement_DepreciationAndAmortisationExpense / IncomeStatement_Revenue) : _IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue;
            }
            set
            {
                _IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = value;
            }
        }

        private Decimal _IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue;
        /// <summary>
        ///what percentage Non-cash expenses (included in Operating expenses) (excluding depreciation & amortisation) are expected to be as a percentage of Revenue. 
        /// </summary>
        public Decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue
        {
            get
            {
                return FinancialYearType.IsActual() ? (IncomeStatement_NonCashExpense / IncomeStatement_Revenue) : _IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue;
            }
            set
            {
                _IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = value;
            }
        }
        private decimal _IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue;

        public Decimal IncomeStatementAnalysis_Calculated_InterestCover { get { return IncomeStatement_EBIT / IncomeStatement_InterestExpense; } }

        ////todo - implement this?  CLIENT INPUTS -> E45
        //public Decimal IncomeStatementAnalysis_PercentageFreeCashFlowOfRevenue { get { return 123456 / IncomeStatement_Revenue; } }
        ////todo - implement this? CLIENT INPUTS -> E46
        //public Decimal IncomeStatementAnalysis_PercentageFreeCashFlowOfEBITDA { get { return 456789 / IncomeStatement_EBITDA; } }


        #endregion



        #region Balance Sheet

        public decimal BalanceSheetTotalAssets_CurrentAssets { get { return BalanceSheetTotalAssets_AccountsReceiveable + BalanceSheetTotalAssets_Inventory + BalanceSheetTotalAssets_OtherCurrentAssets + BalanceSheetTotalAssets_CashAndCashEquivalents; } }
        public decimal BalanceSheetTotalAssets_AccountsReceiveable
        {
            get
            {
                return FinancialYearType.IsActual() ? _BalanceSheetTotalAssets_AccountsReceiveable : IncomeStatement_Revenue * BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue;
            }
            set
            {
                _BalanceSheetTotalAssets_AccountsReceiveable = value;
            }
        }

        private decimal _BalanceSheetTotalAssets_AccountsReceiveable;


        public decimal BalanceSheetTotalAssets_Inventory
        {
            get
            {
                return FinancialYearType.IsActual() ? _BalanceSheetTotalAssets_Inventory : (IncomeStatement_CostOfGoodsSold * BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold);
            }
            set
            {
                _BalanceSheetTotalAssets_Inventory = value;
            }
        }
        private decimal _BalanceSheetTotalAssets_Inventory;

        public decimal BalanceSheetTotalAssets_OtherCurrentAssets
        {
            get
            {
                return FinancialYearType.IsActual() ? _BalanceSheetTotalAssets_OtherCurrentAssets
                    : IncomeStatement_CostOfGoodsSold * BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold;
            }
            set
            {
                _BalanceSheetTotalAssets_OtherCurrentAssets = value;
            }
        }
        private decimal _BalanceSheetTotalAssets_OtherCurrentAssets;



        public decimal BalanceSheetTotalAssets_CashAndCashEquivalents
        {
            get
            {
                return FinancialYearType.IsActual() ? _BalanceSheetTotalAssets_CashAndCashEquivalents : 456789;
            }
            set
            {
                _BalanceSheetTotalAssets_CashAndCashEquivalents = value;
            }
        }
        private decimal _BalanceSheetTotalAssets_CashAndCashEquivalents;

        public decimal BalanceSheetTotalAssets_IntangibleAssets { get; set; }

        public decimal BalanceSheetTotalAssets_FixedAssets { get; set; }

        public decimal BalanceSheetTotalAssets_TotalAssets { get { return BalanceSheetTotalAssets_CurrentAssets + BalanceSheetTotalAssets_IntangibleAssets + BalanceSheetTotalAssets_FixedAssets; } }

        public decimal BalanceSheetEquityAndLiabilities_AccountsPayable
        {
            get
            {
                return FinancialYearType.IsActual() ? _BalanceSheetEquityAndLiabilities_AccountsPayable : IncomeStatement_CostOfGoodsSold *
                    BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold;
            }
            set
            {
                _BalanceSheetEquityAndLiabilities_AccountsPayable = value;
            }
        }
        private decimal _BalanceSheetEquityAndLiabilities_AccountsPayable;
        public decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities { get; }
        public decimal BalanceSheetEquityAndLiabilities_CurrentLiabilities { get; }
        public decimal BalanceSheetEquityAndLiabilities_ShortTermDebt { get; }
        public decimal BalanceSheetEquityAndLiabilities_LongTermDebt { get; }
        public decimal BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities { get; }
        public decimal BalanceSheetEquityAndLiabilities_ShareholderLoans { get; }


        public decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue
        {
            get
            {
                return FinancialYearType.IsActual() ? (_BalanceSheetTotalAssets_AccountsReceiveable / IncomeStatement_Revenue) : _BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue;
            }
            set
            {
                _BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = value;
            }
        }
        private decimal _BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue;

        public decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold
        {
            get
            {
                return FinancialYearType.IsActual() ? (BalanceSheetTotalAssets_Inventory / IncomeStatement_CostOfGoodsSold) : _BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold;
            }
            set
            {
                _BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = value;
            }
        }
        private decimal _BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold;
        public decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold
        {
            get
            {
                return FinancialYearType.IsActual() ? (BalanceSheetTotalAssets_OtherCurrentAssets / IncomeStatement_CostOfGoodsSold) : _BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold;
            }
            set
            {
                _BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = value;
            }
        }
        private decimal _BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold;
        public decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold
        {
            get
            {
                return FinancialYearType.IsActual() ? (BalanceSheetEquityAndLiabilities_AccountsPayable / IncomeStatement_CostOfGoodsSold) : _BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold;
            }
            set
            {
                _BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = value;
            }
        }
        private decimal _BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold;
        public decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold
        {
            get
            {
                return FinancialYearType.IsActual() ? (BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities / IncomeStatement_CostOfGoodsSold) : _BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold;
            }
            set
            {
                _BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = value;
            }
        }
        private decimal _BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold;

        public decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays
        {
            get
            {
                return (((Prior.BalanceSheetTotalAssets_AccountsReceiveable + BalanceSheetTotalAssets_AccountsReceiveable) / 2) / (IncomeStatement_Revenue / 365));
            }
        }
        public decimal BalanceSheetAnalysis_AverageInventoryDays
        {
            get
            {
                try
                {
                    return (((Prior.BalanceSheetTotalAssets_Inventory + BalanceSheetTotalAssets_Inventory) / 2) / (IncomeStatement_CostOfGoodsSold / 365));
                }
                catch (DivideByZeroException)
                {
                    return 0;
                }
            }

        }

        public decimal BalanceSheetAnalysis_AverageAccountsPayableDays
        {
            get
            {
                return ((Prior.BalanceSheetEquityAndLiabilities_AccountsPayable + BalanceSheetEquityAndLiabilities_AccountsPayable) / 2) / (IncomeStatement_CostOfGoodsSold / 365);
            }

        }

        #endregion





        #region CapitalExpenditure
        public decimal CapitalExpenditure_CapitalExpenditure
        {
            get
            {
                return FinancialYearType.IsActual() ? IncomeStatement_Revenue * CapitalExpenditure_PercentageForecastCapitalExpenditure : _CapitalExpenditure_CapitalExpenditure;
            }
            set
            {
                _CapitalExpenditure_CapitalExpenditure = value;
            }
        }
        private decimal _CapitalExpenditure_CapitalExpenditure;

        public decimal CapitalExpenditure_PercentageForecastCapitalExpenditure
        {
            get
            {
                return FinancialYearType.IsActual() ? _CapitalExpenditure_PercentageForecastCapitalExpenditure : (CapitalExpenditure_CapitalExpenditure / IncomeStatement_Revenue);
            }
            set
            {
                _CapitalExpenditure_PercentageForecastCapitalExpenditure = value;
            }
        }
        private decimal _CapitalExpenditure_PercentageForecastCapitalExpenditure;
        #endregion




        #region Sustainable EBITDA
        /// <summary>
        ///  These are unsual once-off expenses not in the normal course of business that are not expected to be repeated e.g. office renovations, losses from sale of operating assets etc.
        /// </summary>
        public decimal SustainableEBITDA_NonRecurringExpenses { get; set; }
        /// <summary>
        /// This is unsual once-off income not in the normal course of business that is not expected to be repeated e.g. unsual and non-recurring sales (e.g. sale of a patent), profits from sale of operating assets etc.
        /// </summary>
        public decimal SustainableEBITDA_NonRecurringIncome { get; set; }

        public decimal SustainableEBITDA_SustainableEBITDA
        {
            get
            {
                return (IncomeStatement_EBITDA + SustainableEBITDA_NonRecurringExpenses - SustainableEBITDA_NonRecurringIncome);
            }
        }

        public decimal SustainableEBITDA_SustainableEBITDA_TTM
        {
            get
            {
                return ((12 - MonthsSinceLastFinancialYearEnd) / 12 * SustainableEBITDA_SustainableEBITDA) + ((MonthsSinceLastFinancialYearEnd) / 12 * Next.SustainableEBITDA_SustainableEBITDA);
            }
        }

        #endregion




        #region Excess Cash

        public decimal ExcessCash_RevenueTTM
        {
            get
            {
                return ((12 - MonthsSinceLastFinancialYearEnd) / 12 * IncomeStatement_Revenue) + ((MonthsSinceLastFinancialYearEnd) / 12 * Next.IncomeStatement_Revenue);
            }
        }

        public decimal ExcessCash_RequiredPercentageOperatingCash { get; set; }

        public decimal ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue
        {
            get
            {
                return ExcessCash_RequiredPercentageOperatingCash * ExcessCash_RevenueTTM;
            }
        }

        public decimal ExcessCash_ExcessCash
        {
            get
            {
                return ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue < CurrentCashOnHand ? (CurrentCashOnHand - ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue) : 0;
            }
        }

        #endregion


        #region Assets Projection 
        public decimal AssetsProjection_FixedAndIntangibleAssets
        {
            get
            {
                return FinancialYearType.IsActual() ? BalanceSheetTotalAssets_FixedAssets + BalanceSheetTotalAssets_IntangibleAssets : Prior.AssetsProjection_FixedAndIntangibleAssets - IncomeStatement_DepreciationAndAmortisationExpense + CapitalExpenditure_CapitalExpenditure;
            }
        }
        #endregion

    }

  
}
