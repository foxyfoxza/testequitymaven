﻿using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.ClientInputs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data
{
    public class EntityBatchedItem: EntityItemInt, IBatchedItem
    {
        private Int64? _InputBatchId;
        [Required]
        public Int64 InputBatchId
        {
            get
            {
                return GetAssociatedBy(_InputBatchId,  _InputBatch);
            }
            set
            {
                SetAssociatedBy(ref _InputBatchId, ref _InputBatch, value);
            }
        }

        private InputBatch _InputBatch;
        [JsonIgnore]
        public IInputBatch InputBatch
        {
            get
            {
                return _InputBatch;
            }
            set
            {
                SetAssociatedBy(ref _InputBatch, value as InputBatch, _InputBatchId);
            }
        }

    }
}
