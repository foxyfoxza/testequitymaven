﻿using EquityMaven.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data
{
    public class EntityItem: IEntityItem
    {
        protected void CheckOnCopyTypeMismatch(EntityItem from)
        {
            if (this.GetType() != from.GetType())
            {
                throw new ArgumentException($"mismatch between this {this.GetType()} and from {from.GetType()}");
            }
        }
        public virtual void OnCopyFrom(IEntityItem from)
        {
            this.LastUpdated = from.LastUpdated;
            this.Created = from.Created;
        }
        private DateTime? _Created;
        private DateTime? _LastUpdated;
        
        public DateTime Created
        {
            get
            {
                _Created = _Created.HasValue ? _Created : DateTime.UtcNow;
                return _Created.Value;
            }
            set
            {
                _Created = value;
            }
        }
        public DateTime LastUpdated
        {
            get
            {
                _LastUpdated = _LastUpdated.HasValue ? _LastUpdated : DateTime.UtcNow;
                return _LastUpdated.Value;
            }
            set
            {
                _LastUpdated = value;
            }
        }


        protected bool? _isNew = false;
        protected virtual bool GetIsNew()
        {
            return !_isNew.HasValue || (_isNew.HasValue && _isNew.Value);
        }

        [NotMapped]
        public bool isNew { get { return GetIsNew(); } set { _isNew = value; } }


        protected void SetAssociatedBy<T>(ref T item, T Value, Int64? associatedId) where T : EntityItemInt
        {
            if (item != Value && Value?.Id != associatedId)
                associatedId = (Value == null) ? 0 : Value.Id;
            item = Value;
        }

        protected void SetAssociatedBy<T>(ref Int64? associatedID, ref T item, Int64? Value) where T : EntityItemInt
        {
            if (item?.Id != Value)
            {
                item = null;
            }

            if (associatedID != Value && item?.Id != Value)
                associatedID = Value ?? 0;
        }


        protected Int64 GetAssociatedBy<T>(Int64? associatedId, T item) where T : EntityItemInt
        {
            return item != null ? item.Id : (associatedId ?? 0);
        }










        protected void SetAssociatedBy<T>(ref T item, T Value, string associatedId) where T : EntityItemString
        {
            if (item != Value && Value?.Id != associatedId)
                associatedId = (Value == null) ? null : Value.Id;
            item = Value;
        }

        protected void SetAssociatedBy<T>(ref string associatedID, ref T item, string Value) where T : EntityItemString
        {
            if (item?.Id != Value)
            {
                item = null;
            }

            if (associatedID != Value && item?.Id != Value)
                associatedID = Value;
        }


        protected string GetAssociatedBy<T>(string associatedId, T item) where T : EntityItemString
        {
            return item != null ? item.Id : (associatedId);
        }

    }

    public class EntityItemGUID: EntityItem
    {
        protected Guid? _Id; 
        public Guid Id
        {
            get
            {
                return _Id.HasValue ? _Id.Value : Guid.Empty;
            }
            set
            {
                _Id = value;
            }
        }

        public override void OnCopyFrom(IEntityItem from)
        {
            if (!(from is EntityItemGUID))
                throw new ArgumentException($"{from.GetType()} passed but {this.GetType()} expected");
            base.OnCopyFrom(from);
            this.Id = (from as EntityItemGUID).Id;
        }


        protected override bool GetIsNew()
        {
            return Id == Guid.Empty;
        }



        protected void SetAssociatedBy<T>(ref T item, T Value, Guid? associatedId) where T : EntityItemGUID
        {
            if (item != Value && Value?.Id != associatedId)
                associatedId = (Value == null) ? Guid.Empty : Value.Id;
            item = Value;
        }

        protected void SetAssociatedBy<T>(ref Guid? associatedID,  ref T item, Guid? Value) where T : EntityItemGUID
        {
            if (item?.Id != Value)
            {
                item = null;
            }

            if (associatedID != Value && item?.Id != Value)
                associatedID = Value ?? Guid.Empty;
        }

       
        protected Guid GetAssociatedBy<T>(Guid? associatedId, T item) where T : EntityItemGUID
        {
            return item != null ? item.Id : (associatedId ?? Guid.Empty);
        }
         

    }

    
    public class EntityItemInt : EntityItem, IEntityItemInt
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 Id { get; set; }

        public override void OnCopyFrom(IEntityItem from)
        {
            if (!(from is EntityItemInt))
                throw new ArgumentException($"{from.GetType()} passed but {this.GetType()} expected");
            base.OnCopyFrom(from);
            this.Id = (from as EntityItemInt).Id;
        }
        
        protected override bool GetIsNew()
        {
            return Id == 0;
        }
        
    }

    public class EntityItemString : EntityItem
    {
        public EntityItemString(bool isNew)
        {
            //this.isNew = isNew; 
        }
        public virtual string Id { get; set; }
        public override void OnCopyFrom(IEntityItem from)
        {
            if (!(from is EntityItemString))
                throw new ArgumentException($"{from.GetType()} passed but {this.GetType()} expected");
            base.OnCopyFrom(from);
            this.Id = (from as EntityItemString).Id;
        }
    }


}
