﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Entities
{
    public class WaccInputsSummary
    {
        public decimal Riskfreerate { get; set; }
        public decimal EquityRiskPremium { get; set; }
        public decimal CompanyRiskPremium { get { return EquityRiskPremium * LeveredBeta; } }
        public decimal PreTaxCostOfDebt { get; set; }
        public decimal LeveredBeta { get; set; }
        public decimal TotalDebtEquity { get; set; }
        public decimal Gearing{ get { return 1 - (1 / (1 + TotalDebtEquity)); } }
        public decimal Taxrate { get; set; }
        public decimal TerminalGrowthRate { get; set; }
    }
}
