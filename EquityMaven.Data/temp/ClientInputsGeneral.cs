﻿using EquityMaven.Data;
using EquityMaven.Data.Calculations;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data 
{
    public partial class ClientInputsGeneral: IPrelimEval
    {
        private List<ClientInputsFinancial> _Financial = new List<ClientInputsFinancial>();
        public List<ClientInputsFinancial> Financial { get { return _Financial; } }
        #region General
        public string CompanyName { get; set; }

        /// <summary>
        /// Industry (Thomson Reuters Business Classification code)
        /// </summary
        public string TRBC2012HierarchicalID { get; set; }
        /// <summary>
        /// Country where majority business operations of take place
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// Cash flow forecast currency
        /// </summary>
        public string Currency { get; set; }
        public DateTime LastFinancialYearEnd { get; set; }
        public DateTime ValuationDate { get; set; }

        /// <summary>
        /// Months since last financial year end
        /// </summary>
        public int MonthsSinceLastFinancialYearEnd
        {
            get
            {
                return FinancialValuation.GetMonthsSinceLastFinancialYearEnd(LastFinancialYearEnd, ValuationDate);
            }
        }
        /// <summary>
        /// Year of commencing business operations
        /// </summary>
        public DateTime DateOfCommencement { get; set; }
        public int NumberOfEmployees { get; set; }
        /// <summary>
        /// Current cash on hand as at last month-end
        /// </summary>
        public decimal CurrentCashOnHand { get; set; }
        /// <summary>
        /// Current interest-bearing debt as at last month-end
        /// </summary>
        public decimal CurrentInterestBearingDebt { get; set; }



        public DateTime ReportDate { get; }

        /// <summary>
        /// Industry(Thomson Reuters Business Classification)
        /// </summary>
        //public IThomsonReutersBusinessClassification ThomsonReutersBusinessClassification { get; }
        /// <summary>
        /// Operating cash required as % Revenue (assumed)
        /// </summary>
        public Decimal PercentageOperatingCashRequired { get; set; }
        public string StageOfDevelopment { get; set; }
        #endregion


    }
}
