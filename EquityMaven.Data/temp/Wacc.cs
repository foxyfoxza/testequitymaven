﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Entities
{
    public class Wacc 
    {
        public Wacc()
        { }
        //public Wacc(WaccUpdateTable1File WaccUpdateTable1File, WaccUpdateTable2File WaccUpdateTable2File, WaccUpdateTable3File WaccUpdateTable3File, WaccUpdateTable4File WaccUpdateTable4File)
        //{

        //}

        //public WaccUpdateTable1File WaccUpdateTable1File


        public decimal RiskFreeRate { get; set; }
        public decimal EquityRiskPremium { get; set; }
        public decimal CompanyRiskPremium { get { return EquityRiskPremium * LeveredBeta; } }
        public decimal PreTaxCostOfDebt { get; set; }
        public decimal LeveredBeta { get; set; }
        public decimal TotalDebtEquity { get; set; }
        public decimal Gearing{ get { return 1 - (1 / (1 + TotalDebtEquity)); } }
        public decimal Taxrate { get; set; }
        public decimal TerminalGrowthRate { get { return RiskFreeRate; } }
    }
}
