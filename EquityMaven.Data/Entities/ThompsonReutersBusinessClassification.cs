﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Entities
{
    public class ThompsonReutersBusinessClassification: EntityBatchedItem
    {

        public string TRBC2012HierarchicalID  { get; set; }
        public string EconomicSectorName { get; set; }
        public string BusinessSectorName { get; set; }
        public string IndustryGroupName { get; set; }
        public string IndustryName { get; set; }
        public string ActivityName { get; set; }

        private string GetCode(int _length)
        {
            return TRBC2012HierarchicalID ?.Substring(0, Math.Min((TRBC2012HierarchicalID  == null ? 0 : TRBC2012HierarchicalID .Length), _length));
        }


        [NotMapped]
        public string EconomicSectorCode { get { return GetCode(2); } }
        [NotMapped]
        public string BusinessSectorCode { get { return GetCode(4); } }
      
        [NotMapped]
        public string IndustryGroupCode { get { return GetCode(6); } }
        [NotMapped]
        public string IndustryCode { get { return GetCode(8); } }
        [NotMapped]
        public string ActivityCode { get { return GetCode(10); } }


    }
}
