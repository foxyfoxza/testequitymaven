﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Entities
{
 
    public class CountryTaxRate : EntityBatchedItem
    {
        public decimal TaxRate { get; set; }

        private Int64? _CountryID;
        [Required]
        public Int64 CountryId
        {
            get
            {
                return GetAssociatedBy(_CountryID, _Country);
            }
            set
            {
                SetAssociatedBy(ref _CountryID, ref _Country, value);
            }
        }

        private Country _Country;
        [JsonIgnore]
        public Country Country
        {
            get
            {
                return _Country;
            }
            set
            {
                SetAssociatedBy(ref _Country, value, _CountryID);
            }
        }



    }
}
