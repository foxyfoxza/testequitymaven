﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Entities
{
    public class TRBCValue : EntityBatchedItem
    {
        

        private Int64? _ThompsonReutersBusinessClassificationId;
        [Required]
        public Int64 ThompsonReutersBusinessClassificationId
        {
            get
            {
                return GetAssociatedBy(_ThompsonReutersBusinessClassificationId, _ThompsonReutersBusinessClassification);
            }
            set
            {
                SetAssociatedBy(ref _ThompsonReutersBusinessClassificationId, ref _ThompsonReutersBusinessClassification, value);
            }
        }

        private ThompsonReutersBusinessClassification _ThompsonReutersBusinessClassification;
        [JsonIgnore]
        public ThompsonReutersBusinessClassification ThompsonReutersBusinessClassification
        {
            get
            {
                return _ThompsonReutersBusinessClassification;
            }
            set
            {
                SetAssociatedBy(ref _ThompsonReutersBusinessClassification, value, _ThompsonReutersBusinessClassificationId);
            }
        }
         

        public bool isActive { get; set; } = true;
        public decimal? UnleveredBeta { get; set; }
        public decimal? World { get; set; }
        public decimal? Africa { get; set; }
        public decimal? NorthAmerica { get; set; }
        public decimal? SouthCentralAmerica { get; set; }
        public decimal? Asia { get; set; }
        public decimal? AustraliaNewZealand { get; set; }
        public decimal? EasternEuropeRussia { get; set; }
        public decimal? SouthernEurope { get; set; }
        public decimal? WesternEurope { get; set; }
        public decimal? MiddleEast { get; set; }

        public decimal? CurrentRatio { get; set; }
        public decimal? InterestCover { get; set; }
        public decimal? GrossMargin { get; set; }
        public decimal? EBITDAMargin { get; set; }
        public decimal? EBITMargin { get; set; }
        public decimal? AccRecDaysAve { get; set; }
        public decimal? AccPayableDaysAve { get; set; }
        public decimal? InventoryDaysAve { get; set; }
        public decimal? TotalDebtMarketCap { get; set; }
        public decimal? TotalDebtEBITDA { get; set; }
        public decimal? InterestCoverAdjusted { get; set; }


    }
}
