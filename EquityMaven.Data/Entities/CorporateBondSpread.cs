﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Entities
{
    public class CorporateBondSpread : EntityBatchedItem
    {
         
        public decimal InterestCoverageFrom { get; set; }
       
        public decimal InterestCoverageTo { get; set; }

    
        public string Rating { get; set; }

      
        public decimal Spread { get; set; }
         
    }
}
