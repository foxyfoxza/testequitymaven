﻿using EquityMaven.Interfaces.ClientInputs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
namespace EquityMaven.Data.Entities
{
    public class ClientProfile : EntityItemInt, IPrelimEvaluationInputs
    {
        public ClientProfile() { }

        public ClientProfile(Client client, PrelimEvaluation preEvaluation)
        {
            Client = client;
            ISOCurrencySymbol = preEvaluation.ISOCurrencySymbol;
            this.CopyFrom(preEvaluation);
        }

        public ClientProfile(Int64 clientId, PrelimEvaluation preEvaluation)
        {
            ClientId = clientId;
            ISOCurrencySymbol = preEvaluation.ISOCurrencySymbol;
            this.CopyFrom(preEvaluation);
        }

        public string CompanyName { get; set; }

        public decimal CurrentCashOnHand { get; set; }

        public decimal CurrentInterestBearingDebt { get; set; }

        public DateTime LastFinancialYearEnd { get; set; }

        public decimal PercentageOperatingCashRequired { get; set; }

        public string TRBC2012HierarchicalID { get; set; }
        
        public decimal IncomeStatement_Revenue { get; set; }

        /// <summary>
        /// Cost of goods sold
        /// </summary>
        public decimal IncomeStatement_CostOfGoodsSold { get; set; }

        /// <summary>
        /// Operating expenses(excluding depreciation & amortisation)
        /// </summary>
        public decimal IncomeStatement_OperatingExpense { get; set; }

        public decimal ExcessCash_CurrentCashOnHandAsAtLastMonthEnd { get; set; }




        private Int64? _ClientId;
        [Required]
        public Int64 ClientId
        {
            get
            {
                return GetAssociatedBy(_ClientId, _client);
            }
            set
            {
                SetAssociatedBy(ref _ClientId, ref _client, value);
            }
        }

        private Client _client;
        [JsonIgnore]
        public Client Client
        {
            get
            {
                return _client;
            }
            set
            {
                SetAssociatedBy(ref _client, value, _ClientId);
            }
        }

        public string ISOCurrencySymbol { get; set; }
        

        //private Int64? _CurrencyId;
        //[Required]
        //public Int64 CurrencyId
        //{
        //    get
        //    {
        //        return GetAssociatedBy(_CurrencyId, _Currency);
        //    }
        //    set
        //    {
        //        SetAssociatedBy(ref _CurrencyId, ref _Currency, value);
        //    }
        //}

        //private Currency _Currency;
        //[JsonIgnore]
        //public Currency Currency
        //{
        //    get
        //    {
        //        return _Currency;
        //    }
        //    set
        //    {
        //        SetAssociatedBy(ref _Currency, value, _CurrencyId);
        //    }
        //}

    }
}
