﻿using EquityMaven.Interfaces.ClientInputs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Entities
{

    public partial class PrelimEvaluation : EntityItemInt, IPrelimEval, IPrelimIncomeStatement, IPrelimExcessCash, IPrelimValuation, IPrelimEvaluationInputs
    {
        public PrelimEvaluation()
        {

        }

         

        //private Int64? _CurrencyID;
        //[Required]
        //public Int64 CurrencyId
        //{
        //    get
        //    {
        //        return GetAssociatedBy(_CurrencyID, _Currency);
        //    }
        //    set
        //    {
        //        SetAssociatedBy(ref _CurrencyID, ref _Currency, value);
        //    }
        //}

        //private Currency _Currency;
         

        //[JsonIgnore]
        //public Currency Currency
        //{
        //    get
        //    {
        //        return _Currency;
        //    }
        //    set
        //    {
        //        SetAssociatedBy(ref _Currency, value, _CurrencyID);
        //    }
        //}
        public string ISOCurrencySymbol { get; set; }
 
        public string Email { get; set; }
        public string CompanyName { get; set; }

        //Valuation C9
        public Decimal IlliquidityDiscount { get; set; }
        

        public decimal CurrentCashOnHand { get; set; }

        public decimal CurrentInterestBearingDebt { get; set; }

        public DateTime LastFinancialYearEnd { get; set; }

        public decimal PercentageOperatingCashRequired { get; set; }




 

        private Int64? _ThomsonReutersBusinessClassificationId;
        [Required]
        public Int64? ThomsonReutersBusinessClassificationId
        {
            get
            {
                return GetAssociatedBy(_ThomsonReutersBusinessClassificationId, _ThomsonReutersBusinessClassification);
            }
            set
            {
                SetAssociatedBy(ref _ThomsonReutersBusinessClassificationId, ref _ThomsonReutersBusinessClassification, value);
            }
        }

        private ThompsonReutersBusinessClassification _ThomsonReutersBusinessClassification;
        [JsonIgnore]
        [NotMapped]
        public ThompsonReutersBusinessClassification ThomsonReutersBusinessClassification
        {
            get
            {
                return _ThomsonReutersBusinessClassification;
            }
            set
            {
                SetAssociatedBy(ref _ThomsonReutersBusinessClassification, value, _ThomsonReutersBusinessClassificationId);
            }
        }
         
        public string TRBC2012HierarchicalID { get; set; }



        public decimal IncomeStatement_Revenue { get; set; }
        /// <summary>
        /// Cost of goods sold
        /// </summary>
        public decimal IncomeStatement_CostOfGoodsSold { get; set; }
        public decimal IncomeStatement_GrossProfit { get { return IncomeStatement_Revenue - IncomeStatement_CostOfGoodsSold; } }
        /// <summary>
        /// Operating expenses(excluding depreciation & amortisation)
        /// </summary>
        public decimal IncomeStatement_OperatingExpense { get; set; }

        /// <summary>
        /// Earnings before interest, tax, depreciation, amortisation("EBITDA")
        /// </summary>

        public decimal IncomeStatement_EBITDA { get { return IncomeStatement_GrossProfit - IncomeStatement_OperatingExpense; } }




         
        public decimal ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue { get { return IncomeStatement_Revenue * PercentageOperatingCashRequired; } }
        public decimal ExcessCash_CurrentCashOnHandAsAtLastMonthEnd { get; set; }
        public decimal ExcessCash_ExcessCash { get { return ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue < CurrentCashOnHand ? (CurrentCashOnHand - ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue) : 0; } }
        public decimal EV_EBITDA_WorldMedian { get; set; }

        /// <summary>
        /// Enterprise Value(pre-illiquidity discount)
        /// </summary>
        public decimal EV_EBITDA_Valuation_EnterpriseValuePreIlliquidityDiscount { get { return EV_EBITDA_WorldMedian * IncomeStatement_EBITDA; } }
        /// <summary>
        /// Equity Value(pre-illiquidity discount)
        /// </summary>
        public decimal EV_EBITDA_Valuation_EquityValuePreIlliquidityDiscount { get { return EV_EBITDA_Valuation_EnterpriseValuePreIlliquidityDiscount + ExcessCash_ExcessCash - CurrentInterestBearingDebt; } }
        /// <summary>
        ///  Valuation Illiquidity Discount  
        /// </summary>
        public decimal ValuationIlliquidityDiscount { get { return (IlliquidityDiscount*-1) * EV_EBITDA_Valuation_EquityValuePreIlliquidityDiscount; } }
        /// <summary>
        /// Equity Value(post-illiquidity discount)
        /// </summary>
        public decimal EV_EBITDA_Valuation_EquityValuePostIlliquidityDiscount { get { return EV_EBITDA_Valuation_EquityValuePreIlliquidityDiscount +ValuationIlliquidityDiscount; } }

    }
}
