﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Entities
{
    public class MoodysRating: EntityBatchedItem
    {
        //public override void OnCopyFrom(EntityItem from)
        //{
        //    CheckOnCopyTypeMismatch(from);
        //    EquityMaven.Data.Extensions.MoodysRatingExtensions.CopyFrom(this, from as MoodysRating);
        //}
        public string Rating { get; set; }
        public decimal? DefaultSpread { get; set; }
    }
}
