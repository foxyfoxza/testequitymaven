﻿using EquityMaven.Interfaces.FinancialYears;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.Calculations;
using EquityMaven.Interfaces.ClientInputs;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace EquityMaven.Data.Entities
{
    public class FullValuationYearReportBudget : EntityItemInt, IFinancialYearBudget
    {
        #region FullValuationReport
        private Int64? _FullValuationReportId;
        [Required]
        public Int64 FullValuationReportId
        {
            get
            {
                return GetAssociatedBy(_FullValuationReportId, _FullValuationReport);
            }
            set
            {
                SetAssociatedBy(ref _FullValuationReportId, ref _FullValuationReport, value);
            }
        }

        private FullValuationReport _FullValuationReport;
        [JsonIgnore]
        public FullValuationReport FullValuationReport
        {
            get
            {
                return _FullValuationReport;
            }
            set
            {
                SetAssociatedBy(ref _FullValuationReport, value, _FullValuationReportId);
            }
        }
        #endregion

        public void CopyFrom(IFinancialYearBudget from)
        {

            AssetsProjection_FixedAndIntangibleAssets = from.AssetsProjection_FixedAndIntangibleAssets;
            BalanceSheetAnalysis_AverageAccountsPayableDays = from.BalanceSheetAnalysis_AverageAccountsPayableDays;
            BalanceSheetAnalysis_AverageAccountsReceiveableDays = from.BalanceSheetAnalysis_AverageAccountsReceiveableDays;
            BalanceSheetAnalysis_AverageInventoryDays = from.BalanceSheetAnalysis_AverageInventoryDays;

            BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = from.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue;
            BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold;
            BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold;
            BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold;
            BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold ;

            BalanceSheetEquityAndLiabilities_AccountsPayable = from.BalanceSheetEquityAndLiabilities_AccountsPayable;

            BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = from.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities;

            BalanceSheetEquityAndLiabilities_TotalLiabilities = from.BalanceSheetEquityAndLiabilities_TotalLiabilities;

            BalanceSheetTotalAssets_AccountsReceiveable = from.BalanceSheetTotalAssets_AccountsReceiveable;

            BalanceSheetTotalAssets_CurrentAssets = from.BalanceSheetTotalAssets_CurrentAssets;

            BalanceSheetTotalAssets_Inventory = from.BalanceSheetTotalAssets_Inventory;
            BalanceSheetTotalAssets_OtherCurrentAssets = from.BalanceSheetTotalAssets_OtherCurrentAssets;
            BalanceSheetTotalAssets_TotalAssets = from.BalanceSheetTotalAssets_TotalAssets;
            CapitalExpenditure_CapitalExpenditure = from.CapitalExpenditure_CapitalExpenditure;

            CapitalExpenditure_PercentageForecastCapitalExpenditure = from.CapitalExpenditure_PercentageForecastCapitalExpenditure;
            DiscountedCashFlow_FreeCashFlow = from.DiscountedCashFlow_FreeCashFlow;


            IncomeStatementAnalysis_EBITDAMargin = from.IncomeStatementAnalysis_EBITDAMargin;
            IncomeStatementAnalysis_EBITMargin = from.IncomeStatementAnalysis_EBITMargin;
            IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = from.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue;
            IncomeStatementAnalysis_PercentageGrossProfitMargin = from.IncomeStatementAnalysis_PercentageGrossProfitMargin;
            IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = from.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue;
            IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = from.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue;
            IncomeStatementAnalysis_PercentageRevenueGrowth = from.IncomeStatementAnalysis_PercentageRevenueGrowth;
            IncomeStatement_CostOfGoodsSold = from.IncomeStatement_CostOfGoodsSold;
            IncomeStatement_DepreciationAndAmortisationExpense = from.IncomeStatement_DepreciationAndAmortisationExpense;
            IncomeStatement_EarningsBeforeTax = from.IncomeStatement_EarningsBeforeTax;
            IncomeStatement_EBIT = from.IncomeStatement_EBIT;
            IncomeStatement_EBITDA = from.IncomeStatement_EBITDA;
            IncomeStatement_GrossProfit = from.IncomeStatement_GrossProfit;
            IncomeStatement_InterestExpense = from.IncomeStatement_InterestExpense;

            IncomeStatement_NonCashExpense = from.IncomeStatement_NonCashExpense;
            IncomeStatement_OperatingExpense = from.IncomeStatement_OperatingExpense;
            IncomeStatement_Revenue = from.IncomeStatement_Revenue;
            NetWorkingCapital_ChangeInNetWorkingCapital = from.NetWorkingCapital_ChangeInNetWorkingCapital;
            NetWorkingCapital_CurrentAssets = from.NetWorkingCapital_CurrentAssets;
            NetWorkingCapital_NetWorkingCapital = from.NetWorkingCapital_NetWorkingCapital;
            SustainableEBITDA_NonRecurringExpenses = from.SustainableEBITDA_NonRecurringExpenses;
            SustainableEBITDA_NonRecurringIncome = from.SustainableEBITDA_NonRecurringIncome;
            SustainableEBITDA_SustainableEBITDA = from.SustainableEBITDA_SustainableEBITDA;

        }

         
        public IFinancialYear PriorFY { get; set; }
        public IFinancialValuation FinancialValuation { get; set; }
         


        public void ApplyValues(IFullValuationInputYear _fyi) { }

        public decimal AssetsProjection_FixedAndIntangibleAssets { get; set; }

        public decimal BalanceSheetAnalysis_AverageAccountsPayableDays { get; set; }
        public decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays { get; set; }

        public decimal BalanceSheetAnalysis_AverageInventoryDays { get; set; }

        public decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue { get; set; }

        public decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold { get; set; }

        public decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold { get; set; }

        public decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold { get; set; }

        public decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_AccountsPayable { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_TotalLiabilities { get; set; }

        public decimal BalanceSheetTotalAssets_AccountsReceiveable { get; set; }

        public decimal BalanceSheetTotalAssets_CurrentAssets { get; set; }

        public decimal BalanceSheetTotalAssets_Inventory { get; set; }

        public decimal BalanceSheetTotalAssets_OtherCurrentAssets { get; set; }

        public decimal BalanceSheetTotalAssets_TotalAssets { get; set; }

        public decimal CapitalExpenditure_CapitalExpenditure { get; set; }

        public decimal CapitalExpenditure_PercentageForecastCapitalExpenditure { get; set; }

        public decimal DiscountedCashFlow_AdjustedFreeCashFlow { get; set; }

        public decimal DiscountedCashFlow_AdjustmentForFirstYear { get; set; }

        public decimal DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure { get; set; }
        public decimal DiscountedCashFlow_DiscountedFreeCashFlow { get; set; }

        public decimal DiscountedCashFlow_DiscountFactor { get; set; }

        public decimal DiscountedCashFlow_DiscountFactorInverse { get; set; }

        public decimal DiscountedCashFlow_DiscountPeriodMidYear { get; set; }

        public decimal DiscountedCashFlow_DiscountPeriodMid_Year { get; set; }

        public decimal DiscountedCashFlow_DiscountPeriodYearEnd { get; set; }

        public decimal DiscountedCashFlow_DiscountPeriodYear_End { get; set; }

        public decimal DiscountedCashFlow_FreeCashFlow { get; set; }

        public decimal DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense { get; set; }

        public decimal DiscountedCashFlow_IncomeStatement_EBIT { get; set; }

        public decimal DiscountedCashFlow_IncomeStatement_NonCashExpense { get; set; }

        public decimal DiscountedCashFlow_IncomeStatement_Tax { get; set; }

        public decimal DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital { get; set; }

        public decimal IncomeStatementAnalysis_EBITDAMargin { get; set; }
        public decimal IncomeStatementAnalysis_EBITMargin { get; set; }

        public decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue { get; set; }

        public decimal IncomeStatementAnalysis_PercentageGrossProfitMargin { get; set; }

        public decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue { get; set; }

        public decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue { get; set; }

        public decimal IncomeStatementAnalysis_PercentageRevenueGrowth { get; set; }

        public decimal IncomeStatement_CostOfGoodsSold { get; set; }
        public decimal IncomeStatement_DepreciationAndAmortisationExpense { get; set; }

        public decimal IncomeStatement_EarningsBeforeTax { get; set; }

        public decimal IncomeStatement_EBIT { get; set; }

        public decimal IncomeStatement_EBITDA { get; set; }

        public decimal IncomeStatement_GrossProfit { get; set; }

        public decimal IncomeStatement_InterestExpense { get; set; }

        public decimal IncomeStatement_NonCashExpense { get; set; }

        public decimal IncomeStatement_OperatingExpense { get; set; }

        public decimal IncomeStatement_Revenue { get; set; }

        public decimal NetWorkingCapital_ChangeInNetWorkingCapital { get; set; }
        public decimal NetWorkingCapital_CurrentAssets { get; set; }

        public decimal NetWorkingCapital_NetWorkingCapital { get; set; }



        public decimal SustainableEBITDA_NonRecurringExpenses { get; set; }

        public decimal SustainableEBITDA_NonRecurringIncome { get; set; }

        public decimal SustainableEBITDA_SustainableEBITDA { get; set; }

        public decimal DiscountedCashFlowTerminalMultiple_AdjustedFreeCashFlow
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public decimal DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public decimal DiscountedCashFlowTerminalMultiple_DiscountFactor
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public decimal DiscountedCashFlowTerminalMultiple_DiscountFactorInverse
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public decimal DiscountedCashFlowTerminalMultiple_FreeCashFlow
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public decimal DiscountedCashFlowTerminalMultiple_DiscountedFreeCashFlow
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public decimal GetBalanceSheetEquityAndLiabilities_AccountsPayable()
        {
            return 0;
        }

        public decimal GetBalanceSheetEquityAndLiabilities_CurrentLiabilities()
        {
            return 0;
        }

        public decimal GetBalanceSheetEquityAndLiabilities_LongTermDebt()
        {
            return 0;
        }

        public decimal GetBalanceSheetEquityAndLiabilities_OtherLongTermLiabilities()
        {
            return 0;
        }

        public decimal GetBalanceSheetEquityAndLiabilities_ShareholderLoans()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_AccountsReceiveable()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_CashAndCashEquivalents()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_CurrentAssets()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_FixedAndIntangibleAssets()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_FixedAssets()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_IntangibleAssets()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_Inventory()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_OtherCurrentAssets()
        {
            return 0;
        }

        public decimal GetIncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue()
        {
            return 0;
        }

        public decimal GetIncomeStatement_CostOfGoodsSold()
        {
            return 0;
        }

        public decimal GetIncomeStatement_DepreciationAndAmortisationExpense()
        {
            return 0;
        }

        public decimal GetIncomeStatement_GrossProfit()
        {
            return 0;
        }

        public decimal GetIncomeStatement_InterestExpense()
        {
            return 0;
        }

        public decimal GetIncomeStatement_OperatingExpense()
        {
            return 0;
        }

        public decimal GetIncomeStatement_Revenue()
        {
            return 0;
        }
    }
}
