﻿using EquityMaven.Interfaces.FinancialYears;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Interfaces.ClientInputs;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace EquityMaven.Data.Entities
{
    public class FullValuationYearReportTerminal : EntityItemInt, IFinancialYearTerminal
    {
        #region FullValuationReport
        private Int64? _FullValuationReportId;
        [Required]
        public Int64 FullValuationReportId
        {
            get
            {
                return GetAssociatedBy(_FullValuationReportId, _FullValuationReport);
            }
            set
            {
                SetAssociatedBy(ref _FullValuationReportId, ref _FullValuationReport, value);
            }
        }

        private FullValuationReport _FullValuationReport;
        [JsonIgnore]
        public FullValuationReport FullValuationReport
        {
            get
            {
                return _FullValuationReport;
            }
            set
            {
                SetAssociatedBy(ref _FullValuationReport, value, _FullValuationReportId);
            }
        }

        #endregion
        public void CopyFrom(IFinancialYearTerminal from)
        {
            DiscountedCashFlow_FreeCashFlow = from.DiscountedCashFlow_FreeCashFlow;
            NetWorkingCapital_ChangeInNetWorkingCapital = from.NetWorkingCapital_ChangeInNetWorkingCapital;
            DiscountedCashFlow_AdjustedFreeCashFlow = from.DiscountedCashFlow_AdjustedFreeCashFlow;
            DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure = from.DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure;
            DiscountedCashFlow_DiscountedFreeCashFlow = from.DiscountedCashFlow_DiscountedFreeCashFlow;
            DiscountedCashFlow_DiscountFactor = from.DiscountedCashFlow_DiscountFactor;
            DiscountedCashFlow_DiscountFactorInverse = from.DiscountedCashFlow_DiscountFactorInverse;
            DiscountedCashFlow_DiscountPeriodMidYear = from.DiscountedCashFlow_DiscountPeriodMidYear;
            DiscountedCashFlow_DiscountPeriodYearEnd = from.DiscountedCashFlow_DiscountPeriodYearEnd;
            DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense = from.DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense;
            DiscountedCashFlow_IncomeStatement_EBIT = from.DiscountedCashFlow_IncomeStatement_EBIT;
            DiscountedCashFlow_IncomeStatement_NonCashExpense = from.DiscountedCashFlow_IncomeStatement_NonCashExpense;
            DiscountedCashFlow_IncomeStatement_Tax = from.DiscountedCashFlow_IncomeStatement_Tax;
            DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital = from.DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital;
            DiscountedCashFlow_TerminalFreeCashFlow = from.DiscountedCashFlow_TerminalFreeCashFlow;
            DiscountedCashFlow_TerminalFreeCashFlowMultiple = from.DiscountedCashFlow_TerminalFreeCashFlowMultiple;

        } 
         
        public IFinancialYearForecast2 Prior { get; set; }
        public void ApplyValues(IFullValuationInputYear _fyi) { }
        public decimal DiscountedCashFlow_AdjustedFreeCashFlow { get; set; }
        public decimal DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure { get; set; }
        public decimal DiscountedCashFlow_DiscountedFreeCashFlow { get; set; }
        public decimal DiscountedCashFlow_DiscountFactor { get; set; }
        public decimal DiscountedCashFlow_DiscountFactorInverse { get; set; }
        public decimal DiscountedCashFlow_DiscountPeriodMidYear { get; set; }
        public decimal DiscountedCashFlow_DiscountPeriodYearEnd { get; set; }
        public decimal DiscountedCashFlow_FreeCashFlow { get; set; }

        public decimal DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense { get; set; }

        public decimal DiscountedCashFlow_IncomeStatement_EBIT { get; set; }
        public decimal DiscountedCashFlow_IncomeStatement_NonCashExpense { get; set; }
        public decimal DiscountedCashFlow_IncomeStatement_Tax { get; set; }

        public decimal DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital { get; set; }

        public decimal DiscountedCashFlow_TerminalFreeCashFlow { get; set; }

        public decimal DiscountedCashFlow_TerminalFreeCashFlowMultiple { get; set; }

        public decimal NetWorkingCapital_ChangeInNetWorkingCapital { get; set; }

        public decimal DiscountedCashFlowTerminalMultiple_Ebitda { get; set; }
        public decimal DiscountedCashFlowTerminalMultiple_Ev_Over_Ebitda { get; set; }

        public decimal DiscountedCashFlowTerminalMultiple_AdjustedFreeCashFlow { get; set; }

        public decimal DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year { get; set; }

        public decimal DiscountedCashFlowTerminalMultiple_DiscountFactor { get; set; }

        public decimal DiscountedCashFlowTerminalMultiple_DiscountFactorInverse { get; set; }

        public decimal DiscountedCashFlowTerminalMultiple_FreeCashFlow { get; set; }

        public decimal DiscountedCashFlowTerminalMultiple_DiscountedFreeCashFlow { get; set; }
    }
}
