﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
namespace EquityMaven.Data.Entities
{

    public class Currency : EntityBatchedItem
    {
        [NotMapped]
        public string Description { get { return $"{ISOCurrencySymbol} - {CurrencyEnglishName}"; } }
        public string ISOCurrencySymbol { get; set; }

        private string _CurrencyEnglishName;
        public string CurrencyEnglishName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_CurrencyEnglishName)&& !string.IsNullOrWhiteSpace(ISOCurrencySymbol))
                {
                    TryGetCurrencyName(ISOCurrencySymbol, out _CurrencyEnglishName);
                }
                return _CurrencyEnglishName;
            }
            set
            {
                //if default EnglishName override to null 
                string temp = null;
                if (TryGetCurrencyName(ISOCurrencySymbol, out temp))
                { 
                    if (temp == value)
                        _CurrencyEnglishName = null;
                    else
                        _CurrencyEnglishName = value;
                }
                else
                    _CurrencyEnglishName = value;
            }
        }

        private bool TryGetCurrencyName(string ISOCurrencySymbol, out string currencyEnglishName)
        {
            currencyEnglishName = System.Globalization.CultureInfo
                .GetCultures(System.Globalization.CultureTypes.AllCultures)
                .Where(c => !c.IsNeutralCulture)
                .Select(culture =>
                {
                    try
                    {
                        return new System.Globalization.RegionInfo(culture.LCID);
                    }
                    catch
                    {
                        return null;
                    }
                })
                .Where(ri => ri != null && ri.ISOCurrencySymbol == ISOCurrencySymbol)
                .Select(ri => ri.CurrencyEnglishName)
                .FirstOrDefault();
            return currencyEnglishName != null;
        }
        public ICollection<CurrencyRiskFreeRate> CurrencyRiskFreeRates { get; set; }

    }
}
