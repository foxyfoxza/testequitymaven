﻿using EquityMaven.Interfaces.FinancialYears;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.Calculations;
using EquityMaven.Interfaces.ClientInputs;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace EquityMaven.Data.Entities
{
    public class FullValuationYearReportActualCurrent : EntityItemInt, IFinancialYearActualCurrent
    {
        #region FullValuationReport
        private Int64? _FullValuationReportId;
        [Required]
        public Int64 FullValuationReportId
        {
            get
            {
                return GetAssociatedBy(_FullValuationReportId, _FullValuationReport);
            }
            set
            {
                SetAssociatedBy(ref _FullValuationReportId, ref _FullValuationReport, value);
            }
        }

        private FullValuationReport _FullValuationReport;
        [JsonIgnore]
        public FullValuationReport FullValuationReport
        {
            get
            {
                return _FullValuationReport;
            }
            set
            {
                SetAssociatedBy(ref _FullValuationReport, value, _FullValuationReportId);
            }
        }
        #endregion

        public void CopyFrom(IFinancialYearActualCurrent from)
        {

            AssetsProjection_FixedAndIntangibleAssets = from.AssetsProjection_FixedAndIntangibleAssets;
            BalanceSheetAnalysis_AverageAccountsPayableDays = from.BalanceSheetAnalysis_AverageAccountsPayableDays;
            BalanceSheetAnalysis_AverageAccountsReceiveableDays = from.BalanceSheetAnalysis_AverageAccountsReceiveableDays;
            BalanceSheetAnalysis_AverageInventoryDays = from.BalanceSheetAnalysis_AverageInventoryDays;
            BalanceSheetAnalysis_CurrentRatio = from.BalanceSheetAnalysis_CurrentRatio;
            BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = from.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue;
            BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold;
            BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold;
            BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold;
            BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold;
            BalanceSheetAnalysis_TotalDebtOverEBITDA = from.BalanceSheetAnalysis_TotalDebtOverEBITDA;
            BalanceSheetEquityAndLiabilities_AccountsPayable = from.BalanceSheetEquityAndLiabilities_AccountsPayable;
            BalanceSheetEquityAndLiabilities_CurrentLiabilities = from.BalanceSheetEquityAndLiabilities_CurrentLiabilities;
            BalanceSheetEquityAndLiabilities_LongTermDebt = from.BalanceSheetEquityAndLiabilities_LongTermDebt;
            BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = from.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities;
            BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = from.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities;
            BalanceSheetEquityAndLiabilities_ShareholderLoans = from.BalanceSheetEquityAndLiabilities_ShareholderLoans;
            BalanceSheetEquityAndLiabilities_ShortTermDebt = from.BalanceSheetEquityAndLiabilities_ShortTermDebt;
            BalanceSheetEquityAndLiabilities_TotalLiabilities = from.BalanceSheetEquityAndLiabilities_TotalLiabilities;
            BalanceSheetShareholdersEquity = from.BalanceSheetShareholdersEquity;
            BalanceSheetTotalAssets_AccountsReceiveable = from.BalanceSheetTotalAssets_AccountsReceiveable;
            BalanceSheetTotalAssets_CashAndCashEquivalents = from.BalanceSheetTotalAssets_CashAndCashEquivalents;
            BalanceSheetTotalAssets_CurrentAssets = from.BalanceSheetTotalAssets_CurrentAssets;
            BalanceSheetTotalAssets_FixedAndIntangibleAssets = from.BalanceSheetTotalAssets_FixedAndIntangibleAssets;
            BalanceSheetTotalAssets_FixedAssets = from.BalanceSheetTotalAssets_FixedAssets;
            BalanceSheetTotalAssets_IntangibleAssets = from.BalanceSheetTotalAssets_IntangibleAssets;
            BalanceSheetTotalAssets_Inventory = from.BalanceSheetTotalAssets_Inventory;
            BalanceSheetTotalAssets_OtherCurrentAssets = from.BalanceSheetTotalAssets_OtherCurrentAssets;
            BalanceSheetTotalAssets_TotalAssets = from.BalanceSheetTotalAssets_TotalAssets;
            CapitalExpenditure_CapitalExpenditure = from.CapitalExpenditure_CapitalExpenditure;
            CapitalExpenditure_CapitalExpenditureTTM = from.CapitalExpenditure_CapitalExpenditureTTM;
            CapitalExpenditure_PercentageForecastCapitalExpenditure = from.CapitalExpenditure_PercentageForecastCapitalExpenditure;
            DiscountedCashFlow_FreeCashFlow = from.DiscountedCashFlow_FreeCashFlow;
            ExcessCash_ExcessCash = from.ExcessCash_ExcessCash;
            ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue = from.ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue;
            ExcessCash_RevenueTTM = from.ExcessCash_RevenueTTM;
            IncomeStatementAnalysis_EBITDAMargin = from.IncomeStatementAnalysis_EBITDAMargin;
            IncomeStatementAnalysis_EBITMargin = from.IncomeStatementAnalysis_EBITMargin;
            IncomeStatementAnalysis_InterestCover = from.IncomeStatementAnalysis_InterestCover;
            IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = from.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue;
            IncomeStatementAnalysis_PercentageGrossProfitMargin = from.IncomeStatementAnalysis_PercentageGrossProfitMargin;
            IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = from.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue;
            IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = from.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue;
            IncomeStatementAnalysis_PercentageRevenueGrowth = from.IncomeStatementAnalysis_PercentageRevenueGrowth;
            IncomeStatement_CostOfGoodsSold = from.IncomeStatement_CostOfGoodsSold;
            IncomeStatement_DepreciationAndAmortisationExpense = from.IncomeStatement_DepreciationAndAmortisationExpense;
            IncomeStatement_EarningsBeforeTax = from.IncomeStatement_EarningsBeforeTax;
            IncomeStatement_EBIT = from.IncomeStatement_EBIT;
            IncomeStatement_EBITDA = from.IncomeStatement_EBITDA;
            IncomeStatement_GrossProfit = from.IncomeStatement_GrossProfit;
            IncomeStatement_InterestExpense = from.IncomeStatement_InterestExpense;
            IncomeStatement_NonCashExpense = from.IncomeStatement_NonCashExpense;
            IncomeStatement_OperatingExpense = from.IncomeStatement_OperatingExpense;
            IncomeStatement_Revenue = from.IncomeStatement_Revenue;
            NetWorkingCapital_ChangeInNetWorkingCapital = from.NetWorkingCapital_ChangeInNetWorkingCapital;
            NetWorkingCapital_CurrentAssets = from.NetWorkingCapital_CurrentAssets;
            NetWorkingCapital_NetWorkingCapital = from.NetWorkingCapital_NetWorkingCapital;
            SustainableEBITDA_NonRecurringExpenses = from.SustainableEBITDA_NonRecurringExpenses;
            SustainableEBITDA_NonRecurringIncome = from.SustainableEBITDA_NonRecurringIncome;
            SustainableEBITDA_SustainableEBITDA = from.SustainableEBITDA_SustainableEBITDA;
            SustainableEBITDA_SustainableEBITDA_TTM = from.SustainableEBITDA_SustainableEBITDA_TTM;



        }
        //#region  FullValuationReport
        //private Int64? _FullValuationReportId;

        //public Int64 FullValuationReportId
        //{
        //    get
        //    {
        //        return GetAssociatedBy(_FullValuationReportId, _FullValuationReport);
        //    }
        //    set
        //    {
        //        SetAssociatedBy(ref _FullValuationReportId, ref _FullValuationReport, value);
        //    }
        //}

        //private  FullValuationReport _FullValuationReport;
        //[JsonIgnore]
        //[NotMapped]
        //public  FullValuationReport  FullValuationReport
        //{
        //    get
        //    {
        //        return _FullValuationReport;
        //    }
        //    set
        //    {
        //        SetAssociatedBy(ref _FullValuationReport, value, _FullValuationReportId);
        //    }
        //}
        //#endregion

        public IFinancialValuation FinancialValuation { get; set; }

        

        public void ApplyValues(IFullValuationInputYear _fyi) { }


        public decimal AssetsProjection_FixedAndIntangibleAssets { get; set; }

        public decimal BalanceSheetAnalysis_AverageAccountsPayableDays { get; set; }

        public decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays { get; set; }

        public decimal BalanceSheetAnalysis_AverageInventoryDays { get; set; }

        public decimal BalanceSheetAnalysis_CurrentRatio { get; set; }

        public decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue { get; set; }

        public decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold { get; set; }

        public decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold { get; set; }

        public decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold { get; set; }

        public decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold { get; set; }

        public decimal BalanceSheetAnalysis_TotalDebtOverEBITDA { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_AccountsPayable { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_CurrentLiabilities { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_LongTermDebt { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_ShareholderLoans { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_ShortTermDebt { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_TotalLiabilities { get; set; }

        public decimal BalanceSheetShareholdersEquity { get; set; }

        public decimal BalanceSheetTotalAssets_AccountsReceiveable { get; set; }

        public decimal BalanceSheetTotalAssets_CashAndCashEquivalents { get; set; }
        public decimal BalanceSheetTotalAssets_CurrentAssets { get; set; }
        public decimal BalanceSheetTotalAssets_FixedAndIntangibleAssets { get; set; }

        public decimal BalanceSheetTotalAssets_FixedAssets { get; set; }

        public decimal BalanceSheetTotalAssets_IntangibleAssets { get; set; }

        public decimal BalanceSheetTotalAssets_Inventory { get; set; }

        public decimal BalanceSheetTotalAssets_OtherCurrentAssets { get; set; }

        public decimal BalanceSheetTotalAssets_TotalAssets { get; set; }

        public decimal CapitalExpenditure_CapitalExpenditure { get; set; }

        public decimal CapitalExpenditure_CapitalExpenditureTTM { get; set; }

        public decimal CapitalExpenditure_PercentageForecastCapitalExpenditure { get; set; }

        public decimal DiscountedCashFlow_FreeCashFlow { get; set; }

        public decimal ExcessCash_ExcessCash { get; set; }

        public decimal ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue { get; set; }

        public decimal ExcessCash_RevenueTTM { get; set; }


        public decimal IncomeStatementAnalysis_EBITDAMargin { get; set; }

        public decimal IncomeStatementAnalysis_EBITMargin { get; set; }

        public decimal IncomeStatementAnalysis_InterestCover { get; set; }

        public decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue { get; set; }
        public decimal IncomeStatementAnalysis_PercentageGrossProfitMargin { get; set; }

        public decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue { get; set; }
        public decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue { get; set; }
        public decimal IncomeStatementAnalysis_PercentageRevenueGrowth { get; set; }

        public decimal IncomeStatement_CostOfGoodsSold { get; set; }
        public decimal IncomeStatement_DepreciationAndAmortisationExpense { get; set; }

        public decimal IncomeStatement_EarningsBeforeTax { get; set; }
        public decimal IncomeStatement_EBIT { get; set; }

        public decimal IncomeStatement_EBITDA { get; set; }

        public decimal IncomeStatement_GrossProfit { get; set; }

        public decimal IncomeStatement_InterestExpense { get; set; }

        public decimal IncomeStatement_NonCashExpense { get; set; }

        public decimal IncomeStatement_OperatingExpense { get; set; }

        public decimal IncomeStatement_Revenue { get; set; }

        public decimal NetWorkingCapital_ChangeInNetWorkingCapital { get; set; }
        public decimal NetWorkingCapital_CurrentAssets { get; set; }

        public decimal NetWorkingCapital_NetWorkingCapital { get; set; }

        public decimal SustainableEBITDA_NonRecurringExpenses { get; set; }

        public decimal SustainableEBITDA_NonRecurringIncome { get; set; }

        public decimal SustainableEBITDA_SustainableEBITDA { get; set; }
        public decimal SustainableEBITDA_SustainableEBITDA_TTM { get; set; }



        #region liquidation value
        public decimal Liquidationvalue_Revenue_Ttm { get; set; }
        public decimal Liquidationvalue_CostOfGoodsSold_Ttm { get; set; }
        public decimal Liquidationvalue_Capex_Ttm { get; set; }
        public decimal Liquidationvalue_DepreciationAmortisation_Ttm { get; set; }
        public decimal Liquidationvalue_CurrentAssets { get; set; }
        public decimal Liquidationvalue_AccountsReceiveable { get; set; }
        public decimal Liquidationvalue_Inventory { get; set; }
        public decimal Liquidationvalue_OtherCurrentAssets { get; set; }
        public decimal Liquidationvalue_CashAndCashEquivalents { get; set; }
        public decimal Liquidationvalue_IntangibleAssets { get; set; }
        public decimal Liquidationvalue_FixedAssets { get; set; }
        public decimal Liquidationvalue_TotalAssets { get; set; }
        public decimal Liquidationvalue_CurrentLiabilities { get; set; }
        public decimal Liquidationvalue_AccountsPayable { get; set; }
        public decimal Liquidationvalue_OtherCurrentLiabilities { get; set; }
        public decimal Liquidationvalue_3rdPartyInterest_BearingDebtLongTermShortTerm { get; set; }
        public decimal Liquidationvalue_OtherLong_TermLiabilities { get; set; }
        public decimal Liquidationvalue_ShareholderLoans { get; set; }
        public decimal Liquidationvalue_TotalLiabilities { get; set; }
        #endregion


        public decimal GetBalanceSheetEquityAndLiabilities_AccountsPayable()
        {
            return 0;
        }

        public decimal GetBalanceSheetEquityAndLiabilities_CurrentLiabilities()
        {
            return 0;
        }

        public decimal GetBalanceSheetEquityAndLiabilities_LongTermDebt()
        {
            return 0;
        }

        public decimal GetBalanceSheetEquityAndLiabilities_OtherLongTermLiabilities()
        {
            return 0;
        }

        public decimal GetBalanceSheetEquityAndLiabilities_ShareholderLoans()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_AccountsReceiveable()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_CashAndCashEquivalents()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_CurrentAssets()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_FixedAndIntangibleAssets()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_FixedAssets()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_IntangibleAssets()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_Inventory()
        {
            return 0;
        }

        public decimal GetBalanceSheetTotalAssets_OtherCurrentAssets()
        {
            return 0;
        }

        public decimal GetIncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue()
        {
            return 0;
        }

        public decimal GetIncomeStatement_CostOfGoodsSold()
        {
            return 0;
        }

        public decimal GetIncomeStatement_DepreciationAndAmortisationExpense()
        {
            return 0;
        }

        public decimal GetIncomeStatement_GrossProfit()
        {
            return 0;
        }

        public decimal GetIncomeStatement_InterestExpense()
        {
            return 0;
        }

        public decimal GetIncomeStatement_OperatingExpense()
        {
            return 0;
        }

        public decimal GetIncomeStatement_Revenue()
        {
            return 0;
        }
    }
}
