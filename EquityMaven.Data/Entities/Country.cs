﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Entities
{
    public class Country : EntityItemInt
    {
        public Country() { }
        public static Country GetCountryByCountryEnglishName(string countryEnglishName)
        {
            Country result = new Country { CountryEnglishName = countryEnglishName };
            RegionInfo _currentRegion;
            if (TryGetRegionInfoByCountryEnglishName(countryEnglishName, out _currentRegion))
            {
                result.CountryEnglishName = _currentRegion.EnglishName;
                result.ThreeLetterISORegionName = _currentRegion.ThreeLetterISORegionName;
                result.TwoLetterISORegionName = _currentRegion.TwoLetterISORegionName;
            }
            else
            {
                result.ThreeLetterISORegionName = countryEnglishName;
                result.TwoLetterISORegionName = countryEnglishName;
            }
            return result;
        }


        [NotMapped]
        public string Description { get { return $"{ThreeLetterISORegionName} - {CountryEnglishName}"; } }

        public string ThreeLetterISORegionName { get; set; }
        

        public string TwoLetterISORegionName { get; set; }

        public string CountryEnglishName { get; set; }

        private static bool TryGetRegionInfoByTwoLetterISORegionName(string twoLetterISORegionName, out RegionInfo region)
        {
            region = System.Globalization.CultureInfo
                .GetCultures(System.Globalization.CultureTypes.AllCultures)
                .Where(c => !c.IsNeutralCulture)
                .Select(culture =>
                {
                    try
                    {
                        return new System.Globalization.RegionInfo(culture.LCID);
                    }
                    catch
                    {
                        return null;
                    }
                })
                .Where(ri => ri != null && ri.TwoLetterISORegionName == twoLetterISORegionName).FirstOrDefault();
            return region != null;
        }
        private static bool TryGetRegionInfoByThreeLetterISORegionName(string threeLetterISORegionName, out RegionInfo region)
        {
            region = System.Globalization.CultureInfo
                .GetCultures(System.Globalization.CultureTypes.AllCultures)
                .Where(c => !c.IsNeutralCulture)
                .Select(culture =>
                {
                    try
                    {
                        return new System.Globalization.RegionInfo(culture.LCID);
                    }
                    catch
                    {
                        return null;
                    }
                })
                .Where(ri => ri != null && ri.ThreeLetterISORegionName == threeLetterISORegionName).FirstOrDefault();
            return region != null;
        }

        private static bool TryGetRegionInfoByCountryEnglishName(string countryEnglishName, out RegionInfo region)
        {
            region = System.Globalization.CultureInfo
                .GetCultures(System.Globalization.CultureTypes.AllCultures)
                .Where(c => !c.IsNeutralCulture)
                .Select(culture =>
                {
                    try
                    {
                        return new System.Globalization.RegionInfo(culture.LCID);
                    }
                    catch
                    {
                        return null;
                    }
                })
                .Where(ri => !string.IsNullOrEmpty(countryEnglishName) && ri != null && ri.EnglishName.ToLower() == countryEnglishName.ToLower()).FirstOrDefault();
            return region != null;
        }


        public ICollection<CountryTaxRate> CountryTaxRates { get; set; }
        public ICollection<CountryMoodysRating> CountryMoodysRatings { get; set; }


        //public ICollection<countryRiskFreeRate> countryRiskFreeRates { get; set; }

    }
}
