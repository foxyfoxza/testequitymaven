﻿using EquityMaven.Interfaces.ClientInputs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.CommonEnums;
using EquityMaven.Data.Calculations;

namespace EquityMaven.Data.Entities
{
    public class FullValuationInput : EntityItemInt, IFullValuationInput
    {
        public int NumberOfEmployees { get; set; }
        public string Description { get; set; }
        public string CompanyName { get; set; }

        public decimal CurrentCashOnHand { get; set; }

        public decimal All3rdPartyInterestBearingDebtAsAtLastMonth_End { get; set; }

        /// <summary>
        /// Months since last financial year end
        /// </summary>
        public int MonthsSinceLastFinancialYearEnd
        {
            get
            {
                return FinancialValuation.GetMonthsSinceLastFinancialYearEnd(LastFinancialYearEnd, ValuationDate);
            }
        }
        public string TRBC2012HierarchicalID { get; set; }
        public DateTime LastFinancialYearEnd { get; set; }
        public DateTime ValuationDate { get; set; }
        public DateTime DateOfCommencement { get; set; }
        public StageOfDevelopment StageOfDevelopment { get; set; }
        public FullValuationInput()
        {
            FinancialYears = FinancialYears ?? new List<IFullValuationInputYear>();
        }

        #region Client
        private Int64? _ClientId;
        [Required]
        public Int64 ClientId
        {
            get
            {
                return GetAssociatedBy(_ClientId, _client);
            }
            set
            {
                SetAssociatedBy(ref _ClientId, ref _client, value);
            }
        }

        private Client _client;
        [JsonIgnore]
        public Client Client
        {
            get
            {
                return _client;
            }
            set
            {
                SetAssociatedBy(ref _client, value, _ClientId);
            }
        }
        #endregion

        #region Currency
        
        public string ISOCurrencySymbol { get; set; }

        [NotMapped]
        public Currency Currency { get; set; }
        #endregion

        #region Country
         
        public string ThreeLetterISORegionName { get; set; }
        [NotMapped]
        public Country Country { get; set; }

        #endregion
         
        public virtual ICollection<IFullValuationInputYear> FinancialYears { get; set; }

         
    }
}
