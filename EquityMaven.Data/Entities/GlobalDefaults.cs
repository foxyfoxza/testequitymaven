﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Entities
{

    public class GlobalDefaults : EntityBatchedItem
    {

        public decimal PercentageOperatingCashRequired { get; set; }
        public decimal IlliquidityDiscount { get; set; }
        public decimal RelativeVolatility { get; set; }
        public decimal EquityRiskPremium { get; set; }


        public decimal SurvivalRateYear0 { get; set; }
        public decimal SurvivalRateYear1 { get; set; }
        public decimal SurvivalRateYear2 { get; set; }
        public decimal SurvivalRateYear3 { get; set; }
        public decimal SurvivalRateYear4 { get; set; }
        public decimal SurvivalRateYear5 { get; set; }
        public decimal SurvivalRateYear6 { get; set; }
        public decimal SurvivalRateYear7 { get; set; }
        public decimal SurvivalRateYear8 { get; set; }
        public decimal SurvivalRateYear9 { get; set; }
        public decimal SurvivalRateYear10 { get; set; }


        public decimal VcDiscountRateStartUp { get; set; }
        public decimal VcDiscountRateFirstStageOrEarlyDevelopment { get; set; }
        public decimal VcDiscountRateSecondStageOrExpansion { get; set; }
        public decimal VcDiscountRateBridgeOrIpo { get; set; }

        [NotMapped] 
        public decimal FailureRateYear1 { get { return SurvivalRateYear0 - SurvivalRateYear1; } }
        [NotMapped]
        public decimal FailureRateYear2 { get { return SurvivalRateYear1 - SurvivalRateYear2; } }
        [NotMapped]
        public decimal FailureRateYear3 { get { return SurvivalRateYear2 - SurvivalRateYear3; } }
        [NotMapped]
        public decimal FailureRateYear4 { get { return SurvivalRateYear3 - SurvivalRateYear4; } }
        [NotMapped]
        public decimal FailureRateYear5 { get { return SurvivalRateYear4 - SurvivalRateYear5; } }
        [NotMapped]
        public decimal FailureRateYear6 { get { return SurvivalRateYear5 - SurvivalRateYear6; } }
        [NotMapped]
        public decimal FailureRateYear7 { get { return SurvivalRateYear6 - SurvivalRateYear7; } }
        [NotMapped]
        public decimal FailureRateYear8 { get { return SurvivalRateYear7 - SurvivalRateYear8; } }
        [NotMapped]
        public decimal FailureRateYear9 { get { return SurvivalRateYear8 - SurvivalRateYear9; } }
        [NotMapped]
        public decimal FailureRateYear10 { get { return SurvivalRateYear9 - SurvivalRateYear10; } }



        public decimal ProbabilityOfFailure(int year)
        {
            if (year < 1)
                throw new ArgumentOutOfRangeException("No handling of year < 1 for ProbabilityOfFailure");
            switch (year)
            {
                case 1:
                    return ProbabilityOfFailureYear1;
                case 2:
                    return ProbabilityOfFailureYear2;
                case 3:
                    return ProbabilityOfFailureYear3;
                case 4:
                    return ProbabilityOfFailureYear4;
                case 5:
                    return ProbabilityOfFailureYear5;
                case 6:
                    return ProbabilityOfFailureYear6;
                case 7:
                    return ProbabilityOfFailureYear7;
                case 8:
                    return ProbabilityOfFailureYear8;
                case 9:
                    return ProbabilityOfFailureYear9;
                case 10:
                    return ProbabilityOfFailureYear10;
                default:
                    
                    return year > 10 ? ProbabilityOfFailureYear10 : 1;
            }
        }

        public decimal ProbabilityOfSurvival(int year)
        {
            if (year < 1)
                throw new ArgumentOutOfRangeException("No handling of year < 1 for ProbabilityOfSurvival");
            return 1 - ProbabilityOfFailure(year);
        }

        [NotMapped]
        public decimal ProbabilityOfFailureYear1 { get { return ProbabilityOfFailureYear2 + FailureRateYear2; } }
        [NotMapped]
        public decimal ProbabilityOfFailureYear2 { get { return ProbabilityOfFailureYear3 + FailureRateYear3; } }
        [NotMapped]
        public decimal ProbabilityOfFailureYear3 { get { return ProbabilityOfFailureYear4 + FailureRateYear4; } }
        [NotMapped]
        public decimal ProbabilityOfFailureYear4 { get { return ProbabilityOfFailureYear5 + FailureRateYear5; } }
        [NotMapped]
        public decimal ProbabilityOfFailureYear5 { get { return ProbabilityOfFailureYear6 + FailureRateYear6; } }
        [NotMapped]
        public decimal ProbabilityOfFailureYear6 { get { return ProbabilityOfFailureYear7 + FailureRateYear7; } }
        [NotMapped]
        public decimal ProbabilityOfFailureYear7 { get { return ProbabilityOfFailureYear8 + FailureRateYear8; } }
        [NotMapped]
        public decimal ProbabilityOfFailureYear8 { get { return ProbabilityOfFailureYear9 + FailureRateYear9; } }
        [NotMapped]
        public decimal ProbabilityOfFailureYear9 { get { return FailureRateYear10; } }
        [NotMapped]
        public decimal ProbabilityOfFailureYear10 { get { return 0; } }



        #region LiquidationvalueRecoveryRate
        public decimal LiquidationvalueRecoveryRate_AccountsReceiveable { get; set; }
        public decimal LiquidationvalueRecoveryRate_Inventory { get; set; }
        public decimal LiquidationvalueRecoveryRate_OtherCurrentAssets { get; set; }
        public decimal LiquidationvalueRecoveryRate_CashAndCashEquivalents { get; set; }
        public decimal LiquidationvalueRecoveryRate_IntangibleAssets { get; set; }
        public decimal LiquidationvalueRecoveryRate_FixedAssets { get; set; }

        public decimal LiquidationvalueRecoveryRate_AccountsPayable { get; set; }
        public decimal LiquidationvalueRecoveryRate_OtherCurrentLiabilities { get; set; }
        public decimal LiquidationvalueRecoveryRate_3rdPartyInterest_BearingDebtLongTermShortTerm { get; set; }
        public decimal LiquidationvalueRecoveryRate_OtherLong_TermLiabilities { get; set; }
        public decimal LiquidationvalueRecoveryRate_ShareholderLoans { get; set; }

        #endregion

        public decimal EBITDA_TTM_Sensitivity { get; set; }
        public decimal EBITDA_Multiple_Sensitivity { get; set; }


        public decimal WACCSensitivity { get; set; }
        public decimal TgSensitivity { get; set; }
        public decimal TermEBITDAMultipleSensitivity { get; set; }


    }
}
