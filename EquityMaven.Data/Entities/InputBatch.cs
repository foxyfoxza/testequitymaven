﻿using EquityMaven.Data.Extensions;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Entities
{
    public enum BatchType {Unknown = 0, Annual = 1, Monthly = 2}
     
    public class InputBatch: EntityItemInt, IInputBatch
    {
        //public override void OnCopyFrom(EntityItem from)
        //{
        //    CheckOnCopyTypeMismatch(from);
        //    InputBatchExtension.CopyFrom(this, from as InputBatch);
        //}


        [NotMapped]
        public BatchType BatchType { get { return (BatchType)BatchTypeId; } set { BatchTypeId = (int)value; } }
        public int BatchTypeId { get; set; } = 2;
        public DateTime? ActiveFrom { get; set; }
        public string Description { get; set; }
        //public ICollection<CorporateBondSpread> CorporateBondSpreads { get; set; }
        //public ICollection<RawCountryLookup> CountryLookups { get; set; }
        //public ICollection<RawCurrencyLookup> CurrencyLookups { get; set; }
        //public ICollection<RawMoodysRatingLookup> MoodysRatingLookups { get; set; }
        //public ICollection<RawTBRCLookup> ThomsonReutersBusinessClassifications { get; set; }
        public bool isActive { get; set; } = true;



        public ICollection<TRBCValue> TRBCValues { get; set; }
        public GlobalDefaults GlobalDefaults { get; set; }
        public ICollection<Currency> CurrencyLookups { get; set; }
        public ICollection<ThompsonReutersBusinessClassification> ThompsonReutersBusinessClassifications { get; set; }
        //public ICollection<TRBCBusinessSector> BusinessSectors { get; set; }
        //public ICollection<TRBCIndustryGroup> IndustryGroups { get; set; }
        //public ICollection<TRBCIndustry> Industries { get; set; }
        //public ICollection<TRBCActivity> Activities { get; set; }

        public ICollection<CountryTaxRate> CountryTaxRates { get; set; }
        public ICollection<CountryMoodysRating> CountryMoodysRatings { get; set; }
         


        //public ICollection<RawMoodysRatingLookup> MoodysRatingLookups { get; set; }
        //public ICollection<RawTBRCLookup> ThomsonReutersBusinessClassifications { get; set; }

    }
}
