﻿using EquityMaven.Interfaces.Calculations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Interfaces.FinancialYears;
using EquityMaven.Data.Calculations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using EquityMaven.CommonEnums;

namespace EquityMaven.Data.Entities
{


    public class FullValuationReport : EntityItemInt, IFinancialValuation
    {



        public void CopyFrom(IFinancialValuation from)
        {


            var fullValuationInput = from as FinancialValuation;
            if (fullValuationInput != null)
            {
                this.ClientId = fullValuationInput.ClientId;
                this.ISOCurrencySymbol = fullValuationInput.ISOCurrencySymbol;
                this.ThreeLetterISORegionName = fullValuationInput.ThreeLetterISORegionName;
                this.TRBC2012HierarchicalID = fullValuationInput.TRBC2012HierarchicalID;
                this.LastFinancialYearEnd = fullValuationInput.LastFinancialYearEnd;
                this.ValuationDate = fullValuationInput.ValuationDate;
                this.DateOfCommencement = fullValuationInput.DateOfCommencement;
                this.Description = fullValuationInput.Description;
                this.CompanyName = fullValuationInput.CompanyName;
                this.All3rdPartyInterestBearingDebtAsAtLastMonth_End = fullValuationInput.All3rdPartyInterestBearingDebtAsAtLastMonth_End;
            }
            VCDiscountRate = from.VCDiscountRate;
            MonthsSinceLastFinancialYearEnd = from.MonthsSinceLastFinancialYearEnd;
            Beta_LeveredBeta = from.Beta_LeveredBeta;
            Beta_MarginalTaxRate = from.Beta_MarginalTaxRate;
            Beta_TotalDebtToEquityMarketCap = from.Beta_TotalDebtToEquityMarketCap;
            Beta_UnleveredBeta = from.Beta_UnleveredBeta;
            CostOfDebt_After_TaxCostOfDebt = from.CostOfDebt_After_TaxCostOfDebt;
            CostOfDebt_EstimatedCorporateDefaultSpread = from.CostOfDebt_EstimatedCorporateDefaultSpread;
            CostOfDebt_EstimatedCostOfDebt = from.CostOfDebt_EstimatedCostOfDebt;
            CostOfDebt_EstimatedCountryDefaultSpread = from.CostOfDebt_EstimatedCountryDefaultSpread;
            CostOfDebt_InterestCoverageRatio = from.CostOfDebt_InterestCoverageRatio;
            CostOfDebt_MarginalTaxRate = from.CostOfDebt_MarginalTaxRate;
            CostOfDebt_RiskFreeRate = from.CostOfDebt_RiskFreeRate;
            CurrentCashOnHand = from.CurrentCashOnHand;
            DiscountedCashFlow_DiscretePeriod = from.DiscountedCashFlow_DiscretePeriod;
            DiscountedCashFlow_EnterpriseValuePre_IlliquidityDiscount = from.DiscountedCashFlow_EnterpriseValuePre_IlliquidityDiscount;
            DiscountedCashFlow_EquityValuePost_IlliquidityDiscount = from.DiscountedCashFlow_EquityValuePost_IlliquidityDiscount;
            DiscountedCashFlow_EquityValuePre_IlliquidityDiscount = from.DiscountedCashFlow_EquityValuePre_IlliquidityDiscount;
            DiscountedCashFlow_ExcessCash = from.DiscountedCashFlow_ExcessCash;
            DiscountedCashFlow_IlliquidityDiscount = from.DiscountedCashFlow_IlliquidityDiscount;
            DiscountedCashFlow_InterestBearingDebt = from.DiscountedCashFlow_InterestBearingDebt;
            DiscountedCashFlow_TerminalPeriod = from.DiscountedCashFlow_TerminalPeriod;
            EquityRiskPremium_CountryRatingBasedDefaultSpread = from.EquityRiskPremium_CountryRatingBasedDefaultSpread;
            EquityRiskPremium_EquityRiskPremium = from.EquityRiskPremium_EquityRiskPremium;
            EquityRiskPremium_MatureMarketImpliedEquityRiskPremium = from.EquityRiskPremium_MatureMarketImpliedEquityRiskPremium;
            EquityRiskPremium_RelativeVolatilityOfEquityVsBonds = from.EquityRiskPremium_RelativeVolatilityOfEquityVsBonds;
            EvOverEbitdaValuation_EnterpriseValuePre_IlliquidityDiscount = from.EvOverEbitdaValuation_EnterpriseValuePre_IlliquidityDiscount;
            EvOverEbitdaValuation_EquityValuePost_IlliquidityDiscount = from.EvOverEbitdaValuation_EquityValuePost_IlliquidityDiscount;
            EvOverEbitdaValuation_EquityValuePre_IlliquidityDiscount = from.EvOverEbitdaValuation_EquityValuePre_IlliquidityDiscount;
            EvOverEbitdaValuation_ExcessCash = from.EvOverEbitdaValuation_ExcessCash;
            EvOverEbitdaValuation_IlliquidityDiscount = from.EvOverEbitdaValuation_IlliquidityDiscount;
            EvOverEbitdaValuation_InterestBearingDebt = from.EvOverEbitdaValuation_InterestBearingDebt;
            EvOverEbitdaValuation_SustainableEbitdaTtm = from.EvOverEbitdaValuation_SustainableEbitdaTtm;
            WorldMedian_World = from.WorldMedian_World;
            IlliquidityDiscount = from.IlliquidityDiscount;
            PercentageOperatingCashRequired = from.PercentageOperatingCashRequired;
            Risk_FreeRate_Risk_FreeRate = from.Risk_FreeRate_Risk_FreeRate;
            WaccCalculation_D_Over_D_Plus_E = from.WaccCalculation_D_Over_D_Plus_E;
            WaccCalculation_E_Over_D_Plus_E = from.WaccCalculation_E_Over_D_Plus_E;
            WaccCalculation_Kd = from.WaccCalculation_Kd;
            WaccCalculation_Ke = from.WaccCalculation_Ke;
            WaccCalculation_Wacc = from.WaccCalculation_Wacc;
            WaccInputsSummary_CompanyRiskPremium = from.WaccInputsSummary_CompanyRiskPremium;
            WaccInputsSummary_CostOfDebtPre_Tax = from.WaccInputsSummary_CostOfDebtPre_Tax;
            WaccInputsSummary_EquityRiskPremium = from.WaccInputsSummary_EquityRiskPremium;
            WaccInputsSummary_GearingD_Over_D_Plus_E = from.WaccInputsSummary_GearingD_Over_D_Plus_E;
            WaccInputsSummary_LeveredBeta = from.WaccInputsSummary_LeveredBeta;
            WaccInputsSummary_Risk_FreeRate = from.WaccInputsSummary_Risk_FreeRate;
            WaccInputsSummary_TaxRate = from.WaccInputsSummary_TaxRate;
            WaccInputsSummary_TerminalGrowthRate = from.WaccInputsSummary_TerminalGrowthRate;
            WaccInputsSummary_TotalDebt_Over_EquityD_Over_E = from.WaccInputsSummary_TotalDebt_Over_EquityD_Over_E;
            NumberOfEmployees = from.NumberOfEmployees;

            EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount = from.EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount;

            EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedEquityValuePost_IlliquidityDiscount = from.EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedEquityValuePost_IlliquidityDiscount;

            EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue = from.EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue;
            EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival = from.EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival;
            EnterpriseValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount = from.EnterpriseValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount;


            EquityValue_PostSurvivalAdjustment_VCMethod_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival = from.EquityValue_PostSurvivalAdjustment_VCMethod_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival;
            ReportDate = from.ReportDate;
            ProbabilityOfFailure = from.ProbabilityOfFailure;
            ProbabilityOfSurvival = from.ProbabilityOfSurvival;
            YearsInOperation = from.YearsInOperation;

            EquityValue_PostSurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount = from.EquityValue_PostSurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount;
            EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedEquityValuePost_IlliquidityDiscount = from.EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedEquityValuePost_IlliquidityDiscount;
            EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue = from.EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue;
            EquityValue_PostSurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival = from.EquityValue_PostSurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival;
            EquityValue_PostSurvivalAdjustment_MULTIPLES_EquityValuePost_IlliquidityDiscount = from.EquityValue_PostSurvivalAdjustment_MULTIPLES_EquityValuePost_IlliquidityDiscount;
            EquityValue_PostSurvivalAdjustment_MULTIPLES_Probability_WeightedEquityValuePost_IlliquidityDiscount = from.EquityValue_PostSurvivalAdjustment_MULTIPLES_Probability_WeightedEquityValuePost_IlliquidityDiscount;
            EquityValue_PostSurvivalAdjustment_MULTIPLES_Probability_WeightedLiquidationValue = from.EquityValue_PostSurvivalAdjustment_MULTIPLES_Probability_WeightedLiquidationValue;
            EquityValue_PostSurvivalAdjustment_MULTIPLES_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival = from.EquityValue_PostSurvivalAdjustment_MULTIPLES_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival;
            EquityValue_PostSurvivalAdjustment_VCMethod_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival = from.EquityValue_PostSurvivalAdjustment_VCMethod_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival;
            StageOfDevelopment = from.StageOfDevelopment;

            Pre_SurvivalAdjustmentDCF_EnterpriseValuePre_IlliquidityDiscount = from.Pre_SurvivalAdjustmentDCF_EnterpriseValuePre_IlliquidityDiscount;
            Pre_SurvivalAdjustmentDCF_ExcessCash = from.Pre_SurvivalAdjustmentDCF_ExcessCash;

            All3rdPartyInterestBearingDebtAsAtLastMonth_End = from.All3rdPartyInterestBearingDebtAsAtLastMonth_End;
            Pre_SurvivalAdjustmentDCF_IlliquidityDiscount = from.Pre_SurvivalAdjustmentDCF_IlliquidityDiscount;
            Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount = from.Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount;



            EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue = from.EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue;
            EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityWeightedLiquidationValue = from.EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityWeightedLiquidationValue;
            EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityWeightedLiquidationValue = from.EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityWeightedLiquidationValue;




            WorldMedian_Africa = from.WorldMedian_Africa;
            WorldMedian_NorthAmerica = from.WorldMedian_NorthAmerica;
            WorldMedian_SouthCentralAmerica = from.WorldMedian_SouthCentralAmerica;
            WorldMedian_Asia = from.WorldMedian_Asia;
            WorldMedian_AustraliaNewZealand = from.WorldMedian_AustraliaNewZealand;
            WorldMedian_EasternEuropeRussia = from.WorldMedian_EasternEuropeRussia;
            WorldMedian_SouthernEurope = from.WorldMedian_SouthernEurope;
            WorldMedian_WesternEurope = from.WorldMedian_WesternEurope;
            WorldMedian_MiddleEast = from.WorldMedian_MiddleEast;

            Industry_EBITMargin  = from.Industry_EBITMargin;
            Industry_EBITDAMargin  = from.Industry_EBITDAMargin;
            Industry_GrossProfitMargin  = from.Industry_GrossProfitMargin;
            Industry_CurrentRatio  = from.Industry_CurrentRatio;
            Industry_InterestCover  = from.Industry_InterestCover;
            Industry_TotalDebt_Over_Ebitda  = from.Industry_TotalDebt_Over_Ebitda;
            Industry_AccountsPayableDaysAverage  = from.Industry_AccountsPayableDaysAverage;
            Industry_InventoryDaysAverage  = from.Industry_InventoryDaysAverage;
            Industry_AccountsReceiveableDaysAverage  = from.Industry_AccountsReceiveableDaysAverage;



            #region Sensitivity Analysis Equity Value Survival Adjusted Multiples

            SurvivalAdjustedMultiples_EBITDA_TTM_1 = from.SurvivalAdjustedMultiples_EBITDA_TTM_1;
            SurvivalAdjustedMultiples_EBITDA_TTM_2 = from.SurvivalAdjustedMultiples_EBITDA_TTM_2;
            SurvivalAdjustedMultiples_EBITDA_TTM_3 = from.SurvivalAdjustedMultiples_EBITDA_TTM_3;
            SurvivalAdjustedMultiples_EBITDA_TTM_4 = from.SurvivalAdjustedMultiples_EBITDA_TTM_4;
            SurvivalAdjustedMultiples_EBITDA_TTM_5 = from.SurvivalAdjustedMultiples_EBITDA_TTM_5;
            SurvivalAdjustedMultiples_AMultiple = from.SurvivalAdjustedMultiples_AMultiple;
            SurvivalAdjustedMultiples_A1 = from.SurvivalAdjustedMultiples_A1;
            SurvivalAdjustedMultiples_A2 = from.SurvivalAdjustedMultiples_A2;
            SurvivalAdjustedMultiples_A3 = from.SurvivalAdjustedMultiples_A3;
            SurvivalAdjustedMultiples_A4 = from.SurvivalAdjustedMultiples_A4;
            SurvivalAdjustedMultiples_A5 = from.SurvivalAdjustedMultiples_A5;
            SurvivalAdjustedMultiples_BMultiple = from.SurvivalAdjustedMultiples_BMultiple;
            SurvivalAdjustedMultiples_B1 = from.SurvivalAdjustedMultiples_B1;
            SurvivalAdjustedMultiples_B2 = from.SurvivalAdjustedMultiples_B2;
            SurvivalAdjustedMultiples_B3 = from.SurvivalAdjustedMultiples_B3;
            SurvivalAdjustedMultiples_B4 = from.SurvivalAdjustedMultiples_B4;
            SurvivalAdjustedMultiples_B5 = from.SurvivalAdjustedMultiples_B5;
            SurvivalAdjustedMultiples_CMultiple = from.SurvivalAdjustedMultiples_CMultiple;
            SurvivalAdjustedMultiples_C1 = from.SurvivalAdjustedMultiples_C1;
            SurvivalAdjustedMultiples_C2 = from.SurvivalAdjustedMultiples_C2;
            SurvivalAdjustedMultiples_C3 = from.SurvivalAdjustedMultiples_C3;
            SurvivalAdjustedMultiples_C4 = from.SurvivalAdjustedMultiples_C4;
            SurvivalAdjustedMultiples_C5 = from.SurvivalAdjustedMultiples_C5;
            SurvivalAdjustedMultiples_DMultiple = from.SurvivalAdjustedMultiples_DMultiple;
            SurvivalAdjustedMultiples_D1 = from.SurvivalAdjustedMultiples_D1;
            SurvivalAdjustedMultiples_D2 = from.SurvivalAdjustedMultiples_D2;
            SurvivalAdjustedMultiples_D3 = from.SurvivalAdjustedMultiples_D3;
            SurvivalAdjustedMultiples_D4 = from.SurvivalAdjustedMultiples_D4;
            SurvivalAdjustedMultiples_D5 = from.SurvivalAdjustedMultiples_D5;
            SurvivalAdjustedMultiples_EMultiple = from.SurvivalAdjustedMultiples_EMultiple;
            SurvivalAdjustedMultiples_E1 = from.SurvivalAdjustedMultiples_E1;
            SurvivalAdjustedMultiples_E2 = from.SurvivalAdjustedMultiples_E2;
            SurvivalAdjustedMultiples_E3 = from.SurvivalAdjustedMultiples_E3;
            SurvivalAdjustedMultiples_E4 = from.SurvivalAdjustedMultiples_E4;
            SurvivalAdjustedMultiples_E5 = from.SurvivalAdjustedMultiples_E5;


            #endregion




            #region Sensitivity Analysis Survival Adjusted DCF 
            SurvivalAdjustedDCFEquity_Wacc_1 = from.SurvivalAdjustedDCFEquity_Wacc_1;
            SurvivalAdjustedDCFEquity_Wacc_2 = from.SurvivalAdjustedDCFEquity_Wacc_2;
            SurvivalAdjustedDCFEquity_Wacc_3 = from.SurvivalAdjustedDCFEquity_Wacc_3;
            SurvivalAdjustedDCFEquity_Wacc_4 = from.SurvivalAdjustedDCFEquity_Wacc_4;
            SurvivalAdjustedDCFEquity_Wacc_5 = from.SurvivalAdjustedDCFEquity_Wacc_5;
            SurvivalAdjustedDCFEquity_AMultiple = from.SurvivalAdjustedDCFEquity_AMultiple;
            SurvivalAdjustedDCFEquity_A1 = from.SurvivalAdjustedDCFEquity_A1;
            SurvivalAdjustedDCFEquity_A2 = from.SurvivalAdjustedDCFEquity_A2;
            SurvivalAdjustedDCFEquity_A3 = from.SurvivalAdjustedDCFEquity_A3;
            SurvivalAdjustedDCFEquity_A4 = from.SurvivalAdjustedDCFEquity_A4;
            SurvivalAdjustedDCFEquity_A5 = from.SurvivalAdjustedDCFEquity_A5;
            SurvivalAdjustedDCFEquity_BMultiple = from.SurvivalAdjustedDCFEquity_BMultiple;
            SurvivalAdjustedDCFEquity_B1 = from.SurvivalAdjustedDCFEquity_B1;
            SurvivalAdjustedDCFEquity_B2 = from.SurvivalAdjustedDCFEquity_B2;
            SurvivalAdjustedDCFEquity_B3 = from.SurvivalAdjustedDCFEquity_B3;
            SurvivalAdjustedDCFEquity_B4 = from.SurvivalAdjustedDCFEquity_B4;
            SurvivalAdjustedDCFEquity_B5 = from.SurvivalAdjustedDCFEquity_B5;
            SurvivalAdjustedDCFEquity_CMultiple = from.SurvivalAdjustedDCFEquity_CMultiple;
            SurvivalAdjustedDCFEquity_C1 = from.SurvivalAdjustedDCFEquity_C1;
            SurvivalAdjustedDCFEquity_C2 = from.SurvivalAdjustedDCFEquity_C2;
            SurvivalAdjustedDCFEquity_C3 = from.SurvivalAdjustedDCFEquity_C3;
            SurvivalAdjustedDCFEquity_C4 = from.SurvivalAdjustedDCFEquity_C4;
            SurvivalAdjustedDCFEquity_C5 = from.SurvivalAdjustedDCFEquity_C5;
            SurvivalAdjustedDCFEquity_DMultiple = from.SurvivalAdjustedDCFEquity_DMultiple;
            SurvivalAdjustedDCFEquity_D1 = from.SurvivalAdjustedDCFEquity_D1;
            SurvivalAdjustedDCFEquity_D2 = from.SurvivalAdjustedDCFEquity_D2;
            SurvivalAdjustedDCFEquity_D3 = from.SurvivalAdjustedDCFEquity_D3;
            SurvivalAdjustedDCFEquity_D4 = from.SurvivalAdjustedDCFEquity_D4;
            SurvivalAdjustedDCFEquity_D5 = from.SurvivalAdjustedDCFEquity_D5;
            SurvivalAdjustedDCFEquity_EMultiple = from.SurvivalAdjustedDCFEquity_EMultiple;
            SurvivalAdjustedDCFEquity_E1 = from.SurvivalAdjustedDCFEquity_E1;
            SurvivalAdjustedDCFEquity_E2 = from.SurvivalAdjustedDCFEquity_E2;
            SurvivalAdjustedDCFEquity_E3 = from.SurvivalAdjustedDCFEquity_E3;
            SurvivalAdjustedDCFEquity_E4 = from.SurvivalAdjustedDCFEquity_E4;
            SurvivalAdjustedDCFEquity_E5 = from.SurvivalAdjustedDCFEquity_E5;
            #endregion




        }
        public FullValuationYearReportActualPrior ActualPrior { get; set; }
        public FullValuationYearReportActualCurrent ActualCurrent { get; set; }
        public FullValuationYearReportBudget Budget { get; set; }
        public FullValuationYearReportForecast1 Forecast1 { get; set; }
        public FullValuationYearReportForecast2 Forecast2 { get; set; }
        public FullValuationYearReportTerminal Terminal { get; set; }

        public int YearsInOperation { get; set; }
        public int NumberOfEmployees { get; set; }
        public decimal VCDiscountRate { get; set; }

        public int StageOfDevelopmentValue { get { return (int)StageOfDevelopment; } set { StageOfDevelopment = (StageOfDevelopment)value; } }

        [NotMapped]
        public StageOfDevelopment StageOfDevelopment { get; set; }


        #region Client
        private Int64? _ClientId;
        [Required]
        public Int64 ClientId
        {
            get
            {
                return GetAssociatedBy(_ClientId, _client);
            }
            set
            {
                SetAssociatedBy(ref _ClientId, ref _client, value);
            }
        }

        private Client _client;
        [JsonIgnore]
        public Client Client
        {
            get
            {
                return _client;
            }
            set
            {
                SetAssociatedBy(ref _client, value, _ClientId);
            }
        }
        #endregion

        #region Currency

        public string ISOCurrencySymbol { get; set; }

        [NotMapped]
        public Currency Currency { get; set; }
        #endregion

        #region Country

        public string ThreeLetterISORegionName { get; set; }
        [NotMapped]
        public Country Country { get; set; }

        #endregion


        public string TRBC2012HierarchicalID { get; set; }
        public DateTime LastFinancialYearEnd { get; set; }
        public DateTime ValuationDate { get; set; }
        public DateTime DateOfCommencement { get; set; }
        public DateTime ReportDate { get; set; }
        public decimal ProbabilityOfFailure { get; set; }
        public decimal ProbabilityOfSurvival { get; set; }

        public string Description { get; set; }
        public string CompanyName { get; set; }


        public decimal All3rdPartyInterestBearingDebtAsAtLastMonth_End { get; set; }





        public decimal Beta_LeveredBeta { get; set; }

        public decimal Beta_MarginalTaxRate { get; set; }

        public decimal Beta_TotalDebtToEquityMarketCap { get; set; }
        public decimal Beta_UnleveredBeta { get; set; }

        public decimal CostOfDebt_After_TaxCostOfDebt { get; set; }

        public decimal CostOfDebt_EstimatedCorporateDefaultSpread { get; set; }

        public decimal CostOfDebt_EstimatedCostOfDebt { get; set; }

        public decimal CostOfDebt_EstimatedCountryDefaultSpread { get; set; }

        public decimal CostOfDebt_InterestCoverageRatio { get; set; }

        public decimal CostOfDebt_MarginalTaxRate { get; set; }

        public decimal CostOfDebt_RiskFreeRate { get; set; }
        public decimal CurrentCashOnHand { get; set; }

        public decimal DiscountedCashFlow_DiscretePeriod { get; set; }

        public decimal DiscountedCashFlow_EnterpriseValuePre_IlliquidityDiscount { get; set; }

        public decimal DiscountedCashFlow_EquityValuePost_IlliquidityDiscount { get; set; }

        public decimal DiscountedCashFlow_EquityValuePre_IlliquidityDiscount { get; set; }
        public decimal DiscountedCashFlow_ExcessCash { get; set; }
        public decimal DiscountedCashFlow_IlliquidityDiscount { get; set; }
        public decimal DiscountedCashFlow_InterestBearingDebt { get; set; }
        public decimal DiscountedCashFlow_TerminalPeriod { get; set; }

        public decimal EquityRiskPremium_CountryRatingBasedDefaultSpread { get; set; }

        public decimal EquityRiskPremium_EquityRiskPremium { get; set; }

        public decimal EquityRiskPremium_MatureMarketImpliedEquityRiskPremium { get; set; }

        public decimal EquityRiskPremium_RelativeVolatilityOfEquityVsBonds { get; set; }

        public decimal EvOverEbitdaValuation_EnterpriseValuePre_IlliquidityDiscount { get; set; }

        public decimal EvOverEbitdaValuation_EquityValuePost_IlliquidityDiscount { get; set; }

        public decimal EvOverEbitdaValuation_EquityValuePre_IlliquidityDiscount { get; set; }

        public decimal EvOverEbitdaValuation_ExcessCash { get; set; }

        public decimal EvOverEbitdaValuation_IlliquidityDiscount { get; set; }

        public decimal EvOverEbitdaValuation_InterestBearingDebt { get; set; }
        public decimal EvOverEbitdaValuation_SustainableEbitdaTtm { get; set; }


        #region EV/EBITDA - World Median


        public decimal WorldMedian_World { get; set; }

        public decimal WorldMedian_Africa { get; set; }
        public decimal WorldMedian_NorthAmerica { get; set; }
        public decimal WorldMedian_SouthCentralAmerica { get; set; }
        public decimal WorldMedian_Asia { get; set; }
        public decimal WorldMedian_AustraliaNewZealand { get; set; }
        public decimal WorldMedian_EasternEuropeRussia { get; set; }
        public decimal WorldMedian_SouthernEurope { get; set; }
        public decimal WorldMedian_WesternEurope { get; set; }
        public decimal WorldMedian_MiddleEast { get; set; }

        #endregion


        #region Industry TRBC Values

        public decimal Industry_EBITMargin { get; set; }
        public decimal Industry_EBITDAMargin { get; set; }
        public decimal Industry_GrossProfitMargin { get; set; }
        public decimal Industry_CurrentRatio { get; set; }
        public decimal Industry_InterestCover { get; set; }
        public decimal Industry_TotalDebt_Over_Ebitda { get; set; }
        public decimal Industry_AccountsPayableDaysAverage { get; set; }
        public decimal Industry_InventoryDaysAverage { get; set; }
        public decimal Industry_AccountsReceiveableDaysAverage { get; set; }


        #endregion


        public decimal IlliquidityDiscount { get; set; }

        public int MonthsSinceLastFinancialYearEnd { get; set; }

        public decimal PercentageOperatingCashRequired { get; set; }

        public decimal Risk_FreeRate_Risk_FreeRate { get; set; }



        public decimal WaccCalculation_D_Over_D_Plus_E { get; set; }

        public decimal WaccCalculation_E_Over_D_Plus_E { get; set; }

        public decimal WaccCalculation_Kd { get; set; }

        public decimal WaccCalculation_Ke { get; set; }

        public decimal WaccCalculation_Wacc { get; set; }

        public decimal WaccInputsSummary_CompanyRiskPremium { get; set; }
        public decimal WaccInputsSummary_CostOfDebtPre_Tax { get; set; }

        public decimal WaccInputsSummary_EquityRiskPremium { get; set; }

        public decimal WaccInputsSummary_GearingD_Over_D_Plus_E { get; set; }

        public decimal WaccInputsSummary_LeveredBeta { get; set; }

        public decimal WaccInputsSummary_Risk_FreeRate { get; set; }

        public decimal WaccInputsSummary_TaxRate { get; set; }

        public decimal WaccInputsSummary_TerminalGrowthRate { get; set; }

        public decimal WaccInputsSummary_TotalDebt_Over_EquityD_Over_E { get; set; }





        #region LiquidationvalueRecoveryRate
        public decimal LiquidationvalueRecoveryRate_AccountsReceiveable { get; set; }
        public decimal LiquidationvalueRecoveryRate_Inventory { get; set; }
        public decimal LiquidationvalueRecoveryRate_OtherCurrentAssets { get; set; }
        public decimal LiquidationvalueRecoveryRate_CashAndCashEquivalents { get; set; }
        public decimal LiquidationvalueRecoveryRate_IntangibleAssets { get; set; }
        public decimal LiquidationvalueRecoveryRate_FixedAssets { get; set; }

        public decimal LiquidationvalueRecoveryRate_AccountsPayable { get; set; }
        public decimal LiquidationvalueRecoveryRate_OtherCurrentLiabilities { get; set; }
        public decimal LiquidationvalueRecoveryRate_3rdPartyInterest_BearingDebtLongTermShortTerm { get; set; }
        public decimal LiquidationvalueRecoveryRate_OtherLong_TermLiabilities { get; set; }
        public decimal LiquidationvalueRecoveryRate_ShareholderLoans { get; set; }

        #endregion


        #region LiquidationvalueRecoveryAmount 
        public decimal LiquidationvalueRecoveryAmount_CurrentAssets { get; set; }

        public decimal LiquidationvalueRecoveryAmount_AccountsReceiveable { get; set; }
        public decimal LiquidationvalueRecoveryAmount_Inventory { get; set; }
        public decimal LiquidationvalueRecoveryAmount_OtherCurrentAssets { get; set; }
        public decimal LiquidationvalueRecoveryAmount_CashAndCashEquivalents { get; set; }
        public decimal LiquidationvalueRecoveryAmount_IntangibleAssets { get; set; }
        public decimal LiquidationvalueRecoveryAmount_FixedAssets { get; set; }
        public decimal LiquidationvalueRecoveryAmount_TotalAssets { get; set; }
        public decimal LiquidationvalueRecoveryAmount_CurrentLiabilities { get; set; }

        public decimal LiquidationvalueRecoveryAmount_AccountsPayable { get; set; }
        public decimal LiquidationvalueRecoveryAmount_OtherCurrentLiabilities { get; set; }
        public decimal LiquidationvalueRecoveryAmount_3rdPartyInterest_BearingDebtLongTermShortTerm { get; set; }
        public decimal LiquidationvalueRecoveryAmount_OtherLong_TermLiabilities { get; set; }
        public decimal LiquidationvalueRecoveryAmount_ShareholderLoans { get; set; }
        public decimal LiquidationvalueRecoveryAmount_TotalLiabilities { get; set; }
        public decimal LiquidationvalueRecoveryAmount_LiquidationValue { get; set; }

        #endregion

        #region EQUITY VALUE - POST SURVIVAL ADJUSTMENT




        #endregion



        #region ENTERPRISE VALUE - POST SURVIVAL ADJUSTMENT



        #endregion


        #region SUMMARY - DCF, MULTIPLES, VC METHOD (PRE-SURVIVAL ADJUSTMENT)


        public decimal Pre_SurvivalAdjustmentDCF_Discrete { get; set; }

        public decimal Pre_SurvivalAdjustmentDCF_Terminal { get; set; }


        public decimal Pre_SurvivalAdjustmentDCF_EnterpriseValuePre_IlliquidityDiscount { get; set; }
        public decimal Pre_SurvivalAdjustmentDCF_ExcessCash { get; set; }
        public decimal Pre_SurvivalAdjustmentMultiples_EnterpriseValuePre_IlliquidityDiscount { get; set; }

        public decimal Pre_SurvivalAdjustmentDCF_EquityValuePre_IlliquidityDiscount { get; set; }
        public decimal Pre_SurvivalAdjustmentMultiples_EquityValuePre_IlliquidityDiscount { get; set; }
        public decimal Pre_SurvivalAdjustmentDCF_IlliquidityDiscount { get; set; }
        public decimal Pre_SurvivalAdjustmentMultiples_IlliquidityDiscount { get; set; }

        public decimal Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount { get; set; }
        public decimal Pre_SurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount { get; set; }


        public decimal Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPre_LiquidityDiscount { get; set; }
        public decimal Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPre_LiquidityDiscount { get; set; }



        public decimal Pre_SurvivalAdjustmentDCF_EnterpriseValuePost_IlliquidityDiscount { get; set; }
        public decimal Pre_SurvivalAdjustmentMultiples_EnterpriseValuePost_IlliquidityDiscount { get; set; }
        public decimal Pre_SurvivalAdjustmentVCMethod_EnterpriseValuePost_IlliquidityDiscount { get; set; }

        public decimal Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPost_LiquidityDiscount { get; set; }
        public decimal Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPost_LiquidityDiscount { get; set; }
        public decimal Pre_SurvivalAdjustmentVCMethod_ImpliedEv_Over_EbitdaPost_LiquidityDiscount { get; set; }



        #endregion




        [NotMapped]
        public IFinancialYearActualPrior iActualPrior { get { return ActualPrior; } }
        [NotMapped]
        public IFinancialYearActualCurrent iActualCurrent { get { return ActualCurrent; } }
        [NotMapped]
        public IFinancialYearBudget iBudget { get { return Budget; } }
        [NotMapped]
        public IFinancialYearForecast1 iForecast1 { get { return Forecast1; } }
        [NotMapped]
        public IFinancialYearForecast2 iForecast2 { get { return Forecast2; } }
        [NotMapped]
        public IFinancialYearTerminal iTerminal { get { return Terminal; } }

        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityOfSurvival { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedEquityValuePostIlliquidityDiscount { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_LiquidationValue { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityOfFailure { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityOfSurvival { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityWeightedEquityValuePostIlliquidityDiscount { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_LiquidationValue { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityOfFailure { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_EnterpriseValuePost_IlliquidityDiscount { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityOfSurvival { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityWeightedEquityValuePostIlliquidityDiscount { get; set; }
        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_LiquidationValue { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityOfFailure { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_VCMethod_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get; set; }



        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount { get; set; }

        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get; set; }
        public decimal EquityValue_PostSurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount { get; set; }
        public decimal EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedEquityValuePost_IlliquidityDiscount { get; set; }
        public decimal EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue { get; set; }
        public decimal EquityValue_PostSurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival { get; set; }
        public decimal EquityValue_PostSurvivalAdjustment_MULTIPLES_EquityValuePost_IlliquidityDiscount { get; set; }
        public decimal EquityValue_PostSurvivalAdjustment_MULTIPLES_Probability_WeightedEquityValuePost_IlliquidityDiscount { get; set; }
        public decimal EquityValue_PostSurvivalAdjustment_MULTIPLES_Probability_WeightedLiquidationValue { get; set; }

        public decimal EquityValue_PostSurvivalAdjustment_MULTIPLES_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival { get; set; }
        public decimal EquityValue_PostSurvivalAdjustment_VCMethod_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival { get; set; }
        public decimal EquityValue_PostSurvivalAdjustment_VCMethod_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get; set; }



        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue { get; set; }
        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityWeightedLiquidationValue { get; set; }
        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityWeightedLiquidationValue { get; set; }





        #region Sensitivity Analysis Equity Value Survival Adjusted Multiples

        public decimal SurvivalAdjustedMultiples_EBITDA_TTM_1 { get; set; }
        public decimal SurvivalAdjustedMultiples_EBITDA_TTM_2 { get; set; }
        public decimal SurvivalAdjustedMultiples_EBITDA_TTM_3 { get; set; }
        public decimal SurvivalAdjustedMultiples_EBITDA_TTM_4 { get; set; }
        public decimal SurvivalAdjustedMultiples_EBITDA_TTM_5 { get; set; }



        public decimal SurvivalAdjustedMultiples_AMultiple { get; set; }
        public decimal SurvivalAdjustedMultiples_A1 { get; set; }
        public decimal SurvivalAdjustedMultiples_A2 { get; set; }
        public decimal SurvivalAdjustedMultiples_A3 { get; set; }
        public decimal SurvivalAdjustedMultiples_A4 { get; set; }
        public decimal SurvivalAdjustedMultiples_A5 { get; set; }

        public decimal SurvivalAdjustedMultiples_BMultiple { get; set; }
        public decimal SurvivalAdjustedMultiples_B1 { get; set; }
        public decimal SurvivalAdjustedMultiples_B2 { get; set; }
        public decimal SurvivalAdjustedMultiples_B3 { get; set; }
        public decimal SurvivalAdjustedMultiples_B4 { get; set; }
        public decimal SurvivalAdjustedMultiples_B5 { get; set; }

        public decimal SurvivalAdjustedMultiples_CMultiple { get; set; }
        public decimal SurvivalAdjustedMultiples_C1 { get; set; }
        public decimal SurvivalAdjustedMultiples_C2 { get; set; }
        public decimal SurvivalAdjustedMultiples_C3 { get; set; }
        public decimal SurvivalAdjustedMultiples_C4 { get; set; }
        public decimal SurvivalAdjustedMultiples_C5 { get; set; }

        public decimal SurvivalAdjustedMultiples_DMultiple { get; set; }
        public decimal SurvivalAdjustedMultiples_D1 { get; set; }
        public decimal SurvivalAdjustedMultiples_D2 { get; set; }
        public decimal SurvivalAdjustedMultiples_D3 { get; set; }
        public decimal SurvivalAdjustedMultiples_D4 { get; set; }
        public decimal SurvivalAdjustedMultiples_D5 { get; set; }

        public decimal SurvivalAdjustedMultiples_EMultiple { get; set; }
        public decimal SurvivalAdjustedMultiples_E1 { get; set; }
        public decimal SurvivalAdjustedMultiples_E2 { get; set; }
        public decimal SurvivalAdjustedMultiples_E3 { get; set; }
        public decimal SurvivalAdjustedMultiples_E4 { get; set; }
        public decimal SurvivalAdjustedMultiples_E5 { get; set; }


        #endregion


        #region Sensitivity Analysis Survival Adjusted DCF 



        public decimal SurvivalAdjustedDCFEquity_Wacc_1 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_Wacc_2 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_Wacc_3 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_Wacc_4 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_Wacc_5 { get; set; }

        public decimal SurvivalAdjustedDCFEquity_AMultiple { get; set; }






        public decimal SurvivalAdjustedDCFEquity_A1 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_A2 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_A3 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_A4 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_A5 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_BMultiple { get; set; }
        public decimal SurvivalAdjustedDCFEquity_B1 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_B2 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_B3 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_B4 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_B5 { get; set; }

        public decimal SurvivalAdjustedDCFEquity_CMultiple { get; set; }
        public decimal SurvivalAdjustedDCFEquity_C1 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_C2 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_C3 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_C4 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_C5 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_DMultiple { get; set; }
        public decimal SurvivalAdjustedDCFEquity_D1 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_D2 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_D3 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_D4 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_D5 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_EMultiple { get; set; }
        public decimal SurvivalAdjustedDCFEquity_E1 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_E2 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_E3 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_E4 { get; set; }
        public decimal SurvivalAdjustedDCFEquity_E5 { get; set; }


        #endregion
    }
}
