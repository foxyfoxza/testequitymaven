﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Entities
{
    public class CurrencyRiskFreeRate : EntityBatchedItem
    {
        private Currency _Currency;
        [NotMapped]

        //public Currency Currency { get { return _Currency; } set { _Currency = value; CurrencyId = value == null ? Guid.Empty : value.Id; } }

        public Currency Currency
        {
            get
            {
                return _Currency;
            }
            set
            {
                SetAssociatedBy(ref _Currency, value, _CurrencyId);
            }
        }
        private Int64? _CurrencyId;

        public Int64 CurrencyId
        {
            get
            {
                return GetAssociatedBy(_CurrencyId, _Currency);
            }
            set
            {
                SetAssociatedBy(ref _CurrencyId, ref _Currency, value);
            }
        }

        public decimal RiskFreeRate { get; set; }
    }
}
