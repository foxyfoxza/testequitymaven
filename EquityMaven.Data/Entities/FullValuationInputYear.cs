﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.ClientInputs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.Entities
{
    public class FullValuationInputYear : EntityItemInt, IFullValuationInputYear

    {
        public FullValuationInputYear() { }
        public FullValuationInputYear (FinancialYearType fyt)
        {
            FinancialYearType = fyt;
        }
        [NotMapped]
        public FinancialYearType FinancialYearType { get { return (FinancialYearType)FinancialYearTypeValue; } set { FinancialYearTypeValue = (int)value; } }

        public int FinancialYearTypeValue { get; set; }


 
        #region Income Statement

        public decimal IncomeStatement_Revenue { get; set; }


        /// <summary>
        /// Cost of goods sold
        /// </summary>
        public decimal IncomeStatement_CostOfGoodsSold { get; set; }

        public decimal IncomeStatement_GrossProfit { get; set; }

        /// <summary>
        /// Operating expenses(excluding depreciation & amortisation)
        /// </summary>
        public decimal IncomeStatement_OperatingExpense { get; set; }


        /// <summary>
        /// Earnings before interest, tax, depreciation, amortisation("EBITDA")
        /// </summary>
        public decimal IncomeStatement_EBITDA { get; set; }

        /// <summary>
        /// Depreciation and amortisation expense
        /// </summary>
        public decimal IncomeStatement_DepreciationAndAmortisationExpense { get; set; }

        /// <summary>
        /// Earnings before interest and tax ("EBIT")
        /// </summary>
        public decimal IncomeStatement_EBIT { get; set; }

        public decimal IncomeStatement_InterestExpense { get; set; }
        public decimal IncomeStatement_EarningsBeforeTax { get; set; }

        /// <summary>
        /// Non-cash expenses included in Operating expenses above
        /// </summary>
        public decimal IncomeStatement_NonCashExpense { get; set; }
        private decimal _IncomeStatement_NonCashExpense;
        public Decimal IncomeStatementAnalysis_PercentageRevenueGrowth { get; set; }


        public decimal IncomeStatementAnalysis_PercentageGrossProfitMargin { get; set; }
        private decimal _IncomeStatementAnalysis_PercentageGrossProfitMargin { get; set; }
        public decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue { get; set; }



        public Decimal IncomeStatementAnalysis_EBITDAMargin { get; set; }
        //private decimal _IncomeStatementAnalysis_EBITDAMargin;
        public Decimal IncomeStatementAnalysis_EBITMargin { get; set; }


        /// <summary>
        ///         Depreciation and amortisation as % of Revenue
        /// </summary>
        public Decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue { get; set; }


        /// <summary>
        ///what percentage Non-cash expenses (included in Operating expenses) (excluding depreciation & amortisation) are expected to be as a percentage of Revenue. 
        /// </summary>
        public Decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue { get; set; }


        public Decimal IncomeStatementAnalysis_Calculated_InterestCover { get; set; }
        ////todo - implement this?  CLIENT INPUTS -> E45
        //public Decimal IncomeStatementAnalysis_PercentageFreeCashFlowOfRevenue { get { return 123456 / IncomeStatement_Revenue; } }
        ////todo - implement this? CLIENT INPUTS -> E46
        //public Decimal IncomeStatementAnalysis_PercentageFreeCashFlowOfEBITDA { get { return 456789 / IncomeStatement_EBITDA; } }


        #endregion

        #region Balance Sheet

        public decimal BalanceSheetTotalAssets_CurrentAssets { get; set; }
        public decimal BalanceSheetTotalAssets_AccountsReceiveable { get; set; }



        public decimal BalanceSheetTotalAssets_Inventory { get; set; }


        public decimal BalanceSheetTotalAssets_OtherCurrentAssets { get; set; }



        public decimal BalanceSheetTotalAssets_CashAndCashEquivalents { get; set; }

        public decimal BalanceSheetTotalAssets_IntangibleAssets { get; set; }

        public decimal BalanceSheetTotalAssets_FixedAssets { get; set; }
        public decimal BalanceSheetTotalAssets_FixedAndIntangibleAssets { get; set; }
        public decimal BalanceSheetTotalAssets_TotalAssets { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_AccountsPayable { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities { get; set; }
        public decimal BalanceSheetEquityAndLiabilities_CurrentLiabilities { get; set; }
                       
        public decimal BalanceSheetEquityAndLiabilities_ShortTermDebt { get; set; }
        public decimal BalanceSheetEquityAndLiabilities_LongTermDebt { get; set; }
        public decimal BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities { get; set; }
        public decimal BalanceSheetEquityAndLiabilities_ShareholderLoans { get; set; }


        public decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue { get; set; }

        public decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold { get; set; }
        public decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold { get; set; }
        public decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold { get; set; }
        public decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold { get; set; }

        public decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays { get; set; }
        public decimal BalanceSheetAnalysis_AverageInventoryDays { get; set; }

        public decimal BalanceSheetAnalysis_AverageAccountsPayableDays { get; set; }

        public decimal BalanceSheetAnalysis_CurrentRatio { get; set; }
        public decimal BalanceSheetAnalysis_TotalDebtOverEBITDA { get; set; }


        #endregion

        #region CapitalExpenditure
        public decimal CapitalExpenditure_CapitalExpenditure { get; set; }

        public decimal CapitalExpenditure_PercentageForecastCapitalExpenditure { get; set; }
        public decimal CapitalExpenditure_TTM { get; set; }
        #endregion

        #region Sustainable EBITDA
        /// <summary>
        ///  These are unsual once-off expenses not in the normal course of business that are not expected to be repeated e.g. office renovations, losses from sale of operating assets etc.
        /// </summary>
        public decimal SustainableEBITDA_NonRecurringExpenses { get; set; }
        /// <summary>
        /// This is unsual once-off income not in the normal course of business that is not expected to be repeated e.g. unsual and non-recurring sales (e.g. sale of a patent), profits from sale of operating assets etc.
        /// </summary>
        public decimal SustainableEBITDA_NonRecurringIncome { get; set; }

        public decimal SustainableEBITDA_SustainableEBITDA { get; set; }

        public decimal SustainableEBITDA_SustainableEBITDA_TTM { get; set; }

        #endregion

        #region Excess Cash

        public decimal ExcessCash_RevenueTTM { get; set; }

        public decimal ExcessCash_RequiredPercentageOperatingCash { get; set; }

        public decimal ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue { get; set; }

        public decimal ExcessCash_ExcessCash { get; set; }

        #endregion

        #region Assets Projection 
        public decimal AssetsProjection_FixedAndIntangibleAssets { get; set; }
        #endregion

        #region FullValuationInput
        private Int64? _FullValuationId;

        public Int64 FullValuationInputId
        {
            get
            {
                return GetAssociatedBy(_FullValuationId, _fullValuation);
            }
            set
            {
                SetAssociatedBy(ref _FullValuationId, ref _fullValuation, value);
            }
        }

        private FullValuationInput _fullValuation;
        [JsonIgnore]
        [NotMapped]
        public FullValuationInput FullValuationInput
        {
            get
            {
                return _fullValuation;
            }
            set
            {
                SetAssociatedBy(ref _fullValuation, value, _FullValuationId);
            }
        }
        #endregion
    }
}
