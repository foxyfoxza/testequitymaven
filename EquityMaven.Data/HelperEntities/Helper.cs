﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.HelperEntities
{
    
    //public class Helper
    //{


    //    public ClientInputsGeneral GeneralClientInputs { get; private set; }
    //    public IEnumerable<RawCurrencyLookup> UpdateTable1Results { get; private set; }
    //    public IEnumerable<RawCountryLookup> UpdateTable2Results { get; private set; }
    //    public IEnumerable<RawMoodysRatingLookup> UpdateTable3Results { get; private set; }
    //    public IEnumerable<RawCorporateBondSpread> UpdateTable4Results { get; private set; }
    //    public IEnumerable<RawTBRCLookup> TrbcMaps { get; private set; }

    //    public Helper(ClientInputsGeneral clientInputsGeneral, IEnumerable<RawCurrencyLookup> waccUpdateTable1Results, IEnumerable<RawCountryLookup> waccUpdateTable2Results, IEnumerable<RawMoodysRatingLookup> waccUpdateTable3Results, IEnumerable<RawCorporateBondSpread> waccUpdateTable4Results, IEnumerable<RawTBRCLookup> trbcMaps)
    //    {
    //        this.GeneralClientInputs = clientInputsGeneral;
    //        this.UpdateTable1Results = waccUpdateTable1Results;
    //        this.UpdateTable2Results = waccUpdateTable2Results;
    //        this.UpdateTable3Results = waccUpdateTable3Results;
    //        this.UpdateTable4Results = waccUpdateTable4Results;
    //        this.TrbcMaps = trbcMaps;
    //    }


    //    public WaccHelper Import()
    //    {
    //        var result = new WaccHelper();

    //        var table1CurrencyValue = UpdateTable1Results.FirstOrDefault(o => o.Currency == GeneralClientInputs.Currency);
    //        var c68_Table2CountryValue = UpdateTable2Results.FirstOrDefault(o => o.Country == GeneralClientInputs.Country);

    //        result.RiskFreeRate = table1CurrencyValue.RiskFreeRate.Value;
    //        var c67 = UpdateTable3Results.FirstOrDefault(o => o.Rating.StartsWith("Equity risk premium"));
    //        var c69 = c68_Table2CountryValue.MOODYSRATING;
    //        var c70_Table3MoodysRatingValue = UpdateTable3Results.FirstOrDefault(o => o.Rating == c68_Table2CountryValue.MOODYSRATING);
    //        var c71 = UpdateTable3Results.FirstOrDefault(o => o.Rating.StartsWith("Relative Volatility"));

    //        MatureMarketImpliedEquityRiskPremium = c67.DefaultSpread.Value;
    //        CountryRatingBasedDefaultSpread = c70_Table3MoodysRatingValue.DefaultSpread.Value / 10000;
    //        RelativeVolatilityOfEquityVSBonds = c71.DefaultSpread.Value;
    //        EquityRiskPremium = MatureMarketImpliedEquityRiskPremium + (CountryRatingBasedDefaultSpread * RelativeVolatilityOfEquityVSBonds);//=C67+(C70*C71)

    //        result.EquityRiskPremium = EquityRiskPremium;
    //        var trbcMap = TrbcMaps.FirstOrDefault(o => o.TRBC2012HierarchicalID == GeneralClientInputs.ThomsonReutersBusinessClassificationCode);

    //        UnleveredBeta = trbcMap.UnleveredBeta.Value;
    //        TotalDebtToEquity = trbcMap.TotalDebtMarketCap.Value;
    //        MarginalTaxRate = c68_Table2CountryValue.TaxRate.Value;//(decimal).28;
    //        RiskFreeRate = table1CurrencyValue.RiskFreeRate.Value;// (decimal)0.06165;
    //        InterestCoverageRatio = trbcMap.InterestCoverAdjusted.Value;//(decimal)0.371097294493;

    //        var table3 = UpdateTable3Results.FirstOrDefault(o => o.Rating == c68_Table2CountryValue.MOODYSRATING); //  

    //        var table4 = UpdateTable4Results.FirstOrDefault(o => o.InterestCoverageFrom.Value < InterestCoverageRatio && InterestCoverageRatio <= o.InterestCoverageTo.Value);
    //        EstimatedCorporateDefaultSpread = table4.Spread.Value;// (decimal)0.006;
    //        EstimatedCountryDefaultSpread = c70_Table3MoodysRatingValue.DefaultSpread.Value / 10000;//(decimal)0.022;
    //        result.LeveredBeta = UnleveredBeta * (1 + TotalDebtToEquity * (1 - MarginalTaxRate));//C60*(1+C63*(1-C62))
    //        result.PreTaxCostOfDebt = RiskFreeRate + EstimatedCorporateDefaultSpread + EstimatedCountryDefaultSpread;
    //        result.TotalDebtEquity = TotalDebtToEquity;
    //        result.Taxrate = MarginalTaxRate;
    //        return result;
    //    }
    //    public Decimal MatureMarketImpliedEquityRiskPremium { get; private set; }
    //    public Decimal CountryRatingBasedDefaultSpread { get; private set; }
    //    public Decimal RelativeVolatilityOfEquityVSBonds { get; private set; }
    //    public Decimal EquityRiskPremium { get; private set; }
    //    public Decimal UnleveredBeta { get; private set; }
    //    public Decimal TotalDebtToEquity { get; private set; }
    //    public Decimal MarginalTaxRate { get; private set; }

    //    public Decimal RiskFreeRate { get; private set; }
    //    public Decimal InterestCoverageRatio { get; private set; }
    //    public Decimal EstimatedCorporateDefaultSpread { get; private set; }
    //    public Decimal EstimatedCountryDefaultSpread { get; private set; }

    //}
}
