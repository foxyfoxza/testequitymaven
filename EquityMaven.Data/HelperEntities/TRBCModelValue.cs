﻿using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.ClientInputs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data
{
    public abstract class TRBCModelValue : EntityItemString, IBatchedItem
    {
        public TRBCModelValue() : base (false){ }
        [NotMapped]
        private Int64? _InputBatchId;
        [Required]
        public Int64 InputBatchId
        {
            get
            {
                return _InputBatchId ?? 0;
            }
            set
            {
                if (_InputBatchId != value && _InputBatch?.Id != value) _InputBatch = null;
                _InputBatchId = value;
            }
        }

        private InputBatch _InputBatch;
        [JsonIgnore]
       
        public InputBatch InputBatch
        {
            get
            {
                return _InputBatch;
            }
            set
            {
                if (_InputBatch != value && value?.Id != _InputBatchId) _InputBatchId = (value as InputBatch)?.Id;
                _InputBatch = value as InputBatch;
            }
        }

        public bool isActive { get; set; } = true;
        //public TRBCModelValue(bool isNew): base(isNew)
        //{

        //}
        public TRBCModelValue(InputBatch inputBatch, string id, string description, bool isNew): base(isNew)
        {
            this.InputBatch = inputBatch;
            this.Id = GetIDFrom(id, GetIDLength());
            this.Description = description;
        }

        //public TRBCModelValue(string id, string description)
        //{
        //    this.Id = GetIDFrom(id, GetIDLength());
        //    this.Description = description;
        //}
        [NotMapped]
        private string _id;

        [MinLength(1)] [MaxLength(10)]
        public override string Id { get { return _id; } set { _id = GetIDFrom(value, GetIDLength()); } }
        public string Description { get; set; }
        protected abstract int GetIDLength();

        protected static string GetIDFrom(string value, int _idLength)
        {
            return value?.Substring(0, Math.Min((value == null ? 0 : value.Length), _idLength));
        }

        private string _parentId;
        [NotMapped]
        protected string parentId { get { return _parentId; } set { _parentId = value; if (_parent?.Id != value) _parent = null; } }
        [NotMapped]
        private TRBCModelValue _parent;
        [NotMapped]
        protected TRBCModelValue parent { get { return _parent; } set { _parent = value; if (_parentId != value?.Id) _parentId = value?.Id; } }
    }
}
