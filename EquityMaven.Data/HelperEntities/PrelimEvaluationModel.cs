﻿using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data
{
    public class PrelimEvaluationModel
    {
        public string ISOCurrencySymbol { get; set; }
        public string Email { get; set; }
        public string CompanyName { get; set; }
 
        public decimal CurrentCashOnHand { get; set; }
        public decimal CurrentInterestBearingDebt { get; set; }
        public DateTime? LastFinancialYearEnd { get; set; }
        public decimal PercentageOperatingCashRequired { get; set; }
        public string TRBC2012HierarchicalID { get; set; }
        public decimal IncomeStatement_CostOfGoodsSold { get; set; }
        public decimal IncomeStatement_OperatingExpense { get; set; }
        public decimal IncomeStatement_Revenue { get; set; }
        public decimal ExcessCash_CurrentCashOnHandAsAtLastMonthEnd { get; set; }
        public IEnumerable<String[]> Currencies { get; set; }
        public IEnumerable<String[]> TRBCS { get; set; }
         

        public PrelimEvaluationModel()
        {
        }
        public PrelimEvaluationModel(IPrelimEval eval, IPrelimIncomeStatement incomestatement, IPrelimExcessCash excessCash)
        {
            CurrentCashOnHand = eval.CurrentCashOnHand;
            CurrentInterestBearingDebt = eval.CurrentInterestBearingDebt;
            LastFinancialYearEnd = eval.LastFinancialYearEnd;
            PercentageOperatingCashRequired = eval.PercentageOperatingCashRequired;
            TRBC2012HierarchicalID = eval.TRBC2012HierarchicalID;
            IncomeStatement_CostOfGoodsSold = incomestatement.IncomeStatement_CostOfGoodsSold;
            IncomeStatement_OperatingExpense = incomestatement.IncomeStatement_OperatingExpense;
            IncomeStatement_Revenue = incomestatement.IncomeStatement_Revenue;
            ExcessCash_CurrentCashOnHandAsAtLastMonthEnd = excessCash.ExcessCash_CurrentCashOnHandAsAtLastMonthEnd;
        }

        public PrelimEvaluation AsPrelimEvaluation()
        {
            return new PrelimEvaluation
            {
                CurrentCashOnHand = this.CurrentCashOnHand,
                CurrentInterestBearingDebt = this.CurrentInterestBearingDebt,
                LastFinancialYearEnd = this.LastFinancialYearEnd.Value ,
                PercentageOperatingCashRequired = this.PercentageOperatingCashRequired,
                TRBC2012HierarchicalID = this.TRBC2012HierarchicalID,
                IncomeStatement_CostOfGoodsSold = this.IncomeStatement_CostOfGoodsSold,
                IncomeStatement_OperatingExpense = this.IncomeStatement_OperatingExpense,
                IncomeStatement_Revenue = this.IncomeStatement_Revenue,
                ExcessCash_CurrentCashOnHandAsAtLastMonthEnd = this.ExcessCash_CurrentCashOnHandAsAtLastMonthEnd,
                Email = this.Email,
                CompanyName = this.CompanyName,
                ISOCurrencySymbol = this.ISOCurrencySymbol,
                //SourceId = this.SourceId,
            };
        }
    }
}
