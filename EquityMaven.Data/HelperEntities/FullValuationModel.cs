﻿using EquityMaven.CommonEnums;
using EquityMaven.Data.Calculations;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.HelperEntities
{
    public class FullValuationModel : EntityItemInt, IFullValuationInput
    {


        public static FullValuationModel ReadFullValuation(FullValuationInput fv, Client client, IEnumerable<IFullValuationInputYear> fullValuationInputYears)
        {

            FullValuationModel result = null;

            //result = Newtonsoft.Json.JsonConvert.DeserializeObject<FullValuationModel>(json);
            result = new FullValuationModel
            {
                Id = fv.Id,
                ClientId = client.Id,
                MemberId = client.MemberId,
                CompanyName = fv.CompanyName,
                ISOCurrencySymbol = fv.ISOCurrencySymbol,
                Currency = fv.Currency,
                Country = fv.Country,
                ThreeLetterISORegionName = fv.ThreeLetterISORegionName,
                CurrentCashOnHand = fv.CurrentCashOnHand,
                TRBC2012HierarchicalID = fv.TRBC2012HierarchicalID,
                LastFinancialYearEnd = fv.LastFinancialYearEnd,
                ValuationDate = DateTime.UtcNow,
                DateOfCommencement = fv.DateOfCommencement,
            };

            foreach(var item in fullValuationInputYears)
            {
                result.FinancialYears.Add(item);
            }
            return result;
        }


        public FullValuationModel() { }

        public FullValuationModel(DateTime lastFinancialYearEnd, decimal currentCashOnHand)
        {
            LastFinancialYearEnd = lastFinancialYearEnd;
            PrePolulateDefaults();
        }

        public void SetDefaults()
        {


            DateTime valuationdate = DateTime.UtcNow;


            InputsActualPrior = new FullValuationInputYear(FinancialYearType.ActualPrior)
            {

            };

            InputsActualCurrent = new FullValuationInputYear(FinancialYearType.ActualCurrent)
            {

            };

            InputsBudget = new FullValuationInputYear(FinancialYearType.Budget)
            {

            };

            InputsForecast1 = new FullValuationInputYear(FinancialYearType.Forecast1)
            {


            };

            InputsForecast2 = new FullValuationInputYear(FinancialYearType.Forecast2)
            {

            };


            ValuationDate = DateTime.UtcNow;

        }
        public void PrePolulateDefaults()
        {


            DateTime valuationdate = DateTime.UtcNow;


            InputsActualPrior = new FullValuationInputYear(FinancialYearType.ActualPrior)
            {
                //index = 0,
                IncomeStatement_Revenue = 23636936,
                IncomeStatement_CostOfGoodsSold = 1305107,
                IncomeStatement_OperatingExpense = 19586949,
                IncomeStatement_DepreciationAndAmortisationExpense = 354554,
                IncomeStatement_InterestExpense = 33582,
                IncomeStatement_NonCashExpense = 1664284,
                BalanceSheetTotalAssets_AccountsReceiveable = 1711697,
                BalanceSheetTotalAssets_Inventory = 0,
                BalanceSheetTotalAssets_OtherCurrentAssets = 383503,
                BalanceSheetTotalAssets_CashAndCashEquivalents = 1317910,
                BalanceSheetTotalAssets_FixedAssets = 1060328,
                BalanceSheetEquityAndLiabilities_AccountsPayable = 606519,


            };

            InputsActualCurrent = new FullValuationInputYear(FinancialYearType.ActualCurrent)
            {
                //index = 1,
                IncomeStatement_Revenue = 29884199,
                IncomeStatement_CostOfGoodsSold = 1814682,
                IncomeStatement_OperatingExpense = 24339108,
                IncomeStatement_DepreciationAndAmortisationExpense = 448262,
                IncomeStatement_InterestExpense = 42598,
                IncomeStatement_NonCashExpense = 970513,

                BalanceSheetTotalAssets_AccountsReceiveable = 4045036,
                BalanceSheetTotalAssets_Inventory = 50000,
                BalanceSheetTotalAssets_OtherCurrentAssets = 400897,
                BalanceSheetTotalAssets_CashAndCashEquivalents = 1089894,
                BalanceSheetTotalAssets_FixedAssets = 1748926,
                BalanceSheetEquityAndLiabilities_AccountsPayable = 437226,
                SustainableEBITDA_NonRecurringExpenses = 10000,
                SustainableEBITDA_NonRecurringIncome = 5000,

            };

            InputsBudget = new FullValuationInputYear(FinancialYearType.Budget)
            {
                //index = 2,
                IncomeStatementAnalysis_PercentageRevenueGrowth = (decimal)0.17,
                IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal).94,
                IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal)0.82,
                IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = InputsActualCurrent.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue,
                IncomeStatement_InterestExpense = 42598,
                IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = InputsActualCurrent.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = InputsActualCurrent.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = 0,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = InputsActualCurrent.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = InputsActualCurrent.
                        BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold,
                CapitalExpenditure_PercentageForecastCapitalExpenditure = (decimal).05,
                CapitalExpenditure_CapitalExpenditure = 1748226,
                SustainableEBITDA_NonRecurringExpenses = 0,
                SustainableEBITDA_NonRecurringIncome = 0,

            };

            InputsForecast1 = new FullValuationInputYear(FinancialYearType.Forecast1)
            {
                //index = 3,
                IncomeStatementAnalysis_PercentageRevenueGrowth = (decimal)0.144445,
                IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal).94,
                IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal)0.825,
                IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = InputsActualCurrent.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue,
                IncomeStatement_InterestExpense = 42598,
                IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = InputsBudget.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = InputsBudget.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = 0,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = InputsBudget.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold =
                    InputsBudget.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold,
                CapitalExpenditure_PercentageForecastCapitalExpenditure = (decimal).03,
                CapitalExpenditure_CapitalExpenditure = 1206276,

            };

            InputsForecast2 = new FullValuationInputYear(FinancialYearType.Forecast2)
            {
                //index = 4,
                IncomeStatementAnalysis_PercentageRevenueGrowth = (decimal)0.12,
                IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal).94,
                IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal)0.825,
                IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = InputsActualCurrent.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue,
                IncomeStatement_InterestExpense = 42598,
                IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = InputsForecast1.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = InputsForecast1.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = 0,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = InputsForecast1.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold =
                    InputsForecast1.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold,
                CapitalExpenditure_PercentageForecastCapitalExpenditure = (decimal).01,
                CapitalExpenditure_CapitalExpenditure = 450343,
            };


            ValuationDate = DateTime.UtcNow;

        }

        public Int64 ClientId { get; set; }
        public string TRBC2012HierarchicalID { get; set; }


        private IFullValuationInputYear GetByType(FinancialYearType fyt)
        {
            var result = _FinancialYears.FirstOrDefault(o => o.FinancialYearTypeValue == (int)fyt);
            var allOther = _FinancialYears.Where(o => o.FinancialYearTypeValue == (int)fyt && o != result).ToList();
            //to be safe, make sure there are no other records for same FinancialYearType, shouldnt happen, but possible
            foreach (var rem in allOther)
            {
                _FinancialYears.Remove(rem);
            }
            return result;
        }
        private void SetByType(FinancialYearType fyt, IFullValuationInputYear value)
        {
            var existing = _FinancialYears.OrderBy(o=> o == value).FirstOrDefault(o => o.FinancialYearTypeValue == (int)fyt);
            var allOther = _FinancialYears.Where(o => o.FinancialYearTypeValue == (int)fyt && o != existing).ToList();
            //to be safe, make sure there are no other records for same FinancialYearType, shouldnt happen, but possible
            foreach (var rem in allOther)
            {
                _FinancialYears.Remove(rem);
            }

            if (existing == value)
                return;
            if (existing != null)
                _FinancialYears.Remove(existing);
            if (value != null)
                _FinancialYears.Add(value);

        }

        public IFullValuationInputYear InputsActualPrior { get { return GetByType(FinancialYearType.ActualPrior); } set { SetByType(FinancialYearType.ActualPrior, value); } }
        public IFullValuationInputYear InputsActualCurrent { get { return GetByType(FinancialYearType.ActualCurrent); } set { SetByType(FinancialYearType.ActualCurrent, value); } }
        public IFullValuationInputYear InputsBudget { get { return GetByType(FinancialYearType.Budget); } set { SetByType(FinancialYearType.Budget, value); } }
        public IFullValuationInputYear InputsForecast1 { get { return GetByType(FinancialYearType.Forecast1); } set { SetByType(FinancialYearType.Forecast1, value); } }
        public IFullValuationInputYear InputsForecast2 { get { return GetByType(FinancialYearType.Forecast2); } set { SetByType(FinancialYearType.Forecast2, value); } }

        public string FieldId { get; set; }

        public string ThreeLetterISORegionName { get; set; }
        public string ISOCurrencySymbol { get; set; }




        #region General
        public string CompanyName { get; set; }


        /// <summary>
        /// Country where majority business operations of take place
        /// </summary>
        public Country Country { get; set; }
        /// <summary>
        /// Cash flow forecast currency
        /// </summary>
        public Currency Currency { get; set; }
        public string CurrencyIso { get; set; }
        public DateTime LastFinancialYearEnd { get; set; }
        public DateTime ValuationDate { get; set; }
        /// <summary>
        /// Months since last financial year end
        /// </summary>
        public int MonthsSinceLastFinancialYearEnd
        {
            get
            {
                return FinancialValuation.GetMonthsSinceLastFinancialYearEnd(LastFinancialYearEnd, ValuationDate);
            }
        }
        /// <summary>
        /// Year of commencing business operations
        /// </summary>
        public DateTime DateOfCommencement { get; set; }
        public int NumberOfEmployees { get; set; }
        /// <summary>
        /// Current cash on hand as at last month-end
        /// </summary>
        public decimal CurrentCashOnHand { get; set; }


        public decimal All3rdPartyInterestBearingDebtAsAtLastMonth_End { get; set; }
        /// <summary>
        /// Current interest-bearing debt as at last month-end
        /// </summary>
        public decimal CurrentInterestBearingDebt { get; set; }





        /// <summary>
        /// Industry(Thomson Reuters Business Classification)
        /// </summary>
        //public IThomsonReutersBusinessClassification ThomsonReutersBusinessClassification { get; }
        /// <summary>
        /// Operating cash required as % Revenue (assumed)
        /// </summary>

        public int StageOfDevelopmentValue { get { return (int)StageOfDevelopment; } set { StageOfDevelopment = (StageOfDevelopment)value; } }

        [NotMapped]
        public StageOfDevelopment StageOfDevelopment { get; set; }

        public decimal ExcessCash_RequiredPercentageOperatingCash { get; set; }

        [NotMapped]
        public int MemberId { get; set; }

        public string Description
        {
            get;
            set;
        }
        private ICollection<IFullValuationInputYear> _FinancialYears = new List<IFullValuationInputYear>();
        public ICollection<IFullValuationInputYear> FinancialYears
        {
            get
            {
                return _FinancialYears;
            }

        }

        #endregion


    }


}
