﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Data.HelperEntities
{
    //public class FullValuationClientInputs 
    //{
    //    public int index { get; set; }
    //    private FinancialYearType FinancialYearType;
    //    public FullValuationClientInputs(FinancialYearType financialYearType)
    //    {
    //        this.FinancialYearType = financialYearType;
    //    }

    //    public FullValuationClientInputs()
    //    {

    //    }
       
    //    public FullValuationClientInputs(ClientInputsFinancial cif)
    //    {
    //        this.FinancialYearType = cif.FinancialYearType;
    //        var test = new FullValuationClientInputs
    //        {

    //        };
    //    }



    //    #region General

    //    ///// <summary>
    //    ///// Months since last financial year end
    //    ///// </summary>
    //    //public int MonthsSinceLastFinancialYearEnd
    //    //{
    //    //    get
    //    //    {
    //    //        return (int)Math.Round((decimal)(ValuationDate.Subtract(LastFinancialYearEnd).Days / 365 * 12), 0);
    //    //    }
    //    //}
    //    //public DateTime ValuationDate { get; set; }
    //    //public DateTime LastFinancialYearEnd { get; set; }
    //    //public decimal CurrentCashOnHand { get; set; }

    //    #endregion

    //    #region Income Statement

    //    public decimal IncomeStatement_Revenue { get; set; }

    //    /// <summary>
    //    /// Cost of goods sold
    //    /// </summary>
    //    public decimal IncomeStatement_CostOfGoodsSold { get; set; }


    //    /// <summary>
    //    /// Operating expenses(excluding depreciation & amortisation)
    //    /// </summary>
    //    public decimal IncomeStatement_OperatingExpense { get; set; }





    //    /// <summary>
    //    /// Depreciation and amortisation expense
    //    /// </summary>
    //    public decimal IncomeStatement_DepreciationAndAmortisationExpense { get; set; }



    //    public decimal IncomeStatement_InterestExpense { get; set; }


    //    /// <summary>
    //    /// Non-cash expenses included in Operating expenses above
    //    /// </summary>
    //    public decimal IncomeStatement_NonCashExpense { get; set; }
    //    public Decimal IncomeStatementAnalysis_PercentageRevenueGrowth { get; set; }

    //    public decimal IncomeStatementAnalysis_PercentageGrossProfitMargin { get; set; }
    //    public decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue { get; set; }



    //    /// <summary>
    //    ///         Depreciation and amortisation as % of Revenue
    //    /// </summary>
    //    public Decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue { get; set; }
    //    /// <summary>
    //    ///what percentage Non-cash expenses (included in Operating expenses) (excluding depreciation & amortisation) are expected to be as a percentage of Revenue. 
    //    /// </summary>
    //    public Decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue { get; set; }


    //    #endregion



    //    #region Balance Sheet

    //    public decimal BalanceSheetTotalAssets_Total { get; set; }

    //    public decimal BalanceSheetTotalAssets_AccountsReceiveable { get; set; }


    //    public decimal BalanceSheetTotalAssets_Inventory { get; set; }

    //    public decimal BalanceSheetTotalAssets_OtherCurrentAssets { get; set; }



    //    public decimal BalanceSheetTotalAssets_CashAndCashEquivalents { get; set; }

    //    public decimal BalanceSheetTotalAssets_IntangibleAssets { get; set; }

    //    public decimal BalanceSheetTotalAssets_FixedAssets { get; set; }
    //    public decimal BalanceSheetTotalAssets_TotalAssets { get; set; }

    //    public decimal BalanceSheetEquityAndLiabilitiesCurrentLiabilities { get; set; }
    //    public decimal BalanceSheetEquityAndLiabilities_AccountsPayable { get; set; }
    //    public decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities { get; set; }
    //    public decimal BalanceSheetTotalAssets_Calculated_CurrentLiabilities { get; set; }
    //    public decimal BalanceSheetEquityAndLiabilities_ShortTermDebt { get; set; }
    //    public decimal BalanceSheetEquityAndLiabilities_LongTermDebt { get; set; }
    //    public decimal BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities { get; set; }
    //    public decimal BalanceSheetEquityAndLiabilities_ShareholderLoans { get; set; }
    //    public decimal BalanceSheetEquityAndLiabilities_TotalLiabilities { get; set; }
    //    public decimal BalanceSheetEquityAndLiabilities_ShareholdersEquity { get; set; }


    //    public decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue { get; set; }

    //    public decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold { get; set; }
    //    public decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold { get; set; }
    //    public decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold { get; set; }
    //    public decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold { get; set; }




    //    public decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays { get; set; }
    //    public decimal BalanceSheetAnalysis_AverageInventoryDays { get; set; }

    //    public decimal BalanceSheetAnalysis_AverageAccountsPayableDays { get; set; }
    //    public decimal BalanceSheetAnalysis_CurrentRatio { get; set; }

    //    public decimal BalanceSheetAnalysis_TotalEBITDA { get; set; }



    //    #endregion





    //    #region CapitalExpenditure
    //    public decimal CapitalExpenditure_CapitalExpenditure { get; set; }

    //    public decimal CapitalExpenditure_PercentageForecastCapitalExpenditure { get; set; }
    //    public decimal CapitalExpenditure_TTM { get; set; }
    //    #endregion



    //    public decimal SustainableEBITDA_Total { get; set; }
    //    #region Sustainable EBITDA
    //    /// <summary>
    //    ///  These are unsual once-off expenses not in the normal course of business that are not expected to be repeated e.g. office renovations, losses from sale of operating assets etc.
    //    /// </summary>
    //    public decimal SustainableEBITDA_NonRecurringExpenses { get; set; }
    //    /// <summary>
    //    /// This is unsual once-off income not in the normal course of business that is not expected to be repeated e.g. unsual and non-recurring sales (e.g. sale of a patent), profits from sale of operating assets etc.
    //    /// </summary>
    //    public decimal SustainableEBITDA_NonRecurringIncome { get; set; }

    //    public decimal SustainableEBITDA_TTM { get; set; }


    //    #endregion




    //    #region Excess Cash



    //    public decimal ExcessCash_RevenueTTM { get; set; }
    //    public decimal ExcessCash_Total { get; set; }
    //    public decimal ExcessCash_AssumedOperatingCashRequiredAsPercentageOfRevenue { get; set; }


    //    #endregion




    //}
}
