﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.FinancialYears;
namespace EquityMaven.Data.Calculations
{



    public class FinancialYearForecast1 : FinancialYearProjected, IFinancialYearForecast1
    {
        public FinancialYearForecast1(FinancialValuation fv, IFullValuationInputYear from) : base(fv, from)
        {
        }

        public void ApplyValues(IFullValuationInputYear fviy)
        {
            var to = fviy as FullValuationInputYear;
            if (to == null)
                throw new ArgumentNullException($"to {fviy}");
            ApplyProjectedValues(to);

        }
        protected override IFinancialYear GetPrior()
        {
            return Prior;
        }
        IFinancialYearBudget Prior { get { return FinancialValuation.iBudget; } }
        IFinancialYearForecast2 Next { get { return FinancialValuation.iForecast2; } }


        protected override decimal GetDiscountedCashFlow_DiscountPeriodYearEnd()
        {
            var fyBudget = Prior as IFinancialYearBudget;
            if (fyBudget == null)
                throw new NullReferenceException("GetDiscountedCashFlow_DiscountPeriodYearEnd");
            return fyBudget.DiscountedCashFlow_DiscountPeriodYearEnd + 1;

        }

        protected override decimal GetDiscountedCashFlow_DiscountPeriodMidYear()
        {
            return DiscountedCashFlow_DiscountPeriodYearEnd - (decimal)0.5;
        }

        #region Valuation

        #region Net Working Capital




        #endregion






        #region Projected Cash Flow
        public decimal DiscountedCashFlow_DiscountPeriodYear_End
        {
            get
            {
                var fyBudget = Prior as FinancialYearBudget;
                if (fyBudget == null)
                    throw new NullReferenceException($"DiscountedCashFlow_DiscountPeriodYear_End {FinancialYearType}");
                return fyBudget.DiscountedCashFlow_DiscountPeriodYear_End + 1;
            }
        }
        public decimal DiscountedCashFlow_DiscountPeriodMid_Year
        {
            get
            {
                return DiscountedCashFlow_DiscountPeriodYear_End - (decimal)0.5;
            }
        }
        public decimal DiscountedCashFlow_DiscountFactor
        {
            get
            {
                var result = Math.Pow((double)(1 + FinancialValuation.WaccCalculation_Wacc), (double)(DiscountedCashFlow_DiscountPeriodMid_Year));
                return (decimal)result;
            }
        } 

        public decimal DiscountedCashFlow_DiscountFactorInverse { get { return (decimal)1 / DiscountedCashFlow_DiscountFactor; } }

        public decimal DiscountedCashFlow_DiscountedFreeCashFlow { get { return DiscountedCashFlow_FreeCashFlow * DiscountedCashFlow_DiscountFactorInverse; } }



        #endregion



        #endregion

     

    }
}
