﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.FinancialYears;

namespace EquityMaven.Data.Calculations
{
    public abstract class FinancialYearActual : FinancialYear, IFinancialYearActual
    {
        public FinancialYearActual(FinancialValuation fv, IFullValuationInputYear from) : base(fv, from)
        {
            this.IncomeStatement_Revenue = from.IncomeStatement_Revenue;
            this.IncomeStatement_CostOfGoodsSold = from.IncomeStatement_CostOfGoodsSold;
            this.IncomeStatement_OperatingExpense = from.IncomeStatement_OperatingExpense;
            this.IncomeStatement_DepreciationAndAmortisationExpense = from.IncomeStatement_DepreciationAndAmortisationExpense;
            this.IncomeStatement_InterestExpense = from.IncomeStatement_InterestExpense;
            this.IncomeStatement_NonCashExpense = from.IncomeStatement_NonCashExpense;
            this.BalanceSheetTotalAssets_AccountsReceiveable = from.BalanceSheetTotalAssets_AccountsReceiveable;
            this.BalanceSheetTotalAssets_Inventory = from.BalanceSheetTotalAssets_Inventory;
            this.BalanceSheetTotalAssets_OtherCurrentAssets = from.BalanceSheetTotalAssets_OtherCurrentAssets;
            this.BalanceSheetTotalAssets_CashAndCashEquivalents = from.BalanceSheetTotalAssets_CashAndCashEquivalents;
            this.BalanceSheetTotalAssets_IntangibleAssets = from.BalanceSheetTotalAssets_IntangibleAssets;
            this.BalanceSheetTotalAssets_FixedAssets = from.BalanceSheetTotalAssets_FixedAssets;
            this.BalanceSheetEquityAndLiabilities_AccountsPayable = from.BalanceSheetEquityAndLiabilities_AccountsPayable;
            this.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = from.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities;
            this.BalanceSheetEquityAndLiabilities_ShortTermDebt = from.BalanceSheetEquityAndLiabilities_ShortTermDebt;
            this.BalanceSheetEquityAndLiabilities_LongTermDebt = from.BalanceSheetEquityAndLiabilities_LongTermDebt;
            this.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = from.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities;
            this.BalanceSheetEquityAndLiabilities_ShareholderLoans = from.BalanceSheetEquityAndLiabilities_ShareholderLoans;
             
            this.CapitalExpenditure_CapitalExpenditure = from.CapitalExpenditure_CapitalExpenditure;
        }

        protected void ApplyActualValues(FullValuationInputYear to)
        {

            to.IncomeStatement_EBIT = this.IncomeStatement_EBIT;
            to.IncomeStatement_EBITDA = this.IncomeStatement_EBITDA;
            to.IncomeStatement_EarningsBeforeTax = this.IncomeStatement_EarningsBeforeTax;
            to.IncomeStatement_GrossProfit = this.IncomeStatement_GrossProfit;
            to.AssetsProjection_FixedAndIntangibleAssets = this.AssetsProjection_FixedAndIntangibleAssets;
            
            to.BalanceSheetAnalysis_CurrentRatio = this.BalanceSheetAnalysis_CurrentRatio;
            to.BalanceSheetAnalysis_TotalDebtOverEBITDA = this.BalanceSheetAnalysis_TotalDebtOverEBITDA;
            to.BalanceSheetEquityAndLiabilities_LongTermDebt = this.BalanceSheetEquityAndLiabilities_LongTermDebt;
            to.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = this.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities;
            to.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = this.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities;
            to.BalanceSheetEquityAndLiabilities_ShareholderLoans = this.BalanceSheetEquityAndLiabilities_ShareholderLoans;
            to.BalanceSheetEquityAndLiabilities_ShortTermDebt = this.BalanceSheetEquityAndLiabilities_ShortTermDebt;
            //to.BalanceSheetEquityAndLiabilities_CurrentLiabilities = this.BalanceSheetEquityAndLiabilities_CurrentLiabilities;
            to.BalanceSheetTotalAssets_CurrentAssets = this.BalanceSheetTotalAssets_CurrentAssets;
            to.BalanceSheetTotalAssets_TotalAssets = this.BalanceSheetTotalAssets_TotalAssets;
           
            to.IncomeStatementAnalysis_Calculated_InterestCover = this.IncomeStatementAnalysis_InterestCover;
            to.IncomeStatementAnalysis_EBITDAMargin = this.IncomeStatementAnalysis_EBITDAMargin;
            to.IncomeStatementAnalysis_EBITMargin = this.IncomeStatementAnalysis_EBITMargin;
            to.BalanceSheetTotalAssets_FixedAndIntangibleAssets = this.BalanceSheetTotalAssets_FixedAndIntangibleAssets;
            ///second part of extension

            to.IncomeStatement_Revenue = this.IncomeStatement_Revenue;
            to.IncomeStatement_CostOfGoodsSold = this.IncomeStatement_CostOfGoodsSold;
            to.IncomeStatement_OperatingExpense = this.IncomeStatement_OperatingExpense;
            to.IncomeStatement_DepreciationAndAmortisationExpense = this.IncomeStatement_DepreciationAndAmortisationExpense;
            to.IncomeStatement_InterestExpense = this.IncomeStatement_InterestExpense;
            to.IncomeStatement_NonCashExpense = this.IncomeStatement_NonCashExpense;
            
            to.IncomeStatementAnalysis_PercentageGrossProfitMargin = this.IncomeStatementAnalysis_PercentageGrossProfitMargin;
            to.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = this.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue;
            to.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = this.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue;
            to.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = this.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue;
            to.BalanceSheetTotalAssets_AccountsReceiveable = this.BalanceSheetTotalAssets_AccountsReceiveable;
            to.BalanceSheetTotalAssets_Inventory = this.BalanceSheetTotalAssets_Inventory;
            to.BalanceSheetTotalAssets_OtherCurrentAssets = this.BalanceSheetTotalAssets_OtherCurrentAssets;
            to.BalanceSheetTotalAssets_CashAndCashEquivalents = this.BalanceSheetTotalAssets_CashAndCashEquivalents;
            to.BalanceSheetTotalAssets_IntangibleAssets = this.BalanceSheetTotalAssets_IntangibleAssets;
            to.BalanceSheetTotalAssets_FixedAssets = this.BalanceSheetTotalAssets_FixedAssets;
            to.BalanceSheetEquityAndLiabilities_AccountsPayable = this.BalanceSheetEquityAndLiabilities_AccountsPayable;
            to.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = this.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities;
            to.BalanceSheetEquityAndLiabilities_ShortTermDebt = this.BalanceSheetEquityAndLiabilities_ShortTermDebt;
            to.BalanceSheetEquityAndLiabilities_LongTermDebt = this.BalanceSheetEquityAndLiabilities_LongTermDebt;
            to.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = this.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities;
            to.BalanceSheetEquityAndLiabilities_ShareholderLoans = this.BalanceSheetEquityAndLiabilities_ShareholderLoans;
            to.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = this.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue;
            to.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = this.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold;
            to.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = this.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold;
            to.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = this.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold;
            to.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = this.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold;
            to.CapitalExpenditure_CapitalExpenditure = this.CapitalExpenditure_CapitalExpenditure;
            to.CapitalExpenditure_PercentageForecastCapitalExpenditure = this.CapitalExpenditure_PercentageForecastCapitalExpenditure;
            
        }

        //var to = fvyi as FullValuationInputYear;
        //    if ((to == null))
        //        throw new ArgumentNullException($"to {fvyi}");


        //to.IncomeStatement_EBIT = this.IncomeStatement_EBIT;
        //    to.IncomeStatement_EBITDA = this.IncomeStatement_EBITDA;
        //    to.IncomeStatement_EarningsBeforeTax = this.IncomeStatement_EarningsBeforeTax;
        //    to.IncomeStatement_GrossProfit = this.IncomeStatement_GrossProfit;



        //    to.AssetsProjection_FixedAndIntangibleAssets = this.AssetsProjection_FixedAndIntangibleAssets;
        //    to.BalanceSheetAnalysis_AverageAccountsPayableDays = 0;
        //    to.BalanceSheetAnalysis_AverageAccountsReceiveableDays = 0;
        //    to.BalanceSheetAnalysis_AverageInventoryDays = 0;
        //    to.BalanceSheetAnalysis_CurrentRatio = this.BalanceSheetAnalysis_CurrentRatio;
        //    to.BalanceSheetAnalysis_TotalDebtOverEBITDA = this.BalanceSheetAnalysis_TotalDebtOverEBITDA;
        //    to.BalanceSheetEquityAndLiabilities_LongTermDebt = this.BalanceSheetEquityAndLiabilities_LongTermDebt;
        //    to.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = this.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities;
        //    to.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = this.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities;
        //    to.BalanceSheetEquityAndLiabilities_ShareholderLoans = this.BalanceSheetEquityAndLiabilities_ShareholderLoans;
        //    to.BalanceSheetEquityAndLiabilities_ShortTermDebt = this.BalanceSheetEquityAndLiabilities_ShortTermDebt;
        //    //to.BalanceSheetEquityAndLiabilities_CurrentLiabilities = this.BalanceSheetEquityAndLiabilities_CurrentLiabilities;
        //    to.BalanceSheetTotalAssets_CurrentAssets = this.BalanceSheetTotalAssets_CurrentAssets;
        //    to.BalanceSheetTotalAssets_TotalAssets = this.BalanceSheetTotalAssets_TotalAssets;
            
 
            
        //    to.IncomeStatementAnalysis_Calculated_InterestCover = this.IncomeStatementAnalysis_InterestCover;
        //    to.IncomeStatementAnalysis_EBITDAMargin = this.IncomeStatementAnalysis_EBITDAMargin;
        //    to.IncomeStatementAnalysis_EBITMargin = this.IncomeStatementAnalysis_EBITMargin;

            
        //    to.BalanceSheetTotalAssets_FixedAndIntangibleAssets = this.BalanceSheetTotalAssets_FixedAndIntangibleAssets;
 
            





        //    ///second part of extension


             

        //    to.IncomeStatement_Revenue = this.IncomeStatement_Revenue;
        //    to.IncomeStatement_CostOfGoodsSold = this.IncomeStatement_CostOfGoodsSold;
        //    to.IncomeStatement_OperatingExpense = this.IncomeStatement_OperatingExpense;
        //    to.IncomeStatement_DepreciationAndAmortisationExpense = this.IncomeStatement_DepreciationAndAmortisationExpense;
        //    to.IncomeStatement_InterestExpense = this.IncomeStatement_InterestExpense;
        //    to.IncomeStatement_NonCashExpense = this.IncomeStatement_NonCashExpense;


 
        //    to.IncomeStatementAnalysis_PercentageGrossProfitMargin = this.IncomeStatementAnalysis_PercentageGrossProfitMargin;
        //    to.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = this.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue;
        //    to.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = this.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue;
        //    to.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = this.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue;




        //    to.BalanceSheetTotalAssets_AccountsReceiveable = this.BalanceSheetTotalAssets_AccountsReceiveable;
        //    to.BalanceSheetTotalAssets_Inventory = this.BalanceSheetTotalAssets_Inventory;
        //    to.BalanceSheetTotalAssets_OtherCurrentAssets = this.BalanceSheetTotalAssets_OtherCurrentAssets;
        //    to.BalanceSheetTotalAssets_CashAndCashEquivalents = this.BalanceSheetTotalAssets_CashAndCashEquivalents;
        //    to.BalanceSheetTotalAssets_IntangibleAssets = this.BalanceSheetTotalAssets_IntangibleAssets;
        //    to.BalanceSheetTotalAssets_FixedAssets = this.BalanceSheetTotalAssets_FixedAssets;


        //    to.BalanceSheetEquityAndLiabilities_AccountsPayable = this.BalanceSheetEquityAndLiabilities_AccountsPayable;
        //    to.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = this.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities;
        //    to.BalanceSheetEquityAndLiabilities_ShortTermDebt = this.BalanceSheetEquityAndLiabilities_ShortTermDebt;
        //    to.BalanceSheetEquityAndLiabilities_LongTermDebt = this.BalanceSheetEquityAndLiabilities_LongTermDebt;
        //    to.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = this.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities;
        //    to.BalanceSheetEquityAndLiabilities_ShareholderLoans = this.BalanceSheetEquityAndLiabilities_ShareholderLoans;



        //    to.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = this.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue;
        //    to.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = this.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold;
        //    to.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = this.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold;
        //    to.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = this.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold;
        //    to.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = this.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold;


        //    to.CapitalExpenditure_CapitalExpenditure = this.CapitalExpenditure_CapitalExpenditure;
        //    to.CapitalExpenditure_PercentageForecastCapitalExpenditure = this.CapitalExpenditure_PercentageForecastCapitalExpenditure;

        public override decimal GetIncomeStatement_Revenue()
        {
            return IncomeStatement_Revenue;
        }



        #region Client Inputs

        #region Income Statement

        public override decimal GetIncomeStatement_CostOfGoodsSold()
        {
            return IncomeStatement_CostOfGoodsSold;
        }
        public override decimal GetIncomeStatement_OperatingExpense()
        {
            return IncomeStatement_OperatingExpense;
        }

        public override decimal GetIncomeStatement_DepreciationAndAmortisationExpense()
        {
            return IncomeStatement_DepreciationAndAmortisationExpense;
        }


        public override decimal GetIncomeStatement_InterestExpense()
        {
            return IncomeStatement_InterestExpense;
        }


        public decimal IncomeStatement_Revenue { get; set; }


        /// <summary>
        /// Cost of goods sold
        /// </summary>
        public decimal IncomeStatement_CostOfGoodsSold { get; set; }




        /// <summary>
        /// Operating expenses(excluding depreciation & amortisation)
        /// </summary>
        public decimal IncomeStatement_OperatingExpense { get; set; }



        public override decimal GetIncomeStatement_GrossProfit()
        {
            return GetIncomeStatement_Revenue() - GetIncomeStatement_CostOfGoodsSold();
        }




        /// <summary>
        /// Depreciation and amortisation expense
        /// </summary>
        public decimal IncomeStatement_DepreciationAndAmortisationExpense { get; set; }



        public decimal IncomeStatement_InterestExpense { get; set; }

        /// <summary>
        /// Non-cash expenses included in Operating expenses above
        /// </summary>
        public decimal IncomeStatement_NonCashExpense { get; set; }


        public decimal IncomeStatementAnalysis_PercentageGrossProfitMargin
        {
            get
            {
                return (IncomeStatement_GrossProfit / IncomeStatement_Revenue);
            }
        }


        public decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue
        {
            get
            {
                return IncomeStatement_OperatingExpense / IncomeStatement_Revenue;
            }

        }

        /// <summary>
        ///         Depreciation and amortisation as % of Revenue
        /// </summary>
        public Decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue
        {
            get
            {
                return (IncomeStatement_DepreciationAndAmortisationExpense / IncomeStatement_Revenue);
            }
        }


        /// <summary>
        ///what percentage Non-cash expenses (included in Operating expenses) (excluding depreciation & amortisation) are expected to be as a percentage of Revenue. 
        /// </summary>
        public Decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue
        {
            get
            {
                return (IncomeStatement_NonCashExpense / IncomeStatement_Revenue);
            }
        }
        public Decimal IncomeStatementAnalysis_InterestCover { get { return GetIncomeStatement_InterestExpense() == 0 ? 0 : IncomeStatement_EBIT / GetIncomeStatement_InterestExpense(); } }


        #endregion


        #region Balance Sheet


        public override decimal GetBalanceSheetTotalAssets_AccountsReceiveable()
        {
            return BalanceSheetTotalAssets_AccountsReceiveable;
        }
        public override decimal GetBalanceSheetTotalAssets_Inventory()
        {
            return BalanceSheetTotalAssets_Inventory;
        }
        public override decimal GetBalanceSheetTotalAssets_OtherCurrentAssets()
        {
            return BalanceSheetTotalAssets_OtherCurrentAssets;
        }
        public override decimal GetBalanceSheetTotalAssets_CashAndCashEquivalents()
        {
            return BalanceSheetTotalAssets_CashAndCashEquivalents;
        }

        public override decimal GetBalanceSheetTotalAssets_IntangibleAssets()
        {
            return BalanceSheetTotalAssets_IntangibleAssets;
        }
        public override decimal GetBalanceSheetTotalAssets_FixedAssets()
        {
            return BalanceSheetTotalAssets_FixedAssets;
        }



        public override decimal GetBalanceSheetEquityAndLiabilities_CurrentLiabilities()
        {
            return BalanceSheetEquityAndLiabilities_CurrentLiabilities;
        }
        public override decimal GetBalanceSheetEquityAndLiabilities_LongTermDebt()
        {
            return BalanceSheetEquityAndLiabilities_LongTermDebt;
        }
        public override decimal GetBalanceSheetEquityAndLiabilities_OtherLongTermLiabilities()
        {
            return BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities;
        }
        public override decimal GetBalanceSheetEquityAndLiabilities_ShareholderLoans()
        {
            return BalanceSheetEquityAndLiabilities_ShareholderLoans;
        }

        public override decimal GetIncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue()
        {
            return IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue;
        }

        public override decimal GetBalanceSheetEquityAndLiabilities_AccountsPayable()
        {
            return BalanceSheetEquityAndLiabilities_AccountsPayable;
        }

        public override decimal GetBalanceSheetTotalAssets_FixedAndIntangibleAssets()
        {
            return BalanceSheetTotalAssets_FixedAndIntangibleAssets;
        }

 







        public decimal BalanceSheetTotalAssets_AccountsReceiveable { get; set; }


        public decimal BalanceSheetTotalAssets_Inventory { get; set; }


        public decimal BalanceSheetTotalAssets_OtherCurrentAssets { get; set; }


        public decimal BalanceSheetTotalAssets_CashAndCashEquivalents { get; set; }
        public decimal BalanceSheetTotalAssets_FixedAssets { get; set; }

        public decimal BalanceSheetTotalAssets_IntangibleAssets { get; set; }

        public decimal BalanceSheetTotalAssets_FixedAndIntangibleAssets { get { return BalanceSheetTotalAssets_IntangibleAssets + BalanceSheetTotalAssets_FixedAssets; } }





        //public decimal BalanceSheetEquityAndLiabilities_CurrentLiabilities { get; }
        public decimal BalanceSheetEquityAndLiabilities_CurrentLiabilities
        {
            get { return BalanceSheetEquityAndLiabilities_AccountsPayable + BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities + BalanceSheetEquityAndLiabilities_ShortTermDebt; }
        }

        public decimal BalanceSheetEquityAndLiabilities_AccountsPayable { get; set; }


        public decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities { get; set; }


        public decimal BalanceSheetEquityAndLiabilities_ShortTermDebt { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_LongTermDebt { get; set; }

        public decimal BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities { get; set; }


        public decimal BalanceSheetEquityAndLiabilities_ShareholderLoans { get; set; }




        public decimal BalanceSheetShareholdersEquity
        {
            get
            {
                return BalanceSheetTotalAssets_TotalAssets - BalanceSheetEquityAndLiabilities_TotalLiabilities;
            }
        }
        public decimal BalanceSheetTotalAssets_CurrentAssets { get { return GetBalanceSheetTotalAssets_AccountsReceiveable() + GetBalanceSheetTotalAssets_Inventory() + GetBalanceSheetTotalAssets_OtherCurrentAssets() + GetBalanceSheetTotalAssets_CashAndCashEquivalents(); } }




        public decimal BalanceSheetTotalAssets_TotalAssets { get { return BalanceSheetTotalAssets_CurrentAssets + GetBalanceSheetTotalAssets_IntangibleAssets() + GetBalanceSheetTotalAssets_FixedAssets(); } }

        #endregion


        #region BalanceSheetAnalysis
        public override decimal GetBalanceSheetTotalAssets_CurrentAssets()
        {
            return BalanceSheetTotalAssets_CurrentAssets;
        }
        public decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue { get { return BalanceSheetTotalAssets_AccountsReceiveable / IncomeStatement_Revenue; } }

        public decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold
        {
            get
            {
                return (BalanceSheetTotalAssets_Inventory / IncomeStatement_CostOfGoodsSold);
            }
        }

        public decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold
        {
            get
            {
                return (BalanceSheetTotalAssets_OtherCurrentAssets / IncomeStatement_CostOfGoodsSold);
            }
        }

        public decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold
        {
            get
            {
                return (BalanceSheetEquityAndLiabilities_AccountsPayable / IncomeStatement_CostOfGoodsSold);
            }

        }


        public decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold
        {
            get
            {
                return (BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities / IncomeStatement_CostOfGoodsSold);
            }
        }


        public decimal BalanceSheetAnalysis_CurrentRatio
        {
            get
            {
                return BalanceSheetEquityAndLiabilities_CurrentLiabilities != 0 ? BalanceSheetTotalAssets_CurrentAssets / BalanceSheetEquityAndLiabilities_CurrentLiabilities : 0;
            }

        }
        public decimal BalanceSheetAnalysis_TotalDebtOverEBITDA
        {
            get
            {
                return IncomeStatement_EBITDA != 0 ? (BalanceSheetEquityAndLiabilities_ShortTermDebt + BalanceSheetEquityAndLiabilities_LongTermDebt) / IncomeStatement_EBITDA : 0;
            }

        }


        #endregion



        #region CapitalExpenditure

        public decimal CapitalExpenditure_CapitalExpenditure { get; set; }

        public decimal CapitalExpenditure_PercentageForecastCapitalExpenditure
        {
            get
            {
                return (CapitalExpenditure_CapitalExpenditure / IncomeStatement_Revenue);
            }
        }
        #endregion

        #region Assets Projection 
        public decimal AssetsProjection_FixedAndIntangibleAssets
        {
            get
            {
                return BalanceSheetTotalAssets_FixedAssets + BalanceSheetTotalAssets_IntangibleAssets;
            }
        }

        #endregion


        #endregion



        #region Valuation

        #region Net Working Capital

       
        public decimal NetWorkingCapital_NetWorkingCapital { get { return NetWorkingCapital_CurrentAssets - BalanceSheetEquityAndLiabilities_CurrentLiabilities; } }

        #endregion

        #region Discounted Cash Flow

        

        #endregion




        #endregion
    }
}