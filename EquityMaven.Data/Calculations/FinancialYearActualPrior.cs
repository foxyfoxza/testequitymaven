﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.FinancialYears;
namespace EquityMaven.Data.Calculations
{



    public class FinancialYearActualPrior : FinancialYearActual, IFinancialYearActualPrior 
    {
        public FinancialYearActualPrior(FinancialValuation fv, IFullValuationInputYear main) : base(fv, main)
        {
            
        }

        public void ApplyValues(IFullValuationInputYear fvyi)
        {
            var to = fvyi as FullValuationInputYear;
            if ((to == null))
                throw new ArgumentNullException($"to {fvyi}");

            ApplyActualValues(to);
        }
 
        IFinancialYearActualCurrent Next { get { return FinancialValuation.iActualCurrent; } }

    }
}
