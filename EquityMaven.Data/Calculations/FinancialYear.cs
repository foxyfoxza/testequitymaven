﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.FinancialYears;
using EquityMaven.Interfaces.Calculations;

namespace EquityMaven.Data.Calculations
{
    public abstract class FinancialYear : IFinancialYear
    {

        public FinancialYear(FinancialValuation fv, IFullValuationInputYear main)
        {

            if (main != null)
            {
                FinancialYearType = (FinancialYearType)main.FinancialYearTypeValue;
            }
            this.FinancialValuation = fv;

        }
 
        public static int GetMonthsSinceLastFinancialYearEnd(DateTime lastFinancialYearEnd, DateTime reportDate)
        {
            var lastMonthEnd = new DateTime(reportDate.Year, reportDate.Month, 1).AddDays(-1);
            var daysSinceLastFinYearEnd = (decimal)(lastMonthEnd.Subtract(lastFinancialYearEnd).TotalDays);
            var result = (int)Math.Round((decimal)(daysSinceLastFinYearEnd / (decimal)365.0 * (decimal)12.0), 0);
            return result;
        }

        public FinancialYearType FinancialYearType { get; set; }

        #region Client Inputs
        public IFinancialValuation FinancialValuation { get; set; }


        #region Income Statement

        public abstract decimal GetIncomeStatement_Revenue();
        public abstract decimal GetIncomeStatement_CostOfGoodsSold();

        public abstract decimal GetIncomeStatement_OperatingExpense();

        public abstract decimal GetIncomeStatement_DepreciationAndAmortisationExpense();

        public abstract decimal GetIncomeStatement_InterestExpense();

        public abstract decimal GetIncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue();

        public abstract decimal GetBalanceSheetEquityAndLiabilities_AccountsPayable();
        public abstract decimal GetBalanceSheetTotalAssets_FixedAndIntangibleAssets();

        public abstract decimal GetIncomeStatement_GrossProfit();

        public decimal IncomeStatement_GrossProfit
        {
            get
            {
                return GetIncomeStatement_GrossProfit();
            }
        }


        int ixIncomeStatement_EBITDA = 0;
        public decimal IncomeStatement_EBITDA
        {
            get
            {
                    return IncomeStatement_GrossProfit - GetIncomeStatement_OperatingExpense();
            }
        }


        public decimal IncomeStatement_EBIT { get { return IncomeStatement_EBITDA - GetIncomeStatement_DepreciationAndAmortisationExpense(); } }
        public decimal IncomeStatement_EarningsBeforeTax { get { return IncomeStatement_EBIT - GetIncomeStatement_InterestExpense(); } }

        public Decimal IncomeStatementAnalysis_EBITDAMargin { get { return IncomeStatement_EBITDA / GetIncomeStatement_Revenue(); } }

        public Decimal IncomeStatementAnalysis_EBITMargin { get { return IncomeStatement_EBIT / GetIncomeStatement_Revenue(); } }


        #endregion


        #region Balance Sheet

        public abstract decimal GetBalanceSheetTotalAssets_AccountsReceiveable();
        public abstract decimal GetBalanceSheetTotalAssets_Inventory();
        public abstract decimal GetBalanceSheetTotalAssets_OtherCurrentAssets();
        public abstract decimal GetBalanceSheetTotalAssets_CashAndCashEquivalents();

        public abstract decimal GetBalanceSheetTotalAssets_IntangibleAssets();
        public abstract decimal GetBalanceSheetTotalAssets_FixedAssets();

        public abstract decimal GetBalanceSheetEquityAndLiabilities_CurrentLiabilities();
        public abstract decimal GetBalanceSheetEquityAndLiabilities_LongTermDebt();
        public abstract decimal GetBalanceSheetEquityAndLiabilities_OtherLongTermLiabilities();
        public abstract decimal GetBalanceSheetEquityAndLiabilities_ShareholderLoans();
        public abstract decimal GetBalanceSheetTotalAssets_CurrentAssets();
         

        public decimal BalanceSheetTotalAssets_TotalAssets { get { return GetBalanceSheetTotalAssets_CurrentAssets() + GetBalanceSheetTotalAssets_IntangibleAssets() + GetBalanceSheetTotalAssets_FixedAssets(); } }

        #endregion

        #region  Balance Sheet Equity And Liabilities
        public decimal BalanceSheetEquityAndLiabilities_TotalLiabilities
        {
            get
            {
                return GetBalanceSheetEquityAndLiabilities_CurrentLiabilities() + GetBalanceSheetEquityAndLiabilities_LongTermDebt() + GetBalanceSheetEquityAndLiabilities_OtherLongTermLiabilities() + GetBalanceSheetEquityAndLiabilities_ShareholderLoans();
            }
        }





        #endregion



        #endregion

        #region NWC

        public decimal NetWorkingCapital_CurrentAssets { get { return GetBalanceSheetTotalAssets_AccountsReceiveable() + GetBalanceSheetTotalAssets_Inventory() + GetBalanceSheetTotalAssets_OtherCurrentAssets(); } }

        #endregion

    }
}
