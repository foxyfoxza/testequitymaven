﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.FinancialYears;
using EquityMaven.Interfaces.Calculations;

namespace EquityMaven.Data.Calculations
{



    public class FinancialYearTerminal : IFinancialYearTerminal
    {
  
        public FinancialYearTerminal(FinancialValuation fv)  
        {
            FinancialValuation = fv;
        }

      
        FinancialYearType FinancialYearType = FinancialYearType.Terminal;
        public IFinancialValuation FinancialValuation { get; set; }
        public IFinancialYearForecast2 Prior { get { return FinancialValuation.iForecast2; } }

        
        #region Valuation

        #region Net Working Capital

        public decimal NetWorkingCapital_ChangeInNetWorkingCapital
        {
            get
            {
                if (Prior == null)
                    throw new NullReferenceException("NetWorkingCapital_ChangeInNetWorkingCapital");
                return (Prior.NetWorkingCapital_ChangeInNetWorkingCapital* (1+FinancialValuation.WaccInputsSummary_TerminalGrowthRate));
            }
        }

        #endregion



        #region Discounted Cash Flow

        public decimal DiscountedCashFlow_IncomeStatement_EBIT { get { return Prior.DiscountedCashFlow_IncomeStatement_EBIT * (1 + FinancialValuation.WaccInputsSummary_TerminalGrowthRate); } }
        public decimal DiscountedCashFlow_IncomeStatement_Tax { get { return -1 * FinancialValuation.WaccInputsSummary_TaxRate * DiscountedCashFlow_IncomeStatement_EBIT; } }

        public decimal DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense { get { return Prior.DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense * (1 + FinancialValuation.WaccInputsSummary_TerminalGrowthRate); } } 
        public decimal DiscountedCashFlow_IncomeStatement_NonCashExpense { get { return Prior.DiscountedCashFlow_IncomeStatement_NonCashExpense * ((decimal)1 + FinancialValuation.WaccInputsSummary_TerminalGrowthRate); } }

        //= -I58 - ('CLIENT INPUTS'!$H$128*WaccInputsSummary_TerminalGrowthRate)
        public decimal DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure { get { return ((decimal)-1 * DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense - (Prior.AssetsProjection_FixedAndIntangibleAssets * FinancialValuation.WaccInputsSummary_TerminalGrowthRate)); } }
        public decimal DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital { get { return Prior.DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital * (1+ FinancialValuation.WaccInputsSummary_TerminalGrowthRate); } }
        public decimal DiscountedCashFlow_FreeCashFlow
        {
            get
            {
                return DiscountedCashFlow_IncomeStatement_EBIT + DiscountedCashFlow_IncomeStatement_Tax + DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense + DiscountedCashFlow_IncomeStatement_NonCashExpense + DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure + DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital;
            }
        }


        protected virtual decimal GetDiscountedCashFlow_AdjustedFreeCashFlow()
        {
            return DiscountedCashFlow_FreeCashFlow;
        }

        public void ApplyValues(IFullValuationInputYear fyi)
        {
           
        }

        public decimal DiscountedCashFlow_AdjustedFreeCashFlow
        {
            get
            {
                return GetDiscountedCashFlow_AdjustedFreeCashFlow();
            }
        }

 
        public decimal DiscountedCashFlow_DiscountPeriodYearEnd
        {
            get
            {
                return Prior.DiscountedCashFlow_DiscountPeriodYearEnd;
            }
        }

        public decimal DiscountedCashFlow_DiscountPeriodMidYear
        {
            get
            {
                return Prior.DiscountedCashFlow_DiscountPeriodMidYear;
            }
        }

        public decimal DiscountedCashFlow_DiscountFactor
        {
            get
            {
                return (decimal)Math.Pow(1 + (double)FinancialValuation.WaccCalculation_Wacc, (double)DiscountedCashFlow_DiscountPeriodMidYear);
            }
        }
        public decimal DiscountedCashFlow_DiscountFactorInverse
        {
            get
            {
                return 1 / DiscountedCashFlow_DiscountFactor;
            }
        }


        public decimal DiscountedCashFlow_DiscountedFreeCashFlow { get { return DiscountedCashFlow_TerminalFreeCashFlow * DiscountedCashFlow_DiscountFactorInverse; } }


        public decimal DiscountedCashFlow_TerminalFreeCashFlowMultiple
        {
            get
            {
              
                return 1 / (FinancialValuation.WaccCalculation_Wacc - FinancialValuation.Risk_FreeRate_Risk_FreeRate);
            }
        }

        public decimal DiscountedCashFlow_TerminalFreeCashFlow
        {
            get
            {
                return DiscountedCashFlow_AdjustedFreeCashFlow * DiscountedCashFlow_TerminalFreeCashFlowMultiple;
            }
        }



        #endregion




        #endregion


        public decimal DiscountedCashFlowTerminalMultiple_Ebitda { get { return Prior.IncomeStatement_EBITDA * ((decimal)1 + FinancialValuation.Risk_FreeRate_Risk_FreeRate); } }
        public decimal DiscountedCashFlowTerminalMultiple_Ev_Over_Ebitda { get { return FinancialValuation.WorldMedian_World; } }
        public decimal DiscountedCashFlowTerminalMultiple_AdjustedFreeCashFlow { get { return DiscountedCashFlowTerminalMultiple_Ebitda * DiscountedCashFlowTerminalMultiple_Ev_Over_Ebitda; } }
        public decimal DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year { get { return Prior.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year + (decimal)0.5; } }
        public decimal DiscountedCashFlowTerminalMultiple_DiscountFactor { get {return (decimal)Math.Pow((double)((decimal)1 + FinancialValuation.WaccCalculation_Wacc), (double)DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year); } }
        public decimal DiscountedCashFlowTerminalMultiple_DiscountFactorInverse { get { return (decimal)1 / DiscountedCashFlowTerminalMultiple_DiscountFactor; } }
        public decimal DiscountedCashFlowTerminalMultiple_FreeCashFlow { get { return DiscountedCashFlowTerminalMultiple_AdjustedFreeCashFlow; } }
        public decimal DiscountedCashFlowTerminalMultiple_DiscountedFreeCashFlow { get { return DiscountedCashFlowTerminalMultiple_FreeCashFlow * DiscountedCashFlowTerminalMultiple_DiscountFactorInverse; }  }

    }
}
