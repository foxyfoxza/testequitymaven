﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.FinancialYears;
using EquityMaven.Interfaces.Calculations;

namespace EquityMaven.Data.Calculations
{
    public class FinancialValuation : IFinancialValuation
    {
        public static int GetMonthsSinceLastFinancialYearEnd(DateTime lastFinancialYearEnd, DateTime reportDate)
        {
            var _reportDateDay = new DateTime(reportDate.Year, reportDate.Month, reportDate.Day);
            var lastMonthEnd = new DateTime(reportDate.Year, reportDate.Month, 1).AddMonths(1).AddDays(-1) == _reportDateDay ? _reportDateDay : new DateTime(reportDate.Year, reportDate.Month, 1).AddDays(-1);
            var daysSinceLastFinYearEnd = (decimal)(lastMonthEnd.Subtract(lastFinancialYearEnd).TotalDays);
            var result = (int)Math.Round((decimal)(daysSinceLastFinYearEnd / (decimal)365.0 * (decimal)12.0), 0);
            return result;
        }

        public FinancialValuation(IFullValuationInput fullValuationInput, List<FullValuationInputYear> financialYears, IEnumerable<ThompsonReutersBusinessClassification> thompsonReutersBusinessClassifications, IEnumerable<TRBCValue> trbcValues, IEnumerable<CurrencyRiskFreeRate> currencyRiskFreeRates, IEnumerable<Currency> currencies, IEnumerable<CountryTaxRate> countryTaxRates, GlobalDefaults globalDefaults, IEnumerable<MoodysRating> moodysRatings, IEnumerable<CorporateBondSpread> corporateBondSpreads, IEnumerable<Country> countries, IEnumerable<CountryMoodysRating> countryMoodysRatings)
        {

            this.ClientId = fullValuationInput.ClientId;
            this.ISOCurrencySymbol = fullValuationInput.ISOCurrencySymbol;
            this.ThreeLetterISORegionName = fullValuationInput.ThreeLetterISORegionName;
            this.TRBC2012HierarchicalID = fullValuationInput.TRBC2012HierarchicalID;
            this.LastFinancialYearEnd = fullValuationInput.LastFinancialYearEnd;
            this.ValuationDate = fullValuationInput.ValuationDate;
            this.DateOfCommencement = fullValuationInput.DateOfCommencement;
            this.Description = fullValuationInput.Description;
            this.CompanyName = fullValuationInput.CompanyName;
            this.All3rdPartyInterestBearingDebtAsAtLastMonth_End = fullValuationInput.All3rdPartyInterestBearingDebtAsAtLastMonth_End;
            this.NumberOfEmployees = fullValuationInput.NumberOfEmployees;

            TRBC = thompsonReutersBusinessClassifications.FirstOrDefault(o => o.TRBC2012HierarchicalID == fullValuationInput.TRBC2012HierarchicalID);
            TRBCValue = trbcValues.FirstOrDefault(o => (o.ThompsonReutersBusinessClassification != null && o.ThompsonReutersBusinessClassification == TRBC) || (o.ThompsonReutersBusinessClassification != null && o.ThompsonReutersBusinessClassificationId == TRBC.Id));


            Currency = currencies.FirstOrDefault(o => o.ISOCurrencySymbol == fullValuationInput.ISOCurrencySymbol);
            CurrencyRiskFreeRate = currencyRiskFreeRates.FirstOrDefault(o => (o.Currency != null && o.Currency == Currency) || (o.Currency == null && o.CurrencyId == Currency.Id));

            var country = countries.FirstOrDefault(o => o.ThreeLetterISORegionName == fullValuationInput.ThreeLetterISORegionName);

            CountryTaxRate = countryTaxRates.FirstOrDefault(o => (o.Country != null && o.Country == country) || (o.Country == null && o.CountryId == country.Id));
            CorporateBondSpreads = corporateBondSpreads;
            GlobalDefaults = globalDefaults;
            var countryMoodysRating = countryMoodysRatings.FirstOrDefault(o => (o.Country != null && o.Country == country) || (o.Country == null && o.CountryId == country.Id));
            MoodysRating = moodysRatings.FirstOrDefault(o => o.Rating == countryMoodysRating.Rating);

            FullValuationInput = fullValuationInput;
            StageOfDevelopment = fullValuationInput.StageOfDevelopment;

            var _ActualPrior = new FinancialYearActualPrior(this, financialYears.FirstOrDefault(o => o.FinancialYearType == FinancialYearType.ActualPrior));
            var _ActualCurrent = new FinancialYearActualCurrent(this, financialYears.FirstOrDefault(o => o.FinancialYearType == FinancialYearType.ActualCurrent));
            var _Budget = new FinancialYearBudget(this, financialYears.FirstOrDefault(o => o.FinancialYearType == FinancialYearType.Budget));
            var _Forecast1 = new FinancialYearForecast1(this, financialYears.FirstOrDefault(o => o.FinancialYearType == FinancialYearType.Forecast1));
            var _Forecast2 = new FinancialYearForecast2(this, financialYears.FirstOrDefault(o => o.FinancialYearType == FinancialYearType.Forecast2));
            var _Terminal = new FinancialYearTerminal(this);


            iActualPrior = _ActualPrior;
            iActualCurrent = _ActualCurrent;
            iBudget = _Budget;
            iForecast1 = _Forecast1;
            iForecast2 = _Forecast2;
            iTerminal = _Terminal;

            ////_ActualPrior
            //_ActualCurrent.Prior = _ActualPrior;
            //_Budget.PriorFY = iActualCurrent;
            //_Forecast1.PriorFY = iBudget;
            //_Forecast2.PriorFY = iForecast1;
            //_Terminal.Prior = iForecast2;


        }

        public void Calculate(IFullValuationInput fullValuationInput)
        {
            this.ClientId = fullValuationInput.ClientId;
            this.ISOCurrencySymbol = fullValuationInput.ISOCurrencySymbol;
            this.ThreeLetterISORegionName = fullValuationInput.ThreeLetterISORegionName;
            this.TRBC2012HierarchicalID = fullValuationInput.TRBC2012HierarchicalID;
            this.LastFinancialYearEnd = fullValuationInput.LastFinancialYearEnd;
            this.ValuationDate = fullValuationInput.ValuationDate;
            this.DateOfCommencement = fullValuationInput.DateOfCommencement;
            this.Description = fullValuationInput.Description;
            this.CompanyName = fullValuationInput.CompanyName;
            this.All3rdPartyInterestBearingDebtAsAtLastMonth_End = fullValuationInput.All3rdPartyInterestBearingDebtAsAtLastMonth_End;

            var _ActualPrior = fullValuationInput.FinancialYears.FirstOrDefault(o => o.FinancialYearTypeValue == (int)FinancialYearType.ActualPrior);
            var _ActualCurrent = fullValuationInput.FinancialYears.FirstOrDefault(o => o.FinancialYearTypeValue == (int)FinancialYearType.ActualCurrent);
            var _Budget = fullValuationInput.FinancialYears.FirstOrDefault(o => o.FinancialYearTypeValue == (int)FinancialYearType.Budget);
            var _Forecast1 = fullValuationInput.FinancialYears.FirstOrDefault(o => o.FinancialYearTypeValue == (int)FinancialYearType.Forecast1);
            var _Forecast2 = fullValuationInput.FinancialYears.FirstOrDefault(o => o.FinancialYearTypeValue == (int)FinancialYearType.Forecast2);
            var _Terminal = fullValuationInput.FinancialYears.FirstOrDefault(o => o.FinancialYearTypeValue == (int)FinancialYearType.Terminal);
            if (_Terminal == null)//add this if not already added
            {
                _Terminal = new FullValuationInputYear(FinancialYearType.Terminal);
                fullValuationInput.FinancialYears.Add(_Terminal);
            }
            try
            {

                this.iActualPrior.ApplyValues(_ActualPrior);
                this.iActualCurrent.ApplyValues(_ActualCurrent);
                this.iBudget.ApplyValues(_Budget);
                this.iForecast1.ApplyValues(_Forecast1);
                this.iForecast2.ApplyValues(_Forecast2);
                this.iTerminal.ApplyValues(_Terminal);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.InnerException != null)
                    Console.WriteLine(ex.InnerException.Message);
                throw ex;
            }
        }

        public void Apply(FullValuationReport fvr)
        {

            fvr.ActualPrior = fvr.ActualPrior ?? new FullValuationYearReportActualPrior();
            fvr.ActualCurrent = fvr.ActualCurrent ?? new FullValuationYearReportActualCurrent();
            fvr.Budget = fvr.Budget ?? new FullValuationYearReportBudget();
            fvr.Forecast1 = fvr.Forecast1 ?? new FullValuationYearReportForecast1();
            fvr.Forecast2 = fvr.Forecast2 ?? new FullValuationYearReportForecast2();
            fvr.Terminal = fvr.Terminal ?? new FullValuationYearReportTerminal();
            fvr.NumberOfEmployees = this.NumberOfEmployees;

            var _ActualPrior = fvr.ActualPrior;
            var _ActualCurrent = fvr.ActualCurrent;
            var _Budget = fvr.Budget;
            var _Forecast1 = fvr.Forecast1;
            var _Forecast2 = fvr.Forecast2;
            var _Terminal = fvr.Terminal;



            try
            {
                fvr.CopyFrom(this);
                _ActualPrior.CopyFrom(iActualPrior);
                _ActualCurrent.CopyFrom(iActualCurrent);
                _Budget.CopyFrom(iBudget);
                _Forecast1.CopyFrom(iForecast1);
                _Forecast2.CopyFrom(iForecast2);
                _Terminal.CopyFrom(iTerminal);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.InnerException != null)
                    Console.WriteLine(ex.InnerException.Message);
                throw ex;
            }

        }

        private ThompsonReutersBusinessClassification TRBC { get; set; }
        private TRBCValue TRBCValue { get; set; }
        //public Country Country { get; set; }
        private CurrencyRiskFreeRate CurrencyRiskFreeRate { get; set; }
        private int CurrencyId { get; set; }
        private Currency Currency { get; set; }
        private CountryTaxRate CountryTaxRate { get; set; }
        private GlobalDefaults GlobalDefaults { get; set; }
        private MoodysRating MoodysRating { get; set; }
        private IEnumerable<CorporateBondSpread> CorporateBondSpreads { get; set; }
        private IFullValuationInput FullValuationInput { get; set; }

        public IFinancialYearActualPrior iActualPrior { get; protected set; }
        public IFinancialYearActualCurrent iActualCurrent { get; protected set; }
        public IFinancialYearBudget iBudget { get; protected set; }
        public IFinancialYearForecast1 iForecast1 { get; protected set; }
        public IFinancialYearForecast2 iForecast2 { get; protected set; }
        public IFinancialYearTerminal iTerminal { get; protected set; }

        public int NumberOfEmployees { get; set; }
        public StageOfDevelopment StageOfDevelopment { get; set; }

        public int YearsInOperation { get { return (int)Math.Round((decimal)ReportDate.Subtract(DateOfCommencement).TotalDays / (decimal)365, 0); } }
        public decimal ProbabilityOfFailure { get { return GlobalDefaults.ProbabilityOfFailure(YearsInOperation); } }
        public decimal ProbabilityOfSurvival { get { return GlobalDefaults.ProbabilityOfSurvival(YearsInOperation); } }



        public decimal VCDiscountRate
        {
            get
            {
                switch (StageOfDevelopment)
                {
                    case StageOfDevelopment.Startup:
                        return GlobalDefaults.VcDiscountRateStartUp;
                    case StageOfDevelopment.FirstStage_EarlyDevelopment:
                        return GlobalDefaults.VcDiscountRateFirstStageOrEarlyDevelopment;
                    case StageOfDevelopment.SecondStage_Expansion:
                        return GlobalDefaults.VcDiscountRateSecondStageOrExpansion;
                    case StageOfDevelopment.BridgeOrIPO:
                        return GlobalDefaults.VcDiscountRateBridgeOrIpo;
                    default:
                        throw new ArgumentOutOfRangeException($"VCDiscountRate: {StageOfDevelopment}");
                }
                ;
            }
        }
        public Decimal IlliquidityDiscount { get { return GlobalDefaults.IlliquidityDiscount; } }
        public int MonthsSinceLastFinancialYearEnd { get { return FullValuationInput.MonthsSinceLastFinancialYearEnd; } }
        public decimal CurrentCashOnHand { get { return FullValuationInput.CurrentCashOnHand; } }

        public decimal PercentageOperatingCashRequired { get { return GlobalDefaults.PercentageOperatingCashRequired; } }

        public decimal DiscountedCashFlow_DiscretePeriod { get { return iBudget.DiscountedCashFlow_DiscountedFreeCashFlow + iForecast1.DiscountedCashFlow_DiscountedFreeCashFlow + iForecast2.DiscountedCashFlow_DiscountedFreeCashFlow; } }

        public decimal DiscountedCashFlow_TerminalPeriod
        {
            get
            {
                if (iTerminal == null)
                    throw new ArgumentNullException("DiscountedCashFlow_TerminalPeriod");
                return iTerminal.DiscountedCashFlow_DiscountedFreeCashFlow;
            }
        }


        public decimal DiscountedCashFlow_EnterpriseValuePre_IlliquidityDiscount { get { return DiscountedCashFlow_DiscretePeriod + DiscountedCashFlow_TerminalPeriod; } }
        public decimal DiscountedCashFlow_ExcessCash
        {
            get
            {
                if (iActualCurrent == null)
                    throw new ArgumentNullException("DiscountedCashFlow_ExcessCash");
                return iActualCurrent.ExcessCash_ExcessCash;
            }
        }

        public decimal DiscountedCashFlow_InterestBearingDebt
        {
            get
            {
                if (FullValuationInput == null)
                    throw new ArgumentNullException("DiscountedCashFlow_InterestBearingDebt");
                return FullValuationInput.All3rdPartyInterestBearingDebtAsAtLastMonth_End;
            }
        }
        public decimal DiscountedCashFlow_EquityValuePre_IlliquidityDiscount { get { return DiscountedCashFlow_EnterpriseValuePre_IlliquidityDiscount + DiscountedCashFlow_ExcessCash + DiscountedCashFlow_InterestBearingDebt; } }
        public decimal DiscountedCashFlow_IlliquidityDiscount { get { return DiscountedCashFlow_EquityValuePre_IlliquidityDiscount * GlobalDefaults.IlliquidityDiscount; } }
        public decimal DiscountedCashFlow_EquityValuePost_IlliquidityDiscount { get { return DiscountedCashFlow_EquityValuePre_IlliquidityDiscount + DiscountedCashFlow_IlliquidityDiscount; } }



        #region EV/EBITDA VALUATION

        public decimal EvOverEbitdaValuation_SustainableEbitdaTtm
        {
            get
            {
                if (iActualCurrent == null)
                    throw new ArgumentNullException("EvOverEbitdaValuation_SustainableEbitdaTtm");
                return iActualCurrent.SustainableEBITDA_SustainableEBITDA_TTM > 0 ? iActualCurrent.SustainableEBITDA_SustainableEBITDA_TTM : 0;
            }
        }

        public decimal EvOverEbitdaValuation_EnterpriseValuePre_IlliquidityDiscount { get { return EvOverEbitdaValuation_SustainableEbitdaTtm * WorldMedian_World; } }
        public decimal EvOverEbitdaValuation_ExcessCash
        {
            get
            {
                if (iActualCurrent == null)
                    throw new ArgumentNullException("EvOverEbitdaValuation_SustainableEbitdaTtm");
                return iActualCurrent.ExcessCash_ExcessCash;
            }
        }

        public decimal EvOverEbitdaValuation_InterestBearingDebt
        {
            get
            {
                if (FullValuationInput == null)
                    throw new ArgumentNullException("EvOverEbitdaValuation_InterestBearingDebt");
                return FullValuationInput.All3rdPartyInterestBearingDebtAsAtLastMonth_End;
            }
        }
        public decimal EvOverEbitdaValuation_EquityValuePre_IlliquidityDiscount { get { return EvOverEbitdaValuation_EnterpriseValuePre_IlliquidityDiscount + EvOverEbitdaValuation_ExcessCash + EvOverEbitdaValuation_InterestBearingDebt; } }
        public decimal EvOverEbitdaValuation_IlliquidityDiscount { get { return IlliquidityDiscount * EvOverEbitdaValuation_EquityValuePre_IlliquidityDiscount * (decimal)-1; } }
        public decimal EvOverEbitdaValuation_EquityValuePost_IlliquidityDiscount { get { return EvOverEbitdaValuation_IlliquidityDiscount + EvOverEbitdaValuation_EquityValuePre_IlliquidityDiscount; } }


        #endregion


        #region EV/EBITDA - World Median


        public decimal WorldMedian_World
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.World.HasValue)
                    throw new ArgumentNullException("WorldMedian_World");
                return TRBCValue.World.Value;
            }
        }


        public decimal WorldMedian_Africa
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.World.HasValue)
                    throw new ArgumentNullException("WorldMedian_Africa");
                return TRBCValue.Africa.Value;
            }
        }
        public decimal WorldMedian_NorthAmerica
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.World.HasValue)
                    throw new ArgumentNullException("WorldMedian_NorthAmerica");
                return TRBCValue.NorthAmerica.Value;
            }
        }
        public decimal WorldMedian_SouthCentralAmerica
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.World.HasValue)
                    throw new ArgumentNullException("WorldMedian_SouthCentralAmerica");
                return TRBCValue.SouthCentralAmerica.Value;
            }
        }
        public decimal WorldMedian_Asia
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.World.HasValue)
                    throw new ArgumentNullException("WorldMedian_Asia");
                return TRBCValue.Asia.Value;
            }
        }
        public decimal WorldMedian_AustraliaNewZealand
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.World.HasValue)
                    throw new ArgumentNullException("WorldMedian_AustraliaNewZealand");
                return TRBCValue.AustraliaNewZealand.Value;
            }
        }
        public decimal WorldMedian_EasternEuropeRussia
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.World.HasValue)
                    throw new ArgumentNullException("WorldMedian_EasternEuropeRussia");
                return TRBCValue.EasternEuropeRussia.Value;
            }
        }
        public decimal WorldMedian_SouthernEurope
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.World.HasValue)
                    throw new ArgumentNullException("WorldMedian_SouthernEurope");
                return TRBCValue.SouthernEurope.Value;
            }
        }
        public decimal WorldMedian_WesternEurope
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.World.HasValue)
                    throw new ArgumentNullException("WorldMedian_WesternEurope");
                return TRBCValue.WesternEurope.Value;
            }
        }
        public decimal WorldMedian_MiddleEast
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.World.HasValue)
                    throw new ArgumentNullException("WorldMedian_MiddleEast");
                return TRBCValue.MiddleEast.Value;
            }
        }



        #endregion

        #region Industry TRBC Values

        public decimal Industry_EBITMargin
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.EBITMargin.HasValue)
                    throw new ArgumentNullException("EBITMargin");
                return TRBCValue.EBITMargin.Value;
            }
        }
        public decimal Industry_EBITDAMargin
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.EBITDAMargin.HasValue)
                    throw new ArgumentNullException("EBITDAMargin");
                return TRBCValue.EBITDAMargin.Value;
            }
        }
        public decimal Industry_GrossProfitMargin
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.GrossMargin.HasValue)
                    throw new ArgumentNullException("GrossMargin");
                return TRBCValue.GrossMargin.Value;
            }
        }
        public decimal Industry_CurrentRatio
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.CurrentRatio.HasValue)
                    throw new ArgumentNullException("CurrentRatio");
                return TRBCValue.CurrentRatio.Value;
            }
        }
        public decimal Industry_InterestCover
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.InterestCover.HasValue)
                    throw new ArgumentNullException("InterestCover");
                return TRBCValue.InterestCover.Value;
            }
        }
        public decimal Industry_TotalDebt_Over_Ebitda
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.TotalDebtEBITDA.HasValue)
                    throw new ArgumentNullException("TotalDebtEBITDA");
                return TRBCValue.TotalDebtEBITDA.Value;
            }
        }
        public decimal Industry_AccountsPayableDaysAverage
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.AccPayableDaysAve.HasValue)
                    throw new ArgumentNullException("AccPayableDaysAve");
                return TRBCValue.AccPayableDaysAve.Value;
            }
        }
        public decimal Industry_InventoryDaysAverage
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.InventoryDaysAve.HasValue)
                    throw new ArgumentNullException("InventoryDaysAve");
                return TRBCValue.InventoryDaysAve.Value;
            }
        }
        public decimal Industry_AccountsReceiveableDaysAverage
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.AccRecDaysAve.HasValue)
                    throw new ArgumentNullException("AccRecDaysAve");
                return TRBCValue.AccRecDaysAve.Value;
            }
        }


        #endregion

        #region WACC


        #region WACC INPUTS SUMMARY

        public decimal WaccInputsSummary_Risk_FreeRate { get { return Risk_FreeRate_Risk_FreeRate; } }
        public decimal WaccInputsSummary_EquityRiskPremium { get { return EquityRiskPremium_EquityRiskPremium; } }
        public decimal WaccInputsSummary_CompanyRiskPremium { get { return EquityRiskPremium_EquityRiskPremium * Beta_LeveredBeta; } }
        public decimal WaccInputsSummary_CostOfDebtPre_Tax { get { return CostOfDebt_EstimatedCostOfDebt; } }
        public decimal WaccInputsSummary_LeveredBeta { get { return Beta_LeveredBeta; } }
        public decimal WaccInputsSummary_TotalDebt_Over_EquityD_Over_E { get { return Beta_TotalDebtToEquityMarketCap; } }
        public decimal WaccInputsSummary_GearingD_Over_D_Plus_E { get { return (1 - 1 / (1 + Beta_TotalDebtToEquityMarketCap)); } }
        public decimal WaccInputsSummary_TaxRate { get { return Beta_MarginalTaxRate; } }
        public decimal WaccInputsSummary_TerminalGrowthRate { get { return Risk_FreeRate_Risk_FreeRate; } }

        #endregion

        #region WACC CALCULATION
        public decimal WaccCalculation_Ke { get { return WaccInputsSummary_Risk_FreeRate + (WaccInputsSummary_LeveredBeta * WaccInputsSummary_EquityRiskPremium); } }
        public decimal WaccCalculation_E_Over_D_Plus_E { get { return 1 - WaccInputsSummary_GearingD_Over_D_Plus_E; } }
        public decimal WaccCalculation_Kd { get { return WaccInputsSummary_CostOfDebtPre_Tax * (1 - WaccInputsSummary_TaxRate); } }
        public decimal WaccCalculation_D_Over_D_Plus_E { get { return WaccInputsSummary_GearingD_Over_D_Plus_E; } }
        public decimal WaccCalculation_Wacc { get { return (WaccCalculation_Ke * WaccCalculation_E_Over_D_Plus_E) + (WaccCalculation_Kd * WaccCalculation_D_Over_D_Plus_E); } }
        #endregion


        #region RISK-FREE RATE  

        public decimal Risk_FreeRate_Risk_FreeRate
        {
            get
            {
                if (CurrencyRiskFreeRate == null)
                    throw new NullReferenceException("ValuationCalculator.WACC_RiskFreeRate");
                return CurrencyRiskFreeRate.RiskFreeRate;
            }
        }
        #endregion
        #region Beta

        public decimal Beta_UnleveredBeta
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.UnleveredBeta.HasValue)
                    throw new NullReferenceException("ValuationCalculator.Beta_UnleveredBeta");
                return TRBCValue.UnleveredBeta.Value;

            }
        }
        //public decimal Beta_CountryWhereMajorityBusinessOperationsOfTakePlace { get { return _replacer; } }
        public decimal Beta_MarginalTaxRate
        {
            get
            {
                if (CountryTaxRate == null)
                    throw new NullReferenceException("ValuationCalculator.Beta_MarginalTaxRate");
                return CountryTaxRate.TaxRate;
            }
        }
        public decimal Beta_TotalDebtToEquityMarketCap
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.TotalDebtMarketCap.HasValue)
                    throw new NullReferenceException("ValuationCalculator.Beta_UnleveredBeta");
                return TRBCValue.TotalDebtMarketCap.Value;

            }
        }
        public decimal Beta_LeveredBeta { get { return Beta_UnleveredBeta * (1 + Beta_TotalDebtToEquityMarketCap * (1 - Beta_MarginalTaxRate)); } }


        #endregion

        #region EQUITY RISK PREMIUM



        public decimal EquityRiskPremium_MatureMarketImpliedEquityRiskPremium

        {
            get
            {
                if (GlobalDefaults == null)
                    throw new NullReferenceException("ValuationCalculator.EquityRiskPremium_MatureMarketImpliedEquityRiskPremium");
                return GlobalDefaults.EquityRiskPremium;
            }
        }

        //public decimal EquityRiskPremium_CountryWhereMajorityBusinessOperationsOfTakePlace
        //{
        //    get
        //    {
        //        return _replacer;
        //    }
        //}
        //public string EquityRiskPremium_MoodysRating
        //{
        //    get
        //    {
        //        if (MoodysRating == null)
        //            throw new NullReferenceException("ValuationCalculator.EquityRiskPremium_MoodysRating");
        //        return MoodysRating.Rating;
        //    }
        //}
        public decimal EquityRiskPremium_CountryRatingBasedDefaultSpread
        {
            get
            {
                if (MoodysRating == null || !MoodysRating.DefaultSpread.HasValue)
                    throw new NullReferenceException("ValuationCalculator.EquityRiskPremium_CountryRatingBasedDefaultSpread");
                return MoodysRating.DefaultSpread.Value / 10000;
            }
        }
        public decimal EquityRiskPremium_RelativeVolatilityOfEquityVsBonds
        {
            get
            {
                if (GlobalDefaults == null)
                    throw new NullReferenceException("ValuationCalculator.EquityRiskPremium_RelativeVolatilityOfEquityVsBonds");
                return GlobalDefaults.RelativeVolatility;
            }
        }

        public decimal EquityRiskPremium_EquityRiskPremium
        {
            get
            {
                return EquityRiskPremium_MatureMarketImpliedEquityRiskPremium + (EquityRiskPremium_CountryRatingBasedDefaultSpread * EquityRiskPremium_RelativeVolatilityOfEquityVsBonds);
            }
        }





        #endregion


        #region COST OF DEBT
        public decimal CostOfDebt_RiskFreeRate { get { return Risk_FreeRate_Risk_FreeRate; } }
        public decimal CostOfDebt_InterestCoverageRatio
        {
            get
            {
                if (TRBCValue == null || !TRBCValue.InterestCoverAdjusted.HasValue)
                    throw new NullReferenceException("ValuationCalculator.CostOfDebt_InterestCoverageRatio");
                return TRBCValue.InterestCoverAdjusted.Value;

            }
        }

        public decimal CostOfDebt_EstimatedCorporateDefaultSpread
        {
            get
            {
                var corporateBondSpread = CorporateBondSpreads?.FirstOrDefault(o => o.InterestCoverageFrom <= CostOfDebt_InterestCoverageRatio && o.InterestCoverageTo > CostOfDebt_InterestCoverageRatio);

                if (corporateBondSpread == null)
                    throw new NullReferenceException("ValuationCalculator.CostOfDebt_EstimatedCorporateDefaultSpread");
                return corporateBondSpread.Spread;

            }
        }
        public decimal CostOfDebt_EstimatedCountryDefaultSpread { get { return EquityRiskPremium_CountryRatingBasedDefaultSpread; } }
        public decimal CostOfDebt_EstimatedCostOfDebt { get { return CostOfDebt_RiskFreeRate + CostOfDebt_EstimatedCountryDefaultSpread + CostOfDebt_EstimatedCorporateDefaultSpread; } }
        public decimal CostOfDebt_MarginalTaxRate { get { return Beta_MarginalTaxRate; } }
        public decimal CostOfDebt_After_TaxCostOfDebt { get { return CostOfDebt_EstimatedCostOfDebt * (1 - CostOfDebt_MarginalTaxRate); } }

        public Int64 ClientId { get; private set; }
        public string ISOCurrencySymbol { get; private set; }
        public string ThreeLetterISORegionName { get; private set; }
        public string TRBC2012HierarchicalID { get; private set; }
        public DateTime LastFinancialYearEnd { get; private set; }

        public DateTime ReportDate { get { return DateTime.Now; } }
        public DateTime ValuationDate { get; private set; }
        public DateTime DateOfCommencement { get; private set; }
        public string Description { get; private set; }
        public string CompanyName { get; private set; }
        public decimal All3rdPartyInterestBearingDebtAsAtLastMonth_End { get; private set; }


        #endregion




        #endregion




        #region SUMMARY - DCF, MULTIPLES, VC METHOD (PRE-SURVIVAL ADJUSTMENT)


        public decimal Pre_SurvivalAdjustmentDCF_Discrete { get { return iBudget.DiscountedCashFlow_DiscountedFreeCashFlow + iForecast1.DiscountedCashFlow_DiscountedFreeCashFlow + iForecast2.DiscountedCashFlow_DiscountedFreeCashFlow; } }
        public decimal Pre_SurvivalAdjustmentDCF_Terminal { get { return iTerminal.DiscountedCashFlow_DiscountedFreeCashFlow; } }

        public decimal Pre_SurvivalAdjustmentDCF_EnterpriseValuePre_IlliquidityDiscount { get { return iBudget.DiscountedCashFlow_DiscountedFreeCashFlow + iForecast1.DiscountedCashFlow_DiscountedFreeCashFlow + iForecast2.DiscountedCashFlow_DiscountedFreeCashFlow + iTerminal.DiscountedCashFlow_DiscountedFreeCashFlow; } }

        public decimal Pre_SurvivalAdjustmentDCF_ExcessCash { get { return iActualCurrent.ExcessCash_ExcessCash; } }

        public decimal Pre_SurvivalAdjustmentMultiples_EnterpriseValuePre_IlliquidityDiscount { get { return EvOverEbitdaValuation_EnterpriseValuePre_IlliquidityDiscount; } }


        public decimal Pre_SurvivalAdjustmentDCF_EquityValuePre_IlliquidityDiscount { get { return Pre_SurvivalAdjustmentDCF_EnterpriseValuePre_IlliquidityDiscount + All3rdPartyInterestBearingDebtAsAtLastMonth_End + iActualCurrent.ExcessCash_ExcessCash; } }
        public decimal Pre_SurvivalAdjustmentMultiples_EquityValuePre_IlliquidityDiscount { get { return EvOverEbitdaValuation_EquityValuePre_IlliquidityDiscount; } }


        public decimal Pre_SurvivalAdjustmentDCF_IlliquidityDiscount { get { return (decimal)-1 * Pre_SurvivalAdjustmentDCF_EquityValuePre_IlliquidityDiscount * IlliquidityDiscount; } }
        public decimal Pre_SurvivalAdjustmentMultiples_IlliquidityDiscount { get { return EvOverEbitdaValuation_IlliquidityDiscount; } }


        public decimal Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount { get { return Pre_SurvivalAdjustmentDCF_IlliquidityDiscount + Pre_SurvivalAdjustmentDCF_EquityValuePre_IlliquidityDiscount; } }
        public decimal Pre_SurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount { get { return EvOverEbitdaValuation_EquityValuePost_IlliquidityDiscount; } }

        public decimal Pre_SurvivalAdjustment_VCMethod_EquityValuePost_IlliquidityDiscount { get { return iForecast2.VCMethod_VCMethodEquityValue; } }

        public decimal Pre_SurvivalAdjustment_VCMethod_ExcessCash { get { return iActualCurrent.ExcessCash_ExcessCash * (decimal)-1; } }
        public decimal Pre_SurvivalAdjustment_VCMethod_InterestBearingDebt { get { return All3rdPartyInterestBearingDebtAsAtLastMonth_End * (decimal)-1; } }




        public decimal Pre_SurvivalAdjustment_VCMethod_EnterpriseValuePostIlliquidityDiscount { get { return Pre_SurvivalAdjustment_VCMethod_EquityValuePost_IlliquidityDiscount + Pre_SurvivalAdjustment_VCMethod_ExcessCash + Pre_SurvivalAdjustment_VCMethod_InterestBearingDebt; } }

        public decimal Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPre_LiquidityDiscount { get { return Pre_SurvivalAdjustmentDCF_EnterpriseValuePre_IlliquidityDiscount / iActualCurrent.SustainableEBITDA_SustainableEBITDA_TTM; } }
        public decimal Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPre_LiquidityDiscount { get { return Pre_SurvivalAdjustmentMultiples_EnterpriseValuePre_IlliquidityDiscount / iActualCurrent.SustainableEBITDA_SustainableEBITDA_TTM; } }




        public decimal Pre_SurvivalAdjustmentDCF_EnterpriseValuePost_IlliquidityDiscount { get { return Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount;
            } }
        public decimal Pre_SurvivalAdjustmentMultiples_EnterpriseValuePost_IlliquidityDiscount { get { return Pre_SurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount; } }
        public decimal Pre_SurvivalAdjustmentVCMethod_EnterpriseValuePost_IlliquidityDiscount { get { return iForecast2.VCMethod_VCMethodEquityValue; } }

        public decimal Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPost_LiquidityDiscount { get { return (Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount + ((decimal)-1 * (iActualCurrent.ExcessCash_ExcessCash + All3rdPartyInterestBearingDebtAsAtLastMonth_End))) / iActualCurrent.SustainableEBITDA_SustainableEBITDA_TTM; } }
        public decimal Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPost_LiquidityDiscount { get { return (Pre_SurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount + ((decimal)-1 * (iActualCurrent.ExcessCash_ExcessCash + All3rdPartyInterestBearingDebtAsAtLastMonth_End))) / iActualCurrent.SustainableEBITDA_SustainableEBITDA_TTM; } }
        public decimal Pre_SurvivalAdjustmentVCMethod_ImpliedEv_Over_EbitdaPost_LiquidityDiscount { get { return (Pre_SurvivalAdjustment_VCMethod_EquityValuePost_IlliquidityDiscount + ((decimal)-1 * (iActualCurrent.ExcessCash_ExcessCash + All3rdPartyInterestBearingDebtAsAtLastMonth_End))) / iActualCurrent.SustainableEBITDA_SustainableEBITDA_TTM; } }




        public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_EnterpriseValuePre_IlliquidityDiscount { get { return iBudget.DiscountedCashFlowTerminalMultiple_DiscountedFreeCashFlow + iForecast1.DiscountedCashFlowTerminalMultiple_DiscountedFreeCashFlow + iForecast2.DiscountedCashFlowTerminalMultiple_DiscountedFreeCashFlow + iTerminal.DiscountedCashFlowTerminalMultiple_DiscountedFreeCashFlow; } }
        public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_ExcessCash { get { return iActualCurrent.ExcessCash_ExcessCash; } }
        public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_InterestBearingDebt { get { return All3rdPartyInterestBearingDebtAsAtLastMonth_End; } }
        public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePre_IlliquidityDiscount { get { return Pre_SurvivalAdjustmentDCF_TermMultiple_EnterpriseValuePre_IlliquidityDiscount + Pre_SurvivalAdjustmentDCF_TermMultiple_ExcessCash + Pre_SurvivalAdjustmentDCF_TermMultiple_InterestBearingDebt; } }
        public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_IlliquidityDiscount { get { return Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePre_IlliquidityDiscount * IlliquidityDiscount * (decimal)-1; } }
        public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePostIlliquidityDiscount { get { return Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePre_IlliquidityDiscount + Pre_SurvivalAdjustmentDCF_TermMultiple_IlliquidityDiscount; } }
        public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_SustainableEbitdaTtm { get { return iActualCurrent.SustainableEBITDA_SustainableEBITDA_TTM; } }
        public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_ImpliedEv_Over_EbitdaPre_LiquidityDiscount { get { return Pre_SurvivalAdjustmentDCF_TermMultiple_EnterpriseValuePre_IlliquidityDiscount / Pre_SurvivalAdjustmentDCF_TermMultiple_SustainableEbitdaTtm; } }
        //public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePostIlliquidityDiscount { get { return 0; } }
        //public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_ExcessCash{ get { return 0; } }
        //public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_InterestBearingDebt { get { return 0; } }
        public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_EnterpriseValuePostIlliquidityDiscount { get { return Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePostIlliquidityDiscount + ((decimal)-1 * (iActualCurrent.ExcessCash_ExcessCash + Pre_SurvivalAdjustmentDCF_TermMultiple_InterestBearingDebt)); } }
        public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_ImpliedEV_Over_EBITDAPostLiquidityDiscount { get { return Pre_SurvivalAdjustmentDCF_TermMultiple_EnterpriseValuePostIlliquidityDiscount / Pre_SurvivalAdjustmentDCF_TermMultiple_SustainableEbitdaTtm; } }




        public decimal Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePost_IlliquidityDiscount { get { return Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePre_IlliquidityDiscount + Pre_SurvivalAdjustmentDCF_TermMultiple_IlliquidityDiscount; } }



        #endregion



        #region EQUITY VALUE - POST SURVIVAL ADJUSTMENT



        public decimal EquityValue_PostSurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount { get { return Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount; } }
        public decimal EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedEquityValuePost_IlliquidityDiscount { get { return EquityValue_PostSurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount * ProbabilityOfSurvival; } }
        public decimal EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue { get { return LiquidationvalueRecoveryAmount_LiquidationValue * ProbabilityOfFailure; } }
        public decimal EquityValue_PostSurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival { get { return EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedEquityValuePost_IlliquidityDiscount + EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue; } }



        public decimal EquityValue_PostSurvivalAdjustment_MULTIPLES_EquityValuePost_IlliquidityDiscount { get { return Pre_SurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount; } }
        public decimal EquityValue_PostSurvivalAdjustment_MULTIPLES_Probability_WeightedEquityValuePost_IlliquidityDiscount { get { return EquityValue_PostSurvivalAdjustment_MULTIPLES_EquityValuePost_IlliquidityDiscount * ProbabilityOfSurvival; } }
        public decimal EquityValue_PostSurvivalAdjustment_MULTIPLES_Probability_WeightedLiquidationValue { get { return LiquidationvalueRecoveryAmount_LiquidationValue * ProbabilityOfFailure; } }
        public decimal EquityValue_PostSurvivalAdjustment_MULTIPLES_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival { get { return EquityValue_PostSurvivalAdjustment_MULTIPLES_Probability_WeightedEquityValuePost_IlliquidityDiscount + EquityValue_PostSurvivalAdjustment_MULTIPLES_Probability_WeightedLiquidationValue; } }

        public decimal EquityValue_PostSurvivalAdjustment_VCMethod_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival { get { return iForecast2.VCMethod_VCMethodEquityValue; } }

        #endregion



        #region Equity VALUE - POST SURVIVAL ADJUSTMENT


        public decimal EquityValue_PostSurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount { get { return this.Pre_SurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount; } }

        public decimal EquityValue_PostSurvivalAdjustment_Multiples_Probability_WeightedEquityValuePost_IlliquidityDiscount { get { return EquityValue_PostSurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount; } }

        public decimal EquityValue_PostSurvivalAdjustment_Multiples_Probability_WeightedLiquidationValue { get { return LiquidationvalueRecoveryAmount_LiquidationValue * ProbabilityOfFailure; } }
        public decimal EquityValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get { return EquityValue_PostSurvivalAdjustment_Multiples_Probability_WeightedEquityValuePost_IlliquidityDiscount + EquityValue_PostSurvivalAdjustment_Multiples_Probability_WeightedLiquidationValue; } }




        public decimal EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_EquityValuePost_IlliquidityDiscount { get { return this.Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePost_IlliquidityDiscount; } }

        public decimal EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_Probability_WeightedEquityValuePost_IlliquidityDiscount { get { return EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_EquityValuePost_IlliquidityDiscount * ProbabilityOfSurvival; } }
        public decimal EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_LiquidationValue { get { return LiquidationvalueRecoveryAmount_LiquidationValue; } }

        public decimal EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_Probability_WeightedLiquidationValue { get { return EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_LiquidationValue * ProbabilityOfFailure; } }
        public decimal EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival { get { return EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_Probability_WeightedEquityValuePost_IlliquidityDiscount + EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_Probability_WeightedLiquidationValue; } }



        public decimal EquityValue_PostSurvivalAdjustment_VCMethod_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get { return iForecast2.VCMethod_VCMethodEquityValue; } }


        #endregion





        #region ENTERPRISE VALUE - POST SURVIVAL ADJUSTMENT

        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount { get { return Pre_SurvivalAdjustmentDCF_EnterpriseValuePost_IlliquidityDiscount + (iActualCurrent.ExcessCash_ExcessCash * (decimal)-1) + (All3rdPartyInterestBearingDebtAsAtLastMonth_End * (decimal)-1); } }

        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount { get { return Pre_SurvivalAdjustmentMultiples_EnterpriseValuePost_IlliquidityDiscount; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_EnterpriseValuePost_IlliquidityDiscount { get { return Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount; } }




        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityOfSurvival { get { return ProbabilityOfSurvival; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityOfSurvival { get { return ProbabilityOfSurvival; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityOfSurvival { get { return ProbabilityOfSurvival; } }


        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedEquityValuePostIlliquidityDiscount { get { return EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount * EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityOfSurvival; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityWeightedEquityValuePostIlliquidityDiscount { get { return EnterpriseValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount * EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityOfSurvival; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityWeightedEquityValuePostIlliquidityDiscount { get { return EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_EnterpriseValuePost_IlliquidityDiscount * EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityOfSurvival; } }



        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_LiquidationValue { get { return LiquidationvalueRecoveryAmount_LiquidationValue; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_LiquidationValue { get { return LiquidationvalueRecoveryAmount_LiquidationValue; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_LiquidationValue { get { return LiquidationvalueRecoveryAmount_LiquidationValue; } }



        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityOfFailure { get { return ProbabilityOfFailure; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityOfFailure { get { return ProbabilityOfFailure; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityOfFailure { get { return ProbabilityOfFailure; } }

        

        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue { get { return EnterpriseValue_PostSurvivalAdjustment_DCF_LiquidationValue * ProbabilityOfFailure; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityWeightedLiquidationValue { get { return EnterpriseValue_PostSurvivalAdjustment_Multiples_LiquidationValue * ProbabilityOfFailure; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityWeightedLiquidationValue { get { return EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_LiquidationValue * ProbabilityOfFailure; } }



        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get { return EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedEquityValuePostIlliquidityDiscount + EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityOfFailure; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get { return EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityWeightedEquityValuePostIlliquidityDiscount + EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityOfFailure; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_VCMethod_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get { return Pre_SurvivalAdjustment_VCMethod_EnterpriseValuePostIlliquidityDiscount; } }
        public decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get { return EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityWeightedEquityValuePostIlliquidityDiscount + EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityOfFailure; } }


        #endregion






        #region LiquidationvalueRecoveryRate
        public decimal LiquidationvalueRecoveryRate_AccountsReceiveable { get { return GlobalDefaults.LiquidationvalueRecoveryRate_AccountsReceiveable; } }
        public decimal LiquidationvalueRecoveryRate_Inventory { get { return GlobalDefaults.LiquidationvalueRecoveryRate_Inventory; } }
        public decimal LiquidationvalueRecoveryRate_OtherCurrentAssets { get { return GlobalDefaults.LiquidationvalueRecoveryRate_OtherCurrentAssets; } }
        public decimal LiquidationvalueRecoveryRate_CashAndCashEquivalents { get { return GlobalDefaults.LiquidationvalueRecoveryRate_CashAndCashEquivalents; } }
        public decimal LiquidationvalueRecoveryRate_IntangibleAssets { get { return GlobalDefaults.LiquidationvalueRecoveryRate_IntangibleAssets; } }
        public decimal LiquidationvalueRecoveryRate_FixedAssets { get { return GlobalDefaults.LiquidationvalueRecoveryRate_FixedAssets; } }

        public decimal LiquidationvalueRecoveryRate_AccountsPayable { get { return GlobalDefaults.LiquidationvalueRecoveryRate_AccountsPayable; } }
        public decimal LiquidationvalueRecoveryRate_OtherCurrentLiabilities { get { return GlobalDefaults.LiquidationvalueRecoveryRate_OtherCurrentLiabilities; } }
        public decimal LiquidationvalueRecoveryRate_3rdPartyInterest_BearingDebtLongTermShortTerm { get { return GlobalDefaults.LiquidationvalueRecoveryRate_3rdPartyInterest_BearingDebtLongTermShortTerm; } }
        public decimal LiquidationvalueRecoveryRate_OtherLong_TermLiabilities { get { return GlobalDefaults.LiquidationvalueRecoveryRate_OtherLong_TermLiabilities; } }
        public decimal LiquidationvalueRecoveryRate_ShareholderLoans { get { return GlobalDefaults.LiquidationvalueRecoveryRate_ShareholderLoans; } }

        #endregion


        #region LiquidationvalueRecoveryAmount 
        public decimal LiquidationvalueRecoveryAmount_CurrentAssets { get { return LiquidationvalueRecoveryAmount_AccountsReceiveable + LiquidationvalueRecoveryAmount_Inventory + LiquidationvalueRecoveryAmount_OtherCurrentAssets + LiquidationvalueRecoveryAmount_CashAndCashEquivalents; } }

        public decimal LiquidationvalueRecoveryAmount_AccountsReceiveable { get { return iActualCurrent.Liquidationvalue_AccountsReceiveable * LiquidationvalueRecoveryRate_AccountsReceiveable; } }
        public decimal LiquidationvalueRecoveryAmount_Inventory { get { return iActualCurrent.Liquidationvalue_Inventory * LiquidationvalueRecoveryRate_Inventory; } }
        public decimal LiquidationvalueRecoveryAmount_OtherCurrentAssets { get { return iActualCurrent.Liquidationvalue_OtherCurrentAssets * LiquidationvalueRecoveryRate_OtherCurrentAssets; } }
        public decimal LiquidationvalueRecoveryAmount_CashAndCashEquivalents { get { return iActualCurrent.Liquidationvalue_CashAndCashEquivalents * LiquidationvalueRecoveryRate_CashAndCashEquivalents; } }
        public decimal LiquidationvalueRecoveryAmount_IntangibleAssets { get { return iActualCurrent.Liquidationvalue_IntangibleAssets * LiquidationvalueRecoveryRate_IntangibleAssets; } }
        public decimal LiquidationvalueRecoveryAmount_FixedAssets { get { return iActualCurrent.Liquidationvalue_FixedAssets * LiquidationvalueRecoveryRate_FixedAssets; } }
        public decimal LiquidationvalueRecoveryAmount_TotalAssets { get { return LiquidationvalueRecoveryAmount_CurrentAssets + LiquidationvalueRecoveryAmount_IntangibleAssets + LiquidationvalueRecoveryAmount_FixedAssets; } }



        public decimal LiquidationvalueRecoveryAmount_CurrentLiabilities { get { return LiquidationvalueRecoveryAmount_AccountsPayable + LiquidationvalueRecoveryAmount_OtherCurrentLiabilities; } }

        public decimal LiquidationvalueRecoveryAmount_AccountsPayable { get { return iActualCurrent.Liquidationvalue_AccountsPayable * LiquidationvalueRecoveryRate_AccountsPayable; } }
        public decimal LiquidationvalueRecoveryAmount_OtherCurrentLiabilities { get { return iActualCurrent.Liquidationvalue_OtherCurrentLiabilities * LiquidationvalueRecoveryRate_OtherCurrentLiabilities; } }
        public decimal LiquidationvalueRecoveryAmount_3rdPartyInterest_BearingDebtLongTermShortTerm { get { return iActualCurrent.Liquidationvalue_3rdPartyInterest_BearingDebtLongTermShortTerm * LiquidationvalueRecoveryRate_3rdPartyInterest_BearingDebtLongTermShortTerm; } }
        public decimal LiquidationvalueRecoveryAmount_OtherLong_TermLiabilities { get { return iActualCurrent.Liquidationvalue_OtherLong_TermLiabilities * LiquidationvalueRecoveryRate_OtherLong_TermLiabilities; } }
        public decimal LiquidationvalueRecoveryAmount_ShareholderLoans { get { return iActualCurrent.Liquidationvalue_ShareholderLoans * LiquidationvalueRecoveryRate_ShareholderLoans; } }
        public decimal LiquidationvalueRecoveryAmount_TotalLiabilities { get { return LiquidationvalueRecoveryAmount_CurrentLiabilities + LiquidationvalueRecoveryAmount_3rdPartyInterest_BearingDebtLongTermShortTerm + LiquidationvalueRecoveryAmount_OtherLong_TermLiabilities; } }
        public decimal LiquidationvalueRecoveryAmount_LiquidationValue { get { return LiquidationvalueRecoveryAmount_TotalAssets - LiquidationvalueRecoveryAmount_TotalLiabilities < 0 ? 0 : LiquidationvalueRecoveryAmount_TotalAssets - LiquidationvalueRecoveryAmount_TotalLiabilities; } }



        #endregion



        #region Sensitivity Analysis Enterprise Value Pre Illiquidity Discount




        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_1 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_3 * ((decimal)1 - (GlobalDefaults.EBITDA_TTM_Sensitivity * (decimal)2)); } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_2 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_3 * ((decimal)1 - (GlobalDefaults.EBITDA_TTM_Sensitivity)); } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_3 { get { return EvOverEbitdaValuation_SustainableEbitdaTtm; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_4 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_3 * ((decimal)1 + (GlobalDefaults.EBITDA_TTM_Sensitivity)); } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_5 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_3 * ((decimal)1 + (GlobalDefaults.EBITDA_TTM_Sensitivity * (decimal)2)); } }

        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_AMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_BMultiple - GlobalDefaults.EBITDA_Multiple_Sensitivity; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A1 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_1 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_AMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A2 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_2 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_AMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A3 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_3 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_AMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A4 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_4 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_AMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A5 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_5 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_AMultiple; } }

        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_BMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_CMultiple - GlobalDefaults.EBITDA_Multiple_Sensitivity; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B1 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_1 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_BMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B2 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_2 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_BMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B3 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_3 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_BMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B4 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_4 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_BMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B5 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_5 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_BMultiple; } }

        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_CMultiple { get { return WorldMedian_World; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C1 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_1 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_CMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C2 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_2 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_CMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C3 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_3 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_CMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C4 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_4 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_CMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C5 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_5 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_CMultiple; } }

        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_DMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_CMultiple +
                    GlobalDefaults.EBITDA_Multiple_Sensitivity; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D1 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_1 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_DMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D2 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_2 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_DMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D3 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_3 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_DMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D4 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_4 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_DMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D5 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_5 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_DMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_DMultiple + GlobalDefaults.EBITDA_Multiple_Sensitivity; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E1 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_1 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E2 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_2 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E3 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_3 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E4 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_4 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EMultiple; } }
        public decimal SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E5 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_5 * SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EMultiple; } }

        #endregion

        #region Sensitivity Analysis Equity Value Post Illiquidity Discount

        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_EBITDA_TTM_1 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_1; } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_EBITDA_TTM_2 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_2; } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_EBITDA_TTM_3 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_3; } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_EBITDA_TTM_4 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_4; } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_EBITDA_TTM_5 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_5; } }

        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_AMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_AMultiple; } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_A1 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_A2 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_A3 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_A4 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_A5 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }

        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_BMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_BMultiple; } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_B1 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_B2 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_B3 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_B4 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_B5 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }

        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_CMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_CMultiple; } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_C1 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_C2 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_C3 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_C4 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_C5 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }

        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_DMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_DMultiple; } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_D1 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_D2 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_D3 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_D4 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_D5 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }

        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_EMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EMultiple; } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_E1 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_E2 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_E3 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_E4 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SensitivityAnalysisEquityValuePostIlliquidityDiscount_E5 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }





        #endregion


        #region Sensitivity Analysis Equity Value Survival Adjusted Multiples

        public decimal SurvivalAdjustedMultiples_EBITDA_TTM_1 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_1; } }
        public decimal SurvivalAdjustedMultiples_EBITDA_TTM_2 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_2; } }
        public decimal SurvivalAdjustedMultiples_EBITDA_TTM_3 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_3; } }
        public decimal SurvivalAdjustedMultiples_EBITDA_TTM_4 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_4; } }
        public decimal SurvivalAdjustedMultiples_EBITDA_TTM_5 { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_5; } }



        public decimal SurvivalAdjustedMultiples_AMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_AMultiple; } }
        public decimal SurvivalAdjustedMultiples_A1 { get { return (SensitivityAnalysisEquityValuePostIlliquidityDiscount_A1 * ProbabilityOfSurvival) + EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue; } }
        public decimal SurvivalAdjustedMultiples_A2 { get { return (SensitivityAnalysisEquityValuePostIlliquidityDiscount_A2 * ProbabilityOfSurvival) + EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue; } }
        public decimal SurvivalAdjustedMultiples_A3 { get { return (SensitivityAnalysisEquityValuePostIlliquidityDiscount_A3 * ProbabilityOfSurvival) + EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue; } }
        public decimal SurvivalAdjustedMultiples_A4 { get { return (SensitivityAnalysisEquityValuePostIlliquidityDiscount_A4 * ProbabilityOfSurvival) + EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue; } }
        public decimal SurvivalAdjustedMultiples_A5 { get { return (SensitivityAnalysisEquityValuePostIlliquidityDiscount_A5 * ProbabilityOfSurvival) + EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue; } }

        public decimal SurvivalAdjustedMultiples_BMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_BMultiple; } }
        public decimal SurvivalAdjustedMultiples_B1 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_B2 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_B3 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_B4 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_B5 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }

        public decimal SurvivalAdjustedMultiples_CMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_CMultiple; } }
        public decimal SurvivalAdjustedMultiples_C1 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_C2 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_C3 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_C4 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_C5 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }

        public decimal SurvivalAdjustedMultiples_DMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_DMultiple; } }
        public decimal SurvivalAdjustedMultiples_D1 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_D2 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_D3 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_D4 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_D5 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }

        public decimal SurvivalAdjustedMultiples_EMultiple { get { return SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EMultiple; } }
        public decimal SurvivalAdjustedMultiples_E1 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_E2 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_E3 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_E4 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }
        public decimal SurvivalAdjustedMultiples_E5 { get { return (SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * (1 - GlobalDefaults.IlliquidityDiscount); } }


        #endregion


         





        #region Sensitivity Analysis Survival Adjusted DCF  Pre Illiquidity Discount




        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2 - GlobalDefaults.WACCSensitivity; } }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3 - GlobalDefaults.WACCSensitivity; } }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3 { get { return WaccCalculation_Wacc; } }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3 + GlobalDefaults.WACCSensitivity; } }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4 + GlobalDefaults.WACCSensitivity; } }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_AMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_BMultiple - GlobalDefaults.TgSensitivity; } }
 
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A1 { get { return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

                                        ((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

                                        (((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

                                        (iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_AMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear)); } }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A2
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_AMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A3
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_AMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A4
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_AMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A5
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_AMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_BMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_CMultiple - GlobalDefaults.TgSensitivity; } }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B1
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_BMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B2
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_BMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B3
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_BMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B4
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_BMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B5
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_BMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_CMultiple { get { return Risk_FreeRate_Risk_FreeRate; } }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C1
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_CMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C2
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_CMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C3
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_CMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C4
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_CMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C5
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_CMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }


        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_DMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_CMultiple + GlobalDefaults.TgSensitivity; } }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D1
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_DMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D2
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_DMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D3
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_DMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D4
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_DMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D5
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_DMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_EMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_DMultiple + GlobalDefaults.TgSensitivity; } }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E1
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_EMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E2
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_EMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }

        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E3
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_EMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E4
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_EMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }
        public decimal SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E5
        {
            get
            {
                return (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (double)iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (double)iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5)), (Double)iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow) +

(iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5 - SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_EMultiple)) / (decimal)Math.Pow((double)((decimal)1 + SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5), (double)iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));
            }
        }


        #endregion

        #region ensitivity Analysis Survival Adjusted DCF Post Illiquidity Discount

        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_1 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1; } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_2 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2; } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_3 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3; } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_4 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4; } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_5 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5; } }

        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_AMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_AMultiple; } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A1 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A2 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A3 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A4 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A5 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }

        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_BMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_BMultiple; } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B1 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B2 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B3 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B4 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B5 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }

        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_CMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_CMultiple; } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C1 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C2 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C3 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C4 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C5 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }

        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_DMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_DMultiple; } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D1 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D2 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D3 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D4 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D5 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }

        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_EMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_EMultiple; } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E1 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E1 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E2 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E2 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E3 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E3 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E4 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E4 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }
        public decimal SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E5 { get { return (SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E5 + iActualCurrent.ExcessCash_ExcessCash - All3rdPartyInterestBearingDebtAsAtLastMonth_End) * ((decimal)1 - IlliquidityDiscount); } }




        #endregion


        #region Sensitivity Analysis Survival Adjusted DCF 



        public decimal SurvivalAdjustedDCFEquity_Wacc_1 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1; } }
        public decimal SurvivalAdjustedDCFEquity_Wacc_2 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2; } }
        public decimal SurvivalAdjustedDCFEquity_Wacc_3 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3; } }
        public decimal SurvivalAdjustedDCFEquity_Wacc_4 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4; } }
        public decimal SurvivalAdjustedDCFEquity_Wacc_5 { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5; } }

        public decimal SurvivalAdjustedDCFEquity_AMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_AMultiple; } }

      



       
        public decimal SurvivalAdjustedDCFEquity_A1 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A1 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_A2 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A2 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_A3 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A3 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_A4 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A4 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_A5 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A5 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_BMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_BMultiple; } }
        public decimal SurvivalAdjustedDCFEquity_B1 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B1 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_B2 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B2 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_B3 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B3 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_B4 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B4 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_B5 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B5 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }

        public decimal SurvivalAdjustedDCFEquity_CMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_CMultiple; } }
        public decimal SurvivalAdjustedDCFEquity_C1 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C1 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_C2 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C2 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_C3 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C3 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_C4 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C4 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_C5 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C5 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_DMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_DMultiple; } }
        public decimal SurvivalAdjustedDCFEquity_D1 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D1 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_D2 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D2 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_D3 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D3 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_D4 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D4 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_D5 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D5 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }

        public decimal SurvivalAdjustedDCFEquity_EMultiple { get { return SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_EMultiple; } }
        public decimal SurvivalAdjustedDCFEquity_E1 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E1 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_E2 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E2 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_E3 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E3 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_E4 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E4 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }
        public decimal SurvivalAdjustedDCFEquity_E5 { get { return (SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E5 * ProbabilityOfSurvival) + (EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue); } }


        #endregion

    }
}
