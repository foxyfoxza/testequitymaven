﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.FinancialYears;
namespace EquityMaven.Data.Calculations
{

    public abstract class FinancialYearProjected : FinancialYear, IFinancialYearProjected 
    {
        public FinancialYearProjected(FinancialValuation fv, IFullValuationInputYear from) : base(fv, from)
        {
            this.IncomeStatementAnalysis_PercentageRevenueGrowth = from.IncomeStatementAnalysis_PercentageRevenueGrowth;
            this.IncomeStatementAnalysis_PercentageGrossProfitMargin = from.IncomeStatementAnalysis_PercentageGrossProfitMargin;
            this.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = from.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue;
            this.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = from.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue;
            this.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = from.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue;
            this.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold;
            this.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold;
            this.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold;
            this.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = from.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold;
            this.CapitalExpenditure_PercentageForecastCapitalExpenditure = from.CapitalExpenditure_PercentageForecastCapitalExpenditure;
        }

        protected void ApplyProjectedValues(FullValuationInputYear to)
        {
            to.IncomeStatement_EBIT = this.IncomeStatement_EBIT;
            to.IncomeStatement_EBITDA = this.IncomeStatement_EBITDA;
            to.IncomeStatement_EarningsBeforeTax = this.IncomeStatement_EarningsBeforeTax;
            to.IncomeStatement_GrossProfit = this.IncomeStatement_GrossProfit;
            to.AssetsProjection_FixedAndIntangibleAssets = this.AssetsProjection_FixedAndIntangibleAssets;
            to.BalanceSheetAnalysis_AverageAccountsPayableDays = this.BalanceSheetAnalysis_AverageAccountsPayableDays;
            to.BalanceSheetAnalysis_AverageAccountsReceiveableDays = this.BalanceSheetAnalysis_AverageAccountsReceiveableDays;
            to.BalanceSheetAnalysis_AverageInventoryDays = this.BalanceSheetAnalysis_AverageInventoryDays;
            to.BalanceSheetAnalysis_CurrentRatio = 0;
            to.BalanceSheetAnalysis_TotalDebtOverEBITDA = 0;
            to.BalanceSheetEquityAndLiabilities_LongTermDebt = 0;
            to.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = this.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities;
            to.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = 0;
            to.BalanceSheetEquityAndLiabilities_ShareholderLoans = 0;
            to.BalanceSheetEquityAndLiabilities_ShortTermDebt = 0;
            to.BalanceSheetEquityAndLiabilities_CurrentLiabilities = 0;
            to.BalanceSheetTotalAssets_CurrentAssets = this.BalanceSheetTotalAssets_CurrentAssets;
            to.BalanceSheetTotalAssets_TotalAssets = this.BalanceSheetTotalAssets_TotalAssets;

            to.ExcessCash_ExcessCash = 0;
            to.ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue = 0;
            to.ExcessCash_RevenueTTM = 0;
            to.IncomeStatementAnalysis_Calculated_InterestCover = 0;
            to.IncomeStatementAnalysis_EBITDAMargin = this.IncomeStatementAnalysis_EBITDAMargin;
            to.IncomeStatementAnalysis_EBITMargin = this.IncomeStatementAnalysis_EBITMargin;
            to.SustainableEBITDA_SustainableEBITDA_TTM = 0;
            to.BalanceSheetTotalAssets_FixedAndIntangibleAssets = 0;
            to.CapitalExpenditure_TTM = 0;

            to.IncomeStatement_Revenue = this.IncomeStatement_Revenue;
            to.IncomeStatement_CostOfGoodsSold = this.IncomeStatement_CostOfGoodsSold;
            to.IncomeStatement_OperatingExpense = this.IncomeStatement_OperatingExpense;
            to.IncomeStatement_DepreciationAndAmortisationExpense = this.IncomeStatement_DepreciationAndAmortisationExpense;
            to.IncomeStatement_InterestExpense = this.IncomeStatement_InterestExpense;
            to.IncomeStatement_NonCashExpense = this.IncomeStatement_NonCashExpense;
            to.IncomeStatementAnalysis_PercentageRevenueGrowth = this.IncomeStatementAnalysis_PercentageRevenueGrowth;
            to.IncomeStatementAnalysis_PercentageGrossProfitMargin = this.IncomeStatementAnalysis_PercentageGrossProfitMargin;
            to.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = this.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue;
            to.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = this.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue;
            to.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = this.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue;
            to.BalanceSheetTotalAssets_AccountsReceiveable = this.BalanceSheetTotalAssets_AccountsReceiveable;
            to.BalanceSheetTotalAssets_Inventory = this.BalanceSheetTotalAssets_Inventory;
            to.BalanceSheetTotalAssets_OtherCurrentAssets = this.BalanceSheetTotalAssets_OtherCurrentAssets;
            to.BalanceSheetTotalAssets_CashAndCashEquivalents = 0;
            to.BalanceSheetTotalAssets_IntangibleAssets = 0;
            to.BalanceSheetTotalAssets_FixedAssets = 0;
            to.BalanceSheetEquityAndLiabilities_AccountsPayable = this.BalanceSheetEquityAndLiabilities_AccountsPayable;
            to.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = this.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities;
            to.BalanceSheetEquityAndLiabilities_ShortTermDebt = 0;
            to.BalanceSheetEquityAndLiabilities_LongTermDebt = 0;
            to.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = 0;
            to.BalanceSheetEquityAndLiabilities_ShareholderLoans = 0;
            to.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = this.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue;
            to.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = this.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold;
            to.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = this.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold;
            to.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = this.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold;
            to.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = this.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold;
            to.CapitalExpenditure_CapitalExpenditure = this.CapitalExpenditure_CapitalExpenditure;
            to.CapitalExpenditure_PercentageForecastCapitalExpenditure = this.CapitalExpenditure_PercentageForecastCapitalExpenditure;
        }


        protected abstract IFinancialYear GetPrior();
        public IFinancialYear PriorFY { get { return GetPrior(); } }

        #region Client Inputs

        public decimal AssetsProjection_FixedAndIntangibleAssets
        {
            get { return PriorFY.GetBalanceSheetTotalAssets_FixedAndIntangibleAssets() - IncomeStatement_DepreciationAndAmortisationExpense + CapitalExpenditure_CapitalExpenditure; }
        }

        public override decimal GetIncomeStatement_Revenue()
        {
            return IncomeStatement_Revenue;
        }

        public override decimal GetIncomeStatement_CostOfGoodsSold()
        {
            return IncomeStatement_CostOfGoodsSold;
        }

        public override decimal GetIncomeStatement_OperatingExpense()
        {
            return IncomeStatement_OperatingExpense;
        }

        public override decimal GetIncomeStatement_DepreciationAndAmortisationExpense()
        {
            return IncomeStatement_DepreciationAndAmortisationExpense;
        }

        public override decimal GetIncomeStatement_InterestExpense()
        {
            return IncomeStatement_InterestExpense;
        }


        public override decimal GetBalanceSheetTotalAssets_FixedAndIntangibleAssets()
        {
            return AssetsProjection_FixedAndIntangibleAssets;
        }

        public override decimal GetBalanceSheetTotalAssets_AccountsReceiveable()
        {
            return BalanceSheetTotalAssets_AccountsReceiveable;
        }

        public override decimal GetBalanceSheetTotalAssets_Inventory()
        {
            return BalanceSheetTotalAssets_Inventory;
        }

        public override decimal GetBalanceSheetTotalAssets_OtherCurrentAssets()
        {
            return BalanceSheetTotalAssets_OtherCurrentAssets;
        }

        public override decimal GetBalanceSheetTotalAssets_CashAndCashEquivalents()
        {
            return 0;
        }

        public override decimal GetBalanceSheetTotalAssets_IntangibleAssets()
        {
            return 0;
        }

        public override decimal GetBalanceSheetTotalAssets_FixedAssets()
        {
            return 0;
        }

        public override decimal GetBalanceSheetEquityAndLiabilities_CurrentLiabilities()
        {
            return BalanceSheetEquityAndLiabilities_AccountsPayable + BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities;
        }

        public override decimal GetBalanceSheetEquityAndLiabilities_LongTermDebt()
        {
            return 0;
        }

        public override decimal GetBalanceSheetEquityAndLiabilities_OtherLongTermLiabilities()
        {
            return 0;
        }

        public override decimal GetBalanceSheetEquityAndLiabilities_ShareholderLoans()
        {
            return 0;
        }

        public override decimal GetBalanceSheetEquityAndLiabilities_AccountsPayable()
        {
            return BalanceSheetEquityAndLiabilities_AccountsPayable;
        }
        public override decimal GetIncomeStatement_GrossProfit()
        {
            return GetIncomeStatement_Revenue() * IncomeStatementAnalysis_PercentageGrossProfitMargin;
        }

        #region Income Statement
        public decimal IncomeStatement_Revenue
        {
            get
            {
                return PriorFY.GetIncomeStatement_Revenue() * (1 + IncomeStatementAnalysis_PercentageRevenueGrowth);
            }
        }

        /// <summary>
        /// Cost of goods sold
        /// </summary>
        public decimal IncomeStatement_CostOfGoodsSold
        {
            get
            {
                return IncomeStatement_Revenue - IncomeStatement_GrossProfit;
            }
        }


        public decimal IncomeStatement_OperatingExpense
        {
            get
            {
                return IncomeStatement_Revenue * IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue;
            }
        }

        /// <summary>
        /// Depreciation and amortisation expense
        /// </summary>
        public decimal IncomeStatement_DepreciationAndAmortisationExpense
        {
            get
            {
                return IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue * IncomeStatement_Revenue;
            }
        }


        public decimal IncomeStatement_InterestExpense
        {
            get
            {
                return PriorFY.GetIncomeStatement_InterestExpense();
            }
        }


        public decimal IncomeStatement_NonCashExpense
        {
            get
            {
                return IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue * IncomeStatement_Revenue;
            }
        }




        #endregion
        #region Income Statement Analysis
        public override decimal GetIncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue()
        {
            return IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue;
        }

        public Decimal IncomeStatementAnalysis_PercentageRevenueGrowth { get; set; }

        public decimal IncomeStatementAnalysis_PercentageGrossProfitMargin { get; set; }



        public decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue { get; set; }


        /// <summary>
        ///         Depreciation and amortisation as % of Revenue
        /// </summary>
        public Decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue
        {
            get
            {
                return PriorFY.GetIncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue();
            }
        }

        /// <summary>
        ///what percentage Non-cash expenses (included in Operating expenses) (excluding depreciation & amortisation) are expected to be as a percentage of Revenue. 
        /// </summary>
        public Decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue { get; set; }


        #endregion

        #region Balance Sheet

        public decimal BalanceSheetTotalAssets_AccountsReceiveable
        {
            get
            {
                return IncomeStatement_Revenue * BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue;
            }
        }


        public decimal BalanceSheetTotalAssets_Inventory
        {
            get
            {
                return (IncomeStatement_CostOfGoodsSold * BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold);
            }
        }


        public decimal BalanceSheetTotalAssets_OtherCurrentAssets
        {
            get
            {
                return IncomeStatement_CostOfGoodsSold * BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold;
            }
        }



        public decimal BalanceSheetEquityAndLiabilities_AccountsPayable
        {
            get
            {
                return IncomeStatement_CostOfGoodsSold *
                    BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold;
            }
        }


        public decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities
        {
            get
            {
                return IncomeStatement_CostOfGoodsSold *
                    BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold;
            }
        }

        #endregion
        #region Balance Sheet Analysis
        public decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue { get; set; }

        public decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold { get; set; }

        public decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold { get; set; }

        public decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold { get; set; }

        public decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold { get; set; }

        public decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays
        {
            get
            {
                return ((PriorFY.GetBalanceSheetTotalAssets_AccountsReceiveable() + BalanceSheetTotalAssets_AccountsReceiveable) / 2) / (IncomeStatement_Revenue / 365);
            }
        }

        public decimal BalanceSheetAnalysis_AverageInventoryDays
        {
            get
            {
                try
                {
                    return (((PriorFY.GetBalanceSheetTotalAssets_Inventory() + BalanceSheetTotalAssets_Inventory) / 2) / (IncomeStatement_CostOfGoodsSold / 365));
                }
                catch (DivideByZeroException)
                {
                    return 0;
                }
            }

        }
        public decimal BalanceSheetAnalysis_AverageAccountsPayableDays
        {
            get
            {
                try
                {
                    return ((PriorFY.GetBalanceSheetEquityAndLiabilities_AccountsPayable() + BalanceSheetEquityAndLiabilities_AccountsPayable) / 2) / (IncomeStatement_CostOfGoodsSold / 365);
                }
                catch (DivideByZeroException)
                {
                    return 0;
                }
            }

        }
        #endregion

        #region CapitalExpenditure

        public decimal CapitalExpenditure_CapitalExpenditure
        {
            get
            {
                return IncomeStatement_Revenue * CapitalExpenditure_PercentageForecastCapitalExpenditure;
            }
        }


        public decimal CapitalExpenditure_PercentageForecastCapitalExpenditure { get; set; }

        #endregion

        #endregion











        #region Valuation

        #region Net Working Capital

        public override decimal GetBalanceSheetTotalAssets_CurrentAssets()
        {
            return BalanceSheetTotalAssets_CurrentAssets;
        }
        public decimal BalanceSheetTotalAssets_CurrentAssets { get { return GetBalanceSheetTotalAssets_AccountsReceiveable() + GetBalanceSheetTotalAssets_Inventory() + GetBalanceSheetTotalAssets_OtherCurrentAssets() + GetBalanceSheetTotalAssets_CashAndCashEquivalents(); } }



        public decimal NetWorkingCapital_NetWorkingCapital
        {
            get
            {
                return NetWorkingCapital_CurrentAssets - GetBalanceSheetEquityAndLiabilities_CurrentLiabilities();
            }
        }


        public decimal NetWorkingCapital_ChangeInNetWorkingCapital
        {
            get
            {
                var priorNetWorkingCapital_NetWorkingCapital = (PriorFY as FinancialYearActualCurrent)?.NetWorkingCapital_NetWorkingCapital ?? (PriorFY as FinancialYearProjected)?.NetWorkingCapital_NetWorkingCapital;

                if (!priorNetWorkingCapital_NetWorkingCapital.HasValue)
                    throw new NullReferenceException("NetWorkingCapital_ChangeInNetWorkingCapital");

                return (decimal)-1 * (NetWorkingCapital_NetWorkingCapital - priorNetWorkingCapital_NetWorkingCapital.Value);
            }
        }

        #endregion



        #region Discounted Cash Flow

        public decimal DiscountedCashFlow_IncomeStatement_EBIT { get { return IncomeStatement_EBIT; } }
        public decimal DiscountedCashFlow_IncomeStatement_Tax { get { return DiscountedCashFlow_IncomeStatement_EBIT * FinancialValuation.Beta_MarginalTaxRate * -1; } }

        public decimal DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense { get { return IncomeStatement_DepreciationAndAmortisationExpense; } }
        public decimal DiscountedCashFlow_IncomeStatement_NonCashExpense { get { return IncomeStatement_NonCashExpense; } }
        public decimal DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure { get { return CapitalExpenditure_CapitalExpenditure * -1; } }

        public decimal DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital { get { return NetWorkingCapital_ChangeInNetWorkingCapital; } }



        public decimal DiscountedCashFlow_FreeCashFlow
        {
            get
            {
                return DiscountedCashFlow_IncomeStatement_EBIT + DiscountedCashFlow_IncomeStatement_Tax + DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense + DiscountedCashFlow_IncomeStatement_NonCashExpense + DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure + DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital;
            }
        }


        protected virtual decimal GetDiscountedCashFlow_AdjustedFreeCashFlow()
        {
            return DiscountedCashFlow_FreeCashFlow;
        }

        public decimal DiscountedCashFlow_AdjustedFreeCashFlow
        {
            get
            {
                return GetDiscountedCashFlow_AdjustedFreeCashFlow();
            }
        }

        protected abstract decimal GetDiscountedCashFlow_DiscountPeriodYearEnd();
        protected abstract decimal GetDiscountedCashFlow_DiscountPeriodMidYear();

        public decimal DiscountedCashFlow_DiscountPeriodYearEnd
        {
            get
            {
                return GetDiscountedCashFlow_DiscountPeriodYearEnd();
            }
        }

        public decimal DiscountedCashFlow_DiscountPeriodMidYear
        {
            get
            {
                return GetDiscountedCashFlow_DiscountPeriodMidYear();
            }
        }

        public decimal DiscountedCashFlow_DiscountFactor
        {
            get
            {
                return (decimal)Math.Pow(1 + (double)FinancialValuation.WaccCalculation_Wacc, (double)DiscountedCashFlow_DiscountPeriodMidYear);
            }
        }
        public decimal DiscountedCashFlow_DiscountFactorInverse
        {
            get
            {
                return 1 / DiscountedCashFlow_DiscountFactor;
            }
        }


        public Decimal DiscountedCashFlow_DiscountedFreeCashFlow { get { return DiscountedCashFlow_AdjustedFreeCashFlow * DiscountedCashFlow_DiscountFactorInverse; } }


        #endregion




        #endregion

        public decimal DiscountedCashFlowTerminalMultiple_AdjustedFreeCashFlow { get { return DiscountedCashFlow_AdjustedFreeCashFlow; } }
        public decimal DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year { get { return DiscountedCashFlow_DiscountPeriodMidYear; } }

        public decimal DiscountedCashFlowTerminalMultiple_DiscountFactor { get { return (decimal)Math.Pow((double)((decimal)1 + FinancialValuation.WaccCalculation_Wacc), (double)DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year); } }
        public decimal DiscountedCashFlowTerminalMultiple_DiscountFactorInverse { get { return (decimal)1 / DiscountedCashFlowTerminalMultiple_DiscountFactor; } }
        public decimal DiscountedCashFlowTerminalMultiple_FreeCashFlow { get { return DiscountedCashFlowTerminalMultiple_AdjustedFreeCashFlow; } }
        public decimal DiscountedCashFlowTerminalMultiple_DiscountedFreeCashFlow { get { return DiscountedCashFlowTerminalMultiple_FreeCashFlow * DiscountedCashFlowTerminalMultiple_DiscountFactorInverse; } }


    }
}
