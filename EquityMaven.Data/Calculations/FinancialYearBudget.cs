﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.FinancialYears;
namespace EquityMaven.Data.Calculations
{



    public class FinancialYearBudget : FinancialYearProjected, IFinancialYearBudget
    {
        public FinancialYearBudget(FinancialValuation fv, IFullValuationInputYear from) : base(fv, from)
        {
            this.SustainableEBITDA_NonRecurringExpenses = from.SustainableEBITDA_NonRecurringExpenses;
            this.SustainableEBITDA_NonRecurringIncome = from.SustainableEBITDA_NonRecurringIncome;

        }

        public void ApplyValues(IFullValuationInputYear fviy)
        {
            var to = fviy as FullValuationInputYear;
            if (to == null)
                throw new ArgumentNullException($"to {fviy}");
            ApplyProjectedValues(to);
            to.SustainableEBITDA_SustainableEBITDA = this.SustainableEBITDA_SustainableEBITDA;
            to.SustainableEBITDA_NonRecurringExpenses = this.SustainableEBITDA_NonRecurringExpenses;
            to.SustainableEBITDA_NonRecurringIncome = this.SustainableEBITDA_NonRecurringIncome;
        }

        protected override IFinancialYear GetPrior()
        {
            return Prior;
        }

        IFinancialYearActualCurrent Prior { get { return FinancialValuation.iActualCurrent; } }
        IFinancialYearForecast1 Next { get { return FinancialValuation.iForecast1; } }


        protected override decimal GetDiscountedCashFlow_DiscountPeriodYearEnd()
        {
            return ((decimal)12 - (decimal)FinancialValuation.MonthsSinceLastFinancialYearEnd) / (decimal)12;
        }

        protected override decimal GetDiscountedCashFlow_DiscountPeriodMidYear()
        {
            return DiscountedCashFlow_DiscountPeriodYearEnd / (decimal)2;
        }


        #region Sustainable EBITDA
        /// <summary>
        ///  These are unsual once-off expenses not in the normal course of business that are not expected to be repeated e.g. office renovations, losses from sale of operating assets etc.
        /// </summary>
        public decimal SustainableEBITDA_NonRecurringExpenses { get; set; }
        /// <summary>
        /// This is unsual once-off income not in the normal course of business that is not expected to be repeated e.g. unsual and non-recurring sales (e.g. sale of a patent), profits from sale of operating assets etc.
        /// </summary>
        public decimal SustainableEBITDA_NonRecurringIncome { get; set; }

        public decimal SustainableEBITDA_SustainableEBITDA
        {
            get
            {
                return (IncomeStatement_EBITDA + SustainableEBITDA_NonRecurringExpenses - SustainableEBITDA_NonRecurringIncome);
            }
        }
        #endregion
        #region Valuation

        #region Net Working Capital




        #endregion



        #region Projected Cash Flow
        public decimal DiscountedCashFlow_DiscountPeriodYear_End
        {
            get
            {
                return ((decimal)12 - (decimal)FinancialValuation.MonthsSinceLastFinancialYearEnd) / (decimal)12;
            }
        }
        public decimal DiscountedCashFlow_DiscountPeriodMid_Year
        {
            get
            {
                return DiscountedCashFlow_DiscountPeriodYear_End / (decimal)2;
            }
        }


        public decimal DiscountedCashFlow_DiscountFactor
        {
            get
            {
                var result = Math.Pow((double)(1 + FinancialValuation.WaccCalculation_Wacc), (double)(DiscountedCashFlow_DiscountPeriodMid_Year));
                return (decimal)result;
            }
        }

        public decimal DiscountedCashFlow_DiscountFactorInverse { get { return (decimal)1 / DiscountedCashFlow_DiscountFactor ; } }

        public decimal DiscountedCashFlow_DiscountedFreeCashFlow { get { return DiscountedCashFlow_AdjustedFreeCashFlow * DiscountedCashFlow_DiscountFactorInverse; } }

        protected override decimal GetDiscountedCashFlow_AdjustedFreeCashFlow()
        {
            return DiscountedCashFlow_FreeCashFlow + DiscountedCashFlow_AdjustmentForFirstYear;
        }


        public decimal DiscountedCashFlow_AdjustmentForFirstYear
        {
            get
            {
                return (decimal)-1 * ((decimal)FinancialValuation.MonthsSinceLastFinancialYearEnd / (decimal)12) * DiscountedCashFlow_FreeCashFlow;
            }
        }

        #endregion



        #endregion

       
    }
}
