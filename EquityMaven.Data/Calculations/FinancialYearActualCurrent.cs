﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.FinancialYears;
namespace EquityMaven.Data.Calculations
{

    public class FinancialYearActualCurrent : FinancialYearActual, IFinancialYearActualCurrent
    {
        public FinancialYearActualCurrent(FinancialValuation fv, IFullValuationInputYear from) : base(fv, from)
        {
            this.SustainableEBITDA_NonRecurringExpenses = from.SustainableEBITDA_NonRecurringExpenses;
            this.SustainableEBITDA_NonRecurringIncome = from.SustainableEBITDA_NonRecurringIncome;
        }

        public void ApplyValues(IFullValuationInputYear fvyi)
        {
            var to = fvyi as FullValuationInputYear;
            if ((to == null))
                throw new ArgumentNullException($"to {fvyi}");

            ApplyActualValues(to);
            to.BalanceSheetAnalysis_AverageAccountsPayableDays = this.BalanceSheetAnalysis_AverageAccountsPayableDays;
            to.BalanceSheetAnalysis_AverageAccountsReceiveableDays = this.BalanceSheetAnalysis_AverageAccountsReceiveableDays;
            to.BalanceSheetAnalysis_AverageInventoryDays = this.BalanceSheetAnalysis_AverageInventoryDays;
            to.ExcessCash_ExcessCash = this.ExcessCash_ExcessCash;
            to.ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue = this.ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue;
            to.ExcessCash_RevenueTTM = this.ExcessCash_RevenueTTM;
            to.SustainableEBITDA_SustainableEBITDA = this.SustainableEBITDA_SustainableEBITDA;
            to.SustainableEBITDA_SustainableEBITDA_TTM = this.SustainableEBITDA_SustainableEBITDA_TTM;
            to.CapitalExpenditure_TTM = this.CapitalExpenditure_CapitalExpenditureTTM;
            to.IncomeStatementAnalysis_PercentageRevenueGrowth = this.IncomeStatementAnalysis_PercentageRevenueGrowth;
            to.SustainableEBITDA_NonRecurringExpenses = this.SustainableEBITDA_NonRecurringExpenses;
            to.SustainableEBITDA_NonRecurringIncome = this.SustainableEBITDA_NonRecurringIncome;
        }




        public IFinancialYearActual Prior { get { return FinancialValuation.iActualPrior; } }
        public IFinancialYearBudget Next { get { return FinancialValuation.iBudget; } }


        #region Client Inputs
        public Decimal IncomeStatementAnalysis_PercentageRevenueGrowth
        {
            get
            {
                return (IncomeStatement_Revenue / Prior.IncomeStatement_Revenue) - 1;
            }
        }

        public decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays
        {
            get
            {
                return ((Prior.BalanceSheetTotalAssets_AccountsReceiveable + BalanceSheetTotalAssets_AccountsReceiveable) / (decimal)2) / (IncomeStatement_Revenue / (decimal)365);
            }
        }

        public decimal BalanceSheetAnalysis_AverageInventoryDays
        {
            get
            {
                try
                {
                    return (((Prior.BalanceSheetTotalAssets_Inventory + BalanceSheetTotalAssets_Inventory) / (decimal)2) / (IncomeStatement_CostOfGoodsSold / (decimal)365));
                }
                catch (DivideByZeroException)
                {
                    return 0;
                }
            }

        }
        public decimal BalanceSheetAnalysis_AverageAccountsPayableDays
        {
            get
            {
                return ((Prior.BalanceSheetEquityAndLiabilities_AccountsPayable + BalanceSheetEquityAndLiabilities_AccountsPayable) / (decimal)2) / (IncomeStatement_CostOfGoodsSold / (decimal)365);
            }

        }


        public decimal CapitalExpenditure_CapitalExpenditureTTM
        {
            get
            {
                return ((decimal)((decimal)12 - (decimal)FinancialValuation.MonthsSinceLastFinancialYearEnd) / (decimal)12 * this.CapitalExpenditure_CapitalExpenditure) + (((decimal)FinancialValuation.MonthsSinceLastFinancialYearEnd / (decimal)12) * Next.CapitalExpenditure_CapitalExpenditure);
            }
        }

        public decimal ExcessCash_RevenueTTM
        {
            get
            {
                return (((decimal)12 - (decimal)FinancialValuation.MonthsSinceLastFinancialYearEnd) / (decimal)12 * IncomeStatement_Revenue) + (((decimal)FinancialValuation.MonthsSinceLastFinancialYearEnd) / (decimal)12 * Next.IncomeStatement_Revenue);
            }
        }


        public decimal ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue
        {
            get
            {
                return FinancialValuation.PercentageOperatingCashRequired * ExcessCash_RevenueTTM;
            }
        }
        public decimal ExcessCash_ExcessCash
        {
            get
            {
                return ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue < FinancialValuation.CurrentCashOnHand ? (FinancialValuation.CurrentCashOnHand - ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue) : 0;
            }
        }

        #region Sustainable EBITDA
        /// <summary>
        ///  These are unsual once-off expenses not in the normal course of business that are not expected to be repeated e.g. office renovations, losses from sale of operating assets etc.
        /// </summary>
        public decimal SustainableEBITDA_NonRecurringExpenses { get; set; }
        /// <summary>
        /// This is unsual once-off income not in the normal course of business that is not expected to be repeated e.g. unsual and non-recurring sales (e.g. sale of a patent), profits from sale of operating assets etc.
        /// </summary>
        public decimal SustainableEBITDA_NonRecurringIncome { get; set; }

        public decimal SustainableEBITDA_SustainableEBITDA
        {
            get
            {
                return (IncomeStatement_EBITDA + SustainableEBITDA_NonRecurringExpenses - SustainableEBITDA_NonRecurringIncome);
            }
        }

        public decimal SustainableEBITDA_SustainableEBITDA_TTM
        {
            get
            {
                return (((decimal)12 - (decimal)FinancialValuation.MonthsSinceLastFinancialYearEnd) / (decimal)12 * SustainableEBITDA_SustainableEBITDA) + (((decimal)FinancialValuation.MonthsSinceLastFinancialYearEnd) / (decimal)12 * Next.SustainableEBITDA_SustainableEBITDA);
            }
        }

        #endregion
        #endregion


        #region Valuation

        #region Net Working Capital
        public decimal NetWorkingCapital_ChangeInNetWorkingCapital
        {
            get
            {
                if (Prior == null)
                    throw new NullReferenceException("NetWorkingCapital_ChangeInNetWorkingCapital");
                return (decimal)-1 * (NetWorkingCapital_NetWorkingCapital - Prior.NetWorkingCapital_NetWorkingCapital);
            }
        }
        #endregion

       
        #region Discounted Cash Flow

        public decimal DiscountedCashFlow_IncomeStatement_EBIT { get { return IncomeStatement_EBIT; } }
        public decimal DiscountedCashFlow_IncomeStatement_Tax { get { return DiscountedCashFlow_IncomeStatement_EBIT * FinancialValuation.Beta_MarginalTaxRate * (decimal)-1; } }

        public decimal DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense { get { return IncomeStatement_DepreciationAndAmortisationExpense; } }
        public decimal DiscountedCashFlow_IncomeStatement_NonCashExpense { get { return IncomeStatement_NonCashExpense; } }
        public decimal DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure { get { return CapitalExpenditure_CapitalExpenditure * (decimal)-1; } }

        public decimal DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital { get { return NetWorkingCapital_ChangeInNetWorkingCapital; } }

   

        public decimal DiscountedCashFlow_FreeCashFlow
        {
            get
            {
                return DiscountedCashFlow_IncomeStatement_EBIT + DiscountedCashFlow_IncomeStatement_Tax + DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense + DiscountedCashFlow_IncomeStatement_NonCashExpense + DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure + DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital;
            }
        }


        public decimal DiscountedCashFlow_AdjustmentForFirstYear
        {
            get
            {
                  return (decimal)-1 * (FinancialValuation.MonthsSinceLastFinancialYearEnd / (decimal)12) * DiscountedCashFlow_FreeCashFlow;
            }
        }

        public decimal DiscountedCashFlow_AdjustedFreeCashFlow
        {
            get
            {
                    return DiscountedCashFlow_FreeCashFlow;
            }
        }


        #endregion


        #endregion




        #region liquidation value
        public decimal Liquidationvalue_Revenue_Ttm {get { return ExcessCash_RevenueTTM;}}
        public decimal Liquidationvalue_CostOfGoodsSold_Ttm {get {return ((decimal)FinancialValuation.MonthsSinceLastFinancialYearEnd/(decimal)12) * IncomeStatement_CostOfGoodsSold + (((decimal)12 - (decimal)FinancialValuation.MonthsSinceLastFinancialYearEnd)/(decimal)12*Next.IncomeStatement_CostOfGoodsSold);}}
        
     
        public decimal Liquidationvalue_Capex_Ttm {get {return CapitalExpenditure_CapitalExpenditureTTM;}}
        public decimal Liquidationvalue_DepreciationAmortisation_Ttm {get {return ((decimal) FinancialValuation.MonthsSinceLastFinancialYearEnd / (decimal)12) * IncomeStatement_DepreciationAndAmortisationExpense + (((decimal)12 - (decimal)FinancialValuation.MonthsSinceLastFinancialYearEnd )/(decimal)12*Next.IncomeStatement_DepreciationAndAmortisationExpense) ;}}
        public decimal Liquidationvalue_CurrentAssets {get {return Liquidationvalue_AccountsReceiveable + Liquidationvalue_Inventory + Liquidationvalue_OtherCurrentAssets + Liquidationvalue_CashAndCashEquivalents; }}
        public decimal Liquidationvalue_AccountsReceiveable {get {return Liquidationvalue_Revenue_Ttm  * BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue;}}
        public decimal Liquidationvalue_Inventory {get {return Liquidationvalue_CostOfGoodsSold_Ttm * BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold; }}
        public decimal Liquidationvalue_OtherCurrentAssets {get {return Liquidationvalue_CostOfGoodsSold_Ttm * BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold; }}
        public decimal Liquidationvalue_CashAndCashEquivalents {get {return FinancialValuation.All3rdPartyInterestBearingDebtAsAtLastMonth_End;}}
        public decimal Liquidationvalue_IntangibleAssets {get { return BalanceSheetTotalAssets_IntangibleAssets; }}
        public decimal Liquidationvalue_FixedAssets {get { return BalanceSheetTotalAssets_FixedAndIntangibleAssets + Liquidationvalue_Capex_Ttm - Liquidationvalue_DepreciationAmortisation_Ttm; }}
        public decimal Liquidationvalue_TotalAssets {get {return (decimal)Liquidationvalue_IntangibleAssets + Liquidationvalue_FixedAssets + Liquidationvalue_CurrentAssets; }}
         
        public decimal Liquidationvalue_CurrentLiabilities {get {return Liquidationvalue_AccountsPayable + Liquidationvalue_OtherCurrentLiabilities; }}
        public decimal Liquidationvalue_AccountsPayable {get { return Liquidationvalue_CostOfGoodsSold_Ttm * BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold; ; }}
        public decimal Liquidationvalue_OtherCurrentLiabilities {get { return Liquidationvalue_CostOfGoodsSold_Ttm * BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold; } }
        public decimal Liquidationvalue_3rdPartyInterest_BearingDebtLongTermShortTerm {get { return FinancialValuation.All3rdPartyInterestBearingDebtAsAtLastMonth_End; } }
        public decimal Liquidationvalue_OtherLong_TermLiabilities {get {return BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities;}}
        public decimal Liquidationvalue_ShareholderLoans {get {return BalanceSheetEquityAndLiabilities_ShareholderLoans; }}
        public decimal Liquidationvalue_TotalLiabilities {get { return Liquidationvalue_CurrentLiabilities + Liquidationvalue_3rdPartyInterest_BearingDebtLongTermShortTerm + Liquidationvalue_OtherLong_TermLiabilities; }}
        #endregion


    }
}
