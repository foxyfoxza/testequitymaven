﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.FinancialYears;
namespace EquityMaven.Data.Calculations
{



    public class FinancialYearForecast2 : FinancialYearProjected, IFinancialYearForecast2
    {
        public FinancialYearForecast2(FinancialValuation fv, IFullValuationInputYear from) : base(fv, from)
        {
        }

        public void ApplyValues(IFullValuationInputYear fviy)
        {
            var to = fviy as FullValuationInputYear;
            if (to == null)
                throw new ArgumentNullException($"to {fviy}");
            ApplyProjectedValues(to);
            
        }
        protected override IFinancialYear GetPrior()
        {
            return Prior;
        }
        IFinancialYearForecast1 Prior { get { return FinancialValuation.iForecast1; } }

        protected override decimal GetDiscountedCashFlow_DiscountPeriodYearEnd()
        {
            var fyForecast1 = Prior as IFinancialYearForecast1;
            if (fyForecast1 == null)
                throw new NullReferenceException("GetDiscountedCashFlow_DiscountPeriodYearEnd");
            return fyForecast1.DiscountedCashFlow_DiscountPeriodYearEnd + 1;
        }

        protected override decimal GetDiscountedCashFlow_DiscountPeriodMidYear()
        {
            return DiscountedCashFlow_DiscountPeriodYearEnd - (decimal)0.5;
        }

        #region Valuation

        #region Net Working Capital




        #endregion


        #region Projected Cash Flow
        public decimal DiscountedCashFlow_DiscountPeriodYear_End
        {
            get
            {
                if (Prior == null)
                    throw new NullReferenceException($"DiscountedCashFlow_DiscountPeriodYear_End {FinancialYearType}");
                return Prior.DiscountedCashFlow_DiscountPeriodYear_End + 1;
            }
        }
        public decimal DiscountedCashFlow_DiscountPeriodMid_Year
        {
            get
            {
                return DiscountedCashFlow_DiscountPeriodYear_End - (decimal)0.5;
            }
        }


        public decimal DiscountedCashFlow_DiscountFactor
        {
            get
            {
                var result = Math.Pow((double)(1 + FinancialValuation.WaccCalculation_Wacc), (double)(DiscountedCashFlow_DiscountPeriodMid_Year));
                return (decimal)result;
            }
        }

        public decimal DiscountedCashFlow_DiscountFactorInverse { get { return (decimal)1 / DiscountedCashFlow_DiscountFactor; } }

        public decimal DiscountedCashFlow_DiscountedFreeCashFlow { get { return DiscountedCashFlow_FreeCashFlow * DiscountedCashFlow_DiscountFactorInverse; } }



        #endregion



        #endregion


        #region VC Method

        //public decimal VCMethod_EVOverEBITDA { get { return FinancialValuation.EvOverEbitdaValuation_WorldEv_Over_Ebitda; } }

        public decimal VCMethod_EnterpriseValue { get { return IncomeStatement_EBITDA * FinancialValuation.WorldMedian_World; } }
        public decimal VCMethod_VCRequiredRateOfReturn
        {
            get
            {
                return (decimal) Math.Pow(((double)1 + (double)FinancialValuation.VCDiscountRate), ((double)FinancialValuation.LastFinancialYearEnd.AddYears(3).Subtract(FinancialValuation.ValuationDate).TotalDays) / (double)365.0);
            }
        }
        public decimal VCMethod_VCMethodEnterpriseValue { get { return  VCMethod_EnterpriseValue / VCMethod_VCRequiredRateOfReturn; } }
       
        public decimal VCMethod_VCMethodEquityValue { get { return VCMethod_VCMethodEnterpriseValue + FinancialValuation.iActualCurrent.ExcessCash_ExcessCash + ((decimal)-1 * FinancialValuation.All3rdPartyInterestBearingDebtAsAtLastMonth_End); } }


        #endregion


    }
}
