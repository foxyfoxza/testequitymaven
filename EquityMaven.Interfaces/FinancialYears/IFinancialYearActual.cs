﻿using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EquityMaven.Interfaces.FinancialYears
{
    public interface IFinancialYearActual : IFinancialYearValues
    {
        

        #region Net Working Capital


        decimal NetWorkingCapital_NetWorkingCapital { get; }
        decimal BalanceSheetTotalAssets_CurrentAssets { get;}
        #endregion
        #region Income Statement

        new decimal IncomeStatement_Revenue { get; set; }

        new decimal IncomeStatement_CostOfGoodsSold { get; set; }

        new decimal IncomeStatement_OperatingExpense { get; set; }
        new decimal IncomeStatement_DepreciationAndAmortisationExpense { get; set; }

        new decimal IncomeStatement_InterestExpense { get; set; }

        new decimal IncomeStatement_NonCashExpense { get; set; }

        #endregion

        
        #region Balance Sheet Total Assets
        new decimal BalanceSheetTotalAssets_AccountsReceiveable { get; set; }
        new decimal BalanceSheetTotalAssets_Inventory { get; set; }
        new decimal BalanceSheetTotalAssets_OtherCurrentAssets { get; set; }

        decimal BalanceSheetTotalAssets_CashAndCashEquivalents { get; }

        decimal BalanceSheetTotalAssets_FixedAndIntangibleAssets { get; }

        decimal BalanceSheetTotalAssets_IntangibleAssets { get; set; }

        decimal BalanceSheetTotalAssets_FixedAssets { get; set; }
        decimal BalanceSheetTotalAssets_TotalAssets { get; }

        #endregion


        #region Balance Sheet Equity And Liabilities

        decimal BalanceSheetEquityAndLiabilities_CurrentLiabilities { get; }

        new decimal BalanceSheetEquityAndLiabilities_AccountsPayable { get; set; }
        new decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities { get; set; }

        decimal BalanceSheetEquityAndLiabilities_ShortTermDebt { get; set; }
        decimal BalanceSheetEquityAndLiabilities_LongTermDebt { get; set; }
        decimal BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities { get; set; }
        decimal BalanceSheetEquityAndLiabilities_ShareholderLoans { get; set; }
        decimal BalanceSheetShareholdersEquity { get; }

        #endregion


        decimal BalanceSheetAnalysis_CurrentRatio { get; }
        decimal BalanceSheetAnalysis_TotalDebtOverEBITDA { get; }


        #region CapitalExpenditure
        new decimal CapitalExpenditure_CapitalExpenditure { get; set; }

        #endregion

     

    }
}
