﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.Calculations;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.FinancialYears
{

    public interface IFinancialYear
    {
        decimal BalanceSheetEquityAndLiabilities_TotalLiabilities { get; }
        decimal BalanceSheetTotalAssets_TotalAssets { get; }
        IFinancialValuation FinancialValuation { get; set; }
       
        decimal IncomeStatementAnalysis_EBITDAMargin { get; }
        decimal IncomeStatementAnalysis_EBITMargin { get; }
        decimal IncomeStatement_EarningsBeforeTax { get; }
        decimal IncomeStatement_EBIT { get; }
        decimal IncomeStatement_EBITDA { get; }
        decimal IncomeStatement_GrossProfit { get; }
        decimal NetWorkingCapital_CurrentAssets { get; }

        decimal GetBalanceSheetEquityAndLiabilities_AccountsPayable();
        decimal GetBalanceSheetEquityAndLiabilities_CurrentLiabilities();
        decimal GetBalanceSheetEquityAndLiabilities_LongTermDebt();
        decimal GetBalanceSheetEquityAndLiabilities_OtherLongTermLiabilities();
        decimal GetBalanceSheetEquityAndLiabilities_ShareholderLoans();
        decimal GetBalanceSheetTotalAssets_AccountsReceiveable();
        decimal GetBalanceSheetTotalAssets_CashAndCashEquivalents();
        decimal GetBalanceSheetTotalAssets_CurrentAssets();
        decimal GetBalanceSheetTotalAssets_FixedAndIntangibleAssets();
        decimal GetBalanceSheetTotalAssets_FixedAssets();
        decimal GetBalanceSheetTotalAssets_IntangibleAssets();
        decimal GetBalanceSheetTotalAssets_Inventory();
        decimal GetBalanceSheetTotalAssets_OtherCurrentAssets();
        decimal GetIncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue();
        decimal GetIncomeStatement_CostOfGoodsSold();
        decimal GetIncomeStatement_DepreciationAndAmortisationExpense();
        decimal GetIncomeStatement_GrossProfit();
        decimal GetIncomeStatement_InterestExpense();
        decimal GetIncomeStatement_OperatingExpense();
        decimal GetIncomeStatement_Revenue();
       
    }
     
}
