﻿using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.FinancialYears
{
    public interface IFinancialYearActualPrior : IFinancialYearActual
    {
        decimal IncomeStatementAnalysis_InterestCover { get; }
        void ApplyValues(IFullValuationInputYear _fyi);

    }
}
