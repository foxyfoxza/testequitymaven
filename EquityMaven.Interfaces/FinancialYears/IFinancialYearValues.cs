﻿
using EquityMaven.CommonEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.FinancialYears
{
    public interface IFinancialYearValues: IFinancialYear
    {
 
        #region Income Statement
        decimal IncomeStatement_Revenue { get; }
        decimal IncomeStatement_CostOfGoodsSold { get; }
        decimal IncomeStatement_GrossProfit { get; }
        decimal IncomeStatement_OperatingExpense { get; }
        decimal IncomeStatement_EBITDA { get; }
        decimal IncomeStatement_DepreciationAndAmortisationExpense { get; }
        decimal IncomeStatement_EBIT { get; }
        decimal IncomeStatement_InterestExpense { get; }
        decimal IncomeStatement_EarningsBeforeTax { get; }
        decimal IncomeStatement_NonCashExpense { get; }
        decimal IncomeStatementAnalysis_PercentageGrossProfitMargin { get; }
        decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue { get; }
        decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue { get; }
        decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue { get; }
        decimal IncomeStatementAnalysis_EBITDAMargin { get; }
        decimal IncomeStatementAnalysis_EBITMargin { get; }
        #endregion


        #region Balance Sheet Total Assets
        decimal BalanceSheetTotalAssets_AccountsReceiveable { get; }
        decimal BalanceSheetTotalAssets_Inventory { get; }
        decimal BalanceSheetTotalAssets_OtherCurrentAssets { get; }

        #endregion


        #region Balance Sheet Equity And Liabilities



        decimal BalanceSheetEquityAndLiabilities_AccountsPayable { get; }
        decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities { get; }

        decimal BalanceSheetEquityAndLiabilities_TotalLiabilities { get; }

        #endregion


        #region BalanceSheetAnalysis

        decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue { get; }

        decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold { get; }
        decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold { get; }

        decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold { get; }


        decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold { get; }




        #endregion



        #region CapitalExpenditure

        decimal CapitalExpenditure_CapitalExpenditure { get; }

        decimal CapitalExpenditure_PercentageForecastCapitalExpenditure { get; }
        #endregion

        #region Assets Projection 
        decimal AssetsProjection_FixedAndIntangibleAssets { get; }

        #endregion



    }
}
