﻿using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.FinancialYears
{
    public interface IFinancialYearBudget: IFinancialYearProjected
    {
        decimal DiscountedCashFlow_AdjustmentForFirstYear { get; }
        decimal DiscountedCashFlow_DiscountPeriodMid_Year { get; }
        decimal DiscountedCashFlow_DiscountPeriodYear_End { get; }
        decimal SustainableEBITDA_NonRecurringExpenses { get; set; }
        decimal SustainableEBITDA_NonRecurringIncome { get; set; }
        decimal SustainableEBITDA_SustainableEBITDA { get; }

        void ApplyValues(IFullValuationInputYear _fyi);
    }
     
}
