﻿using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.FinancialYears
{
    public interface IFinancialYearForecast2 : IFinancialYearProjected 
      
    {
        void ApplyValues(IFullValuationInputYear _fyi);
        decimal DiscountedCashFlow_DiscountPeriodMid_Year { get; }
        decimal DiscountedCashFlow_DiscountPeriodYear_End { get; }



        #region VC Method

        //decimal VCMethod_EVOverEBITDA { get; }//World EV/EBITDA

        decimal VCMethod_EnterpriseValue { get; }
        decimal VCMethod_VCRequiredRateOfReturn{ get; }
        decimal VCMethod_VCMethodEnterpriseValue { get; }
        //decimal VCMethod_ExcessCash { get; }
        decimal VCMethod_VCMethodEquityValue { get; }



        #endregion

       
    }

}
