﻿using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.FinancialYears
{
    public interface IFinancialYearForecast1 : IFinancialYearProjected
    {
        void ApplyValues(IFullValuationInputYear _fyi);
        decimal DiscountedCashFlow_DiscountPeriodMid_Year { get; }
        decimal DiscountedCashFlow_DiscountPeriodYear_End { get; }
    }
}
