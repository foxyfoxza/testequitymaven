﻿using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.FinancialYears
{
    public interface IFinancialYearActualCurrent : IFinancialYearActual
    {
        void ApplyValues(IFullValuationInputYear _fyi);
        //IFinancialYear Next { get; }
        decimal DiscountedCashFlow_FreeCashFlow { get; }
        decimal IncomeStatementAnalysis_InterestCover { get; }
        decimal NetWorkingCapital_ChangeInNetWorkingCapital { get; }
        decimal IncomeStatementAnalysis_PercentageRevenueGrowth { get; }

        decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays { get; }

        decimal BalanceSheetAnalysis_AverageInventoryDays { get; }
        decimal BalanceSheetAnalysis_AverageAccountsPayableDays { get; }



        decimal ExcessCash_RevenueTTM { get; }


        decimal ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue { get; }
        decimal ExcessCash_ExcessCash { get; }

        #region Sustainable EBITDA
        decimal SustainableEBITDA_NonRecurringExpenses { get; set; }
        decimal SustainableEBITDA_NonRecurringIncome { get; set; }

        decimal SustainableEBITDA_SustainableEBITDA { get; }

        decimal SustainableEBITDA_SustainableEBITDA_TTM { get; }
        #endregion

        #region CapitalExpenditure

        decimal CapitalExpenditure_CapitalExpenditureTTM { get; }


        #endregion


        #region liquidation value

            decimal Liquidationvalue_Revenue_Ttm {get;}
            decimal Liquidationvalue_CostOfGoodsSold_Ttm {get;}
            decimal Liquidationvalue_Capex_Ttm {get;}
            decimal Liquidationvalue_DepreciationAmortisation_Ttm {get;}
            decimal Liquidationvalue_CurrentAssets {get;}
            decimal Liquidationvalue_AccountsReceiveable {get;}
            decimal Liquidationvalue_Inventory {get;}
            decimal Liquidationvalue_OtherCurrentAssets {get;}
            decimal Liquidationvalue_CashAndCashEquivalents {get;}
            decimal Liquidationvalue_IntangibleAssets {get;}
            decimal Liquidationvalue_FixedAssets {get;}
            decimal Liquidationvalue_TotalAssets {get;}
            decimal Liquidationvalue_CurrentLiabilities {get;}
            decimal Liquidationvalue_AccountsPayable {get;}
            decimal Liquidationvalue_OtherCurrentLiabilities {get;}
            decimal Liquidationvalue_3rdPartyInterest_BearingDebtLongTermShortTerm {get;}
            decimal Liquidationvalue_OtherLong_TermLiabilities {get;}
            decimal Liquidationvalue_ShareholderLoans {get;}
            decimal Liquidationvalue_TotalLiabilities {get;}

        #endregion


    }
}
