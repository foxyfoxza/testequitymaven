﻿using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.FinancialYears
{
    public interface IFinancialYearTerminal
     
    {
        decimal DiscountedCashFlow_AdjustedFreeCashFlow { get; }
        decimal DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure { get; }
        decimal DiscountedCashFlow_DiscountedFreeCashFlow { get; }
        decimal DiscountedCashFlow_DiscountFactor { get; }
        decimal DiscountedCashFlow_DiscountFactorInverse { get; }
        decimal DiscountedCashFlow_DiscountPeriodMidYear { get; }
        decimal DiscountedCashFlow_DiscountPeriodYearEnd { get; }
        decimal DiscountedCashFlow_FreeCashFlow { get; }
        decimal DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense { get; }
        decimal DiscountedCashFlow_IncomeStatement_EBIT { get; }
        decimal DiscountedCashFlow_IncomeStatement_NonCashExpense { get; }
        decimal DiscountedCashFlow_IncomeStatement_Tax { get; }
        decimal DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital { get; }
        decimal DiscountedCashFlow_TerminalFreeCashFlow { get; }
        decimal DiscountedCashFlow_TerminalFreeCashFlowMultiple { get; }
       
        decimal NetWorkingCapital_ChangeInNetWorkingCapital { get; }

        decimal DiscountedCashFlowTerminalMultiple_Ebitda { get; }
        decimal DiscountedCashFlowTerminalMultiple_Ev_Over_Ebitda { get; }


        decimal DiscountedCashFlowTerminalMultiple_AdjustedFreeCashFlow { get; }
        decimal DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year { get; }
        decimal DiscountedCashFlowTerminalMultiple_DiscountFactor { get; }
        decimal DiscountedCashFlowTerminalMultiple_DiscountFactorInverse { get; }
        decimal DiscountedCashFlowTerminalMultiple_FreeCashFlow { get; }
        decimal DiscountedCashFlowTerminalMultiple_DiscountedFreeCashFlow { get; }



        IFinancialYearForecast2 Prior { get; }

        void ApplyValues(IFullValuationInputYear fyi);
    }
}
