﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.FinancialYears
{
    public interface IFinancialYearProjected : IFinancialYear, IFinancialYearValues
    {
        decimal AssetsProjection_FixedAndIntangibleAssets { get; }
        decimal BalanceSheetAnalysis_AverageAccountsPayableDays { get; }
        decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays { get; }
        decimal BalanceSheetAnalysis_AverageInventoryDays { get; }
        decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue { get; set; }
        decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold { get; set; }
        decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold { get; set; }
        decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold { get; set; }
        decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold { get; set; }
        decimal BalanceSheetEquityAndLiabilities_AccountsPayable { get; }
        decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities { get; }
        decimal BalanceSheetTotalAssets_AccountsReceiveable { get; }
        decimal BalanceSheetTotalAssets_CurrentAssets { get; }
        decimal BalanceSheetTotalAssets_Inventory { get; }
        decimal BalanceSheetTotalAssets_OtherCurrentAssets { get; }
        decimal CapitalExpenditure_CapitalExpenditure { get; }
        decimal CapitalExpenditure_PercentageForecastCapitalExpenditure { get; set; }
        decimal DiscountedCashFlow_AdjustedFreeCashFlow { get; }
        decimal DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure { get; }
        decimal DiscountedCashFlow_DiscountedFreeCashFlow { get; }
        decimal DiscountedCashFlow_DiscountFactor { get; }
        decimal DiscountedCashFlow_DiscountFactorInverse { get; }
        decimal DiscountedCashFlow_DiscountPeriodMidYear { get; }
        decimal DiscountedCashFlow_DiscountPeriodYearEnd { get; }
        decimal DiscountedCashFlow_FreeCashFlow { get; }
        decimal DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense { get; }
        decimal DiscountedCashFlow_IncomeStatement_EBIT { get; }
        decimal DiscountedCashFlow_IncomeStatement_NonCashExpense { get; }
        decimal DiscountedCashFlow_IncomeStatement_Tax { get; }
        decimal DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital { get; }
        decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue { get; }
        decimal IncomeStatementAnalysis_PercentageGrossProfitMargin { get; set; }
        decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue { get; set; }
        decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue { get; set; }
        decimal IncomeStatementAnalysis_PercentageRevenueGrowth { get; set; }
        decimal IncomeStatement_CostOfGoodsSold { get; }
        decimal IncomeStatement_DepreciationAndAmortisationExpense { get; }
        decimal IncomeStatement_InterestExpense { get; }
        decimal IncomeStatement_NonCashExpense { get; }
        decimal IncomeStatement_OperatingExpense { get; }
        decimal IncomeStatement_Revenue { get; }
        decimal NetWorkingCapital_ChangeInNetWorkingCapital { get; }
        decimal NetWorkingCapital_NetWorkingCapital { get; }

         
    
        decimal DiscountedCashFlowTerminalMultiple_AdjustedFreeCashFlow { get; }
        decimal DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year { get; }
        decimal DiscountedCashFlowTerminalMultiple_DiscountFactor { get; }
        decimal DiscountedCashFlowTerminalMultiple_DiscountFactorInverse { get; }
        decimal DiscountedCashFlowTerminalMultiple_FreeCashFlow { get; }
        decimal DiscountedCashFlowTerminalMultiple_DiscountedFreeCashFlow { get; }



       


        IFinancialYear PriorFY { get; }


    }
}
