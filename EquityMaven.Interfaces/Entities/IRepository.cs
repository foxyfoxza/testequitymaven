﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.Entities
{
    public interface IRepository<T> where T: IEntityItem
    {
        void AddUpdate(T item, Action OnSave);
        void AddUpdate(T item);

        void AddUpdate(IEnumerable<T> items, Action OnSave);
        void AddUpdate(IEnumerable<T> items);


        void Delete(T item);
        IQueryable<T> All();

        void Delete(object id);
        void SaveChanges();

        void Detach(T entity);
        

    }
}
