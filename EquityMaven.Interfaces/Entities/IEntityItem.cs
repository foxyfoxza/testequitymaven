﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.Entities
{
    public interface IEntityItem
    {
        DateTime Created { get; set; }
        DateTime LastUpdated { get; set; }
        bool isNew { get; }

    }

    public interface IEntityItemInt: IEntityItem
    {
        Int64 Id { get; set; }

    }

}
