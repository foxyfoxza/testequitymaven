﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces
{
    public interface IRecord
    {
        Guid Id { get; set; }
    }
}
