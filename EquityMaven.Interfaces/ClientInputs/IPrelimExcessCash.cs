﻿namespace EquityMaven.Interfaces.ClientInputs
{
    public interface IPrelimExcessCash
    {
        decimal PercentageOperatingCashRequired { get; set; }
        decimal ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue { get; }
        decimal ExcessCash_CurrentCashOnHandAsAtLastMonthEnd { get; }
        decimal ExcessCash_ExcessCash { get; }
        decimal EV_EBITDA_WorldMedian { get; }



        /// <summary>
        ///  Valuation Illiquidity Discount  
        /// </summary>
        decimal ValuationIlliquidityDiscount { get; }
        /// <summary>
        /// Enterprise Value(pre-illiquidity discount)
        /// </summary>
        decimal EV_EBITDA_Valuation_EnterpriseValuePreIlliquidityDiscount { get; }
        /// <summary>
        /// Equity Value(pre-illiquidity discount)
        /// </summary>
        decimal EV_EBITDA_Valuation_EquityValuePreIlliquidityDiscount { get; }

        /// <summary>
        /// Equity Value(post-illiquidity discount)
        /// </summary>
        decimal EV_EBITDA_Valuation_EquityValuePostIlliquidityDiscount { get; }
    }

}
