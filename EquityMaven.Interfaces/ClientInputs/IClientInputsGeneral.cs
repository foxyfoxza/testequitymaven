﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.ClientInputs
{
    /// <summary>
    /// Initial Inputs 
    /// </summary>
    public interface IClientInputs
    {
        #region General
        string CompanyName { get; set; }

        /// <summary>
        /// Industry (Thomson Reuters Business Classification code)
        /// </summary
        string TRBC2012HierarchicalID { get; set; }
        /// <summary>
        /// Country where majority business operations of take place
        /// </summary>
        string ThreeLetterISORegionName { get; set; }
        /// <summary>
        /// Cash flow forecast currency
        /// </summary>
        string ISOCurrencySymbol { get; set; }
        DateTime LastFinancialYearEnd { get; set; }
        /// <summary>
        /// Year of commencing business operations
        /// </summary>
        DateTime DateOfCommencement { get; set; }
        int NumberOfEmployees { get; set; }
        /// <summary>
        /// Current cash on hand as at last month-end
        /// </summary>
        decimal CurrentCashOnHand { get; set; }
        /// <summary>
        /// Current interest-bearing debt as at last month-end
        /// </summary>
        decimal CurrentInterestBearingDebt { get; set; }

        /// <summary>
        /// Months since last financial year end
        /// </summary>
        int Months { get; }

        DateTime ReportDate { get; }

        /// <summary>
        /// Industry(Thomson Reuters Business Classification)
        /// </summary>
        //IThomsonReutersBusinessClassification ThomsonReutersBusinessClassification { get; }
        /// <summary>
        /// Operating cash required as % Revenue (assumed)
        /// </summary>
        Decimal PercentageOperatingCashRequired { get; set; }
        string StageOfDevelopment { get; set; }
        #endregion
        #region Income Statement
        decimal IncomeStatement_Revenue { get; }
        /// <summary>
        /// Cost of goods sold
        /// </summary>
        decimal IncomeStatement_CostOfGoods { get; }
        decimal IncomeStatement_Calculated_GrossProfit { get; }
        /// <summary>
        /// Operating expenses(excluding depreciation & amortisation)
        /// </summary>
        decimal IncomeStatement_OperatingExpense { get; }
        /// <summary>
        /// Earnings before interest, tax, depreciation, amortisation ("EBITDA")
        /// </summary>
        decimal IncomeStatement_Calculated_EBITDA { get; }
        /// <summary>
        /// Depreciation and amortisation expense
        /// </summary>
        decimal IncomeStatement_DepreciationAndAmortisationExpense { get; }
        /// <summary>
        /// Earnings before interest and tax ("EBIT")
        /// </summary>
        decimal IncomeStatement_Calculated_EBIT { get; }

        decimal IncomeStatement_InterestExpense { get; }
        decimal IncomeStatement_Calculated_EarningsBeforeTax { get; }

        /// <summary>
        /// Non-cash expenses included in Operating expenses above
        /// </summary>
        decimal IncomeStatement_NonCashExpense { get; }

        Decimal IncomeStatementAnalysis_PercentageRevenueGrowth { get; }
        decimal IncomeStatementAnalysis_PercentageGrossProfitMargin { get; }

        Decimal IncomeStatementAnalysis_EBITDAMargin { get; }
        Decimal IncomeStatementAnalysis_EBITMargin { get; }


        /// <summary>
                                                             ///         Depreciation and amortisation as % of Revenue
                                                             /// </summary>
        Decimal IncomeStatementAnalysis_PercentageDepreciatedAndAmortOfRevenue { get; }
        /// <summary>
        ///what percentage Non-cash expenses (included in Operating expenses) (excluding depreciation & amortisation) are expected to be as a percentage of Revenue. 
        /// </summary>
        Decimal IncomeStatementAnalysis_NonCashExpenses { get; }
        Decimal IncomeStatementAnalysis_Calculated_InterestCover { get; }
        #endregion
        #region Balance Sheet
        decimal BalanceSheetTotalAssets_AccountsReceiveable { get; }
        decimal BalanceSheetTotalAssets_Inventory { get; }
        decimal BalanceSheetTotalAssets_ { get; }
        decimal BalanceSheetTotalAssets_CashAndCashEquivalents { get; }
        decimal BalanceSheetTotalAssets_Calculated_CurrentAssets { get; }
        decimal BalanceSheetTotalAssets_FixedAndIntangibleAssets { get; }
        decimal BalanceSheetTotalAssets_Calculated_TotalAssets { get; }

        decimal BalanceSheetEquityAndLiabilities_AccountsPayable { get; }
        decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities { get; }
        decimal BalanceSheetEquityAndLiabilities_CurrentLiabilities { get; }

        decimal BalanceSheetEquityAndLiabilities_ShortTermDebt { get; }
        decimal BalanceSheetEquityAndLiabilities_LongTermDebt { get; }
        decimal BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities { get; }
        decimal BalanceSheetEquityAndLiabilities_ShareholderLoans { get; }

        decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays { get; }
        decimal BalanceSheetAnalysis_AverageInventoryDays { get; }
        decimal BalanceSheetAnalysis_AverageAccountsPayableDays { get; }
        #endregion
        #region CapitalExpenditure
        decimal CapitalExpenditure_PercentageForecastCapitalExpenditure { get; }
        #endregion
        #region Sustainable EBITDA
        /// <summary>
        ///  These are unsual once-off expenses not in the normal course of business that are not expected to be repeated e.g. office renovations, losses from sale of operating assets etc.
        /// </summary>
        decimal SustainableEBITDA_NonRecurringExpenses { get; }
        /// <summary>
        /// This is unsual once-off income not in the normal course of business that is not expected to be repeated e.g. unsual and non-recurring sales (e.g. sale of a patent), profits from sale of operating assets etc.
        /// </summary>
        decimal SustainableEBITDA_NonRecurringIncome { get; }
        #endregion
    }
}
