﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.ClientInputs
{
    public interface IFullValuationInput : IEntityItemInt
    {


        Int64 ClientId { get; set; }
         
        int NumberOfEmployees { get; set; }
        string Description { get; set; }
        string CompanyName { get; set; }

        decimal CurrentCashOnHand { get; set; }
        decimal All3rdPartyInterestBearingDebtAsAtLastMonth_End { get; set; }// todo - is this used anywhere else (added late in the game)
        /// <summary>
        /// Months since last financial year end
        /// </summary>
        int MonthsSinceLastFinancialYearEnd { get; }
        string TRBC2012HierarchicalID { get; set; }
        DateTime LastFinancialYearEnd { get; set; }
        DateTime ValuationDate { get; set; }
        DateTime DateOfCommencement { get; set; }
        StageOfDevelopment StageOfDevelopment { get; }

        #region Currency
        string ISOCurrencySymbol { get; set; }
        #endregion

        #region Country
        string ThreeLetterISORegionName { get; set; }
        #endregion

        ICollection<IFullValuationInputYear> FinancialYears { get; }

    }
}
