﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.ClientInputs
{
    public interface IBatchedItem
    {
        Int64 InputBatchId { get; set; }
        //IInputBatch InputBatch { get; set; }
    }
}
