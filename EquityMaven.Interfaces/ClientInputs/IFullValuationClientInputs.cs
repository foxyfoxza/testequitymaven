﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.ClientInputs
{
    //public interface IFullValuationClientInputs
    //{

    //    int FinancialYearTypeValue { get; set; }

    //    #region Income Statement

    //    decimal IncomeStatement_Revenue { get; set; }

    //    /// <summary>
    //    /// Cost of goods sold
    //    /// </summary>
    //    decimal IncomeStatement_CostOfGoodsSold { get; set; }


    //    /// <summary>
    //    /// Operating expenses(excluding depreciation & amortisation)
    //    /// </summary>
    //    decimal IncomeStatement_OperatingExpense { get; set; }


         


    //    /// <summary>
    //    /// Depreciation and amortisation expense
    //    /// </summary>
    //    decimal IncomeStatement_DepreciationAndAmortisationExpense { get; set; }



    //    decimal IncomeStatement_InterestExpense { get; set; }


    //    /// <summary>
    //    /// Non-cash expenses included in Operating expenses above
    //    /// </summary>
    //    decimal IncomeStatement_NonCashExpense { get; set; }
    //    Decimal IncomeStatementAnalysis_PercentageRevenueGrowth { get; set; }

    //    decimal IncomeStatementAnalysis_PercentageGrossProfitMargin { get; set; }
    //    decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue { get; set; }



    //    /// <summary>
    //    ///         Depreciation and amortisation as % of Revenue
    //    /// </summary>
    //    Decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue { get; set; }
    //    /// <summary>
    //    ///what percentage Non-cash expenses (included in Operating expenses) (excluding depreciation & amortisation) are expected to be as a percentage of Revenue. 
    //    /// </summary>
    //    Decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue { get; set; }


    //    #endregion



    //    #region Balance Sheet

    //    decimal BalanceSheetTotalAssets_Total { get; set; }

    //    decimal BalanceSheetTotalAssets_AccountsReceiveable { get; set; }


    //    decimal BalanceSheetTotalAssets_Inventory { get; set; }

    //    decimal BalanceSheetTotalAssets_OtherCurrentAssets { get; set; }



    //    decimal BalanceSheetTotalAssets_CashAndCashEquivalents { get; set; }

    //    decimal BalanceSheetTotalAssets_IntangibleAssets { get; set; }

    //    decimal BalanceSheetTotalAssets_FixedAssets { get; set; }
    //    decimal BalanceSheetTotalAssets_TotalAssets { get; set; }

    //    decimal BalanceSheetEquityAndLiabilitiesCurrentLiabilities { get; set; }
    //    decimal BalanceSheetEquityAndLiabilities_AccountsPayable { get; set; }
    //    decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities { get; set; }
    //    decimal BalanceSheetTotalAssets_Calculated_CurrentLiabilities { get; set; }
    //    decimal BalanceSheetEquityAndLiabilities_ShortTermDebt { get; set; }
    //    decimal BalanceSheetEquityAndLiabilities_LongTermDebt { get; set; }
    //    decimal BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities { get; set; }
    //    decimal BalanceSheetEquityAndLiabilities_ShareholderLoans { get; set; }
    //    decimal BalanceSheetEquityAndLiabilities_TotalLiabilities { get; set; }
    //    decimal BalanceSheetEquityAndLiabilities_ShareholdersEquity { get; set; }


    //    decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue { get; set; }

    //    decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold { get; set; }
    //    decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold { get; set; }
    //    decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold { get; set; }
    //    decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold { get; set; }




    //    decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays { get; set; }
    //    decimal BalanceSheetAnalysis_AverageInventoryDays { get; set; }

    //    decimal BalanceSheetAnalysis_AverageAccountsPayableDays { get; set; }
    //    decimal BalanceSheetAnalysis_CurrentRatio { get; set; }

    //    decimal BalanceSheetAnalysis_TotalEBITDA { get; set; }



    //    #endregion





    //    #region CapitalExpenditure
    //    decimal CapitalExpenditure_CapitalExpenditure { get; set; }

    //    decimal CapitalExpenditure_PercentageForecastCapitalExpenditure { get; set; }
    //    decimal CapitalExpenditure_TTM { get; set; }
    //    #endregion



    //    decimal SustainableEBITDA_Total { get; set; }
    //    #region Sustainable EBITDA
    //    /// <summary>
    //    ///  These are unsual once-off expenses not in the normal course of business that are not expected to be repeated e.g. office renovations, losses from sale of operating assets etc.
    //    /// </summary>
    //    decimal SustainableEBITDA_NonRecurringExpenses { get; set; }
    //    /// <summary>
    //    /// This is unsual once-off income not in the normal course of business that is not expected to be repeated e.g. unsual and non-recurring sales (e.g. sale of a patent), profits from sale of operating assets etc.
    //    /// </summary>
    //    decimal SustainableEBITDA_NonRecurringIncome { get; set; }

    //    decimal SustainableEBITDA_TTM { get; set; }


    //    #endregion




    //    #region Excess Cash



    //    decimal ExcessCash_RevenueTTM { get; set; }
    //    decimal ExcessCash_Total { get; set; }
    //    decimal ExcessCash_AssumedOperatingCashRequiredAsPercentageOfRevenue { get; set; }
    //    decimal ExcessCash_RequiredPercentageOperatingCash { get; set; }
        

    //    #endregion



    //}
}
