﻿
 
using System;

namespace EquityMaven.Interfaces.ClientInputs
{
    public interface IPrelimEval
    {
        #region General
        /// <summary>
        /// Industry (Thomson Reuters Business Classification code)
        /// </summary
        string TRBC2012HierarchicalID { get; set; }
        DateTime LastFinancialYearEnd { get; set; }
        /// <summary>
        /// Current cash on hand as at last month-end
        /// </summary>
        decimal CurrentCashOnHand { get; set; }
        /// <summary>
        /// Current interest-bearing debt as at last month-end
        /// </summary>
        decimal CurrentInterestBearingDebt { get; set; }

        /// <summary>
        /// Industry(Thomson Reuters Business Classification)
        /// </summary>
        //IThomsonReutersBusinessClassification ThomsonReutersBusinessClassification { get; }
        /// <summary>
        /// Operating cash required as % Revenue (assumed) -> CLIENT INPUTS C 106
        /// </summary>
        Decimal PercentageOperatingCashRequired { get; set; }
        #endregion
    }


}
