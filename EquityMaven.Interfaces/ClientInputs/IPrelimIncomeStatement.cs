﻿

namespace EquityMaven.Interfaces.ClientInputs
{
    public interface IPrelimIncomeStatement
    {
        decimal IncomeStatement_Revenue { get; set; }
        /// <summary>
        /// Cost of goods sold
        /// </summary>
        decimal IncomeStatement_CostOfGoodsSold { get; set; }
        decimal IncomeStatement_GrossProfit { get; }
        /// <summary>
        /// Operating expenses(excluding depreciation & amortisation)
        /// </summary>
        decimal IncomeStatement_OperatingExpense { get; set; }

        /// <summary>
        /// Earnings before interest, tax, depreciation, amortisation("EBITDA")
        /// </summary>

        decimal IncomeStatement_EBITDA { get; }
    }

    
   

}
