﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.ClientInputs
{
    public interface IPrelimEvaluationInputs
    {
        string CompanyName { get; set; }

        decimal CurrentCashOnHand { get; set; }

        decimal CurrentInterestBearingDebt { get; set; }

        DateTime LastFinancialYearEnd { get; set; }

        decimal PercentageOperatingCashRequired { get; set; }

        string TRBC2012HierarchicalID { get; set; }

        decimal IncomeStatement_Revenue { get; set; }

        /// <summary>
        /// Cost of goods sold
        /// </summary>
        decimal IncomeStatement_CostOfGoodsSold { get; set; }

        /// <summary>
        /// Operating expenses(excluding depreciation & amortisation)
        /// </summary>
        decimal IncomeStatement_OperatingExpense { get; set; }

        decimal ExcessCash_CurrentCashOnHandAsAtLastMonthEnd { get; set; }


    }
}
