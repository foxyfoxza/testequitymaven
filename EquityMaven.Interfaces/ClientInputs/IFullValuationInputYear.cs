﻿using EquityMaven.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.ClientInputs
{
    //public interface IFullValuationInputYear { }
    public interface IFullValuationInputYear: IEntityItemInt
    {
        int FinancialYearTypeValue { get; set; }

      

        #region Income Statement

        decimal IncomeStatement_Revenue { get; set; }


        /// <summary>
        /// Cost of goods sold
        /// </summary>
        decimal IncomeStatement_CostOfGoodsSold { get; set; }


        decimal IncomeStatement_GrossProfit { get; }

        /// <summary>
        /// Operating expenses(excluding depreciation & amortisation)
        /// </summary>
        decimal IncomeStatement_OperatingExpense { get; set; }



        /// <summary>
        /// Earnings before interest, tax, depreciation, amortisation("EBITDA")
        /// </summary>
        decimal IncomeStatement_EBITDA { get; }



        /// <summary>
        /// Depreciation and amortisation expense
        /// </summary>
        decimal IncomeStatement_DepreciationAndAmortisationExpense { get; set; }

        /// <summary>
        /// Earnings before interest and tax ("EBIT")
        /// </summary>
        decimal IncomeStatement_EBIT { get; }

        decimal IncomeStatement_InterestExpense { get; set; }
        decimal IncomeStatement_EarningsBeforeTax { get; }

        /// <summary>
        /// Non-cash expenses included in Operating expenses above
        /// </summary>
        decimal IncomeStatement_NonCashExpense { get; set; }
        Decimal IncomeStatementAnalysis_PercentageRevenueGrowth { get; set; }

        decimal IncomeStatementAnalysis_PercentageGrossProfitMargin { get; set; }

        decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue { get; set; }

        Decimal IncomeStatementAnalysis_EBITDAMargin { get; }
        Decimal IncomeStatementAnalysis_EBITMargin { get; }


        /// <summary>
        ///         Depreciation and amortisation as % of Revenue
        /// </summary>
        Decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue { get; set; }

        /// <summary>
        ///what percentage Non-cash expenses (included in Operating expenses) (excluding depreciation & amortisation) are expected to be as a percentage of Revenue. 
        /// </summary>
        Decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue { get; set; }

        Decimal IncomeStatementAnalysis_Calculated_InterestCover { get; }

        ////todo - implement this?  CLIENT INPUTS -> E45
        //public Decimal IncomeStatementAnalysis_PercentageFreeCashFlowOfRevenue { get { return 123456 / IncomeStatement_Revenue; } }
        ////todo - implement this? CLIENT INPUTS -> E46
        //public Decimal IncomeStatementAnalysis_PercentageFreeCashFlowOfEBITDA { get { return 456789 / IncomeStatement_EBITDA; } }


        #endregion

        #region Balance Sheet

        decimal BalanceSheetTotalAssets_CurrentAssets { get; }
        decimal BalanceSheetTotalAssets_AccountsReceiveable { get; set; }


        decimal BalanceSheetTotalAssets_Inventory { get; set; }

        decimal BalanceSheetTotalAssets_OtherCurrentAssets { get; set; }

        decimal BalanceSheetTotalAssets_CashAndCashEquivalents { get; set; }

        decimal BalanceSheetTotalAssets_IntangibleAssets { get; set; }
        decimal BalanceSheetTotalAssets_FixedAndIntangibleAssets { get;   }
        decimal BalanceSheetTotalAssets_FixedAssets { get; set; }

        decimal BalanceSheetTotalAssets_TotalAssets { get; }

        decimal BalanceSheetEquityAndLiabilities_AccountsPayable { get; set; }
        decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities { get; }
        decimal BalanceSheetEquityAndLiabilities_CurrentLiabilities { get; }
        decimal BalanceSheetEquityAndLiabilities_ShortTermDebt { get; }
        decimal BalanceSheetEquityAndLiabilities_LongTermDebt { get; }
        decimal BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities { get; }
        decimal BalanceSheetEquityAndLiabilities_ShareholderLoans { get; }


        decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue { get; set; }

        decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold { get; set; }

        decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold { get; set; }

        decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold { get; set; }

        decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold { get; set; }

        decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays { get; }
        decimal BalanceSheetAnalysis_AverageInventoryDays { get; }

        decimal BalanceSheetAnalysis_AverageAccountsPayableDays { get; }

        decimal BalanceSheetAnalysis_CurrentRatio { get; }
        decimal BalanceSheetAnalysis_TotalDebtOverEBITDA { get; }



        #endregion

        #region CapitalExpenditure
        decimal CapitalExpenditure_CapitalExpenditure { get; set; }

        decimal CapitalExpenditure_PercentageForecastCapitalExpenditure { get; set; }
        decimal CapitalExpenditure_TTM { get; }
        #endregion

        #region Sustainable EBITDA
        /// <summary>
        ///  These are unsual once-off expenses not in the normal course of business that are not expected to be repeated e.g. office renovations, losses from sale of operating assets etc.
        /// </summary>
        decimal SustainableEBITDA_NonRecurringExpenses { get; set; }
        /// <summary>
        /// This is unsual once-off income not in the normal course of business that is not expected to be repeated e.g. unsual and non-recurring sales (e.g. sale of a patent), profits from sale of operating assets etc.
        /// </summary>
        decimal SustainableEBITDA_NonRecurringIncome { get; set; }

        decimal SustainableEBITDA_SustainableEBITDA { get; }

        decimal SustainableEBITDA_SustainableEBITDA_TTM { get; }

        #endregion

        #region Excess Cash

        decimal ExcessCash_RevenueTTM { get; }

        decimal ExcessCash_RequiredPercentageOperatingCash { get; set; }

        decimal ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue { get; }

        decimal ExcessCash_ExcessCash { get; }

        #endregion

        #region Assets Projection 
        decimal AssetsProjection_FixedAndIntangibleAssets { get; }
        #endregion

        #region FullValuationInput
        Int64 FullValuationInputId { get; set; }
        #endregion
    }

}
