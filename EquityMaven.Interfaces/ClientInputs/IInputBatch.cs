﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.ClientInputs
{
    public interface IInputBatch
    {
        DateTime? ActiveFrom { get; set; }
        string Description { get; set; }
    }
}
