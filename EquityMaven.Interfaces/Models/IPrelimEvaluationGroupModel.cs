﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.Models
{
    public interface IPrelimEvaluationGroupModel

    {
        int Year { get; set; }
        int? Month { get; set; }
        int? Day { get; set; }
        int Total { get; set; }
    }
}
