﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Repository.Validation
{
    
    
    
    public interface IEntityItemValidation<T>
    {
        bool ExceedsThresholdMessage(float count, float failedValidationCount);
        void Validate<TdbItem>(T item, IQueryable<T> dbItems, bool isNew, out TdbItem dbItem, out Exception error) where TdbItem : T;
        void Validate<TdbItem>(T item, IQueryable<T> dbItems, bool isNew, out TdbItem dbItem) where TdbItem : T;
    }

}
