﻿using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.FinancialYears;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Interfaces.Calculations
{


    public interface IFinancialValuation
    {
        IFinancialYearActualPrior iActualPrior { get; }
        IFinancialYearActualCurrent iActualCurrent { get; }
        IFinancialYearBudget iBudget { get; }
        IFinancialYearForecast1 iForecast1 { get; }
        IFinancialYearForecast2 iForecast2 { get; }
        IFinancialYearTerminal iTerminal { get; }
        DateTime LastFinancialYearEnd { get; }
        DateTime ReportDate { get; }
        DateTime ValuationDate { get; }
        decimal VCDiscountRate { get; }
        int NumberOfEmployees { get; }
        StageOfDevelopment StageOfDevelopment { get; }

        int YearsInOperation { get; }
        decimal ProbabilityOfFailure { get; }
        decimal ProbabilityOfSurvival { get; }


        decimal All3rdPartyInterestBearingDebtAsAtLastMonth_End { get; }
        Decimal IlliquidityDiscount { get; }
        int MonthsSinceLastFinancialYearEnd { get; }
        decimal CurrentCashOnHand { get; }

        decimal PercentageOperatingCashRequired { get; }

        decimal DiscountedCashFlow_DiscretePeriod { get; }
        decimal DiscountedCashFlow_TerminalPeriod { get; }


        decimal DiscountedCashFlow_EnterpriseValuePre_IlliquidityDiscount { get; }
        decimal DiscountedCashFlow_ExcessCash { get; }

        decimal DiscountedCashFlow_InterestBearingDebt { get; }
        decimal DiscountedCashFlow_EquityValuePre_IlliquidityDiscount { get; }
        decimal DiscountedCashFlow_IlliquidityDiscount { get; }
        decimal DiscountedCashFlow_EquityValuePost_IlliquidityDiscount { get; }



        #region EV/EBITDA VALUATION

        decimal EvOverEbitdaValuation_SustainableEbitdaTtm { get; }

        
        decimal EvOverEbitdaValuation_EnterpriseValuePre_IlliquidityDiscount { get; }
        decimal EvOverEbitdaValuation_ExcessCash { get; }

        decimal EvOverEbitdaValuation_InterestBearingDebt { get; }
        decimal EvOverEbitdaValuation_EquityValuePre_IlliquidityDiscount { get; }
        decimal EvOverEbitdaValuation_IlliquidityDiscount { get; }
        decimal EvOverEbitdaValuation_EquityValuePost_IlliquidityDiscount { get; }


        #endregion

        #region EV/EBITDA - World Median

        decimal WorldMedian_World { get; }
        decimal WorldMedian_Africa { get; }
        decimal WorldMedian_NorthAmerica { get; }
        decimal WorldMedian_SouthCentralAmerica { get; }
        decimal WorldMedian_Asia { get; }
        decimal WorldMedian_AustraliaNewZealand { get; }
        decimal WorldMedian_EasternEuropeRussia { get; }
        decimal WorldMedian_SouthernEurope { get; }
        decimal WorldMedian_WesternEurope { get; }
        decimal WorldMedian_MiddleEast { get; }
        #endregion

        #region Industry TRBC Values

        decimal Industry_EBITMargin { get; }
        decimal Industry_EBITDAMargin { get; }
        decimal Industry_GrossProfitMargin { get; }
        decimal Industry_CurrentRatio { get; }
        decimal Industry_InterestCover { get; }
        decimal Industry_TotalDebt_Over_Ebitda { get; }
        decimal Industry_AccountsPayableDaysAverage { get; }
        decimal Industry_InventoryDaysAverage { get; }
        decimal Industry_AccountsReceiveableDaysAverage { get; }





        #endregion

        #region WACC INPUTS SUMMARY

        decimal WaccInputsSummary_Risk_FreeRate { get; }
        decimal WaccInputsSummary_EquityRiskPremium { get; }
        decimal WaccInputsSummary_CompanyRiskPremium { get; }
        decimal WaccInputsSummary_CostOfDebtPre_Tax { get; }
        decimal WaccInputsSummary_LeveredBeta { get; }
        decimal WaccInputsSummary_TotalDebt_Over_EquityD_Over_E { get; }
        decimal WaccInputsSummary_GearingD_Over_D_Plus_E { get; }
        decimal WaccInputsSummary_TaxRate { get; }
        decimal WaccInputsSummary_TerminalGrowthRate { get; }

        #endregion

        #region WACC CALCULATION
        decimal WaccCalculation_Ke { get; }
        decimal WaccCalculation_E_Over_D_Plus_E { get; }
        decimal WaccCalculation_Kd { get; }
        decimal WaccCalculation_D_Over_D_Plus_E { get; }
        decimal WaccCalculation_Wacc { get; }
        #endregion


        #region RISK-FREE RATE  

        decimal Risk_FreeRate_Risk_FreeRate { get; }
        #endregion
        #region Beta

        decimal Beta_UnleveredBeta { get; }

        decimal Beta_MarginalTaxRate { get; }
        decimal Beta_TotalDebtToEquityMarketCap { get; }
        decimal Beta_LeveredBeta { get; }


        #endregion

        #region EQUITY RISK PREMIUM



        decimal EquityRiskPremium_MatureMarketImpliedEquityRiskPremium { get; }


        decimal EquityRiskPremium_CountryRatingBasedDefaultSpread { get; }
        decimal EquityRiskPremium_RelativeVolatilityOfEquityVsBonds { get; }

        decimal EquityRiskPremium_EquityRiskPremium { get; }
        #endregion


        #region COST OF DEBT
        decimal CostOfDebt_RiskFreeRate { get; }
        decimal CostOfDebt_InterestCoverageRatio { get; }

        decimal CostOfDebt_EstimatedCorporateDefaultSpread { get; }
        decimal CostOfDebt_EstimatedCountryDefaultSpread { get; }
        decimal CostOfDebt_EstimatedCostOfDebt { get; }
        decimal CostOfDebt_MarginalTaxRate { get; }
        decimal CostOfDebt_After_TaxCostOfDebt { get; }
        #endregion






        #region SUMMARY - DCF, MULTIPLES, VC METHOD (PRE-SURVIVAL ADJUSTMENT)


        decimal Pre_SurvivalAdjustmentDCF_Discrete { get; }
      

        decimal Pre_SurvivalAdjustmentDCF_Terminal { get; }
       

        decimal Pre_SurvivalAdjustmentDCF_EnterpriseValuePre_IlliquidityDiscount { get; }
       
        decimal Pre_SurvivalAdjustmentDCF_ExcessCash { get; }

        decimal Pre_SurvivalAdjustmentMultiples_EnterpriseValuePre_IlliquidityDiscount { get; }
  
        decimal Pre_SurvivalAdjustmentDCF_EquityValuePre_IlliquidityDiscount { get; }
        decimal Pre_SurvivalAdjustmentMultiples_EquityValuePre_IlliquidityDiscount { get; }
  
        decimal Pre_SurvivalAdjustmentDCF_IlliquidityDiscount { get; }
        decimal Pre_SurvivalAdjustmentMultiples_IlliquidityDiscount { get; }
     

        decimal Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount { get; }
        decimal Pre_SurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount { get; }
       

        decimal Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPre_LiquidityDiscount { get; }
        decimal Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPre_LiquidityDiscount { get; }
      


        decimal Pre_SurvivalAdjustmentDCF_EnterpriseValuePost_IlliquidityDiscount { get; }
        decimal Pre_SurvivalAdjustmentMultiples_EnterpriseValuePost_IlliquidityDiscount { get; }
        decimal Pre_SurvivalAdjustmentVCMethod_EnterpriseValuePost_IlliquidityDiscount { get; }

        decimal Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPost_LiquidityDiscount { get; }
        decimal Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPost_LiquidityDiscount { get; }
        decimal Pre_SurvivalAdjustmentVCMethod_ImpliedEv_Over_EbitdaPost_LiquidityDiscount { get; }



        #endregion




        #region LiquidationvalueRecoveryRate
        decimal LiquidationvalueRecoveryRate_AccountsReceiveable { get; }
        decimal LiquidationvalueRecoveryRate_Inventory { get; }
        decimal LiquidationvalueRecoveryRate_OtherCurrentAssets { get; }
        decimal LiquidationvalueRecoveryRate_CashAndCashEquivalents { get; }
        decimal LiquidationvalueRecoveryRate_IntangibleAssets { get; }
        decimal LiquidationvalueRecoveryRate_FixedAssets { get; }

        decimal LiquidationvalueRecoveryRate_AccountsPayable { get; }
        decimal LiquidationvalueRecoveryRate_OtherCurrentLiabilities { get; }
        decimal LiquidationvalueRecoveryRate_3rdPartyInterest_BearingDebtLongTermShortTerm { get; }
        decimal LiquidationvalueRecoveryRate_OtherLong_TermLiabilities { get; }
        decimal LiquidationvalueRecoveryRate_ShareholderLoans { get; }

        #endregion


        #region LiquidationvalueRecoveryAmount 
        decimal LiquidationvalueRecoveryAmount_CurrentAssets { get; }

        decimal LiquidationvalueRecoveryAmount_AccountsReceiveable { get; }
        decimal LiquidationvalueRecoveryAmount_Inventory { get; }
        decimal LiquidationvalueRecoveryAmount_OtherCurrentAssets { get; }
        decimal LiquidationvalueRecoveryAmount_CashAndCashEquivalents { get; }
        decimal LiquidationvalueRecoveryAmount_IntangibleAssets { get; }
        decimal LiquidationvalueRecoveryAmount_FixedAssets { get; }
        decimal LiquidationvalueRecoveryAmount_TotalAssets { get; }
        decimal LiquidationvalueRecoveryAmount_CurrentLiabilities { get; }

        decimal LiquidationvalueRecoveryAmount_AccountsPayable { get; }
        decimal LiquidationvalueRecoveryAmount_OtherCurrentLiabilities { get; }
        decimal LiquidationvalueRecoveryAmount_3rdPartyInterest_BearingDebtLongTermShortTerm { get; }
        decimal LiquidationvalueRecoveryAmount_OtherLong_TermLiabilities { get; }
        decimal LiquidationvalueRecoveryAmount_ShareholderLoans { get; }
        decimal LiquidationvalueRecoveryAmount_TotalLiabilities { get; }
        decimal LiquidationvalueRecoveryAmount_LiquidationValue { get; }

        #endregion


        #region EQUITY VALUE - POST SURVIVAL ADJUSTMENT

       

        decimal EquityValue_PostSurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount { get; }
        decimal EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedEquityValuePost_IlliquidityDiscount { get; }
        decimal EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue { get; }
        decimal EquityValue_PostSurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival { get; }



        decimal EquityValue_PostSurvivalAdjustment_MULTIPLES_EquityValuePost_IlliquidityDiscount { get; }
        decimal EquityValue_PostSurvivalAdjustment_MULTIPLES_Probability_WeightedEquityValuePost_IlliquidityDiscount { get; }
        decimal EquityValue_PostSurvivalAdjustment_MULTIPLES_Probability_WeightedLiquidationValue { get; }
        decimal EquityValue_PostSurvivalAdjustment_MULTIPLES_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival { get; }

        decimal EquityValue_PostSurvivalAdjustment_VCMethod_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival { get; }
        decimal EquityValue_PostSurvivalAdjustment_VCMethod_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get; }

        #endregion
        #region ENTERPRISE VALUE - POST SURVIVAL ADJUSTMENT


        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityOfSurvival { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedEquityValuePostIlliquidityDiscount { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_LiquidationValue { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityOfFailure { get;}
        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get; }

        decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityOfSurvival { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityWeightedEquityValuePostIlliquidityDiscount { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_LiquidationValue { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityOfFailure { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get; }

        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_EnterpriseValuePost_IlliquidityDiscount { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityOfSurvival { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityWeightedEquityValuePostIlliquidityDiscount { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_LiquidationValue { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityOfFailure { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get; }

        decimal EnterpriseValue_PostSurvivalAdjustment_VCMethod_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival { get; }




        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_Multiples_ProbabilityWeightedLiquidationValue { get; }
        decimal EnterpriseValue_PostSurvivalAdjustment_DCF_Term_Multiple_ProbabilityWeightedLiquidationValue { get; }

         
        #endregion


        #region Sensitivity Analysis Equity Value Survival Adjusted Multiples

        decimal SurvivalAdjustedMultiples_EBITDA_TTM_1 { get; }
        decimal SurvivalAdjustedMultiples_EBITDA_TTM_2 { get; }
        decimal SurvivalAdjustedMultiples_EBITDA_TTM_3 { get; }
        decimal SurvivalAdjustedMultiples_EBITDA_TTM_4 { get; }
        decimal SurvivalAdjustedMultiples_EBITDA_TTM_5 { get; }



        decimal SurvivalAdjustedMultiples_AMultiple { get; }
        decimal SurvivalAdjustedMultiples_A1 { get; }
        decimal SurvivalAdjustedMultiples_A2 { get; }
        decimal SurvivalAdjustedMultiples_A3 { get; }
        decimal SurvivalAdjustedMultiples_A4 { get; }
        decimal SurvivalAdjustedMultiples_A5 { get; }

        decimal SurvivalAdjustedMultiples_BMultiple { get; }
        decimal SurvivalAdjustedMultiples_B1 { get; }
        decimal SurvivalAdjustedMultiples_B2 { get; }
        decimal SurvivalAdjustedMultiples_B3 { get; }
        decimal SurvivalAdjustedMultiples_B4 { get; }
        decimal SurvivalAdjustedMultiples_B5 { get; }

        decimal SurvivalAdjustedMultiples_CMultiple { get; }
        decimal SurvivalAdjustedMultiples_C1 { get; }
        decimal SurvivalAdjustedMultiples_C2 { get; }
        decimal SurvivalAdjustedMultiples_C3 { get; }
        decimal SurvivalAdjustedMultiples_C4 { get; }
        decimal SurvivalAdjustedMultiples_C5 { get; }

        decimal SurvivalAdjustedMultiples_DMultiple { get; }
        decimal SurvivalAdjustedMultiples_D1 { get; }
        decimal SurvivalAdjustedMultiples_D2 { get; }
        decimal SurvivalAdjustedMultiples_D3 { get; }
        decimal SurvivalAdjustedMultiples_D4 { get; }
        decimal SurvivalAdjustedMultiples_D5 { get; }

        decimal SurvivalAdjustedMultiples_EMultiple { get; }
        decimal SurvivalAdjustedMultiples_E1 { get; }
        decimal SurvivalAdjustedMultiples_E2 { get; }
        decimal SurvivalAdjustedMultiples_E3 { get; }
        decimal SurvivalAdjustedMultiples_E4 { get; }
        decimal SurvivalAdjustedMultiples_E5 { get; }


        #endregion


        #region Sensitivity Analysis Survival Adjusted DCF 



        decimal SurvivalAdjustedDCFEquity_Wacc_1 { get; }
        decimal SurvivalAdjustedDCFEquity_Wacc_2 { get; }
        decimal SurvivalAdjustedDCFEquity_Wacc_3 { get; }
        decimal SurvivalAdjustedDCFEquity_Wacc_4 { get; }
        decimal SurvivalAdjustedDCFEquity_Wacc_5 { get; }

        decimal SurvivalAdjustedDCFEquity_AMultiple { get; }






        decimal SurvivalAdjustedDCFEquity_A1 { get; }
        decimal SurvivalAdjustedDCFEquity_A2 { get; }
        decimal SurvivalAdjustedDCFEquity_A3 { get; }
        decimal SurvivalAdjustedDCFEquity_A4 { get; }
        decimal SurvivalAdjustedDCFEquity_A5 { get; }
        decimal SurvivalAdjustedDCFEquity_BMultiple { get; }
        decimal SurvivalAdjustedDCFEquity_B1 { get; }
        decimal SurvivalAdjustedDCFEquity_B2 { get; }
        decimal SurvivalAdjustedDCFEquity_B3 { get; }
        decimal SurvivalAdjustedDCFEquity_B4 { get; }
        decimal SurvivalAdjustedDCFEquity_B5 { get; }

        decimal SurvivalAdjustedDCFEquity_CMultiple { get; }
        decimal SurvivalAdjustedDCFEquity_C1 { get; }
        decimal SurvivalAdjustedDCFEquity_C2 { get; }
        decimal SurvivalAdjustedDCFEquity_C3 { get; }
        decimal SurvivalAdjustedDCFEquity_C4 { get; }
        decimal SurvivalAdjustedDCFEquity_C5 { get; }
        decimal SurvivalAdjustedDCFEquity_DMultiple { get; }
        decimal SurvivalAdjustedDCFEquity_D1 { get; }
        decimal SurvivalAdjustedDCFEquity_D2 { get; }
        decimal SurvivalAdjustedDCFEquity_D3 { get; }
        decimal SurvivalAdjustedDCFEquity_D4 { get; }
        decimal SurvivalAdjustedDCFEquity_D5 { get; }
        decimal SurvivalAdjustedDCFEquity_EMultiple { get; }
        decimal SurvivalAdjustedDCFEquity_E1 { get; }
        decimal SurvivalAdjustedDCFEquity_E2 { get; }
        decimal SurvivalAdjustedDCFEquity_E3 { get; }
        decimal SurvivalAdjustedDCFEquity_E4 { get; }
        decimal SurvivalAdjustedDCFEquity_E5 { get; }


        #endregion


    }

}
