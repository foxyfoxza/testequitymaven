﻿using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Repository
{
    public class FullValuationInputYearRepository : EquityMavenRepositoryInt<FullValuationInputYear>
    {
        public FullValuationInputYearRepository(EquityMavenContext context) : base(context)
        {
        }

        protected override IEnumerable<FullValuationInputYear> GetDBItems(IEnumerable<FullValuationInputYear> items)
        {
            List<FullValuationInputYear> dbItems = new List<FullValuationInputYear>();
            var idList = items.Where(o=> o.Id != 0).Select(o => o.Id).ToList();
            while (idList.Any())
            {
                var nextCount = Math.Min(100, idList.Count);
                var sublist = idList.Take(nextCount);
                dbItems.AddRange(Set.Where(o => sublist.Any(id => id == o.Id)));
                idList = idList.Skip(nextCount).Take((idList.Count - nextCount)).ToList();
            }
            var otherList = items.Where(o => o.Id == 0).ToList();
            foreach (var item in otherList)
            {
                var dbItem = Set.FirstOrDefault(o => o.FullValuationInputId == item.FullValuationInputId && o.FinancialYearTypeValue == item.FinancialYearTypeValue);
                if (dbItem != null)
                {
                    item.Id = dbItem.Id;
                    dbItems.Add(dbItem);
                }
            }


            return dbItems;
        }

    }
}
