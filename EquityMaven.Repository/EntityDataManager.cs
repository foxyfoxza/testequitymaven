﻿using EquityMaven.Data;
using EquityMaven.Repository.Validation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.Data.Entities;
using System.Data.Entity.Validation;
using EquityMaven.Interfaces.ClientInputs;
using EquityMaven.CommonEnums;
using EquityMaven.Data.HelperEntities;
using EquityMaven.Interfaces.Entities;
using EquityMaven.ImportExport;

namespace EquityMaven.Repository
{
    public class EntityDataManager
    {
        private ValidateEntityItemGUID ValidateEntity = new ValidateEntityItemGUID();

        private readonly IRepository<Client> ClientRepository;
        private readonly IRepository<Currency> CurrencyRepository;
        private readonly IRepository<InputBatch> InputBatchRepository;
        private readonly IRepository<CurrencyRiskFreeRate> CurrencyRiskFreeRateRepository;
        private readonly IRepository<MoodysRating> MoodysRatingRepository;

        private readonly IRepository<ThompsonReutersBusinessClassification> ThompsonReutersBusinessClassificationRepository;
        private readonly IRepository<TRBCValue> TRBCValueRepository;
        private readonly IRepository<PrelimEvaluation> PrelimEvaluationRepository;
        private readonly IRepository<GlobalDefaults> GlobalDefaultsRepository;
        private readonly IRepository<ClientProfile> ClientProfileRepository;

        private readonly IRepository<CountryMoodysRating> CountryMoodysRatingRepository;
        private readonly IRepository<CountryTaxRate> CountryTaxRateRepository;
        private readonly IRepository<Country> CountryRepository;

        private readonly IRepository<FullValuationInput> FullValuationRepository;
        private readonly IRepository<FullValuationInputYear> FullValuationInputYearRepository;
        private readonly IRepository<CorporateBondSpread> CorporateBondSpreadRepository;

        private readonly IRepository<FullValuationYearReportActualCurrent> FullValuationYearReportActualCurrentRepository;
        private readonly IRepository<FullValuationYearReportActualPrior> FullValuationYearReportActualPriorRepository;
        private readonly IRepository<FullValuationYearReportBudget> FullValuationYearReportBudgetRepository;
        private readonly IRepository<FullValuationYearReportForecast1> FullValuationYearReportForecast1Repository;
        private readonly IRepository<FullValuationYearReportForecast2> FullValuationYearReportForecast2Repository;
        private readonly IRepository<FullValuationYearReportTerminal> FullValuationYearReportTerminalRepository;
        private readonly IRepository<FullValuationReport> FullValuationReportRepository;

        public EntityDataManager(EquityMavenContext _context)
        {
            //DataContext = _context;
            PrelimEvaluationRepository = new EquityMavenRepositoryInt<PrelimEvaluation>(_context);
            GlobalDefaultsRepository = new EquityMavenRepositoryInt<GlobalDefaults>(_context);
            ClientRepository = new EquityMavenRepositoryInt<Client>(_context);
            CurrencyRepository = new EquityMavenRepositoryInt<Currency>(_context);
            InputBatchRepository = new EquityMavenRepositoryInt<InputBatch>(_context);
            CurrencyRiskFreeRateRepository = new EquityMavenRepositoryInt<CurrencyRiskFreeRate>(_context);
            MoodysRatingRepository = new EquityMavenRepositoryInt<MoodysRating>(_context);

            ThompsonReutersBusinessClassificationRepository = new EquityMavenRepositoryInt<ThompsonReutersBusinessClassification>(_context);
            TRBCValueRepository = new EquityMavenRepositoryInt<TRBCValue>(_context);
            ClientProfileRepository = new EquityMavenRepositoryInt<ClientProfile>(_context);

            CountryMoodysRatingRepository = new EquityMavenRepositoryInt<CountryMoodysRating>(_context);
            CountryTaxRateRepository = new EquityMavenRepositoryInt<CountryTaxRate>(_context);
            CountryRepository = new EquityMavenRepositoryInt<Country>(_context);

            FullValuationRepository = new EquityMavenRepositoryInt<FullValuationInput>(_context);
            FullValuationInputYearRepository = new FullValuationInputYearRepository(_context);

            CorporateBondSpreadRepository = new EquityMavenRepositoryInt<CorporateBondSpread>(_context);

            FullValuationYearReportActualCurrentRepository = new EquityMavenRepositoryInt<FullValuationYearReportActualCurrent>(_context);
            FullValuationYearReportActualPriorRepository = new EquityMavenRepositoryInt<FullValuationYearReportActualPrior>(_context);
            FullValuationYearReportBudgetRepository = new EquityMavenRepositoryInt<FullValuationYearReportBudget>(_context);
            FullValuationYearReportForecast1Repository = new EquityMavenRepositoryInt<FullValuationYearReportForecast1>(_context);
            FullValuationYearReportForecast2Repository = new EquityMavenRepositoryInt<FullValuationYearReportForecast2>(_context);
            FullValuationYearReportTerminalRepository = new EquityMavenRepositoryInt<FullValuationYearReportTerminal>(_context);
            FullValuationReportRepository = new EquityMavenRepositoryInt<FullValuationReport>(_context);


        }

        private static BatchType GetBatchTypeFor<T>() where T : IBatchedItem
        {
            var forType = typeof(T).ToString();
            switch (forType)
            {
                case "EquityMaven.Data.Entities.TRBCActivity":
                case "EquityMaven.Data.Entities.TRBCValue":
                case "EquityMaven.Data.Entities.TRBCEconomicSector":
                case "EquityMaven.Data.Entities.TRBCIndustry":
                case "EquityMaven.Data.Entities.TRBCIndustryGroup":
                case "EquityMaven.Data.Entities.ThompsonReutersBusinessClassification":
                case "EquityMaven.Data.Entities.CurrencyRiskFreeRate":
                case "EquityMaven.Data.Entities.Currency":


                case "EquityMaven.Data.Entities.CountryMoodysRating":

                    return BatchType.Monthly;
                case "EquityMaven.Data.Entities.MoodysRating":
                case "EquityMaven.Data.Entities.GlobalDefaults":
                case "EquityMaven.Data.Entities.CountryTaxRate":
                case "EquityMaven.Data.Entities.CorporateBondSpread":
                    return BatchType.Annual;
                default:
                    throw new Exception($"GetBatchTypeFor not defined for {forType}");
            }



        }

        #region InputBatch

        public void SaveInputBatch(InputBatch item)
        {
            InputBatchRepository.AddUpdate(item);
        }
        public void SaveInputBatches(IEnumerable<InputBatch> items)
        {
            InputBatchRepository.AddUpdate(items);
        }

        public InputBatch GetCurrentInputBatch<T>() where T : IBatchedItem
        {
            var batchType = GetBatchTypeFor<T>();
            var result = InputBatchRepository.All().Where(o => o.isActive && o.BatchTypeId == (int)batchType).OrderByDescending(o => o.ActiveFrom).FirstOrDefault();

            return result;
        }



        public IQueryable<InputBatch> GetInputBatches(bool includeInactive = false, BatchType batchType = BatchType.Monthly)
        {
            return InputBatchRepository.All().Where(o => (includeInactive || o.isActive) && o.BatchTypeId == (int)batchType);
        }
        #endregion

        #region GetMoodysRatings

        public void SaveMoodysRating(MoodysRating item)
        {
            MoodysRatingRepository.AddUpdate(item);
        }



        public IQueryable<MoodysRating> GetMoodysRatings(Int64? InputBatchId = null)
        {

            if (!InputBatchId.HasValue)
            {
                var InputBatch = GetCurrentInputBatch<MoodysRating>();
                InputBatchId = InputBatch?.Id;
            }
            return MoodysRatingRepository.All().Where(o => o.InputBatchId == InputBatchId.Value);
        }
        #endregion
        #region ThomsonReutersBusinessClassification


        public IQueryable<ThompsonReutersBusinessClassification> GetThompsonReutersBusinessClassifications(Int64? InputBatchId = null, bool includeAllTRBCValues = false)
        {
            if (!InputBatchId.HasValue)
            {
                InputBatchId = GetCurrentInputBatch<ThompsonReutersBusinessClassification>()?.Id;
            }
            if (!InputBatchId.HasValue)
                return Enumerable.Empty<ThompsonReutersBusinessClassification>().AsQueryable();



            return ThompsonReutersBusinessClassificationRepository.All().Where(o => o.InputBatchId == InputBatchId.Value);
        }



        #endregion
        #region TRBCValue

        public IQueryable<TRBCValue> GetAllTRBCValues(Int64 InputBatchId)
        {
            return TRBCValueRepository.All().Where(o => o.InputBatchId == InputBatchId);
        }

        public IQueryable<TRBCValue> GetTRBCValues(Int64? InputBatchId = null)
        {
            if (!InputBatchId.HasValue)
            {
                InputBatchId = GetCurrentInputBatch<TRBCValue>()?.Id;
            }
            if (!InputBatchId.HasValue)
                return Enumerable.Empty<TRBCValue>().AsQueryable();

            return TRBCValueRepository.All().Where(o => o.InputBatchId == InputBatchId.Value);
        }

        public TRBCValue GetTRBCValue(string trbcCode, Int64? InputBatchId = null)
        {
            if (!InputBatchId.HasValue)
            {
                InputBatchId = GetCurrentInputBatch<TRBCValue>()?.Id;
            }
            var trbc = GetThompsonReutersBusinessClassifications(InputBatchId).FirstOrDefault(o => o.TRBC2012HierarchicalID == trbcCode);
            var result = !InputBatchId.HasValue || trbc == null ? null : GetTRBCValues(InputBatchId.Value).FirstOrDefault(o => o.ThompsonReutersBusinessClassificationId == trbc.Id);
            if (result != null)
                result.ThompsonReutersBusinessClassification = trbc;
            return result;
        }

        public void ImportData(ImportConversion import)
        {
            ImportData(import.Currencies, import.CurrencyRiskFreeRates, import.MoodysRatings, import.ThompsonReutersBusinessClassifications, import.TRBCValues, import.CountryMoodysRatings, import.CountryTaxRates, import.CorporateBondSpreads, import.InputBatch);
        }

        public InputBatch ImportData(IEnumerable<Currency> currencies, IEnumerable<CurrencyRiskFreeRate> currencyRiskFreeRates, IEnumerable<MoodysRating> moodysRatings, IEnumerable<ThompsonReutersBusinessClassification> thompsonReutersBusinessClassifications, IEnumerable<TRBCValue> trbcValues, IEnumerable<ImportExport.Import.CountryMoodysRatingMap> countryMoodysRatingMaps, IEnumerable<ImportExport.Import.CountryTaxRateMap> countryTaxRateMaps, IEnumerable<ImportExport.Import.CorporateBondSpreadMap> corporateBondSpreadMaps, InputBatch inputBatch)
        {
            var testValidation = new List<KeyValuePair<string, IEnumerable<DbEntityValidationResult>>>();
            try
            {
                inputBatch.Description = string.IsNullOrWhiteSpace(inputBatch.Description) ? DateTime.UtcNow.ToString("yyyyMMdd hhmmss") : inputBatch.Description;

                SaveInputBatch(inputBatch);



                if (inputBatch.BatchType == BatchType.Monthly)
                {

                    //we do this here so that we can access existing countries and ratings
                    var countryMoodysRatings = new List<CountryMoodysRating>();
                    foreach (var cmr in countryMoodysRatingMaps)
                    {
                        var country = CountryRepository.All().FirstOrDefault(o => o.CountryEnglishName.ToLower() == cmr.Country.ToLower());

                        if (country == null)
                        {
                            country = Country.GetCountryByCountryEnglishName(cmr.Country);
                        }

                        countryMoodysRatings.Add(new CountryMoodysRating { InputBatch = inputBatch, Country = country, Rating = cmr.MoodysRating });
                    }
                    var _1 = currencies.Where(o => o.InputBatch != inputBatch).ToList();
                    CurrencyRepository.AddUpdate(currencies);
                    CurrencyRiskFreeRateRepository.AddUpdate(currencyRiskFreeRates);
                    ThompsonReutersBusinessClassificationRepository.AddUpdate(thompsonReutersBusinessClassifications);
                    TRBCValueRepository.AddUpdate(trbcValues);
                    CountryMoodysRatingRepository.AddUpdate(countryMoodysRatings);

                }
                else if (inputBatch.BatchType == BatchType.Annual)
                {
                    MoodysRatingRepository.AddUpdate(moodysRatings);
                    //we do this here so that we can access existing countries and ratings

                    var countries = countryTaxRateMaps.Select(o => Country.GetCountryByCountryEnglishName(o.Country));
                    CountryRepository.AddUpdate(countries);

                    var countryTaxRates = new List<CountryTaxRate>();
                    foreach (var ctr in countryTaxRateMaps)
                    {
                        var country = CountryRepository.All().FirstOrDefault(o => o.CountryEnglishName.ToLower() == ctr.Country.ToLower());
                        countryTaxRates.Add(new CountryTaxRate { InputBatch = inputBatch, CountryId = country.Id, TaxRate = ctr.TaxRate.HasValue ? ctr.TaxRate.Value : 0 });
                    }
                    CountryTaxRateRepository.AddUpdate(countryTaxRates);
                    var corporateBondSpreads = new List<CorporateBondSpread>();
                    foreach (var cbs in corporateBondSpreadMaps)
                    {
                        corporateBondSpreads.Add(ImportExport.Implementation.CorporateBondSpreadImport.Convert(cbs, inputBatch));
                    }
                    CorporateBondSpreadRepository.AddUpdate(corporateBondSpreads);
                }

                return inputBatch;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region  CurrencyRiskFreeRates
        public IQueryable<CurrencyRiskFreeRate> GetCurrencyRiskFreeRates(Int64? inputBatchId = null)
        {
            if (!inputBatchId.HasValue)
            {
                inputBatchId = GetCurrentInputBatch<CurrencyRiskFreeRate>()?.Id;
            }
            if (!inputBatchId.HasValue)
                return Enumerable.Empty<CurrencyRiskFreeRate>().AsQueryable();
            return CurrencyRiskFreeRateRepository.All().Where(o => o.InputBatchId == inputBatchId);
        }

        #endregion

        #region CountryTaxRates
        public IQueryable<CountryTaxRate> GetCountryTaxRates(Int64? inputBatchId = null)
        {
            if (!inputBatchId.HasValue)
            {
                inputBatchId = GetCurrentInputBatch<CountryTaxRate>()?.Id;
            }
            if (!inputBatchId.HasValue)
                return Enumerable.Empty<CountryTaxRate>().AsQueryable();
            return CountryTaxRateRepository.All().Where(o => o.InputBatchId == inputBatchId);
        }


        #endregion

        #region CorporateBondSpreads
        public IQueryable<CorporateBondSpread> GetCorporateBondSpreads(Int64? inputBatchId = null)
        {
            if (!inputBatchId.HasValue)
            {
                inputBatchId = GetCurrentInputBatch<CorporateBondSpread>()?.Id;
            }
            if (!inputBatchId.HasValue)
                return Enumerable.Empty<CorporateBondSpread>().AsQueryable();
            return CorporateBondSpreadRepository.All().Where(o => o.InputBatchId == inputBatchId);
        }


        #endregion

        #region CountryMoodysRatings
        public IQueryable<CountryMoodysRating> GetCountryMoodysRatings(Int64? inputBatchId = null)
        {
            if (!inputBatchId.HasValue)
            {
                inputBatchId = GetCurrentInputBatch<CountryMoodysRating>()?.Id;
            }
            if (!inputBatchId.HasValue)
                return Enumerable.Empty<CountryMoodysRating>().AsQueryable();
            return CountryMoodysRatingRepository.All().Where(o => o.InputBatchId == inputBatchId);
        }


        #endregion



        #region CurrencyLookup

        public IQueryable<CurrencyRiskFreeRate> GetCurrencyRiskFreeRates(Int64 inputBatch)
        {
            return CurrencyRiskFreeRateRepository.All().Where(o => o.InputBatchId == inputBatch);
        }


        #endregion
        #region Currency

        public IQueryable<Currency> GetCurrencies(Int64? inputBatchId = null)
        {
            if (!inputBatchId.HasValue)
            {
                inputBatchId = GetCurrentInputBatch<Currency>()?.Id;
            }
            if (!inputBatchId.HasValue)
                return Enumerable.Empty<Currency>().AsQueryable();
            return CurrencyRepository.All().Where(o => o.InputBatchId == inputBatchId);
        }


        #endregion

        #region ClientProfile
        public IQueryable<ClientProfile> GetClientProfiles(Int64 clientId)
        {
            return ClientProfileRepository.All().Where(o => o.ClientId == clientId);
        }

        public void SaveClientProfiles(ClientProfile item)
        {
            ClientProfileRepository.AddUpdate(item);
        }
        #endregion

        #region Client



        public Client GetClientByMemberId(int memberId)
        {
            return ClientRepository.All().FirstOrDefault(o => o.MemberId == memberId);
        }
        public Client GetClientByClientId(Int64 clientId)
        {
            return ClientRepository.All().FirstOrDefault(o => o.Id == clientId);
        }

        public Client GetClientByEmail(string emailAddress)
        {
            return ClientRepository.All().FirstOrDefault(o => o.Email == emailAddress);
        }
        public IQueryable<Client> GetClients()
        {
            return ClientRepository.All();
        }


        public void SaveClient(Client client)
        {
            ClientRepository.AddUpdate(client);
        }
        public void SaveClients(IEnumerable<Client> clients)
        {
            ClientRepository.AddUpdate(clients);
        }


        #endregion

        #region FullValuationInputYear
        public void SaveFullValuationInputYear(FullValuationInputYear item)
        {
            FullValuationInputYearRepository.AddUpdate(item);
        }

        public void SaveFullValuationInputYear(IEnumerable<FullValuationInputYear> items)
        {
            FullValuationInputYearRepository.AddUpdate(items);
        }
        public IQueryable<FullValuationInputYear> GetFullValuationInputYears()
        {
            return FullValuationInputYearRepository.All();
        }

        public IQueryable<IFullValuationInputYear> GetFullValuationInputYears(Int64 fullValuationInputId)
        {
            return FullValuationInputYearRepository.All().Where(o => o.FullValuationInputId == fullValuationInputId);
        }

        public FullValuationInputYear GetFullValuationInputYears(Int64 fullValuationInputId, FinancialYearType financialYearType)
        {
            return FullValuationInputYearRepository.All().FirstOrDefault(o => o.FullValuationInputId == fullValuationInputId && o.FinancialYearTypeValue == (int)financialYearType);
        }

        #endregion


        #region FullValuation

        public FullValuationModel GetFullValuationModel(int memberId, int? fullValuationInputId = null)
        {

            var client = GetClientByMemberId(memberId);

            var fvs = GetClientFullValuations(client.Id).Where(o => !fullValuationInputId.HasValue || o.Id == fullValuationInputId.Value);
            if (fvs.Any())
            {
                //there should either be only 1 (fullValuationInputId.hasvalue) or we take the latest one that was created
                var fv = fvs.OrderByDescending(o => o.Created).FirstOrDefault();

                var fvfys = (fv.FinancialYears != null && fv.FinancialYears.Any()) ? fv.FinancialYears : GetFullValuationInputYears(fv.Id).AsEnumerable();
                fv.Currency = string.IsNullOrEmpty(fv.ISOCurrencySymbol) ? null : GetCurrencies().FirstOrDefault(o => o.ISOCurrencySymbol == fv.ISOCurrencySymbol);
                fv.Country = string.IsNullOrEmpty(fv.ThreeLetterISORegionName) ? null : GetCountries().FirstOrDefault(o => o.ThreeLetterISORegionName == fv.ThreeLetterISORegionName);

                var fvm = FullValuationModel.ReadFullValuation(fv, client, fvfys);
                return fvm;
            }
            else if (!fullValuationInputId.HasValue)//shouldnt enter if we were expecting a specific fv and none found (dont create from preval)
            {



                PrelimEvaluation preval = null;
                if (client != null)
                {
                    preval = GetPrelimEvaluations(false).Where(o => o.Email == client.Email).OrderByDescending(o => o.Created).FirstOrDefault();
                }

                FullValuationModel result = null;


                //result = Newtonsoft.Json.JsonConvert.DeserializeObject<FullValuationModel>(json);
                result = new FullValuationModel
                {
                    ClientId = client?.Id ?? 0,
                    MemberId = memberId,
                    CompanyName = preval?.CompanyName,
                    //SelectedCurrencyId = preval?.CurrencyId,
                    CurrentCashOnHand = preval?.CurrentCashOnHand ?? 0,
                    CurrentInterestBearingDebt = preval?.CurrentInterestBearingDebt ?? 0,
                    TRBC2012HierarchicalID = preval?.TRBC2012HierarchicalID,
                    //IncomeStatement_Revenue = preval?.IncomeStatement_Revenue ?? 0,
                    //IncomeStatement_CostOfGoodsSold = preval?.IncomeStatement_CostOfGoodsSold ?? 0,
                    //IncomeStatement_OperatingExpense = preval?.IncomeStatement_OperatingExpense ?? 0
                    LastFinancialYearEnd = preval != null ? preval.LastFinancialYearEnd : new DateTime(),
                    ValuationDate = DateTime.UtcNow,

                };

                result.PrePolulateDefaults();

                return result;
            }
            throw new Exception("No record found");
        }

        

        public IQueryable<FullValuationInput> GetFullValuations()
        {
            return FullValuationRepository.All();
        }

        public IQueryable<FullValuationInput> GetClientFullValuations(Int64 clientId)
        {
            return FullValuationRepository.All().Where(o => o.ClientId == clientId);
        }

        public void SaveFullValuation(FullValuationInput item)
        {
            var fullValuationInputYears = item.FinancialYears.Select(o => o as FullValuationInputYear).ToList();
            try
            {
                item.FinancialYears.Clear();
                FullValuationRepository.AddUpdate(item);
                foreach (var fviy in fullValuationInputYears)
                {
                    fviy.FullValuationInput = item;
                }
                SaveFullValuationInputYear(fullValuationInputYears);
            }
            finally
            {
                //add them back
                foreach (var fviy in fullValuationInputYears)
                {
                    item.FinancialYears.Add(fviy);
                }
            }

        }
        #endregion


        #region Global Defaults
        /// <summary>
        /// Get the record by the Umbraco (or other) source id
        /// </summary>
        /// <param name="sourceId"></param>
        /// <returns></returns>
        public GlobalDefaults GetGlobalDefaults(Int64? inputBatchId = null)
        {
            InputBatch inputBatch = null;
            if (!inputBatchId.HasValue)
            {
                inputBatch = GetCurrentInputBatch<GlobalDefaults>();
                inputBatchId = inputBatch?.Id;
            }
            if (!inputBatchId.HasValue)
                return null;
            return GlobalDefaultsRepository.All().FirstOrDefault(o => o.InputBatchId == inputBatchId.Value);
        }

        public void SaveGlobalDefaults(GlobalDefaults item)
        {
            GlobalDefaultsRepository.AddUpdate(item);
        }


        #endregion

        #region Client
        /// <summary>
        /// Get the record by the Umbraco (or other) source id
        /// </summary>
        /// <param name="sourceId"></param>
        /// <returns></returns>
        //public PrelimEvaluation GetPrelimEvaluation(Guid sourceId)
        //{
        //    return PrelimEvaluationRepository.All().FirstOrDefault(o => o.SourceId == sourceId);
        //}

        public IEnumerable<PrelimEvaluation> GetPrelimEvaluations(bool includeAllTRBCValues, int? year = null, int? month = null, int? day = null)
        {
            List<ThompsonReutersBusinessClassification> trbcs = null;
            if (includeAllTRBCValues)
                trbcs = GetThompsonReutersBusinessClassifications().ToList();

            var result = PrelimEvaluationRepository.All().Where(o => (!year.HasValue || o.Created.Year == year.Value) && ((!year.HasValue || !month.HasValue) || (o.Created.Year == year.Value && o.Created.Month == month.Value) && ((!year.HasValue || !month.HasValue || !day.HasValue) || (o.Created.Year == year.Value && o.Created.Month == month.Value && o.Created.Day == day.Value)))).ToList().Select(o =>
                 {
                     if (includeAllTRBCValues)
                     {
                         o.ThomsonReutersBusinessClassification = trbcs.FirstOrDefault(a => a.TRBC2012HierarchicalID == o.TRBC2012HierarchicalID);
                     }
                     return o;
                 }
            );




            return result;
        }

        public IQueryable<PrelimEvaluation> GetPrelimEvaluation()
        {
            return PrelimEvaluationRepository.All();
        }


        public void SavePrelimEvaluation(PrelimEvaluation item)
        {
            PrelimEvaluationRepository.SaveChanges();
            PrelimEvaluationRepository.AddUpdate(item);
        }


        #endregion


        #region CountryLookup

        public IQueryable<Country> GetCountries()
        {
            return CountryRepository.All();
        }


        #endregion


        #region FullValuationReport
        public FullValuationReport GetFullValuationReport(Int64 fullValuationReportId)
        {
            var result = FullValuationReportRepository.All().Include(o => o.ActualPrior).Include(o => o.ActualCurrent).Include(o => o.Budget).Include(o => o.Forecast1).Include(o => o.Forecast2).FirstOrDefault(o => o.Id == fullValuationReportId);

            //if (result.ActualCurrent == null)
            //    result.ActualCurrent = FullValuationYearReportActualCurrentRepository.All().FirstOrDefault(o=> o.Id == result.ActualCurrentId)
            return result;
        }
        public IQueryable<FullValuationReport> GetClientFullValuationReports(Int64 clientId)
        {
            return FullValuationReportRepository.All().Include(o => o.ActualPrior).Include(o => o.ActualCurrent).Include(o => o.Budget).Include(o => o.Forecast1).Include(o => o.Forecast2).Include(o => o.Terminal).Where(o => o.ClientId == clientId);
        }

        public FullValuationReport GetFullValuationReport(int memberId, int? fullValuationReportId = null)
        {

            var client = GetClientByMemberId(memberId);

            var fvs = GetClientFullValuationReports(client.Id).Where(o => !fullValuationReportId.HasValue || o.Id == fullValuationReportId.Value);
            if (fvs.Any())
            {
                var result = fvs.FirstOrDefault();
 
                return result;
            }
            //else if (!fullValuationReportId.HasValue)//shouldnt enter if we were expecting a specific fv and none found (dont create from preval)
            //{
                 
            //    PrelimEvaluation preval = null;
            //    if (client != null)
            //    {
            //        preval = GetPrelimEvaluations(false).Where(o => o.Email == client.Email).OrderByDescending(o => o.Created).FirstOrDefault();
            //    }

            //    FullValuationModel result = null;


            //    //result = Newtonsoft.Json.JsonConvert.DeserializeObject<FullValuationModel>(json);
            //    result = new FullValuationModel
            //    {
            //        ClientId = client?.Id ?? 0,
            //        MemberId = memberId,
            //        CompanyName = preval?.CompanyName,
            //        //SelectedCurrencyId = preval?.CurrencyId,
            //        CurrentCashOnHand = preval?.CurrentCashOnHand ?? 0,
            //        CurrentInterestBearingDebt = preval?.CurrentInterestBearingDebt ?? 0,
            //        TRBC2012HierarchicalID = preval?.TRBC2012HierarchicalID,
            //        //IncomeStatement_Revenue = preval?.IncomeStatement_Revenue ?? 0,
            //        //IncomeStatement_CostOfGoodsSold = preval?.IncomeStatement_CostOfGoodsSold ?? 0,
            //        //IncomeStatement_OperatingExpense = preval?.IncomeStatement_OperatingExpense ?? 0
            //        LastFinancialYearEnd = preval != null ? preval.LastFinancialYearEnd : new DateTime(),
            //        ValuationDate = DateTime.UtcNow,

            //    };

            //    result.PrePolulateDefaults();

                //return result;
            //}
            throw new Exception("No record found");
        }


        public void SaveFullValuationReport(FullValuationReport item)
        {
            if (item.ActualPrior != null)
                FullValuationYearReportActualPriorRepository.AddUpdate(item.ActualPrior);
            if (item.ActualCurrent != null)
                FullValuationYearReportActualCurrentRepository.AddUpdate(item.ActualCurrent);
            if (item.Budget != null)
                FullValuationYearReportBudgetRepository.AddUpdate(item.Budget);
            if (item.Forecast1 != null)
                FullValuationYearReportForecast1Repository.AddUpdate(item.Forecast1);
            if (item.Forecast2 != null)
                FullValuationYearReportForecast2Repository.AddUpdate(item.Forecast2);
            if (item.Terminal != null)
                FullValuationYearReportTerminalRepository.AddUpdate(item.Terminal);

            FullValuationReportRepository.AddUpdate(item);
        }

        #endregion
    }
}
