﻿using EquityMaven.Data;
using EquityMaven.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Repository
{
    public abstract class EquityMavenRepositoryBase<T> : IRepository<T> where T : EntityItem
    {
        public void SaveChanges()
        {
            try
            {
                Context.SaveChanges();
            }
            catch(Exception ex)
            {
                var test = Context.GetValidationErrors().ToList();
                throw ex;
            }
        }

       
        

        protected EquityMavenContext Context;
        protected IDbSet<T> Set { get { return Context.Set<T>(); } }

        public EquityMavenRepositoryBase(EquityMavenContext context)
        {
            Context = context;
        }

        
        protected abstract T GetFromDb(T item);
        protected abstract T GetFromDb(T item, IEnumerable<T> dbItems);

        public void AddUpdate(IEnumerable<T> items, Action OnSave)
        {
            if (!items.Any())
                return;
            var dbItems = GetDBItems(items);
            foreach (var item in items)
            {
                AddUpdate(item, dbItems, null);
            }
            OnSave?.Invoke();
        }

        protected abstract IEnumerable<T> GetDBItems(IEnumerable<T> items);
        public void AddUpdate(T item, Action OnSave)
        {
            var dbItems = GetDBItems(new List<T> { item });
            AddUpdate(item, dbItems, OnSave);
        }

        public void AddUpdate(T item, IEnumerable<T> dbItems,Action OnSave)
        {
            var isNew = item.isNew;
            if (!isNew)
            {
                var dbItem = GetFromDb(item, dbItems);
                if (dbItem == null)
                    throw new Exception($"{item} exists but not found in Database");
                item.OnCopyFrom(dbItem);//copy values such as DateCreated
                Detach(dbItem);
            }
            Context.Entry(item).State = isNew ? EntityState.Added : EntityState.Modified;

            item.Created = isNew ? DateTime.UtcNow : item.Created;
            item.LastUpdated = DateTime.UtcNow;
            BeforeSave(item);
            OnSave?.Invoke();
        }

        public void AddUpdate(T item)
        {
            AddUpdate(item, () => SaveChanges());
        }

        protected abstract void BeforeSave(T item);
         
        public void AddUpdate(IEnumerable<T> items)
        {
            AddUpdate(items, () => { SaveChanges(); });
        }

        public void Delete(T item)
        {
            Set.Attach(item);
            Set.Remove(item);
        }
        public IQueryable<T> All()
        {
            return Set;
        }

        protected abstract T GetFromDbById(object Id);
        public void Delete(object id)
        {
            T item = id as T;
            item = item ?? GetFromDbById(id);
            if (item != null)
            {
                Delete(item);
            }
        }

        public void Detach(T entity)
        {
            Context.Detach(entity);
        }
    }


    public class EquityMavenRepositoryGUID<T> : EquityMavenRepositoryBase<T> where T : EntityItemGUID
    {
        public EquityMavenRepositoryGUID(EquityMavenContext context) : base(context)
        {
        }
        protected override T GetFromDb(T item, IEnumerable<T> dbItems)
        {
            return (dbItems.FirstOrDefault(o => o.Id == item.Id));
        }
        protected override T GetFromDb(T item)
        {
            return GetFromDbById(item.Id);
        }
        protected override IEnumerable<T> GetDBItems(IEnumerable<T> items)
        {
            //results in a sql statement along the lines of Where x in (1,2,3,4)
            //which has practical limitations, so break it into multiple calls max 100 ids at a time
            List<T> dbItems = new List<T>();
            var idList = items.Select(o => o.Id).ToList();
            while (idList.Any())
            {
                var nextCount = Math.Min(100, idList.Count);
                var sublist = idList.Take(nextCount);
                dbItems.AddRange(Set.Where(o => sublist.Any(id => id == o.Id)));
                idList = idList.Skip(nextCount).Take((idList.Count - nextCount)).ToList();
            }

            return dbItems;
        }
        protected override T GetFromDbById(object Id)
        {
            if (!(Id is Guid))
            {
                throw new ArgumentException($"{Id} but Guid Expected");
            }
            return Set.FirstOrDefault(o => o.Id == (Guid)Id);
        }
        protected override void BeforeSave(T item)
        {
            if (item.isNew)
            {
                item.Id = Guid.NewGuid();
            }
        }

    }

    public class EquityMavenRepositoryString<T> : EquityMavenRepositoryBase<T> where T : EntityItemString
    {

        public EquityMavenRepositoryString(EquityMavenContext context) : base(context)
        {
        }
 

        protected override IEnumerable<T> GetDBItems(IEnumerable<T> items)
        {
            //results in a sql statement along the lines of Where x in (1,2,3,4)
            //which has practical limitations, so break it into multiple calls max 100 ids at a time
            List<T> dbItems = new List<T>();
            var idList = items.Select(o => o.Id).ToList();
            while (idList.Any())
            {
                var nextCount = Math.Min(100, idList.Count);
                var sublist = idList.Take(nextCount);
                dbItems.AddRange(Set.Where(o => sublist.Any(id => id == o.Id)));
                idList = idList.Skip(nextCount).Take((idList.Count - nextCount)).ToList();
            }

            return dbItems;
        }
        protected override T GetFromDb(T item, IEnumerable<T> dbItems)
        {
            return (dbItems.FirstOrDefault(o => o.Id == item.Id));
        }
        protected override T GetFromDb(T item)
        {
            return GetFromDbById(item.Id);
        }

        protected override T GetFromDbById(object Id)
        {
            if (!(Id is string))
            {
                throw new ArgumentException($"{Id} but String Expected");
            }
            return Set.FirstOrDefault(o => o.Id == (string)Id);
        }
        protected override void BeforeSave(T item)
        {
            if (item.isNew)
            {
            }
        }

        
    }
    public class EquityMavenRepositoryInt<T> : EquityMavenRepositoryBase<T> where T : EntityItemInt
    {
        public EquityMavenRepositoryInt(EquityMavenContext context) : base(context)
        {
        }

         

        protected override IEnumerable<T> GetDBItems(IEnumerable<T> items)
        {
            //results in a sql statement along the lines of Where x in (1,2,3,4)
            //which has practical limitations, so break it into multiple calls max 100 ids at a time
            List<T> dbItems = new List<T>();
            var idList = items.Select(o => o.Id).ToList();
            while (idList.Any())
            {
                var nextCount = Math.Min(100, idList.Count);
                var sublist = idList.Take(nextCount);
                dbItems.AddRange(Set.Where(o => sublist.Any(id => id == o.Id)));
                idList = idList.Skip(nextCount).Take((idList.Count - nextCount)).ToList();
            }

            return dbItems;
        }
        protected override T GetFromDb(T item, IEnumerable<T> dbItems)
        {
            return (dbItems.FirstOrDefault(o => o.Id == item.Id));
        }
        protected override T GetFromDb(T item)
        {
            return GetFromDbById(item.Id);
        }

        protected override T GetFromDbById(object Id)
        {
            if (!(Id is Int64))
            {
                throw new ArgumentException($"{Id} but Int64 Expected");
            }
            return Set.FirstOrDefault(o => o.Id == (Int64)Id);
        }
        protected override void BeforeSave(T item)
        {
            if (item.isNew)
            {
            }
        }

    }





}
