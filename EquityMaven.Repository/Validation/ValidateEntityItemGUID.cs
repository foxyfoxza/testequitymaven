﻿using EquityMaven.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Repository.Validation
{
    public class ValidateEntityItemGUID : IEntityItemValidation<EntityItemGUID>
    {
        public bool ExceedsThresholdMessage(float count, float failedValidationCount)
        {
            throw new NotImplementedException();
        }

        public void Validate<TdbItem>(EntityItemGUID item, IQueryable<EntityItemGUID> dbItems, bool isNew, out TdbItem dbItem, out Exception error) where TdbItem : EntityItemGUID
        {
            var errors = new List<Exception>();
            var _dbItem = dbItems.FirstOrDefault(o => o.Id.Equals(item.Id)) ;
            dbItem = _dbItem as TdbItem;

            if (!((_dbItem == null || _dbItem is TdbItem) && item is TdbItem))
                errors.Add(new InvalidOperationException($"{typeof(TdbItem)} is not {item.GetType()} "));

            if (_dbItem != null && isNew)
                errors.Add(new InvalidOperationException($"{item.GetType()} cannot be inserted as duplicate ID {item.Id} already exists"));
            error = !errors.Any() ? null : new AggregateException($"Validation failed {item}", errors);
        }

        public void Validate<TdbItem>(EntityItemGUID item, IQueryable<EntityItemGUID> dbItems, bool isNew, out TdbItem dbItem ) where TdbItem : EntityItemGUID
        {
            Exception error;
            Validate<TdbItem>(item, dbItems, isNew, out dbItem, out error);
            if (error != null)
                throw error;
        }

    }
}
