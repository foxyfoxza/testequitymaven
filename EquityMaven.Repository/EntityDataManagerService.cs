﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Repository
{
    public static class EntityDataManagerService
    {
        private static object padlock = new object();
        private static EntityDataManager _DataManager;
        private static EquityMavenContext _DBContext;
        public static EntityDataManager DataManager
        {
            get
            {
                
                if (_DataManager == null)
                {
                    lock (padlock)
                    {
                        //weird code to ensure that the minor chance that _datamanager was assigned before entering the lock is catered for
                        if (_DataManager == null)
                        { 
                            _DBContext = _DBContext ?? new EquityMavenContext();
                            _DataManager = _DataManager ?? new EntityDataManager(_DBContext);
                        }
                    }
                }
                return _DataManager;
            }
        }



    }
}
