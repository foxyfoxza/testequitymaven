﻿using EquityMaven.Data;
using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EquityMaven.Interfaces.ClientInputs;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;

namespace EquityMaven.Repository
{


    public class EquityMavenContext : DbContext
    {

        public EquityMavenContext() : base("EquityMavenContext")
        {

        }


        public DbSet<CountryMoodysRating> CountryMoodysRatings { get; set; }
        public DbSet<CountryTaxRate> CountryTaxRates { get; set; }
        public DbSet<ClientProfile> ClientProfiles { get; set; }
        public DbSet<PrelimEvaluation> PrelimEvaluations { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Country> Countries { get; set; }

        public DbSet<CorporateBondSpread> CorporateBondSpreads { get; set; }


        public DbSet<MoodysRating> MoodysRatings { get; set; }
        public DbSet<TRBCValue> TRBCValues { get; set; }
        //public DbSet<RawMoodysRatingLookup> MoodysRatingLookups { get; set; }
        //public DbSet<RawCorporateBondSpread> CorporateBondSpreads { get; set; }
        //public DbSet<RawCountryLookup> CountryLookups { get; set; }
        public DbSet<CurrencyRiskFreeRate> CurrencyRiskFreeRate { get; set; }
        public DbSet<InputBatch> InputsBatches { get; set; }

        public DbSet<ThompsonReutersBusinessClassification> ThompsonReutersBusinessClassifications { get; set; }

        public DbSet<FullValuationInput> FullValuations { get; set; }
        public DbSet<FullValuationInputYear> FullValuationInputYears { get; set; }

        public DbSet<FullValuationReport> FullValuationReports { get; set; }
        public DbSet<FullValuationYearReportActualPrior> FullValuationYearReportsActualPrior { get; set; }
        public DbSet<FullValuationYearReportActualCurrent> FullValuationYearReportsActualCurrent { get; set; }
        public DbSet<FullValuationYearReportBudget> FullValuationYearReportsBudget { get; set; }
        public DbSet<FullValuationYearReportForecast1> FullValuationYearReportsForecast1 { get; set; }
        public DbSet<FullValuationYearReportForecast2> FullValuationYearReportsForecast2 { get; set; }
        public DbSet<FullValuationYearReportTerminal> FullValuationYearReportsTerminal { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {



            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Add(new AttributeToColumnAnnotationConvention<MinLengthAttribute, int>("MinLength", (property, attributes) => attributes.Single().Length));

            var _entityItem = modelBuilder.Types<EntityItem>();
            _entityItem.Configure(x => x.Property(y => y.LastUpdated).IsRequired());
            _entityItem.Configure(x => x.Property(y => y.Created).IsRequired());

            var _entityItemGUID = modelBuilder.Types<EntityItemGUID>();
            _entityItemGUID.Configure(x => x.HasKey(y => y.Id));

            var _entityItemInt = modelBuilder.Types<EntityItemInt>();
            _entityItemInt.Configure(x => x.HasKey(y => y.Id));


            var _entityBatchedItem = modelBuilder.Types<EntityBatchedItem>();
            _entityBatchedItem.Configure(x => x.Property(y => y.InputBatchId).IsRequired());


            var _cpr = modelBuilder.Entity<PrelimEvaluation>();
            _cpr.HasEntitySetName("PrelimEvaluation");
            _cpr.Property(t => t.ISOCurrencySymbol).IsRequired();
            //_cpr.HasIndex(p => new { p.InputBatchId, p.TRBC2012HierarchicalID }).IsUnique();
            _cpr.HasKey(t => t.Id);

            _cpr.Property(t => t.Email).IsRequired();

            var _client = modelBuilder.Entity<Client>();
            //_client.HasIndex(p => new { p.InputBatchId, p.TRBC2012HierarchicalID }).IsUnique();
            _client.HasKey(t => t.Id);

            var _clientProfile = modelBuilder.Entity<ClientProfile>();
            _clientProfile.HasKey(t => t.Id);
            var _country = modelBuilder.Entity<Country>();
            _country.HasKey(t => t.Id);
            //_country.HasIndex(p => new { p.ThreeLetterISORegionName}).IsUnique();
            //_country.HasIndex(p => new { p.TwoLetterISORegionName }).IsUnique();

            _country.HasMany<CountryTaxRate>(s => s.CountryTaxRates)
                    .WithRequired(g => g.Country)
                    .WillCascadeOnDelete(false);

            _country.HasMany<CountryMoodysRating>(s => s.CountryMoodysRatings)
                    .WithRequired(g => g.Country)
                    .WillCascadeOnDelete(false);


            var _currency = modelBuilder.Entity<Currency>();
            _currency.HasIndex(p => new { p.ISOCurrencySymbol, p.InputBatchId }).IsUnique();
            _currency.Property(t => t.ISOCurrencySymbol).IsRequired().HasColumnType("varchar").HasMaxLength(3);
            _currency.Property(t => t.CurrencyEnglishName).IsOptional().HasColumnType("varchar").HasMaxLength(50);

            //_currency.HasMany<CurrencyRiskFreeRate>(s => s.CurrencyRiskFreeRates)
            //        .WithRequired(g => g.Currency)
            //        .WillCascadeOnDelete(false);

            var _moodysRating = modelBuilder.Entity<MoodysRating>();
            _moodysRating.HasIndex(p => new { p.InputBatchId, p.Rating }).IsUnique();
            _moodysRating.Property(t => t.Rating).IsRequired().HasColumnType("varchar").HasMaxLength(10);

            var _corporateBondSpread = modelBuilder.Entity<CorporateBondSpread>();
            _corporateBondSpread.HasIndex(p => new { p.InputBatchId, p.InterestCoverageFrom }).IsUnique();
            _corporateBondSpread.Property(t => t.Rating).IsRequired().HasColumnType("varchar").HasMaxLength(10);

            var _CurrencyRiskFreeRate = modelBuilder.Entity<CurrencyRiskFreeRate>();
            _CurrencyRiskFreeRate.HasIndex(p => new { p.CurrencyId, p.InputBatchId }).IsUnique();
            _CurrencyRiskFreeRate.Property(t => t.RiskFreeRate).IsRequired().HasPrecision(38, 15);

            var _ib = modelBuilder.Entity<InputBatch>();
            _ib.HasIndex(p => new { p.ActiveFrom, p.BatchTypeId }).IsUnique();
            //_ib.HasKey(t => t.Id);
            //_ib.Property(t => t.ActiveFrom).IsRequired();
            _ib.Property(y => y.isActive).IsRequired();


            var _gd = modelBuilder.Entity<GlobalDefaults>();
            //_gd.HasKey(p => new { p.InputBatchId });
            _gd.Property(t => t.IlliquidityDiscount).IsRequired().HasPrecision(38, 15);
            _gd.Property(t => t.PercentageOperatingCashRequired).IsRequired().HasPrecision(38, 15);


            var _ThompsonReutersBusinessClassification = modelBuilder.Entity<ThompsonReutersBusinessClassification>();
            //_trbcActivity.HasKey(p => new { p.Id, p.InputBatchId });

            //_ThompsonReutersBusinessClassification.HasRequired<IInputBatch>(s => s.InputBatch);
            //.WithMany(g => g.ThompsonReutersBusinessClassifications)
            //.HasForeignKey<Int64>(s => s.InputBatchId).WillCascadeOnDelete(false);

            var _ctr = modelBuilder.Entity<CountryTaxRate>();
            _ctr.Property(t => t.CountryId).IsRequired();
            _ctr.Property(t => t.TaxRate).IsRequired().HasPrecision(38, 15);



            //_ctr.HasRequired<Country>(s => s.Country)
            //        .WithOptional(g => g.CountryTaxRate)
            //        .WillCascadeOnDelete(false);


            var _cmr = modelBuilder.Entity<CountryMoodysRating>();
            _cmr.Property(t => t.CountryId).IsRequired();
            //_cmr.HasRequired<Country>(s => s.Country)
            //        .WithOptional(g => g.CountryMoodysRating)
            //        .WillCascadeOnDelete(false);



            var _trbcValue = modelBuilder.Entity<TRBCValue>();
            //_trbcValue.HasIndex(p => new { p.InputBatchId, p.ActivityId, p.Id}).IsUnique();
            _trbcValue.Property(t => t.ThompsonReutersBusinessClassificationId).IsRequired();
            _trbcValue.Property(t => t.AccPayableDaysAve).IsOptional();
            _trbcValue.Property(t => t.AccRecDaysAve).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.Africa).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.Asia).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.AustraliaNewZealand).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.CurrentRatio).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.EasternEuropeRussia).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.EBITDAMargin).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.EBITMargin).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.GrossMargin).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.InterestCover).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.InterestCoverAdjusted).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.InventoryDaysAve).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.MiddleEast).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.NorthAmerica).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.SouthCentralAmerica).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.SouthernEurope).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.TotalDebtEBITDA).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.TotalDebtMarketCap).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.UnleveredBeta).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.WesternEurope).IsOptional().HasPrecision(38, 15);
            _trbcValue.Property(t => t.World).IsOptional().HasPrecision(38, 15);


            var _fullValuationInput = modelBuilder.Entity<FullValuationInput>();
            _fullValuationInput.Property(t => t.ThreeLetterISORegionName).IsRequired();
            _fullValuationInput.Property(t => t.ISOCurrencySymbol).IsRequired();


            var _fullValuationInputYear = modelBuilder.Entity<FullValuationInputYear>();

            //_fullValuationInputYear.HasIndex(p => new { p.FullValuationInputId, p.FinancialYearTypeValue}).IsUnique();



            var _fullValuationReport = modelBuilder.Entity<FullValuationReport>();

            var _fullValuationYearReportActualPrior = modelBuilder.Entity<FullValuationYearReportActualPrior>();
            var _fullValuationYearReportActualCurrent = modelBuilder.Entity<FullValuationYearReportActualCurrent>();
            var _fullValuationYearReportBudget = modelBuilder.Entity<FullValuationYearReportBudget>();
            var _fullValuationYearReportForecast1 = modelBuilder.Entity<FullValuationYearReportForecast1>();
            var _fullValuationYearReportForecast2 = modelBuilder.Entity<FullValuationYearReportForecast2>();
            var _fullValuationYearReportTerminal = modelBuilder.Entity<FullValuationYearReportTerminal>();

            modelBuilder.Entity<FullValuationYearReportActualPrior>()
              .HasOptional(fy => fy.FullValuationReport) // Mark FullValuationReport property optional in ActualPrior entity
              .WithOptionalPrincipal(fvr => fvr.ActualPrior); // mark FullValuationReport property as required in ActualPrior entity. Cannot save ActualPrior without FullValuationReport

            modelBuilder.Entity<FullValuationYearReportActualCurrent>()
             .HasOptional(fy => fy.FullValuationReport) // Mark FullValuationReport property optional in ActualPrior entity
             .WithOptionalPrincipal(fvr => fvr.ActualCurrent); // mark FullValuationReport property as required in ActualPrior entity. Cannot save ActualPrior without FullValuationReport

            modelBuilder.Entity<FullValuationYearReportBudget>()
             .HasOptional(fy => fy.FullValuationReport) // Mark FullValuationReport property optional in ActualPrior entity
             .WithOptionalPrincipal(fvr => fvr.Budget); // mark FullValuationReport property as required in ActualPrior entity. Cannot save ActualPrior without FullValuationReport


            modelBuilder.Entity<FullValuationYearReportForecast1>()
            .HasOptional(fy => fy.FullValuationReport) // Mark FullValuationReport property optional in ActualPrior entity
            .WithOptionalPrincipal(fvr => fvr.Forecast1); // mark FullValuationReport property as required in ActualPrior entity. Cannot save ActualPrior without FullValuationReport


            modelBuilder.Entity<FullValuationYearReportForecast2>()
            .HasOptional(fy => fy.FullValuationReport) // Mark FullValuationReport property optional in ActualPrior entity
            .WithOptionalPrincipal(fvr => fvr.Forecast2); // mark FullValuationReport property as required in ActualPrior entity. Cannot save ActualPrior without FullValuationReport


            modelBuilder.Entity<FullValuationYearReportTerminal>()
            .HasOptional(fy => fy.FullValuationReport) // Mark FullValuationReport property optional in ActualPrior entity
            .WithOptionalPrincipal(fvr => fvr.Terminal); // mark FullValuationReport property as required in ActualPrior entity. Cannot save ActualPrior without FullValuationReport


            #region fullValuationReport Precision
            _fullValuationReport.Property(o => o.ProbabilityOfFailure).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.ProbabilityOfSurvival).HasPrecision(23, 10);


            _fullValuationReport.Property(o => o.Beta_LeveredBeta).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.Beta_MarginalTaxRate).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.Beta_TotalDebtToEquityMarketCap).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Beta_UnleveredBeta).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.CostOfDebt_After_TaxCostOfDebt).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.CostOfDebt_EstimatedCorporateDefaultSpread).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.CostOfDebt_EstimatedCostOfDebt).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.CostOfDebt_EstimatedCountryDefaultSpread).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.CostOfDebt_InterestCoverageRatio).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.CostOfDebt_MarginalTaxRate).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.CostOfDebt_RiskFreeRate).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.CurrentCashOnHand).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.DiscountedCashFlow_DiscretePeriod).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.DiscountedCashFlow_EnterpriseValuePre_IlliquidityDiscount).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.DiscountedCashFlow_EquityValuePost_IlliquidityDiscount).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.DiscountedCashFlow_EquityValuePre_IlliquidityDiscount).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.DiscountedCashFlow_ExcessCash).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.DiscountedCashFlow_IlliquidityDiscount).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.DiscountedCashFlow_InterestBearingDebt).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.DiscountedCashFlow_TerminalPeriod).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.EquityRiskPremium_CountryRatingBasedDefaultSpread).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.EquityRiskPremium_EquityRiskPremium).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.EquityRiskPremium_MatureMarketImpliedEquityRiskPremium).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.EquityRiskPremium_RelativeVolatilityOfEquityVsBonds).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.EvOverEbitdaValuation_EnterpriseValuePre_IlliquidityDiscount).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.EvOverEbitdaValuation_EquityValuePost_IlliquidityDiscount).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.EvOverEbitdaValuation_EquityValuePre_IlliquidityDiscount).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.EvOverEbitdaValuation_ExcessCash).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.EvOverEbitdaValuation_IlliquidityDiscount).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.EvOverEbitdaValuation_InterestBearingDebt).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.EvOverEbitdaValuation_SustainableEbitdaTtm).HasPrecision(23, 10);


            #region EV/EBITDA - World Median


            _fullValuationReport.Property(o => o.WorldMedian_World).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.WorldMedian_Africa).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.WorldMedian_NorthAmerica).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.WorldMedian_SouthCentralAmerica).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.WorldMedian_Asia).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.WorldMedian_AustraliaNewZealand).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.WorldMedian_EasternEuropeRussia).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.WorldMedian_SouthernEurope).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.WorldMedian_WesternEurope).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.WorldMedian_MiddleEast).HasPrecision(23, 10);

            #endregion


            #region Industry TRBC Values

            _fullValuationReport.Property(o => o.Industry_EBITMargin).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Industry_EBITDAMargin).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Industry_GrossProfitMargin).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Industry_CurrentRatio).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Industry_InterestCover).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Industry_TotalDebt_Over_Ebitda).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Industry_AccountsPayableDaysAverage).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Industry_InventoryDaysAverage).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Industry_AccountsReceiveableDaysAverage).HasPrecision(23, 10);


            #endregion


            _fullValuationReport.Property(o => o.IlliquidityDiscount).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.PercentageOperatingCashRequired).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.Risk_FreeRate_Risk_FreeRate).HasPrecision(23, 10);



            _fullValuationReport.Property(o => o.WaccCalculation_D_Over_D_Plus_E).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.WaccCalculation_E_Over_D_Plus_E).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.WaccCalculation_Kd).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.WaccCalculation_Ke).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.WaccCalculation_Wacc).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.WaccInputsSummary_CompanyRiskPremium).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.WaccInputsSummary_CostOfDebtPre_Tax).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.WaccInputsSummary_EquityRiskPremium).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.WaccInputsSummary_GearingD_Over_D_Plus_E).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.WaccInputsSummary_LeveredBeta).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.WaccInputsSummary_Risk_FreeRate).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.WaccInputsSummary_TaxRate).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.WaccInputsSummary_TerminalGrowthRate).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.WaccInputsSummary_TotalDebt_Over_EquityD_Over_E).HasPrecision(23, 10);





            #region LiquidationvalueRecoveryRate
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryRate_AccountsReceiveable).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryRate_Inventory).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryRate_OtherCurrentAssets).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryRate_CashAndCashEquivalents).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryRate_IntangibleAssets).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryRate_FixedAssets).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryRate_AccountsPayable).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryRate_OtherCurrentLiabilities).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryRate_3rdPartyInterest_BearingDebtLongTermShortTerm).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryRate_OtherLong_TermLiabilities).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryRate_ShareholderLoans).HasPrecision(23, 10);

            #endregion


            #region LiquidationvalueRecoveryAmount 
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_CurrentAssets).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_AccountsReceiveable).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_Inventory).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_OtherCurrentAssets).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_CashAndCashEquivalents).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_IntangibleAssets).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_FixedAssets).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_TotalAssets).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_CurrentLiabilities).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_AccountsPayable).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_OtherCurrentLiabilities).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_3rdPartyInterest_BearingDebtLongTermShortTerm).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_OtherLong_TermLiabilities).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_ShareholderLoans).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_TotalLiabilities).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.LiquidationvalueRecoveryAmount_LiquidationValue).HasPrecision(23, 10);

            #endregion

            #region EQUITY VALUE - POST SURVIVAL ADJUSTMENT




            #endregion



            #region ENTERPRISE VALUE - POST SURVIVAL ADJUSTMENT



            #endregion


            #region SUMMARY - DCF, MULTIPLES, VC METHOD (PRE-SURVIVAL ADJUSTMENT)


            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentDCF_Discrete).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentDCF_Terminal).HasPrecision(23, 10);


            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentDCF_EnterpriseValuePre_IlliquidityDiscount).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentDCF_ExcessCash).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentMultiples_EnterpriseValuePre_IlliquidityDiscount).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentDCF_EquityValuePre_IlliquidityDiscount).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentMultiples_EquityValuePre_IlliquidityDiscount).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentDCF_IlliquidityDiscount).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentMultiples_IlliquidityDiscount).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount).HasPrecision(23, 10);


            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPre_LiquidityDiscount).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPre_LiquidityDiscount).HasPrecision(23, 10);



            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentDCF_EnterpriseValuePost_IlliquidityDiscount).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentMultiples_EnterpriseValuePost_IlliquidityDiscount).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentVCMethod_EnterpriseValuePost_IlliquidityDiscount).HasPrecision(23, 10);

            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPost_LiquidityDiscount).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPost_LiquidityDiscount).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.Pre_SurvivalAdjustmentVCMethod_ImpliedEv_Over_EbitdaPost_LiquidityDiscount).HasPrecision(23, 10);



            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_EBITDA_TTM_1).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_EBITDA_TTM_2).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_EBITDA_TTM_3).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_EBITDA_TTM_4).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_EBITDA_TTM_5).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_AMultiple).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_A1).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_A2).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_A3).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_A4).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_A5).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_BMultiple).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_B1).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_B2).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_B3).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_B4).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_B5).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_CMultiple).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_C1).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_C2).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_C3).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_C4).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_C5).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_DMultiple).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_D1).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_D2).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_D3).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_D4).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_D5).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_EMultiple).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_E1).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_E2).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_E3).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_E4).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedMultiples_E5).HasPrecision(23, 10);


            #endregion


            #endregion


            #region fullValuationYearReportActualPrior Precision

            _fullValuationYearReportActualPrior.Property(o => o.AssetsProjection_FixedAndIntangibleAssets).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetAnalysis_CurrentRatio).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetAnalysis_TotalDebtOverEBITDA).HasPrecision(23, 10);
            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetEquityAndLiabilities_AccountsPayable).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetEquityAndLiabilities_CurrentLiabilities).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetEquityAndLiabilities_LongTermDebt).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetEquityAndLiabilities_ShareholderLoans).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetEquityAndLiabilities_ShortTermDebt).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetEquityAndLiabilities_TotalLiabilities).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetShareholdersEquity).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetTotalAssets_AccountsReceiveable).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetTotalAssets_CashAndCashEquivalents).HasPrecision(23, 10);
            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetTotalAssets_CurrentAssets).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetTotalAssets_FixedAndIntangibleAssets).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetTotalAssets_FixedAssets).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetTotalAssets_IntangibleAssets).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetTotalAssets_Inventory).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetTotalAssets_OtherCurrentAssets).HasPrecision(23, 10);
            _fullValuationYearReportActualPrior.Property(o => o.BalanceSheetTotalAssets_TotalAssets).HasPrecision(23, 10);
            _fullValuationYearReportActualPrior.Property(o => o.CapitalExpenditure_CapitalExpenditure).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.CapitalExpenditure_PercentageForecastCapitalExpenditure).HasPrecision(23, 10);


            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatementAnalysis_EBITDAMargin).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatementAnalysis_EBITMargin).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatementAnalysis_InterestCover).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatementAnalysis_PercentageGrossProfitMargin).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatement_CostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatement_DepreciationAndAmortisationExpense).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatement_EarningsBeforeTax).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatement_EBIT).HasPrecision(23, 10);
            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatement_EBITDA).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatement_GrossProfit).HasPrecision(23, 10);
            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatement_InterestExpense).HasPrecision(23, 10);
            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatement_NonCashExpense).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatement_OperatingExpense).HasPrecision(23, 10);

            _fullValuationYearReportActualPrior.Property(o => o.IncomeStatement_Revenue).HasPrecision(23, 10);
            _fullValuationYearReportActualPrior.Property(o => o.NetWorkingCapital_CurrentAssets).HasPrecision(23, 10);
            _fullValuationYearReportActualPrior.Property(o => o.NetWorkingCapital_NetWorkingCapital).HasPrecision(23, 10);


            #endregion

            #region  FullValuationYearReportActualCurrent  Precision


            _fullValuationYearReportActualCurrent.Property(o => o.AssetsProjection_FixedAndIntangibleAssets).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetAnalysis_AverageAccountsPayableDays).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetAnalysis_AverageAccountsReceiveableDays).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetAnalysis_AverageInventoryDays).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetAnalysis_CurrentRatio).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetAnalysis_TotalDebtOverEBITDA).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetEquityAndLiabilities_AccountsPayable).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetEquityAndLiabilities_CurrentLiabilities).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetEquityAndLiabilities_LongTermDebt).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetEquityAndLiabilities_ShareholderLoans).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetEquityAndLiabilities_ShortTermDebt).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetEquityAndLiabilities_TotalLiabilities).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetShareholdersEquity).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetTotalAssets_AccountsReceiveable).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetTotalAssets_CashAndCashEquivalents).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetTotalAssets_CurrentAssets).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetTotalAssets_FixedAndIntangibleAssets).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetTotalAssets_FixedAssets).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetTotalAssets_IntangibleAssets).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetTotalAssets_Inventory).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetTotalAssets_OtherCurrentAssets).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.BalanceSheetTotalAssets_TotalAssets).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.CapitalExpenditure_CapitalExpenditure).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.CapitalExpenditure_CapitalExpenditureTTM).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.CapitalExpenditure_PercentageForecastCapitalExpenditure).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.DiscountedCashFlow_FreeCashFlow).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.ExcessCash_ExcessCash).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.ExcessCash_RevenueTTM).HasPrecision(23, 10);


            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatementAnalysis_EBITDAMargin).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatementAnalysis_EBITMargin).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatementAnalysis_InterestCover).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatementAnalysis_PercentageGrossProfitMargin).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatementAnalysis_PercentageRevenueGrowth).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatement_CostOfGoodsSold).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatement_DepreciationAndAmortisationExpense).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatement_EarningsBeforeTax).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatement_EBIT).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatement_EBITDA).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatement_GrossProfit).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatement_InterestExpense).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatement_NonCashExpense).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatement_OperatingExpense).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.IncomeStatement_Revenue).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.NetWorkingCapital_ChangeInNetWorkingCapital).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.NetWorkingCapital_CurrentAssets).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.NetWorkingCapital_NetWorkingCapital).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.SustainableEBITDA_NonRecurringExpenses).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.SustainableEBITDA_NonRecurringIncome).HasPrecision(23, 10);

            _fullValuationYearReportActualCurrent.Property(o => o.SustainableEBITDA_SustainableEBITDA).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.SustainableEBITDA_SustainableEBITDA_TTM).HasPrecision(23, 10);



            #region liquidation value
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_Revenue_Ttm).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_CostOfGoodsSold_Ttm).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_Capex_Ttm).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_DepreciationAmortisation_Ttm).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_CurrentAssets).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_AccountsReceiveable).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_Inventory).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_OtherCurrentAssets).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_CashAndCashEquivalents).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_IntangibleAssets).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_FixedAssets).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_TotalAssets).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_CurrentLiabilities).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_AccountsPayable).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_OtherCurrentLiabilities).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_3rdPartyInterest_BearingDebtLongTermShortTerm).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_OtherLong_TermLiabilities).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_ShareholderLoans).HasPrecision(23, 10);
            _fullValuationYearReportActualCurrent.Property(o => o.Liquidationvalue_TotalLiabilities).HasPrecision(23, 10);
            #endregion



            #endregion

            #region FullValuationYearReportBudget Precision


            _fullValuationYearReportBudget.Property(o => o.AssetsProjection_FixedAndIntangibleAssets).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetAnalysis_AverageAccountsPayableDays).HasPrecision(23, 10);
            _fullValuationYearReportBudget.Property(o => o.BalanceSheetAnalysis_AverageAccountsReceiveableDays).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetAnalysis_AverageInventoryDays).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetEquityAndLiabilities_AccountsPayable).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetEquityAndLiabilities_TotalLiabilities).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetTotalAssets_AccountsReceiveable).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetTotalAssets_CurrentAssets).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetTotalAssets_Inventory).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetTotalAssets_OtherCurrentAssets).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.BalanceSheetTotalAssets_TotalAssets).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.CapitalExpenditure_CapitalExpenditure).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.CapitalExpenditure_PercentageForecastCapitalExpenditure).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_AdjustedFreeCashFlow).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_AdjustmentForFirstYear).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure).HasPrecision(23, 10);
            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_DiscountedFreeCashFlow).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_DiscountFactor).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_DiscountFactorInverse).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_DiscountPeriodMidYear).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_DiscountPeriodMid_Year).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_DiscountPeriodYearEnd).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_DiscountPeriodYear_End).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_FreeCashFlow).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_IncomeStatement_EBIT).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_IncomeStatement_NonCashExpense).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_IncomeStatement_Tax).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatementAnalysis_EBITDAMargin).HasPrecision(23, 10);
            _fullValuationYearReportBudget.Property(o => o.IncomeStatementAnalysis_EBITMargin).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatementAnalysis_PercentageGrossProfitMargin).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatementAnalysis_PercentageRevenueGrowth).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatement_CostOfGoodsSold).HasPrecision(23, 10);
            _fullValuationYearReportBudget.Property(o => o.IncomeStatement_DepreciationAndAmortisationExpense).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatement_EarningsBeforeTax).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatement_EBIT).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatement_EBITDA).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatement_GrossProfit).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatement_InterestExpense).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatement_NonCashExpense).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatement_OperatingExpense).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.IncomeStatement_Revenue).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.NetWorkingCapital_ChangeInNetWorkingCapital).HasPrecision(23, 10);
            _fullValuationYearReportBudget.Property(o => o.NetWorkingCapital_CurrentAssets).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.NetWorkingCapital_NetWorkingCapital).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.SustainableEBITDA_NonRecurringExpenses).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.SustainableEBITDA_NonRecurringIncome).HasPrecision(23, 10);

            _fullValuationYearReportBudget.Property(o => o.SustainableEBITDA_SustainableEBITDA).HasPrecision(23, 10);


            #endregion

            #region FullValuationYearReportForecast1 Precision


            _fullValuationYearReportForecast1.Property(o => o.AssetsProjection_FixedAndIntangibleAssets).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetAnalysis_AverageAccountsPayableDays).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetAnalysis_AverageAccountsReceiveableDays).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetAnalysis_AverageInventoryDays).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetEquityAndLiabilities_AccountsPayable).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetEquityAndLiabilities_TotalLiabilities).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetTotalAssets_AccountsReceiveable).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetTotalAssets_CurrentAssets).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetTotalAssets_Inventory).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetTotalAssets_OtherCurrentAssets).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.BalanceSheetTotalAssets_TotalAssets).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.CapitalExpenditure_CapitalExpenditure).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.CapitalExpenditure_PercentageForecastCapitalExpenditure).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_AdjustedFreeCashFlow).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_DiscountedFreeCashFlow).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_DiscountFactor).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_DiscountFactorInverse).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_DiscountPeriodMidYear).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_DiscountPeriodMid_Year).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_DiscountPeriodYearEnd).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_DiscountPeriodYear_End).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_FreeCashFlow).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_IncomeStatement_EBIT).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_IncomeStatement_NonCashExpense).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_IncomeStatement_Tax).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital).HasPrecision(23, 10);


            _fullValuationYearReportForecast1.Property(o => o.IncomeStatementAnalysis_EBITDAMargin).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.IncomeStatementAnalysis_EBITMargin).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.IncomeStatementAnalysis_PercentageGrossProfitMargin).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.IncomeStatementAnalysis_PercentageRevenueGrowth).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.IncomeStatement_CostOfGoodsSold).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.IncomeStatement_DepreciationAndAmortisationExpense).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.IncomeStatement_EarningsBeforeTax).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.IncomeStatement_EBIT).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.IncomeStatement_EBITDA).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.IncomeStatement_GrossProfit).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.IncomeStatement_InterestExpense).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.IncomeStatement_NonCashExpense).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.IncomeStatement_OperatingExpense).HasPrecision(23, 10);
            _fullValuationYearReportForecast1.Property(o => o.IncomeStatement_Revenue).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.NetWorkingCapital_ChangeInNetWorkingCapital).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.NetWorkingCapital_CurrentAssets).HasPrecision(23, 10);

            _fullValuationYearReportForecast1.Property(o => o.NetWorkingCapital_NetWorkingCapital).HasPrecision(23, 10);


            #endregion

            #region FullValuationYearReportForecast2 Precision
            _fullValuationYearReportForecast2.Property(o => o.AssetsProjection_FixedAndIntangibleAssets).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetAnalysis_AverageAccountsPayableDays).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetAnalysis_AverageAccountsReceiveableDays).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetAnalysis_AverageInventoryDays).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetEquityAndLiabilities_AccountsPayable).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetEquityAndLiabilities_TotalLiabilities).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetTotalAssets_AccountsReceiveable).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetTotalAssets_CurrentAssets).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetTotalAssets_Inventory).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetTotalAssets_OtherCurrentAssets).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.BalanceSheetTotalAssets_TotalAssets).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.CapitalExpenditure_CapitalExpenditure).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.CapitalExpenditure_PercentageForecastCapitalExpenditure).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_AdjustedFreeCashFlow).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_DiscountedFreeCashFlow).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_DiscountFactor).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_DiscountFactorInverse).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_DiscountPeriodMidYear).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_DiscountPeriodMid_Year).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_DiscountPeriodYearEnd).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_DiscountPeriodYear_End).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_FreeCashFlow).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_IncomeStatement_EBIT).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_IncomeStatement_NonCashExpense).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_IncomeStatement_Tax).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital).HasPrecision(23, 10);



            _fullValuationYearReportForecast2.Property(o => o.IncomeStatementAnalysis_EBITDAMargin).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.IncomeStatementAnalysis_EBITMargin).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.IncomeStatementAnalysis_PercentageGrossProfitMargin).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.IncomeStatementAnalysis_PercentageRevenueGrowth).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.IncomeStatement_CostOfGoodsSold).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.IncomeStatement_DepreciationAndAmortisationExpense).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.IncomeStatement_EarningsBeforeTax).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.IncomeStatement_EBIT).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.IncomeStatement_EBITDA).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.IncomeStatement_GrossProfit).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.IncomeStatement_InterestExpense).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.IncomeStatement_NonCashExpense).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.IncomeStatement_OperatingExpense).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.IncomeStatement_Revenue).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.NetWorkingCapital_ChangeInNetWorkingCapital).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.NetWorkingCapital_CurrentAssets).HasPrecision(23, 10);

            _fullValuationYearReportForecast2.Property(o => o.NetWorkingCapital_NetWorkingCapital).HasPrecision(23, 10);



            _fullValuationYearReportForecast2.Property(o => o.VCMethod_EnterpriseValue).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.VCMethod_VCRequiredRateOfReturn).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.VCMethod_VCMethodEnterpriseValue).HasPrecision(23, 10);
            _fullValuationYearReportForecast2.Property(o => o.VCMethod_VCMethodEquityValue).HasPrecision(23, 10);

            #endregion

            #region FullValuationYearReportTerminal Precision

            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_AdjustedFreeCashFlow).HasPrecision(23, 10);
            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure).HasPrecision(23, 10);
            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_DiscountedFreeCashFlow).HasPrecision(23, 10);
            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_DiscountFactor).HasPrecision(23, 10);
            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_DiscountFactorInverse).HasPrecision(23, 10);
            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_DiscountPeriodMidYear).HasPrecision(23, 10);
            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_DiscountPeriodYearEnd).HasPrecision(23, 10);
            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_FreeCashFlow).HasPrecision(23, 10);

            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense).HasPrecision(23, 10);

            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_IncomeStatement_EBIT).HasPrecision(23, 10);
            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_IncomeStatement_NonCashExpense).HasPrecision(23, 10);
            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_IncomeStatement_Tax).HasPrecision(23, 10);

            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital).HasPrecision(23, 10);

            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_TerminalFreeCashFlow).HasPrecision(23, 10);

            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlow_TerminalFreeCashFlowMultiple).HasPrecision(23, 10);

            _fullValuationYearReportTerminal.Property(o => o.NetWorkingCapital_ChangeInNetWorkingCapital).HasPrecision(23, 10);

            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlowTerminalMultiple_Ebitda).HasPrecision(23, 10);
            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlowTerminalMultiple_Ev_Over_Ebitda).HasPrecision(23, 10);

            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlowTerminalMultiple_AdjustedFreeCashFlow).HasPrecision(23, 10);

            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year).HasPrecision(23, 10);

            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlowTerminalMultiple_DiscountFactor).HasPrecision(23, 10);

            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlowTerminalMultiple_DiscountFactorInverse).HasPrecision(23, 10);

            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlowTerminalMultiple_FreeCashFlow).HasPrecision(23, 10);

            _fullValuationYearReportTerminal.Property(o => o.DiscountedCashFlowTerminalMultiple_DiscountedFreeCashFlow).HasPrecision(23, 10);


            #endregion

            #region Sensitivity Analysis Survival Adjusted DCF 
            
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_Wacc_1).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_Wacc_2).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_Wacc_3).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_Wacc_4).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_Wacc_5).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_AMultiple).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_A1).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_A2).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_A3).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_A4).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_A5).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_BMultiple).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_B1).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_B2).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_B3).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_B4).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_B5).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_CMultiple).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_C1).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_C2).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_C3).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_C4).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_C5).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_DMultiple).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_D1).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_D2).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_D3).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_D4).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_D5).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_EMultiple).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_E1).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_E2).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_E3).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_E4).HasPrecision(23, 10);
            _fullValuationReport.Property(o => o.SurvivalAdjustedDCFEquity_E5).HasPrecision(23, 10);
            #endregion

        }

        private ObjectContext ObjectContext => ((IObjectContextAdapter)this).ObjectContext;

        public void Detach(object entity)
        {
            if (Entry(entity).State != EntityState.Detached)
            {
                ObjectContext.Detach(entity);
            }
        }
    }
}
