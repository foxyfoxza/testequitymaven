﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EquityMaven.Data.Calculations;
using EquityMaven.Tests.UnitTests.TestData;
using EquityMaven.Interfaces.FinancialYears;
using EquityMaven.TestData;
namespace EquityMaven.Tests.UnitTests.Calculators

{


    [TestClass]
    public class UnitTestValuationCalculator
    {
        [TestInitialize]
        public virtual void InitializeTest()
        {
            FinancialValuation = TestDataHelper.GetFinancialValuation();
        }
        public FinancialValuation FinancialValuation { get; set; }
        [TestMethod]
        public void Test_MonthsSinceLastFinancialYearEnd()
        {
            Assert.AreEqual(0, FinancialValuation.GetMonthsSinceLastFinancialYearEnd(new DateTime(2018, 02, 28), new DateTime(2018, 03, 25)));
            Assert.AreEqual(1, FinancialValuation.GetMonthsSinceLastFinancialYearEnd(new DateTime(2018, 02, 28), new DateTime(2018, 03, 31)));
            Assert.AreEqual(1, FinancialValuation.GetMonthsSinceLastFinancialYearEnd(new DateTime(2018, 02, 28), new DateTime(2018, 04, 25)));
            Assert.AreEqual(2, FinancialValuation.GetMonthsSinceLastFinancialYearEnd(new DateTime(2018, 02, 28), new DateTime(2018, 05, 25)));
            Assert.AreEqual(3, FinancialValuation.GetMonthsSinceLastFinancialYearEnd(new DateTime(2018, 02, 28), new DateTime(2018, 05, 31)));
            Assert.AreEqual(7, FinancialValuation.GetMonthsSinceLastFinancialYearEnd(new DateTime(2018, 02, 28), new DateTime(2018, 10, 1)));
            Assert.AreEqual(8, FinancialValuation.GetMonthsSinceLastFinancialYearEnd(new DateTime(2018, 02, 28), new DateTime(2018, 11, 1)));
            Assert.AreEqual(11, FinancialValuation.GetMonthsSinceLastFinancialYearEnd(new DateTime(2018, 02, 28), new DateTime(2019, 02, 26)));
            Assert.AreEqual(12, FinancialValuation.GetMonthsSinceLastFinancialYearEnd(new DateTime(2018, 02, 28), new DateTime(2019, 02, 28)));
            Assert.AreEqual(24, FinancialValuation.GetMonthsSinceLastFinancialYearEnd(new DateTime(2018, 02, 28), new DateTime(2020, 02, 29)));
            Assert.AreEqual(36, FinancialValuation.GetMonthsSinceLastFinancialYearEnd(new DateTime(2018, 02, 28), new DateTime(2021, 02, 28)));
        }

        #region IncomeStatement

        [TestMethod]
        public void Test_IncomeStatement_FinancialYearActualPrior()
        {
            var incomeStatement_Revenue = (decimal)23636936.000000000;
            var incomeStatement_CostOfGoodsSold = (decimal)1305107.000000000;
            var incomeStatement_GrossProfit = (decimal)22331829.000000000;
            var incomeStatement_OperatingExpense = (decimal)19586949.000000000;
            var incomeStatement_EBITDA = (decimal)2744880.000000000;
            var incomeStatement_DepreciationAndAmortisationExpense = (decimal)354554.000000000;
            var incomeStatement_EBIT = (decimal)2390326.000000000;
            var incomeStatement_InterestExpense = (decimal)33582.000000000;
            var incomeStatement_EarningsBeforeTax = (decimal)2356744.000000000;
            var incomeStatement_NonCashExpense = (decimal)1664284.000000000;


            Test_IncomeStatement(FinancialValuation.iActualPrior, incomeStatement_Revenue, incomeStatement_CostOfGoodsSold, incomeStatement_GrossProfit, incomeStatement_OperatingExpense, incomeStatement_EBITDA, incomeStatement_DepreciationAndAmortisationExpense, incomeStatement_EBIT, incomeStatement_InterestExpense, incomeStatement_EarningsBeforeTax, incomeStatement_NonCashExpense);



        }

        [TestMethod]
        public void Test_IncomeStatement_FinancialYearActualCurrent()
        {
            var incomeStatement_Revenue = (decimal)29884199.000000;
            var incomeStatement_CostOfGoodsSold = (decimal)1814682.000000;
            var incomeStatement_GrossProfit = (decimal)28069517.000000;
            var incomeStatement_OperatingExpense = (decimal)24339108.000000;
            var incomeStatement_EBITDA = (decimal)3730409.000000;
            var incomeStatement_DepreciationAndAmortisationExpense = (decimal)448262.000000;
            var incomeStatement_EBIT = (decimal)3282147.000000;
            var incomeStatement_InterestExpense = (decimal)42598.000000;
            var incomeStatement_EarningsBeforeTax = (decimal)3239549.000000;
            var incomeStatement_NonCashExpense = (decimal)970513.000000;

            Test_IncomeStatement(FinancialValuation.iActualCurrent, incomeStatement_Revenue, incomeStatement_CostOfGoodsSold, incomeStatement_GrossProfit, incomeStatement_OperatingExpense, incomeStatement_EBITDA, incomeStatement_DepreciationAndAmortisationExpense, incomeStatement_EBIT, incomeStatement_InterestExpense, incomeStatement_EarningsBeforeTax, incomeStatement_NonCashExpense);


        }

        [TestMethod]
        public void Test_IncomeStatement_FinancialYearBudget()
        {

            var incomeStatement_Revenue = (decimal)34964512.83;
            var incomeStatement_CostOfGoodsSold = (decimal)2097870.7698000;
            var incomeStatement_GrossProfit = (decimal)32866642.0602;
            var incomeStatement_OperatingExpense = (decimal)28670900.5206;
            var incomeStatement_EBITDA = (decimal)4195741.5396;
            var incomeStatement_DepreciationAndAmortisationExpense = (decimal)524466.5400;
            var incomeStatement_EBIT = (decimal)3671274.9996;
            var incomeStatement_InterestExpense = (decimal)42598;
            var incomeStatement_EarningsBeforeTax = (decimal)3628676.9996;
            var incomeStatement_NonCashExpense = (decimal)1135500.21;


            Test_IncomeStatement(FinancialValuation.iBudget, incomeStatement_Revenue, incomeStatement_CostOfGoodsSold, incomeStatement_GrossProfit, incomeStatement_OperatingExpense, incomeStatement_EBITDA, incomeStatement_DepreciationAndAmortisationExpense, incomeStatement_EBIT, incomeStatement_InterestExpense, incomeStatement_EarningsBeforeTax, incomeStatement_NonCashExpense);



        }


        [TestMethod]
        public void Test_IncomeStatement_FinancialYearForecast1()
        {

            var incomeStatement_Revenue = (decimal)40209189.7545000;
            var incomeStatement_CostOfGoodsSold = (decimal)2412551.3852700;
            var incomeStatement_GrossProfit = (decimal)37796638.3692300;
            var incomeStatement_OperatingExpense = (decimal)33172581.5474625;
            var incomeStatement_EBITDA = (decimal)4624056.8217675;
            var incomeStatement_DepreciationAndAmortisationExpense = (decimal)603136.5210000;
            var incomeStatement_EBIT = (decimal)4020920.3007675;
            var incomeStatement_InterestExpense = (decimal)42598.0000000;
            var incomeStatement_EarningsBeforeTax = (decimal)3978322.3007675;
            var incomeStatement_NonCashExpense = (decimal)1305825.2415000;

            Test_IncomeStatement(FinancialValuation.iForecast1, incomeStatement_Revenue, incomeStatement_CostOfGoodsSold, incomeStatement_GrossProfit, incomeStatement_OperatingExpense, incomeStatement_EBITDA, incomeStatement_DepreciationAndAmortisationExpense, incomeStatement_EBIT, incomeStatement_InterestExpense, incomeStatement_EarningsBeforeTax, incomeStatement_NonCashExpense);



        }

        [TestMethod]
        public void Test_IncomeStatement_FinancialYearForecast2()
        {

            var incomeStatement_Revenue = (decimal)45034292.52504000000;
            var incomeStatement_CostOfGoodsSold = (decimal)2702057.55150240000;
            var incomeStatement_GrossProfit = (decimal)42332234.97353760000;
            var incomeStatement_OperatingExpense = (decimal)37153291.33315800000;
            var incomeStatement_EBITDA = (decimal)5178943.64037960000;
            var incomeStatement_DepreciationAndAmortisationExpense = (decimal)675512.90352000000;
            var incomeStatement_EBIT = (decimal)4503430.73685960000;
            var incomeStatement_InterestExpense = (decimal)42598.00000000000;
            var incomeStatement_EarningsBeforeTax = (decimal)4460832.73685960000;
            var incomeStatement_NonCashExpense = (decimal)1462524.27048000000;


            Test_IncomeStatement(FinancialValuation.iForecast2, incomeStatement_Revenue, incomeStatement_CostOfGoodsSold, incomeStatement_GrossProfit, incomeStatement_OperatingExpense, incomeStatement_EBITDA, incomeStatement_DepreciationAndAmortisationExpense, incomeStatement_EBIT, incomeStatement_InterestExpense, incomeStatement_EarningsBeforeTax, incomeStatement_NonCashExpense);
        }


        public void Test_IncomeStatement(IFinancialYearValues fy, decimal incomeStatement_Revenue, decimal incomeStatement_CostOfGoodsSold, decimal incomeStatement_GrossProfit, decimal incomeStatement_OperatingExpense, decimal incomeStatement_EBITDA, decimal incomeStatement_DepreciationAndAmortisationExpense, decimal incomeStatement_EBIT, decimal incomeStatement_InterestExpense, decimal incomeStatement_EarningsBeforeTax, decimal incomeStatement_NonCashExpense)
        {
            var name = fy.GetType().ToString();
            Assert.AreEqual(Math.Round(incomeStatement_Revenue, 6), Math.Round(fy.IncomeStatement_Revenue, 6), $"{name}.IncomeStatement_Revenue");
            Assert.AreEqual(Math.Round(incomeStatement_CostOfGoodsSold, 6), Math.Round(fy.IncomeStatement_CostOfGoodsSold, 6), $"{name}.IncomeStatement_CostOfGoodsSold");
            Assert.AreEqual(Math.Round(incomeStatement_GrossProfit, 6), Math.Round(fy.IncomeStatement_GrossProfit, 6), $"{name}.IncomeStatement_GrossProfit");
            Assert.AreEqual(Math.Round(incomeStatement_OperatingExpense, 6), Math.Round(fy.IncomeStatement_OperatingExpense, 6), $"{name}.IncomeStatement_OperatingExpense");
            Assert.AreEqual(Math.Round(incomeStatement_EBITDA, 6), Math.Round(fy.IncomeStatement_EBITDA, 6), $"{name}.IncomeStatement_EBITDA");
            Assert.AreEqual(Math.Round(incomeStatement_DepreciationAndAmortisationExpense, 6), Math.Round(fy.IncomeStatement_DepreciationAndAmortisationExpense, 6), $"{name}.IncomeStatement_DepreciationAndAmortisationExpense");
            Assert.AreEqual(Math.Round(incomeStatement_EBIT, 6), Math.Round(fy.IncomeStatement_EBIT, 6), $"{name}.IncomeStatement_EBIT");
            Assert.AreEqual(Math.Round(incomeStatement_InterestExpense, 6), Math.Round(fy.IncomeStatement_InterestExpense, 6), $"{name}.IncomeStatement_InterestExpense");
            Assert.AreEqual(Math.Round(incomeStatement_EarningsBeforeTax, 6), Math.Round(fy.IncomeStatement_EarningsBeforeTax, 6), $"{name}.IncomeStatement_EarningsBeforeTax");
            Assert.AreEqual(Math.Round(incomeStatement_NonCashExpense, 6), Math.Round(fy.IncomeStatement_NonCashExpense, 6), $"{name}.IncomeStatement_NonCashExpense");
        }


        #endregion

        #region IncomeStatementAnalysis

        [TestMethod]
        public void Test_IncomeStatementAnalysisFinancialYearActualPrior()
        {

            decimal IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal)94.47852716612680000000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal)82.86585452530730000000 / (decimal)100;
            decimal IncomeStatementAnalysis_EBITDAMargin = (decimal)11.61267264081940000000 / (decimal)100;
            decimal IncomeStatementAnalysis_EBITMargin = (decimal)10.11267281004610000000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = (decimal)1.49999983077333000000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = (decimal)7.04103103718688000000 / (decimal)100;
            decimal IncomeStatementAnalysis_InterestCover = (decimal)71.1787862545;


            Test_ActualPriorIncomeStatementAnalysis(FinancialValuation.iActualPrior, IncomeStatementAnalysis_PercentageGrossProfitMargin, IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, IncomeStatementAnalysis_EBITDAMargin, IncomeStatementAnalysis_EBITMargin, IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue, IncomeStatementAnalysis_InterestCover);

        }

        [TestMethod]
        public void Test_IncomeStatementAnalysisFinancialYearActualCurrent()
        {


            decimal IncomeStatementAnalysis_PercentageRevenueGrowth = (decimal)26.43008806217520000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal)93.92762041237910000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal)81.44473940894320000 / (decimal)100;
            decimal IncomeStatementAnalysis_EBITDAMargin = (decimal)12.48288100343600000 / (decimal)100;
            decimal IncomeStatementAnalysis_EBITMargin = (decimal)10.98288429949220000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = (decimal)1.49999670394378000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = (decimal)3.24757909690000000 / (decimal)100;
            decimal IncomeStatementAnalysis_InterestCover = (decimal)77.049321564;

            Test_ActualCurrentIncomeStatementAnalysis(FinancialValuation.iActualCurrent, IncomeStatementAnalysis_PercentageRevenueGrowth, IncomeStatementAnalysis_PercentageGrossProfitMargin, IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, IncomeStatementAnalysis_EBITDAMargin, IncomeStatementAnalysis_EBITMargin, IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue, IncomeStatementAnalysis_InterestCover);

        }

        [TestMethod]
        public void Test_IncomeStatementAnalysisFinancialYearBudget()
        {


            decimal IncomeStatementAnalysis_PercentageRevenueGrowth = (decimal)17.00000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal)94.00000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal)82.00000000000 / (decimal)100;
            decimal IncomeStatementAnalysis_EBITDAMargin = (decimal)12.0000000000000 / (decimal)100;
            decimal IncomeStatementAnalysis_EBITMargin = (decimal)10.5000032960562 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = (decimal)1.4999967039438 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = (decimal)3.2475790969000 / (decimal)100;
            Test_ProjectedIncomeStatementAnalysis(FinancialValuation.iBudget, IncomeStatementAnalysis_PercentageRevenueGrowth, IncomeStatementAnalysis_PercentageGrossProfitMargin, IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, IncomeStatementAnalysis_EBITDAMargin, IncomeStatementAnalysis_EBITMargin, IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue);

        }


        [TestMethod]
        public void Test_IncomeStatementAnalysisFinancialYearForecast1()
        {



            decimal IncomeStatementAnalysis_PercentageRevenueGrowth = (decimal)15.0000000000000000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal)94.0000000000000000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal)82.5000000000000000 / (decimal)100;
            decimal IncomeStatementAnalysis_EBITDAMargin = (decimal)11.5000000000000000 / (decimal)100;
            decimal IncomeStatementAnalysis_EBITMargin = (decimal)10.0000032960562000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = (decimal)1.4999967039437800 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = (decimal)3.2475790969000000 / (decimal)100;



            Test_ProjectedIncomeStatementAnalysis(FinancialValuation.iForecast1, IncomeStatementAnalysis_PercentageRevenueGrowth, IncomeStatementAnalysis_PercentageGrossProfitMargin, IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, IncomeStatementAnalysis_EBITDAMargin, IncomeStatementAnalysis_EBITMargin, IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue);

        }

        [TestMethod]
        public void Test_IncomeStatementAnalysisFinancialYearForecast2()
        {


            decimal IncomeStatementAnalysis_PercentageRevenueGrowth = (decimal)12.0000000000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal)94.0000000000 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal)82.5000000000 / (decimal)100;
            decimal IncomeStatementAnalysis_EBITDAMargin = (decimal)11.5000000000 / (decimal)100;
            decimal IncomeStatementAnalysis_EBITMargin = (decimal)10.0000032961 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = (decimal)1.4999967039 / (decimal)100;
            decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = (decimal)3.2475790969 / (decimal)100;

            Test_ProjectedIncomeStatementAnalysis(FinancialValuation.iForecast2, IncomeStatementAnalysis_PercentageRevenueGrowth, IncomeStatementAnalysis_PercentageGrossProfitMargin, IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, IncomeStatementAnalysis_EBITDAMargin, IncomeStatementAnalysis_EBITMargin, IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue);

        }


        public void Test_IncomeStatementAnalysis(IFinancialYearValues fy, decimal IncomeStatementAnalysis_PercentageGrossProfitMargin, decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, decimal IncomeStatementAnalysis_EBITDAMargin, decimal IncomeStatementAnalysis_EBITMargin, decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue)
        {
            var name = fy.GetType().ToString();

            Assert.AreEqual(Math.Round(IncomeStatementAnalysis_PercentageGrossProfitMargin, 6), Math.Round(fy.IncomeStatementAnalysis_PercentageGrossProfitMargin, 6), $"{name}.IncomeStatementAnalysis_PercentageGrossProfitMargin");
            Assert.AreEqual(Math.Round(IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, 6), Math.Round(fy.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, 6), $"{name}.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue");
            Assert.AreEqual(Math.Round(IncomeStatementAnalysis_EBITDAMargin, 6), Math.Round(fy.IncomeStatementAnalysis_EBITDAMargin, 6), $"{name}.IncomeStatementAnalysis_EBITDAMargin");

            Assert.AreEqual(Math.Round(IncomeStatementAnalysis_EBITMargin, 6), Math.Round(fy.IncomeStatementAnalysis_EBITMargin, 6), $"{name}.IncomeStatementAnalysis_EBITMargin");

            Assert.AreEqual(Math.Round(IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, 6), Math.Round(fy.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, 6), $"{name}.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue");

            Assert.AreEqual(Math.Round(IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue, 6), Math.Round(fy.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue, 6), $"{name}.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue");
        }

        public void Test_ProjectedIncomeStatementAnalysis(IFinancialYearProjected fy, decimal IncomeStatementAnalysis_PercentageRevenueGrowth, decimal IncomeStatementAnalysis_PercentageGrossProfitMargin, decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, decimal IncomeStatementAnalysis_EBITDAMargin, decimal IncomeStatementAnalysis_EBITMargin, decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue)
        {
            var name = fy.GetType().ToString();
            Assert.AreEqual(Math.Round(IncomeStatementAnalysis_PercentageRevenueGrowth, 6), Math.Round(fy.IncomeStatementAnalysis_PercentageRevenueGrowth, 6), $"{name}.IncomeStatementAnalysis_PercentageRevenueGrowth");

            Test_IncomeStatementAnalysis(fy, IncomeStatementAnalysis_PercentageGrossProfitMargin, IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, IncomeStatementAnalysis_EBITDAMargin, IncomeStatementAnalysis_EBITMargin, IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue);
        }


        public void Test_ActualCurrentIncomeStatementAnalysis(IFinancialYearActualCurrent fy, decimal IncomeStatementAnalysis_PercentageRevenueGrowth, decimal IncomeStatementAnalysis_PercentageGrossProfitMargin, decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, decimal IncomeStatementAnalysis_EBITDAMargin, decimal IncomeStatementAnalysis_EBITMargin, decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue, decimal IncomeStatementAnalysis_InterestCover)
        {
            var name = fy.GetType().ToString();
            Assert.AreEqual(Math.Round(IncomeStatementAnalysis_PercentageRevenueGrowth, 6), Math.Round(fy.IncomeStatementAnalysis_PercentageRevenueGrowth, 6), $"{name}.IncomeStatementAnalysis_PercentageRevenueGrowth");

            Test_IncomeStatementAnalysis(fy, IncomeStatementAnalysis_PercentageGrossProfitMargin, IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, IncomeStatementAnalysis_EBITDAMargin, IncomeStatementAnalysis_EBITMargin, IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue);
            Assert.AreEqual(Math.Round(IncomeStatementAnalysis_InterestCover, 6), Math.Round(fy.IncomeStatementAnalysis_InterestCover, 6), $"{name}.IncomeStatementAnalysis_InterestCover");
            Test_IncomeStatementAnalysis(fy, IncomeStatementAnalysis_PercentageGrossProfitMargin, IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, IncomeStatementAnalysis_EBITDAMargin, IncomeStatementAnalysis_EBITMargin, IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue);

        }


        public void Test_ActualPriorIncomeStatementAnalysis(IFinancialYearActualPrior fy, decimal IncomeStatementAnalysis_PercentageGrossProfitMargin, decimal IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, decimal IncomeStatementAnalysis_EBITDAMargin, decimal IncomeStatementAnalysis_EBITMargin, decimal IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, decimal IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue, decimal IncomeStatementAnalysis_InterestCover)
        {
            var name = fy.GetType().ToString();
            Assert.AreEqual(Math.Round(IncomeStatementAnalysis_InterestCover, 6), Math.Round(fy.IncomeStatementAnalysis_InterestCover, 6), $"{name}.IncomeStatementAnalysis_InterestCover");
            Test_IncomeStatementAnalysis(fy, IncomeStatementAnalysis_PercentageGrossProfitMargin, IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue, IncomeStatementAnalysis_EBITDAMargin, IncomeStatementAnalysis_EBITMargin, IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue, IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue);

        }


        #endregion

        #region BalanceSheetTotalAssets


        [TestMethod]
        public void Test_BalanceSheetTotalAssets_FinancialYearActualPrior()
        {
            var fy = FinancialValuation.iActualPrior;
            BalanceSheetTotalAssets(fy,
                BalanceSheetTotalAssets_CurrentAssets: (decimal)3413110,
                BalanceSheetTotalAssets_AccountsReceiveable: (decimal)1711697,
                BalanceSheetTotalAssets_Inventory: (decimal)0000000,
                BalanceSheetTotalAssets_OtherCurrentAssets: (decimal)383503,
                BalanceSheetTotalAssets_CashAndCashEquivalents: (decimal)1317910,
                BalanceSheetTotalAssets_IntangibleAssets: (decimal)0000000,
                BalanceSheetTotalAssets_FixedAssets: (decimal)1060328,
                BalanceSheetTotalAssets_TotalAssets: (decimal)4473438);
        }


        [TestMethod]
        public void Test_BalanceSheetTotalAssets_FinancialYearActualCurrent()
        {

            var fy = FinancialValuation.iActualCurrent;
            BalanceSheetTotalAssets(fy,
                     BalanceSheetTotalAssets_CurrentAssets: (decimal)5535827,
                     BalanceSheetTotalAssets_AccountsReceiveable: (decimal)4045036,
                     BalanceSheetTotalAssets_Inventory: (decimal)0000000,
                     BalanceSheetTotalAssets_OtherCurrentAssets: (decimal)400897,
                     BalanceSheetTotalAssets_CashAndCashEquivalents: (decimal)1089894,
                     BalanceSheetTotalAssets_IntangibleAssets: (decimal)0000000,
                     BalanceSheetTotalAssets_FixedAssets: (decimal)1748926,
                     BalanceSheetTotalAssets_TotalAssets: (decimal)7284753);
        }


        [TestMethod]
        public void Test_BalanceSheetTotalAssets_FinancialYearBudget()
        {

            var fy = FinancialValuation.iBudget;
            BalanceSheetTotalAssets(fy,

                BalanceSheetTotalAssets_AccountsReceiveable: (decimal)4732692.1200000000,
                BalanceSheetTotalAssets_Inventory: (decimal)0000000,
                BalanceSheetTotalAssets_OtherCurrentAssets: (decimal)463458.6654854740,
                BalanceSheetTotalAssets_TotalAssets: (decimal)0000000);
        }



        [TestMethod]
        public void Test_BalanceSheetTotalAssets_FinancialYearForecast1()
        {

            var fy = FinancialValuation.iForecast1;

            BalanceSheetTotalAssets(fy,

                    BalanceSheetTotalAssets_AccountsReceiveable: (decimal)5442595.93800000000,
                    BalanceSheetTotalAssets_Inventory: (decimal)0000000,
                    BalanceSheetTotalAssets_OtherCurrentAssets: (decimal)532977.46530829500,
                    BalanceSheetTotalAssets_TotalAssets: (decimal)0000000);
        }



        [TestMethod]
        public void Test_BalanceSheetTotalAssets_FinancialYearForecast2()
        {
            var fy = FinancialValuation.iForecast2;
            BalanceSheetTotalAssets(fy,

                  BalanceSheetTotalAssets_AccountsReceiveable: (decimal)6095707.4505600000000,
                  BalanceSheetTotalAssets_Inventory: (decimal)0000000,
                  BalanceSheetTotalAssets_OtherCurrentAssets: (decimal)596934.7611452900000,
                  BalanceSheetTotalAssets_TotalAssets: (decimal)0000000);
        }


        private void BalanceSheetTotalAssets(IFinancialYearActual fy, decimal BalanceSheetTotalAssets_CurrentAssets, decimal BalanceSheetTotalAssets_AccountsReceiveable, decimal BalanceSheetTotalAssets_Inventory, decimal BalanceSheetTotalAssets_OtherCurrentAssets, decimal BalanceSheetTotalAssets_CashAndCashEquivalents, decimal BalanceSheetTotalAssets_IntangibleAssets, decimal BalanceSheetTotalAssets_FixedAssets, decimal BalanceSheetTotalAssets_TotalAssets)
        {
            var name = fy.GetType().ToString();
            Assert.AreEqual(Math.Round(BalanceSheetTotalAssets_CurrentAssets, 6), Math.Round(fy.BalanceSheetTotalAssets_CurrentAssets, 6), $"{name}.BalanceSheetTotalAssets_CurrentAssets");

            Assert.AreEqual(Math.Round(BalanceSheetTotalAssets_AccountsReceiveable, 6), Math.Round(fy.BalanceSheetTotalAssets_AccountsReceiveable, 6), $"{name}.BalanceSheetTotalAssets_AccountsReceiveable");
            Assert.AreEqual(Math.Round(BalanceSheetTotalAssets_Inventory, 6), Math.Round(fy.BalanceSheetTotalAssets_Inventory, 6), $"{name}.BalanceSheetTotalAssets_Inventory");
            Assert.AreEqual(Math.Round(BalanceSheetTotalAssets_OtherCurrentAssets, 6), Math.Round(fy.BalanceSheetTotalAssets_OtherCurrentAssets, 6), $"{name}.BalanceSheetTotalAssets_OtherCurrentAssets");
            Assert.AreEqual(Math.Round(BalanceSheetTotalAssets_CashAndCashEquivalents, 6), Math.Round(fy.BalanceSheetTotalAssets_CashAndCashEquivalents, 6), $"{name}.BalanceSheetTotalAssets_CashAndCashEquivalents");
            Assert.AreEqual(Math.Round(BalanceSheetTotalAssets_IntangibleAssets, 6), Math.Round(fy.BalanceSheetTotalAssets_IntangibleAssets, 6), $"{name}.BalanceSheetTotalAssets_IntangibleAssets");
            Assert.AreEqual(Math.Round(BalanceSheetTotalAssets_FixedAssets, 6), Math.Round(fy.BalanceSheetTotalAssets_FixedAssets, 6), $"{name}.BalanceSheetTotalAssets_FixedAssets");
            Assert.AreEqual(Math.Round(BalanceSheetTotalAssets_TotalAssets, 6), Math.Round(fy.BalanceSheetTotalAssets_TotalAssets, 6), $"{name}.BalanceSheetTotalAssets_TotalAssets");



        }

        private void BalanceSheetTotalAssets(IFinancialYearProjected fy, decimal BalanceSheetTotalAssets_AccountsReceiveable, decimal BalanceSheetTotalAssets_Inventory, decimal BalanceSheetTotalAssets_OtherCurrentAssets, decimal BalanceSheetTotalAssets_TotalAssets)
        {
            var name = fy.GetType().ToString();
            //Assert.AreEqual(Math.Round(BalanceSheetTotalAssets_CurrentAssets, 6), Math.Round(fy.BalanceSheetTotalAssets_CurrentAssets, 6), $"{name}.BalanceSheetTotalAssets_CurrentAssets");
            Assert.AreEqual(Math.Round(BalanceSheetTotalAssets_AccountsReceiveable, 6), Math.Round(fy.BalanceSheetTotalAssets_AccountsReceiveable, 6), $"{name}.BalanceSheetTotalAssets_AccountsReceiveable");
            Assert.AreEqual(Math.Round(BalanceSheetTotalAssets_Inventory, 6), Math.Round(fy.BalanceSheetTotalAssets_Inventory, 6), $"{name}.BalanceSheetTotalAssets_Inventory");
            Assert.AreEqual(Math.Round(BalanceSheetTotalAssets_OtherCurrentAssets, 6), Math.Round(fy.BalanceSheetTotalAssets_OtherCurrentAssets, 6), $"{name}.BalanceSheetTotalAssets_OtherCurrentAssets");
            //Assert.AreEqual(Math.Round(BalanceSheetTotalAssets_TotalAssets, 6), Math.Round(fy.BalanceSheetTotalAssets_TotalAssets, 6), $"{name}.BalanceSheetTotalAssets_TotalAssets");
        }


        #endregion

        #region BalanceSheetEquityAndLiabilities

        [TestMethod]
        public void Test_BalanceSheetEquityAndLiabilities_FinancialYearActualPrior()
        {
            var fy = FinancialValuation.iActualPrior;
            BalanceSheetEquityAndLiabilities(fy,
                BalanceSheetEquityAndLiabilities_CurrentLiabilities: (decimal)606519,
                BalanceSheetEquityAndLiabilities_AccountsPayable: (decimal)606519,
                BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities: (decimal)0000000,
                BalanceSheetEquityAndLiabilities_ShortTermDebt: (decimal)0000000,
                BalanceSheetEquityAndLiabilities_LongTermDebt: (decimal)0000000,
                BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities: (decimal)467259,
                BalanceSheetEquityAndLiabilities_ShareholderLoans: (decimal)000000,
                BalanceSheetEquityAndLiabilities_TotalLiabilities: (decimal)1073778,
                BalanceSheetShareholdersEquity: (decimal)3399660);
        }

        [TestMethod]
        public void Test_BalanceSheetEquityAndLiabilities_FinancialYearActualCurrent()
        {
            var fy = FinancialValuation.iActualCurrent;
            BalanceSheetEquityAndLiabilities(fy,
                BalanceSheetEquityAndLiabilities_CurrentLiabilities: (decimal)437226,
                BalanceSheetEquityAndLiabilities_AccountsPayable: (decimal)437226,
                BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities: (decimal)0000000,
                BalanceSheetEquityAndLiabilities_ShortTermDebt: (decimal)0000000,
                BalanceSheetEquityAndLiabilities_LongTermDebt: (decimal)0000000,
                BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities: (decimal)597269,
                BalanceSheetEquityAndLiabilities_ShareholderLoans: (decimal)000000,
                BalanceSheetEquityAndLiabilities_TotalLiabilities: (decimal)1034495,
                BalanceSheetShareholdersEquity: (decimal)6250258);
        }

        [TestMethod]
        public void Test_BalanceSheetEquityAndLiabilities_FinancialYearBudget()
        {
            var fy = FinancialValuation.iBudget;

            BalanceSheetEquityAndLiabilities(fy,
                BalanceSheetEquityAndLiabilities_AccountsPayable: (decimal)505456.958958415,
                BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities: (decimal)0000000,
                BalanceSheetEquityAndLiabilities_TotalLiabilities: (decimal)505456.95895842);
        }

        [TestMethod]
        public void Test_BalanceSheetEquityAndLiabilities_FinancialYearForecast1()
        {
            var fy = FinancialValuation.iForecast1;
            BalanceSheetEquityAndLiabilities(fy,
                BalanceSheetEquityAndLiabilities_AccountsPayable: (decimal)581275.502802177,
                BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities: (decimal)0000000,
                BalanceSheetEquityAndLiabilities_TotalLiabilities: (decimal)581275.50280218);
        }
        [TestMethod]
        public void Test_BalanceSheetEquityAndLiabilities_FinancialYearForecast2()
        {
            var fy = FinancialValuation.iForecast2;
            BalanceSheetEquityAndLiabilities(fy,
                BalanceSheetEquityAndLiabilities_AccountsPayable: (decimal)651028.563138439,
                BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities: (decimal)0000000,
                BalanceSheetEquityAndLiabilities_TotalLiabilities: (decimal)651028.56313844);
        }
        private void BalanceSheetEquityAndLiabilities(IFinancialYearProjected fy, decimal BalanceSheetEquityAndLiabilities_AccountsPayable, decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, decimal BalanceSheetEquityAndLiabilities_TotalLiabilities)
        {
            var name = fy.GetType().ToString();
            //Assert.AreEqual(Math.Round(BalanceSheetEquityAndLiabilities_CurrentLiabilities, 6), Math.Round(fy.BalanceSheetEquityAndLiabilities_CurrentLiabilities, 6), $"{name}.BalanceSheetEquityAndLiabilities_CurrentLiabilities");
            Assert.AreEqual(Math.Round(BalanceSheetEquityAndLiabilities_AccountsPayable, 6), Math.Round(fy.BalanceSheetEquityAndLiabilities_AccountsPayable, 6), $"{name}.BalanceSheetEquityAndLiabilities_AccountsPayable");
            Assert.AreEqual(Math.Round(BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, 6), Math.Round(fy.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, 6), $"{name}.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities");

            Assert.AreEqual(Math.Round(BalanceSheetEquityAndLiabilities_TotalLiabilities, 6), Math.Round(fy.BalanceSheetEquityAndLiabilities_TotalLiabilities, 6), $"{name}.BalanceSheetEquityAndLiabilities_TotalLiabilities");


        }

        private void BalanceSheetEquityAndLiabilities(IFinancialYearActual fy, decimal BalanceSheetEquityAndLiabilities_CurrentLiabilities, decimal BalanceSheetEquityAndLiabilities_AccountsPayable, decimal BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, decimal BalanceSheetEquityAndLiabilities_ShortTermDebt, decimal BalanceSheetEquityAndLiabilities_LongTermDebt, decimal BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities, decimal BalanceSheetEquityAndLiabilities_ShareholderLoans, decimal BalanceSheetEquityAndLiabilities_TotalLiabilities, decimal BalanceSheetShareholdersEquity)
        {
            var name = fy.GetType().ToString();
            Assert.AreEqual(Math.Round(BalanceSheetEquityAndLiabilities_CurrentLiabilities, 6), Math.Round(fy.BalanceSheetEquityAndLiabilities_CurrentLiabilities, 6), $"{name}.BalanceSheetEquityAndLiabilities_CurrentLiabilities");
            Assert.AreEqual(Math.Round(BalanceSheetEquityAndLiabilities_AccountsPayable, 6), Math.Round(fy.BalanceSheetEquityAndLiabilities_AccountsPayable, 6), $"{name}.BalanceSheetEquityAndLiabilities_AccountsPayable");
            Assert.AreEqual(Math.Round(BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, 6), Math.Round(fy.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, 6), $"{name}.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities");
            Assert.AreEqual(Math.Round(BalanceSheetEquityAndLiabilities_ShortTermDebt, 6), Math.Round(fy.BalanceSheetEquityAndLiabilities_ShortTermDebt, 6), $"{name}.BalanceSheetEquityAndLiabilities_ShortTermDebt");
            Assert.AreEqual(Math.Round(BalanceSheetEquityAndLiabilities_LongTermDebt, 6), Math.Round(fy.BalanceSheetEquityAndLiabilities_LongTermDebt, 6), $"{name}.BalanceSheetEquityAndLiabilities_LongTermDebt");
            Assert.AreEqual(Math.Round(BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities, 6), Math.Round(fy.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities, 6), $"{name}.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities");
            Assert.AreEqual(Math.Round(fy.BalanceSheetEquityAndLiabilities_ShareholderLoans, 6), Math.Round(fy.BalanceSheetEquityAndLiabilities_ShareholderLoans, 6), $"{name}.fy.BalanceSheetEquityAndLiabilities_ShareholderLoans");
            Assert.AreEqual(Math.Round(BalanceSheetEquityAndLiabilities_TotalLiabilities, 6), Math.Round(fy.BalanceSheetEquityAndLiabilities_TotalLiabilities, 6), $"{name}.BalanceSheetEquityAndLiabilities_TotalLiabilities");
            Assert.AreEqual(Math.Round(BalanceSheetShareholdersEquity, 6), Math.Round(fy.BalanceSheetShareholdersEquity, 6), $"{name}.BalanceSheetShareholdersEquity");

        }

        #endregion

        #region BalanceSheetWorkingCapitalAndLeverageAnalysis

        [TestMethod]
        public void Test_BalanceSheetWorkingCapitalAndLeverageAnalysis_FinancialYearActualPrior()
        {
            var fy = FinancialValuation.iActualPrior;
            BalanceSheetWorkingCapitalAndLeverageAnalysisActual(fy,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue: (decimal)7.2416196414 / (decimal)100,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold: (decimal)0.0000000000 / (decimal)100,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold: (decimal)29.3847937372 / (decimal)100,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold: (decimal)46.4727413155 / (decimal)100,
                BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold: (decimal)0.0000000000 / (decimal)100,
                BalanceSheetAnalysis_CurrentRatio: (decimal)5.627375235,
                BalanceSheetAnalysis_TotalDebtOverEBITDA: (decimal)0000
                );
        }

        [TestMethod]
        public void Test_BalanceSheetWorkingCapitalAndLeverageAnalysis_FinancialYearActualCurrent()
        {
            var fy = FinancialValuation.iActualCurrent;


            BalanceSheetWorkingCapitalAndLeverageAnalysisCurrent(fy,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue: (decimal)13.5357015927 / (decimal)100,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold: (decimal)0.0000000000 / (decimal)100,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold: (decimal)22.0918596206 / (decimal)100,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold: (decimal)24.0938081713 / (decimal)100,
                BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold: (decimal)0.0000000000 / (decimal)100,
                BalanceSheetAnalysis_AverageAccountsReceiveableDays: (decimal)35.1558284196,
                BalanceSheetAnalysis_AverageInventoryDays: (decimal)0,
                BalanceSheetAnalysis_AverageAccountsPayableDays: (decimal)104.9679571958,
                BalanceSheetAnalysis_CurrentRatio: (decimal)12.661248416,
                BalanceSheetAnalysis_TotalDebtOverEBITDA: (decimal)0000
                );
        }

        [TestMethod]
        public void Test_BalanceSheetWorkingCapitalAndLeverageAnalysis_FinancialYearBudget()
        {
            var fy = FinancialValuation.iBudget;


            BalanceSheetWorkingCapitalAndLeverageAnalysisProjected(fy,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue: (decimal)13.5357015927 / (decimal)100,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold: (decimal)0000 / (decimal)100,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold: (decimal)22.0918596206 / (decimal)100,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold: (decimal)24.0938081713 / (decimal)100,
                BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold: (decimal)0000 / (decimal)100,
                BalanceSheetAnalysis_AverageAccountsReceiveableDays: (decimal)45.8160360960,
                BalanceSheetAnalysis_AverageInventoryDays: (decimal)0000,
                BalanceSheetAnalysis_AverageAccountsPayableDays: (decimal)82.0067863505
                );
        }
        [TestMethod]
        public void Test_BalanceSheetWorkingCapitalAndLeverageAnalysis_FinancialYearForecast1()
        {
            var fy = FinancialValuation.iForecast1;


            BalanceSheetWorkingCapitalAndLeverageAnalysisProjected(fy,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue: (decimal)13.5357015927 / (decimal)100,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold: (decimal)0000 / (decimal)100,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold: (decimal)22.0918596206 / (decimal)100,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold: (decimal)24.0938081713 / (decimal)100,
                BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold: (decimal)0000 / (decimal)100,
                BalanceSheetAnalysis_AverageAccountsReceiveableDays: (decimal)46.1832253254,
                BalanceSheetAnalysis_AverageInventoryDays: (decimal)0000,
                BalanceSheetAnalysis_AverageAccountsPayableDays: (decimal)82.2070259238
                );
        }
        [TestMethod]
        public void Test_BalanceSheetWorkingCapitalAndLeverageAnalysis_FinancialYearForecast2()
        {
            var fy = FinancialValuation.iForecast2;
            BalanceSheetWorkingCapitalAndLeverageAnalysisProjected(fy,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue: (decimal)13.5357015927 / (decimal)100,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold: (decimal)0000 / (decimal)100,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold: (decimal)22.0918596206 / (decimal)100,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold: (decimal)24.0938081713 / (decimal)100,
                BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold: (decimal)0000 / (decimal)100,
                BalanceSheetAnalysis_AverageAccountsReceiveableDays: (decimal)46.7585977340,
                BalanceSheetAnalysis_AverageInventoryDays: (decimal)0000,
                BalanceSheetAnalysis_AverageAccountsPayableDays: (decimal)83.2311998348
                );
        }

        private void BalanceSheetWorkingCapitalAndLeverageAnalysisActual(IFinancialYearActual fy, decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue, decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold, decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold, decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold, decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold, decimal BalanceSheetAnalysis_CurrentRatio, decimal BalanceSheetAnalysis_TotalDebtOverEBITDA)
        {
            var name = fy.GetType().ToString();
            Assert.AreEqual(Math.Round(BalanceSheetAnalysis_CurrentRatio, 6), Math.Round(fy.BalanceSheetAnalysis_CurrentRatio, 6), $"{name}.BalanceSheetAnalysis_CurrentRatio");
            Assert.AreEqual(Math.Round(BalanceSheetAnalysis_TotalDebtOverEBITDA, 6), Math.Round(fy.BalanceSheetAnalysis_TotalDebtOverEBITDA, 6), $"{name}.BalanceSheetAnalysis_TotalDebtOverEBITDA");
            BalanceSheetWorkingCapitalAndLeverageAnalysis(fy,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold
                );
        }

        private void BalanceSheetWorkingCapitalAndLeverageAnalysisCurrent(IFinancialYearActualCurrent fy, decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue, decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold, decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold, decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold, decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold, decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays, decimal BalanceSheetAnalysis_AverageInventoryDays, decimal BalanceSheetAnalysis_AverageAccountsPayableDays, decimal BalanceSheetAnalysis_CurrentRatio, decimal BalanceSheetAnalysis_TotalDebtOverEBITDA)
        {

            var name = fy.GetType().ToString();

            Assert.AreEqual(Math.Round(BalanceSheetAnalysis_AverageAccountsReceiveableDays, 6), Math.Round(fy.BalanceSheetAnalysis_AverageAccountsReceiveableDays, 6), $"{name}.BalanceSheetAnalysis_AverageAccountsReceiveableDays");
            Assert.AreEqual(Math.Round(BalanceSheetAnalysis_AverageInventoryDays, 6), Math.Round(fy.BalanceSheetAnalysis_AverageInventoryDays, 6), $"{name}.BalanceSheetAnalysis_AverageInventoryDays");
            Assert.AreEqual(Math.Round(BalanceSheetAnalysis_AverageAccountsPayableDays, 6), Math.Round(fy.BalanceSheetAnalysis_AverageAccountsPayableDays, 6), $"{name}.BalanceSheetAnalysis_AverageAccountsPayableDays");

            BalanceSheetWorkingCapitalAndLeverageAnalysisActual(fy,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold,
                BalanceSheetAnalysis_CurrentRatio,
                BalanceSheetAnalysis_TotalDebtOverEBITDA
                );
        }

        private void BalanceSheetWorkingCapitalAndLeverageAnalysis(IFinancialYearValues fy, decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue, decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold, decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold, decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold, decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold)
        {

            var name = fy.GetType().ToString();
            Assert.AreEqual(Math.Round(BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold, 6), Math.Round(fy.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold, 6), $"{name}.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold");
            Assert.AreEqual(Math.Round(BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold, 6), Math.Round(fy.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold, 6), $"{name}.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold");
            Assert.AreEqual(Math.Round(BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold, 6), Math.Round(fy.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold, 6), $"{name}.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold");
            Assert.AreEqual(Math.Round(BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold, 6), Math.Round(fy.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold, 6), $"{name}.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold");
            Assert.AreEqual(Math.Round(BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue, 6), Math.Round(fy.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue, 6), $"{name}.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue");

        }


        private void BalanceSheetWorkingCapitalAndLeverageAnalysisProjected(IFinancialYearProjected fy, decimal BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue, decimal BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold, decimal BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold, decimal BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold, decimal BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold, decimal BalanceSheetAnalysis_AverageAccountsReceiveableDays, decimal BalanceSheetAnalysis_AverageInventoryDays, decimal BalanceSheetAnalysis_AverageAccountsPayableDays)
        {

            var name = fy.GetType().ToString();

            Assert.AreEqual(Math.Round(BalanceSheetAnalysis_AverageAccountsReceiveableDays, 6), Math.Round(fy.BalanceSheetAnalysis_AverageAccountsReceiveableDays, 6), $"{name}.BalanceSheetAnalysis_AverageAccountsReceiveableDays");
            Assert.AreEqual(Math.Round(BalanceSheetAnalysis_AverageInventoryDays, 6), Math.Round(fy.BalanceSheetAnalysis_AverageInventoryDays, 6), $"{name}.BalanceSheetAnalysis_AverageInventoryDays");
            Assert.AreEqual(Math.Round(BalanceSheetAnalysis_AverageAccountsPayableDays, 6), Math.Round(fy.BalanceSheetAnalysis_AverageAccountsPayableDays, 6), $"{name}.BalanceSheetAnalysis_AverageAccountsPayableDays");

            BalanceSheetWorkingCapitalAndLeverageAnalysis(fy,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold
                );
        }
        #endregion

        #region CapitalExpenditure

        [TestMethod]
        public void Test_CapitalExpenditure_FinancialYearActualPrior()
        {
            var fy = FinancialValuation.iActualPrior;
            CapitalExpenditure(fy,
                CapitalExpenditure_CapitalExpenditure: (decimal)0000,
                CapitalExpenditure_PercentageForecastCapitalExpenditure: (decimal)0.0000000000 / (decimal)100);
        }




        [TestMethod]
        public void Test_CapitalExpenditure_FinancialYearActualCurrent()
        {
            var fy = FinancialValuation.iActualCurrent;

            CapitalExpenditureActualCurrent(fy,
                CapitalExpenditure_CapitalExpenditure: (decimal)0000,
                CapitalExpenditure_PercentageForecastCapitalExpenditure: (decimal)0.0000000000 / (decimal)100,
                CapitalExpenditure_CapitalExpenditureTTM: (decimal)437056.4103750000
                );
        }



        [TestMethod]
        public void Test_CapitalExpenditure_FinancialYearBudget()
        {
            var fy = FinancialValuation.iBudget;
            CapitalExpenditure(fy,
                CapitalExpenditure_CapitalExpenditure: (decimal)1748225.641500000
,
                CapitalExpenditure_PercentageForecastCapitalExpenditure: (decimal)5 / (decimal)100);
        }
        [TestMethod]
        public void Test_CapitalExpenditure_FinancialYearForecast1()
        {
            var fy = FinancialValuation.iForecast1;
            CapitalExpenditure(fy,
                CapitalExpenditure_CapitalExpenditure: (decimal)1206275.692635000
,
                CapitalExpenditure_PercentageForecastCapitalExpenditure: (decimal)3 / (decimal)100);
        }
        [TestMethod]
        public void Test_CapitalExpenditure_FinancialYearForecast2()
        {
            var fy = FinancialValuation.iForecast2;
            CapitalExpenditure(fy,
                CapitalExpenditure_CapitalExpenditure: (decimal)450342.925250400
,
                CapitalExpenditure_PercentageForecastCapitalExpenditure: (decimal)1 / (decimal)100);
        }
        private void CapitalExpenditure(IFinancialYearValues fy, decimal CapitalExpenditure_CapitalExpenditure, decimal CapitalExpenditure_PercentageForecastCapitalExpenditure)
        {
            var name = fy.GetType().ToString();

            Assert.AreEqual(Math.Round(CapitalExpenditure_CapitalExpenditure, 6), Math.Round(fy.CapitalExpenditure_CapitalExpenditure, 6), $"{name}.CapitalExpenditure_CapitalExpenditure");
            Assert.AreEqual(Math.Round(CapitalExpenditure_PercentageForecastCapitalExpenditure, 6), Math.Round(fy.CapitalExpenditure_PercentageForecastCapitalExpenditure, 6), $"{name}.CapitalExpenditure_PercentageForecastCapitalExpenditure");
        }
        private void CapitalExpenditureActualCurrent(IFinancialYearActualCurrent fy, decimal CapitalExpenditure_CapitalExpenditure, decimal CapitalExpenditure_PercentageForecastCapitalExpenditure, decimal CapitalExpenditure_CapitalExpenditureTTM)
        {
            var name = fy.GetType().ToString();

            Assert.AreEqual(Math.Round(CapitalExpenditure_CapitalExpenditureTTM, 6), Math.Round(fy.CapitalExpenditure_CapitalExpenditureTTM, 6), $"{name}.CapitalExpenditure_CapitalExpenditureTTM");
            CapitalExpenditure(fy, CapitalExpenditure_CapitalExpenditure, CapitalExpenditure_PercentageForecastCapitalExpenditure);
        }
        #endregion

        #region SUSTAINABLE EBITDA

        [TestMethod]
        public void Test_SustainableEBITDA_FinancialYearActualCurrent()
        {
            var fy = FinancialValuation.iActualCurrent;

            Assert.AreEqual(Math.Round((decimal)3730409.0000000, 6), Math.Round(fy.SustainableEBITDA_SustainableEBITDA, 6), $"{fy.GetType()}.SustainableEBITDA_SustainableEBITDA");
            Assert.AreEqual(Math.Round((decimal)0000, 6), Math.Round(fy.SustainableEBITDA_NonRecurringExpenses, 6), $"{fy.GetType()}.SustainableEBITDA_NonRecurringExpenses");
            Assert.AreEqual(Math.Round((decimal)0000, 6), Math.Round(fy.SustainableEBITDA_NonRecurringIncome, 6), $"{fy.GetType()}.SustainableEBITDA_NonRecurringIncome");
            Assert.AreEqual(Math.Round((decimal)3730409, 6), Math.Round(fy.SustainableEBITDA_SustainableEBITDA, 6), $"{fy.GetType()}.SustainableEBITDA_SustainableEBITDA");
            Assert.AreEqual(Math.Round((decimal)3846742.1349, 6), Math.Round(fy.SustainableEBITDA_SustainableEBITDA_TTM, 6), $"{fy.GetType()}.SustainableEBITDA_SustainableEBITDA_TTM");

        }

        [TestMethod]
        public void Test_SustainableEBITDA_FinancialYearActualBudget()
        {
            var fy = FinancialValuation.iBudget;

            Assert.AreEqual(Math.Round((decimal)4195741.539600000, 6), Math.Round(fy.SustainableEBITDA_SustainableEBITDA, 6), $"{fy.GetType()}.SustainableEBITDA_SustainableEBITDA");
            Assert.AreEqual(Math.Round((decimal)0000, 6), Math.Round(fy.SustainableEBITDA_NonRecurringExpenses, 6), $"{fy.GetType()}.SustainableEBITDA_NonRecurringExpenses");
            Assert.AreEqual(Math.Round((decimal)0000, 6), Math.Round(fy.SustainableEBITDA_NonRecurringIncome, 6), $"{fy.GetType()}.SustainableEBITDA_NonRecurringIncome");
            Assert.AreEqual(Math.Round((decimal)4195741.53960000, 6), Math.Round(fy.SustainableEBITDA_SustainableEBITDA, 6), $"{fy.GetType()}.SustainableEBITDA_SustainableEBITDA");

        }

        #endregion

        #region EXCESS CASH


        [TestMethod]
        public void Test_ExcessCash_FinancialYearActualCurrent()
        {
            var fy = FinancialValuation.iActualCurrent;

            Assert.AreEqual(Math.Round((decimal)826914.45085, 6), Math.Round(fy.ExcessCash_ExcessCash, 6), $"{fy.GetType()}.SustainableEBITDA_SustainableEBITDA");
            Assert.AreEqual(Math.Round((decimal)623085.54915, 6), Math.Round(fy.ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue, 6), $"{fy.GetType()}.ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue");
            Assert.AreEqual(Math.Round((decimal)31154277.4575, 6), Math.Round(fy.ExcessCash_RevenueTTM, 6), $"{fy.GetType()}.ExcessCash_RevenueTTM");
        }



        #endregion

        #region Changes in Net Working Capital ("NWC") workings

        [TestMethod]
        public void Test_NWC_FinancialYearActualPrior()
        {
            var fy = FinancialValuation.iActualPrior;
            Assert.AreEqual(Math.Round((decimal)1488681, 6), Math.Round(fy.NetWorkingCapital_NetWorkingCapital, 6), $"{fy.GetType()}.NetWorkingCapital_NetWorkingCapital");

        }

        [TestMethod]
        public void Test_NWC_FinancialYearActualCurrent()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)4008707, 6), Math.Round(fy.NetWorkingCapital_NetWorkingCapital, 6), $"{fy.GetType()}.NetWorkingCapital_NetWorkingCapital");
            Assert.AreEqual(Math.Round((decimal)-2520026, 6), Math.Round(fy.NetWorkingCapital_ChangeInNetWorkingCapital, 6), $"{fy.GetType()}.NetWorkingCapital_ChangeInNetWorkingCapital");

        }

        [TestMethod]
        public void Test_NWC_FinancialYearBudget()
        {
            var fy = FinancialValuation.iBudget;
            Assert.AreEqual(Math.Round((decimal)4690693.82652706000, 6), Math.Round(fy.NetWorkingCapital_NetWorkingCapital, 6), $"{fy.GetType()}.NetWorkingCapital_NetWorkingCapital");
            Assert.AreEqual(Math.Round((decimal)-681986.8265271, 6), Math.Round(fy.NetWorkingCapital_ChangeInNetWorkingCapital, 6), $"{fy.GetType()}.NetWorkingCapital_ChangeInNetWorkingCapital");
        }
        [TestMethod]
        public void Test_NWC_FinancialYearForecast1()
        {
            var fy = FinancialValuation.iForecast1;
            Assert.AreEqual(Math.Round((decimal)5394297.9005061, 6), Math.Round(fy.NetWorkingCapital_NetWorkingCapital, 6), $"{fy.GetType()}.NetWorkingCapital_NetWorkingCapital");
            Assert.AreEqual(Math.Round((decimal)-703604.0739791, 6), Math.Round(fy.NetWorkingCapital_ChangeInNetWorkingCapital, 6), $"{fy.GetType()}.NetWorkingCapital_ChangeInNetWorkingCapital");
        }
        [TestMethod]
        public void Test_NWC_FinancialYearForecast2()
        {
            var fy = FinancialValuation.iForecast2;
            Assert.AreEqual(Math.Round((decimal)6041613.6485669, 6), Math.Round(fy.NetWorkingCapital_NetWorkingCapital, 6), $"{fy.GetType()}.NetWorkingCapital_NetWorkingCapital");
            Assert.AreEqual(Math.Round((decimal)-647315.7480607, 6), Math.Round(fy.NetWorkingCapital_ChangeInNetWorkingCapital, 6), $"{fy.GetType()}.NetWorkingCapital_ChangeInNetWorkingCapital");
        }

        [TestMethod]
        public void Test_NWC_FinancialYearTerminal()
        {
            var fy = FinancialValuation.iTerminal;
            Assert.AreEqual(Math.Round((decimal)-687222.7639287, 6), Math.Round(fy.NetWorkingCapital_ChangeInNetWorkingCapital, 6), $"{fy.GetType()}.NetWorkingCapital_ChangeInNetWorkingCapital");
        }
        #endregion

        #region DISCOUNTED CASH FLOW - TERMINAL GROWTH RATE

        [TestMethod]
        public void Test_DiscountedCashFlow_FinancialYearActualCurrent()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)1261894.840000000, 6), Math.Round(fy.DiscountedCashFlow_FreeCashFlow, 6), $"{fy.GetType()}.DiscountedCashFlow_FreeCashFlow");

        }

        [TestMethod]
        public void Test_DiscountedCashFlow_FinancialYearBudget()
        {
            var fy = FinancialValuation.iBudget;
            DiscountedCashFlow_FreeCashFlow(fy, (decimal)1873072.28168494);


            Assert.AreEqual(Math.Round((decimal)-468268.070421236000, 6), Math.Round(fy.DiscountedCashFlow_AdjustmentForFirstYear, 6), $"{fy.GetType()}.DiscountedCashFlow_AdjustmentForFirstYear");


            Assert.AreEqual(Math.Round((decimal)1873072.28168494, 6), Math.Round(fy.DiscountedCashFlow_FreeCashFlow, 6), $"{fy.GetType()}.DiscountedCashFlow_FreeCashFlow");

            DiscountedCashFlow_AdjustedFreeCashFlow(fy, (decimal)1404804.2112637100);
            Assert.AreEqual(Math.Round((decimal)0.750000, 6), Math.Round(fy.DiscountedCashFlow_DiscountPeriodYearEnd, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountPeriodYearEnd");
            Assert.AreEqual(Math.Round((decimal)0.37500000, 6), Math.Round(fy.DiscountedCashFlow_DiscountPeriodMidYear, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountPeriodMidYear");
            Assert.AreEqual(Math.Round((decimal)1.05011037, 6), Math.Round(fy.DiscountedCashFlow_DiscountFactor, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountFactor");
            Assert.AreEqual(Math.Round((decimal)0.95228085, 6), Math.Round(fy.DiscountedCashFlow_DiscountFactorInverse, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountFactorInverse");
            Assert.AreEqual(Math.Round((decimal)1337768.1534730500, 6), Math.Round(fy.DiscountedCashFlow_DiscountedFreeCashFlow, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountedFreeCashFlow");
        }

        [TestMethod]
        public void Test_DiscountedCashFlow_FinancialYearForecast1()
        {
            var fy = FinancialValuation.iForecast1;
            DiscountedCashFlow_FreeCashFlow(fy, (decimal)2894144.612438540000);
            DiscountedCashFlow_AdjustedFreeCashFlow(fy, (decimal)2894144.6124385400);

            Assert.AreEqual(Math.Round((decimal)1.7500000000, 6), Math.Round(fy.DiscountedCashFlow_DiscountPeriodYearEnd, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountPeriodYearEnd");
            Assert.AreEqual(Math.Round((decimal)1.2500000000, 6), Math.Round(fy.DiscountedCashFlow_DiscountPeriodMidYear, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountPeriodMidYear");
            Assert.AreEqual(Math.Round((decimal)1.1770181452, 6), Math.Round(fy.DiscountedCashFlow_DiscountFactor, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountFactor");
            Assert.AreEqual(Math.Round((decimal)0.8496045741, 6), Math.Round(fy.DiscountedCashFlow_DiscountFactorInverse, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountFactorInverse");
            Assert.AreEqual(Math.Round((decimal)2458878.5009459200, 6), Math.Round(fy.DiscountedCashFlow_DiscountedFreeCashFlow, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountedFreeCashFlow");
        }

        [TestMethod]
        public void Test_DiscountedCashFlow_FinancialYearForecast2()
        {
            var fy = FinancialValuation.iForecast2;
            DiscountedCashFlow_FreeCashFlow(fy, (decimal)4282848.631227780);
            DiscountedCashFlow_AdjustedFreeCashFlow(fy, (decimal)4282848.6312277800);

            Assert.AreEqual(Math.Round((decimal)2.7500000000, 6), Math.Round(fy.DiscountedCashFlow_DiscountPeriodYearEnd, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountPeriodYearEnd");
            Assert.AreEqual(Math.Round((decimal)2.2500000000, 6), Math.Round(fy.DiscountedCashFlow_DiscountPeriodMidYear, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountPeriodMidYear");
            Assert.AreEqual(Math.Round((decimal)1.3409410456, 6), Math.Round(fy.DiscountedCashFlow_DiscountFactor, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountFactor");
            Assert.AreEqual(Math.Round((decimal)0.7457449403, 6), Math.Round(fy.DiscountedCashFlow_DiscountFactorInverse, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountFactorInverse");
            Assert.AreEqual(Math.Round((decimal)3193912.6967412800, 6), Math.Round(fy.DiscountedCashFlow_DiscountedFreeCashFlow, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountedFreeCashFlow");
        }
        [TestMethod]
        public void Test_DiscountedCashFlow_FinancialYearTerminal()
        {
            var fy = FinancialValuation.iTerminal;

            Assert.AreEqual(Math.Round((decimal)4781067.241787, 6), Math.Round(fy.DiscountedCashFlow_IncomeStatement_EBIT, 6), $"{fy.GetType()}.DiscountedCashFlow_IncomeStatement_EBIT");
            Assert.AreEqual(Math.Round((decimal)-1338698.827700, 6), Math.Round(fy.DiscountedCashFlow_IncomeStatement_Tax, 6), $"{fy.GetType()}.DiscountedCashFlow_IncomeStatement_Tax");
            Assert.AreEqual(Math.Round((decimal)717158.274022, 6), Math.Round(fy.DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense, 6), $"{fy.GetType()}.DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense");
            Assert.AreEqual(Math.Round((decimal)1552688.891755, 6), Math.Round(fy.DiscountedCashFlow_IncomeStatement_NonCashExpense, 6), $"{fy.GetType()}.DiscountedCashFlow_IncomeStatement_NonCashExpense");

            Assert.AreEqual(Math.Round((decimal)-923726.111300, 6), Math.Round(fy.DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure, 6), $"{fy.GetType()}.DiscountedCashFlow_CapitalExpenditure_CapitalExpenditure");
            Assert.AreEqual(Math.Round((decimal)-687222.763929, 6), Math.Round(fy.DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital, 6), $"{fy.GetType()}.DiscountedCashFlow_NetWorkingCapital_ChangeInNetWorkingCapital");

            DiscountedCashFlow_FreeCashFlow(fy, (decimal)4101266.704634600000);

            DiscountedCashFlow_AdjustedFreeCashFlow(fy, (decimal)4101266.7046346000);
            Assert.AreEqual(Math.Round((decimal)12.883336, 6), Math.Round(fy.DiscountedCashFlow_TerminalFreeCashFlowMultiple, 6), $"{fy.GetType()}.DiscountedCashFlow_TerminalFreeCashFlowMultiple");
            Assert.AreEqual(Math.Round((decimal)52837998.0450205000, 6), Math.Round(fy.DiscountedCashFlow_TerminalFreeCashFlow, 6), $"{fy.GetType()}.DiscountedCashFlow_TerminalFreeCashFlow");

            Assert.AreEqual(Math.Round((decimal)2.7500000000, 6), Math.Round(fy.DiscountedCashFlow_DiscountPeriodYearEnd, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountPeriodYearEnd");
            Assert.AreEqual(Math.Round((decimal)2.2500000000, 6), Math.Round(fy.DiscountedCashFlow_DiscountPeriodMidYear, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountPeriodMidYear");
            Assert.AreEqual(Math.Round((decimal)1.3409410456, 6), Math.Round(fy.DiscountedCashFlow_DiscountFactor, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountFactor");
            Assert.AreEqual(Math.Round((decimal)0.7457449403, 6), Math.Round(fy.DiscountedCashFlow_DiscountFactorInverse, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountFactorInverse");
            Assert.AreEqual(Math.Round((decimal)39403669.6968212000, 6), Math.Round(fy.DiscountedCashFlow_DiscountedFreeCashFlow, 6), $"{fy.GetType()}.DiscountedCashFlow_DiscountedFreeCashFlow");

        }
        [TestMethod]
        public void Test_Terminal_DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense()
        {
            var fy = FinancialValuation.iTerminal;
            Assert.AreEqual(Math.Round((decimal)717158.274022, 6), Math.Round(fy.DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense, 6), $"{fy.GetType()}.DiscountedCashFlow_IncomeStatement_DepreciationAndAmortisationExpense");
        }
        [TestMethod]
        public void Test_AssetsProjection_FixedAndIntangibleAssetsForecast2()
        {
            var fy = FinancialValuation.iForecast2;
            Assert.AreEqual(Math.Round((decimal)3350654.294865, 6), Math.Round(fy.AssetsProjection_FixedAndIntangibleAssets, 6), $"{fy.GetType()}.AssetsProjection_FixedAndIntangibleAssets");
        }
        [TestMethod]
        public void Test_AssetsProjection_FixedAndIntangibleAssetsForecast1()
        {
            var fy = FinancialValuation.iForecast1;
            Assert.AreEqual(Math.Round((decimal)3575824.27313500000, 6), Math.Round(fy.AssetsProjection_FixedAndIntangibleAssets, 6), $"{fy.GetType()}.AssetsProjection_FixedAndIntangibleAssets");
        }
        [TestMethod]
        public void Test_AssetsProjection_FixedAndIntangibleAssetsBudget()
        {
            var fy = FinancialValuation.iBudget;
            Assert.AreEqual(Math.Round((decimal)2972685.10150000, 6), Math.Round(fy.AssetsProjection_FixedAndIntangibleAssets, 6), $"{fy.GetType()}.AssetsProjection_FixedAndIntangibleAssets");
        }
        [TestMethod]
        public void Test_AssetsProjection_FixedAndIntangibleAssetsActualCurrent()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)1748926.0000000000000000, 6), Math.Round(fy.AssetsProjection_FixedAndIntangibleAssets, 6), $"{fy.GetType()}.AssetsProjection_FixedAndIntangibleAssets");
        }
        [TestMethod]
        public void Test_AssetsProjection_FixedAndIntangibleAssetsActualPrior()
        {
            var fy = FinancialValuation.iActualPrior;
            Assert.AreEqual(Math.Round((decimal)1060328.0000000000000000, 6), Math.Round(fy.AssetsProjection_FixedAndIntangibleAssets, 6), $"{fy.GetType()}.AssetsProjection_FixedAndIntangibleAssets");
        }
        [TestMethod]
        public void Test_Terminal_WaccInputsSummary_TerminalGrowthRate()
        {
            var fy = FinancialValuation.iTerminal;
            Assert.AreEqual(Math.Round((decimal)6.16500000 / (decimal)100, 6), Math.Round(FinancialValuation.WaccInputsSummary_TerminalGrowthRate, 6), $"{fy.GetType()}.WaccInputsSummary_TerminalGrowthRate");

        }
        private void DiscountedCashFlow_FreeCashFlow(IFinancialYear fy, decimal DiscountedCashFlow_FreeCashFlow)
        {
            var fyDiscountedCashFlow_FreeCashFlow = (fy as FinancialYearProjected)?.DiscountedCashFlow_FreeCashFlow ?? (fy as FinancialYearActualCurrent)?.DiscountedCashFlow_FreeCashFlow ?? (fy as FinancialYearTerminal)?.DiscountedCashFlow_FreeCashFlow;
            Assert.IsNotNull(fyDiscountedCashFlow_FreeCashFlow);
            Assert.AreEqual(Math.Round(DiscountedCashFlow_FreeCashFlow, 6), Math.Round(fyDiscountedCashFlow_FreeCashFlow.Value, 6), $"{fy.GetType()}.DiscountedCashFlow_FreeCashFlow");
        }
        private void DiscountedCashFlow_FreeCashFlow(IFinancialYearTerminal fy, decimal DiscountedCashFlow_FreeCashFlow)
        {
            var fyDiscountedCashFlow_FreeCashFlow = (fy as FinancialYearProjected)?.DiscountedCashFlow_FreeCashFlow ?? (fy as FinancialYearActualCurrent)?.DiscountedCashFlow_FreeCashFlow ?? (fy as FinancialYearTerminal)?.DiscountedCashFlow_FreeCashFlow;
            Assert.IsNotNull(fyDiscountedCashFlow_FreeCashFlow);
            Assert.AreEqual(Math.Round(DiscountedCashFlow_FreeCashFlow, 6), Math.Round(fyDiscountedCashFlow_FreeCashFlow.Value, 6), $"{fy.GetType()}.DiscountedCashFlow_FreeCashFlow");
        }
        private void DiscountedCashFlow_AdjustedFreeCashFlow(IFinancialYear fy, decimal DiscountedCashFlow_AdjustedFreeCashFlow)
        {
            var fyDiscountedCashFlow_AdjustedFreeCashFlow = (fy as FinancialYearForecast2)?.DiscountedCashFlow_AdjustedFreeCashFlow ?? (fy as FinancialYearForecast1)?.DiscountedCashFlow_AdjustedFreeCashFlow ?? (fy as FinancialYearBudget)?.DiscountedCashFlow_AdjustedFreeCashFlow ?? (fy as FinancialYearActualCurrent)?.DiscountedCashFlow_FreeCashFlow ?? (fy as FinancialYearTerminal)?.DiscountedCashFlow_AdjustedFreeCashFlow;
            Assert.IsNotNull(fyDiscountedCashFlow_AdjustedFreeCashFlow);
            Assert.AreEqual(Math.Round(DiscountedCashFlow_AdjustedFreeCashFlow, 6), Math.Round(fyDiscountedCashFlow_AdjustedFreeCashFlow.Value, 6), $"{fy.GetType()}.DiscountedCashFlow_AdjustedFreeCashFlow");
        }
        private void DiscountedCashFlow_AdjustedFreeCashFlow(IFinancialYearTerminal fy, decimal DiscountedCashFlow_AdjustedFreeCashFlow)
        {
            var fyDiscountedCashFlow_AdjustedFreeCashFlow = (fy as FinancialYearForecast2)?.DiscountedCashFlow_AdjustedFreeCashFlow ?? (fy as FinancialYearForecast1)?.DiscountedCashFlow_AdjustedFreeCashFlow ?? (fy as FinancialYearBudget)?.DiscountedCashFlow_AdjustedFreeCashFlow ?? (fy as FinancialYearActualCurrent)?.DiscountedCashFlow_FreeCashFlow ?? (fy as FinancialYearTerminal)?.DiscountedCashFlow_AdjustedFreeCashFlow;
            Assert.IsNotNull(fyDiscountedCashFlow_AdjustedFreeCashFlow);
            Assert.AreEqual(Math.Round(DiscountedCashFlow_AdjustedFreeCashFlow, 6), Math.Round(fyDiscountedCashFlow_AdjustedFreeCashFlow.Value, 6), $"{fy.GetType()}.DiscountedCashFlow_AdjustedFreeCashFlow");
        }
        #endregion

        #region EV/EBITDA VALUATION



        [TestMethod]
        public void Test_EvOverEbitdaValuation_SustainableEbitdaTtm()
        {
            Assert.AreEqual(Math.Round((decimal)3846742.134900, 6), Math.Round(FinancialValuation.EvOverEbitdaValuation_SustainableEbitdaTtm, 6), $"{FinancialValuation.GetType()}.EvOverEbitdaValuation_SustainableEbitdaTtm");
        }
        [TestMethod]
        public void Test_EvOverEbitdaValuation_WorldEv_Over_Ebitda()
        {
            Assert.AreEqual(Math.Round((decimal)11.788265, 6), Math.Round(FinancialValuation.WorldMedian_World, 6), $"{FinancialValuation.GetType()}.EvOverEbitdaValuation_WorldEv_Over_Ebitda");
        }
        [TestMethod]
        public void Test_EvOverEbitdaValuation_EnterpriseValuePre_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)45346417.549809, 6), Math.Round(FinancialValuation.EvOverEbitdaValuation_EnterpriseValuePre_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.EvOverEbitdaValuation_EnterpriseValuePre_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_EvOverEbitdaValuation_ExcessCash()
        {
            Assert.AreEqual(Math.Round((decimal)826914.450850, 6), Math.Round(FinancialValuation.EvOverEbitdaValuation_ExcessCash, 6), $"{FinancialValuation.GetType()}.EvOverEbitdaValuation_ExcessCash");
        }
        [TestMethod]
        public void Test_EvOverEbitdaValuation_InterestBearingDebt()
        {
            Assert.AreEqual(Math.Round((decimal)0.000000, 6), Math.Round(FinancialValuation.EvOverEbitdaValuation_InterestBearingDebt, 6), $"{FinancialValuation.GetType()}.EvOverEbitdaValuation_InterestBearingDebt");
        }
        [TestMethod]
        public void Test_EvOverEbitdaValuation_EquityValuePre_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)46173332.000659, 6), Math.Round(FinancialValuation.EvOverEbitdaValuation_EquityValuePre_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.EvOverEbitdaValuation_EquityValuePre_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_EvOverEbitdaValuation_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)-16160666.200231, 6), Math.Round(FinancialValuation.EvOverEbitdaValuation_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.EvOverEbitdaValuation_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_EvOverEbitdaValuation_EquityValuePost_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)30012665.800428, 6), Math.Round(FinancialValuation.EvOverEbitdaValuation_EquityValuePost_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.EvOverEbitdaValuation_EquityValuePost_IlliquidityDiscount");
        }
        #endregion


        #region 

        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_Revenue_Ttm()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)31154277.4575000, 6), Math.Round(fy.Liquidationvalue_Revenue_Ttm, 6), $"{fy.GetType()}.Liquidationvalue_Revenue_Ttm");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_CostOfGoodsSold_Ttm()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)2027073.5773500, 6), Math.Round(fy.Liquidationvalue_CostOfGoodsSold_Ttm, 6), $"{fy.GetType()}.Liquidationvalue_CostOfGoodsSold_Ttm");
        }


        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_Capex_Ttm()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)437056.4103750, 6), Math.Round(fy.Liquidationvalue_Capex_Ttm, 6), $"{fy.GetType()}.Liquidationvalue_Capex_Ttm");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_DepreciationAmortisation_Ttm()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)505415.4050000, 6), Math.Round(fy.Liquidationvalue_DepreciationAmortisation_Ttm, 6), $"{fy.GetType()}.Liquidationvalue_DepreciationAmortisation_Ttm");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_CurrentAssets()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)4664768.2791141, 6), Math.Round(fy.Liquidationvalue_CurrentAssets, 6), $"{fy.GetType()}.Liquidationvalue_CurrentAssets");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_AccountsReceiveable()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)4216950.0300000, 6), Math.Round(fy.Liquidationvalue_AccountsReceiveable, 6), $"{fy.GetType()}.Liquidationvalue_AccountsReceiveable");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_Inventory()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)0, 6), Math.Round(fy.Liquidationvalue_Inventory, 6), $"{fy.GetType()}.Liquidationvalue_Inventory");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_OtherCurrentAssets()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)447818.2491141, 6), Math.Round(fy.Liquidationvalue_OtherCurrentAssets, 6), $"{fy.GetType()}.Liquidationvalue_OtherCurrentAssets");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_CashAndCashEquivalents()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)0.0000000, 6), Math.Round(fy.Liquidationvalue_CashAndCashEquivalents, 6), $"{fy.GetType()}.Liquidationvalue_CashAndCashEquivalents");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_IntangibleAssets()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)0.0000000, 6), Math.Round(fy.Liquidationvalue_IntangibleAssets, 6), $"{fy.GetType()}.Liquidationvalue_IntangibleAssets");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_FixedAssets()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)1680567.0053750, 6), Math.Round(fy.Liquidationvalue_FixedAssets, 6), $"{fy.GetType()}.Liquidationvalue_FixedAssets");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_TotalAssets()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)6345335.2844891, 6), Math.Round(fy.Liquidationvalue_TotalAssets, 6), $"{fy.GetType()}.Liquidationvalue_TotalAssets");
        }

        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_CurrentLiabilities()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)488399.2192188, 6), Math.Round(fy.Liquidationvalue_CurrentLiabilities, 6), $"{fy.GetType()}.Liquidationvalue_CurrentLiabilities");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_AccountsPayable()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)488399.2192188, 6), Math.Round(fy.Liquidationvalue_AccountsPayable, 6), $"{fy.GetType()}.Liquidationvalue_AccountsPayable");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_OtherCurrentLiabilities()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)0.00000, 6), Math.Round(fy.Liquidationvalue_OtherCurrentLiabilities, 6), $"{fy.GetType()}.Liquidationvalue_OtherCurrentLiabilities");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_3rdPartyInterest_BearingDebtLongTermShortTerm()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)0.00000, 6), Math.Round(fy.Liquidationvalue_3rdPartyInterest_BearingDebtLongTermShortTerm, 6), $"{fy.GetType()}.Liquidationvalue_3rdPartyInterest_BearingDebtLongTermShortTerm");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_OtherLong_TermLiabilities()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)597269.0000000, 6), Math.Round(fy.Liquidationvalue_OtherLong_TermLiabilities, 6), $"{fy.GetType()}.Liquidationvalue_OtherLong_TermLiabilities");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_ShareholderLoans()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)00000, 6), Math.Round(fy.Liquidationvalue_ShareholderLoans, 6), $"{fy.GetType()}.Liquidationvalue_ShareholderLoans");
        }
        [TestMethod]
        public void Test_LiquidationValue_Liquidationvalue_TotalLiabilities()
        {
            var fy = FinancialValuation.iActualCurrent;
            Assert.AreEqual(Math.Round((decimal)1085668.2192188, 6), Math.Round(fy.Liquidationvalue_TotalLiabilities, 6), $"{fy.GetType()}.Liquidationvalue_TotalLiabilities");
        }
        #endregion




        #region VC Method




        [TestMethod]
        public void Test_VCMethod_VCMethodEquityValueForecast2()
        {
            var fy = FinancialValuation.iForecast2;
            Assert.AreEqual(Math.Round((decimal)25022654.3491107000, 6), Math.Round(fy.VCMethod_VCMethodEquityValue, 6), $"{fy.GetType()}.VCMethod_VCMethodEquityValue");
        }


        [TestMethod]
        public void Test_VCMethod_VCMethod_EnterpriseValueForecast2()
        {
            var fy = FinancialValuation.iForecast2;
            Assert.AreEqual(Math.Round((decimal)61050762.5798227000, 3), Math.Round(fy.VCMethod_EnterpriseValue, 3), $"{fy.GetType()}.VCMethod_EnterpriseValue");
        }


        [TestMethod]
        public void Test_VCMethod_VCMethod_VCRequiredRateOfReturnForecast2()
        {
            var fy = FinancialValuation.iForecast2;
            Assert.AreEqual(Math.Round((decimal)252.32029620 / (decimal)100, 6), Math.Round(fy.VCMethod_VCRequiredRateOfReturn, 6), $"{fy.GetType()}.VCMethod_VCRequiredRateOfReturn");
        }

        #endregion


        #region SUMMARY - DCF, MULTIPLES, VC METHOD (PRE-SURVIVAL ADJUSTMENT)
        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_Discrete()
        {
            Assert.AreEqual(Math.Round((decimal)6990559.35116026, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_Discrete, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentDCF_Discrete");
        }

        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_Terminal()
        {
            Assert.AreEqual(Math.Round((decimal)39403669.69682120, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_Terminal, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentDCF_Terminal");
        }


        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_EnterpriseValuePre_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)46394229.04798150, 3), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_EnterpriseValuePre_IlliquidityDiscount, 3), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentDCF_EnterpriseValuePre_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentMultiples_EnterpriseValuePre_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)45346417.54980880, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentMultiples_EnterpriseValuePre_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentMultiples_EnterpriseValuePre_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_EquityValuePre_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)47221143.49883150, 4), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_EquityValuePre_IlliquidityDiscount, 4), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentDCF_EquityValuePre_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentMultiples_EquityValuePre_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)46173332.00065880, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentMultiples_EquityValuePre_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentMultiples_EquityValuePre_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)-16527400.22459100, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentDCF_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentMultiples_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)-16160666.20023060, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentMultiples_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentMultiples_IlliquidityDiscount");
        }

        [TestMethod]
        public void Test_Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)30693743.27424050, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_Pre_SurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)30012665.80042820, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount");
        }

        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPre_LiquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)12.06065481, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPre_LiquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPre_LiquidityDiscount");
        }
        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPre_LiquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)11.78826549, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPre_LiquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPre_LiquidityDiscount");
        }

        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_EnterpriseValuePost_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)30693743.27424050, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_EnterpriseValuePost_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentDCF_EnterpriseValuePost_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentMultiples_EnterpriseValuePost_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)30012665.80042820, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentMultiples_EnterpriseValuePost_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentMultiples_EnterpriseValuePost_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentVCMethod_EnterpriseValuePost_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)25022654.34911070, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentVCMethod_EnterpriseValuePost_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentVCMethod_EnterpriseValuePost_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPost_LiquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)7.76418792, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPost_LiquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentDCF_ImpliedEv_Over_EbitdaPost_LiquidityDiscount");
        }
        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPost_LiquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)7.58713486, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPost_LiquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentMultiples_ImpliedEv_Over_EbitdaPost_LiquidityDiscount");
        }
        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentVCMethod_ImpliedEv_Over_EbitdaPost_LiquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)6.28993030, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentVCMethod_ImpliedEv_Over_EbitdaPost_LiquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentVCMethod_ImpliedEv_Over_EbitdaPost_LiquidityDiscount");
        }



        #endregion


        #region EQUITY VALUE - POST SURVIVAL ADJUSTMENT


        [TestMethod]
        public void Test_EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_Probability_WeightedEquityValuePost_IlliquidityDiscount_G273()
        {
            Assert.AreEqual(Math.Round((decimal)34516278.39160840, 6), Math.Round(FinancialValuation.EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_Probability_WeightedEquityValuePost_IlliquidityDiscount, 6));
        }
        [TestMethod]
        public void Test_EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_LiquidationValue()
        {
            Assert.AreEqual(Math.Round((decimal)3128175.307468690, 6), Math.Round(FinancialValuation.EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_LiquidationValue, 6), $"{FinancialValuation.GetType()}.EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_LiquidationValue");
        }
        [TestMethod]
        public void Test_EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)29866828.82339050, 6), Math.Round(FinancialValuation.EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount, 6));
        }


        [TestMethod]
        public void Test_EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedEquityValuePost_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)30693743.2742405, 6), Math.Round(FinancialValuation.EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedEquityValuePost_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedEquityValuePost_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_EquityValue_PostSurvivalAdjustment_DCF_LiquidationValue()
        {
            Assert.AreEqual(Math.Round((decimal)3128175.3074687, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_LiquidationValue, 6), $"{FinancialValuation.GetType()}.EquityValue_PostSurvivalAdjustment_DCF_LiquidationValue");
        }

        [TestMethod]
        public void Test_EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue()
        {
            Assert.AreEqual(Math.Round((decimal)00000, 6), Math.Round(FinancialValuation.EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue, 6), $"{FinancialValuation.GetType()}.EquityValue_PostSurvivalAdjustment_DCF_Probability_WeightedLiquidationValue");
        }


        [TestMethod]
        public void Test_EquityValue_PostSurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)30012665.80042820, 6), Math.Round(FinancialValuation.EquityValue_PostSurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.EquityValue_PostSurvivalAdjustment_Multiples_EquityValuePost_IlliquidityDiscount");
        }


        [TestMethod]
        public void Test_ProbabilityOfSurvival()
        {
            Assert.AreEqual(Math.Round((decimal)1, 6), Math.Round(FinancialValuation.ProbabilityOfSurvival, 6), $"{FinancialValuation.GetType()}.ProbabilityOfSurvival");
        }
        [TestMethod]
        public void Test_EquityValue_PostSurvivalAdjustment_Multiples_Probability_WeightedEquityValuePost_IlliquidityDiscount()
        {


            Assert.AreEqual(Math.Round((decimal)30012665.80042820, 6), Math.Round(FinancialValuation.EquityValue_PostSurvivalAdjustment_Multiples_Probability_WeightedEquityValuePost_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.EquityValue_PostSurvivalAdjustment_Multiples_Probability_WeightedEquityValuePost_IlliquidityDiscount");
        }
        [TestMethod]
        public void Test_EquityValue_PostSurvivalAdjustment_Multiples_LiquidationValue()
        {
            Assert.AreEqual(Math.Round((decimal)3128175.30746869, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_LiquidationValue, 6), $"{FinancialValuation.GetType()}.EquityValue_PostSurvivalAdjustment_Multiples_LiquidationValue");
        }

        [TestMethod]
        public void Test_EquityValue_PostSurvivalAdjustment_Multiples_Probability_WeightedLiquidationValue()
        {
            Assert.AreEqual(Math.Round((decimal)00000, 6), Math.Round(FinancialValuation.EquityValue_PostSurvivalAdjustment_Multiples_Probability_WeightedLiquidationValue, 6), $"{FinancialValuation.GetType()}.EquityValue_PostSurvivalAdjustment_Multiples_Probability_WeightedLiquidationValue");
        }
        [TestMethod]
        public void Test_EquityValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival()
        {

            Assert.AreEqual(Math.Round((decimal)30012665.8004282, 6), Math.Round(FinancialValuation.EquityValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival, 6), $"{FinancialValuation.GetType()}.EquityValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival");
        }

        [TestMethod]
        public void Test_EquityValue_PostSurvivalAdjustment_VCMethod_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival()
        {

            Assert.AreEqual(Math.Round((decimal)25022654.34911070, 6), Math.Round(FinancialValuation.EquityValue_PostSurvivalAdjustment_VCMethod_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival, 6), $"{FinancialValuation.GetType()}.EquityValue_PostSurvivalAdjustment_Multiples_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival");
        }

        [TestMethod]
        public void Test_EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival()
        {

            var t1 = FinancialValuation.EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_Probability_WeightedEquityValuePost_IlliquidityDiscount;
            var t2 = FinancialValuation.EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_Probability_WeightedLiquidationValue;

            Assert.AreEqual(Math.Round((decimal)34516278.39160840, 6), Math.Round(FinancialValuation.EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_EquityValuePost_IlliquidityDiscount_AdjustedForSurvival, 6));
        }


        #endregion
        #region ENTERPRISE VALUE - POST SURVIVAL ADJUSTMENT


        //[TestMethod]
        //public void Test_EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount()
        //{
        //    var t1 = FinancialValuation.Pre_SurvivalAdjustment_DCF_EquityValuePost_IlliquidityDiscount;
        //    var t2 = FinancialValuation.iActualCurrent.ExcessCash_ExcessCash * (decimal)-1;
        //    var t3 = FinancialValuation.All3rdPartyInterestBearingDebtAsAtLastMonth_End * (decimal)-1;
        //    Assert.AreEqual(Math.Round((decimal)29866828.82339050, 6), Math.Round(FinancialValuation.EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount, 6));
        //}


        [TestMethod]
        public void Test_EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival()
        {
            Assert.AreEqual(Math.Round((decimal)29866828.82339050, 6), Math.Round(FinancialValuation.EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival, 6), $"{FinancialValuation.GetType()}.EnterpriseValue_PostSurvivalAdjustment_DCF_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival");
        }



        [TestMethod]
        public void Test_EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_Probability_WeightedLiquidationValue()
        {
            Assert.AreEqual(Math.Round((decimal)0.0, 6), Math.Round(FinancialValuation.EquityValue_PostSurvivalAdjustment_Dcf_Termmultiple_Probability_WeightedLiquidationValue, 6));
        }

        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_TermMultiple_EnterpriseValuePre_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)52275052.30547050, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_TermMultiple_EnterpriseValuePre_IlliquidityDiscount, 6), $"{FinancialValuation.GetType()}.Pre_SurvivalAdjustmentDCF_TermMultiple_EnterpriseValuePre_IlliquidityDiscount");
        }


        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_TermMultiple_ExcessCash()
        {
            Assert.AreEqual(Math.Round((decimal)826914.4508500, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_TermMultiple_ExcessCash, 6));
        }

        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_TermMultiple_InterestBearingDebt()
        {
            Assert.AreEqual(Math.Round((decimal)0.0000000, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_TermMultiple_InterestBearingDebt, 6));
        }

        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePre_IlliquidityDiscount()
        {

            //Pre_SurvivalAdjustmentDCF_TermMultiple_EnterpriseValuePre_IlliquidityDiscount + Pre_SurvivalAdjustmentDCF_TermMultiple_ExcessCash + Pre_SurvivalAdjustmentDCF_TermMultiple_InterestBearingDebt

            Assert.AreEqual(Math.Round((decimal)53101966.75632050, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePre_IlliquidityDiscount, 6));
        }

        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_TermMultiple_IlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)-18585688.36471220, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_TermMultiple_IlliquidityDiscount, 6));
        }

        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePostIlliquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)34516278.39160840, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePostIlliquidityDiscount, 6));
        }

        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_TermMultiple_SustainableEbitdaTtm()
        {
            Assert.AreEqual(Math.Round((decimal)3846742.13490000, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_TermMultiple_SustainableEbitdaTtm, 6));
        }

        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_TermMultiple_ImpliedEv_Over_EbitdaPre_LiquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)13.58943503, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_TermMultiple_ImpliedEv_Over_EbitdaPre_LiquidityDiscount, 6));
        }

        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_TermMultiple_EnterpriseValuePostIlliquidityDiscount()
        {
            var t1 = FinancialValuation.Pre_SurvivalAdjustmentDCF_TermMultiple_EquityValuePostIlliquidityDiscount;
            var t2 = FinancialValuation.iActualCurrent.ExcessCash_ExcessCash;
            var t3 = FinancialValuation.Pre_SurvivalAdjustmentDCF_TermMultiple_InterestBearingDebt;
            var t4 = ((decimal)-1 * (t3 + t2));

            Assert.AreEqual(Math.Round((decimal)33689363.94075840, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_TermMultiple_EnterpriseValuePostIlliquidityDiscount, 6));
        }

        [TestMethod]
        public void Test_Pre_SurvivalAdjustmentDCF_TermMultiple_ImpliedEV_Over_EBITDAPostLiquidityDiscount()
        {
            Assert.AreEqual(Math.Round((decimal)8.75789506, 6), Math.Round(FinancialValuation.Pre_SurvivalAdjustmentDCF_TermMultiple_ImpliedEV_Over_EBITDAPostLiquidityDiscount, 6));
        }






        [TestMethod]
        public void Test_DiscountedCashFlowTerminalMultiple_Ebitda()
        {

            var fy = FinancialValuation.iTerminal;

            Assert.AreEqual(Math.Round((decimal)5498225.51580900, 6), Math.Round(fy.DiscountedCashFlowTerminalMultiple_Ebitda, 6));
        }


        [TestMethod]
        public void Test_DiscountedCashFlowTerminalMultiple_Ev_Over_Ebitda()
        {

            var fy = FinancialValuation.iTerminal;

            Assert.AreEqual(Math.Round((decimal)11.78826549, 6), Math.Round(fy.DiscountedCashFlowTerminalMultiple_Ev_Over_Ebitda, 6));
        }


        [TestMethod]
        public void Test_Terminal_DiscountedCashFlowTerminalMultiple_AdjustedFreeCashFlow()
        {

            var fy = FinancialValuation.iTerminal;

            Assert.AreEqual(Math.Round((decimal)64814542.09286880, 4), Math.Round(fy.DiscountedCashFlowTerminalMultiple_AdjustedFreeCashFlow, 4));
        }

        [TestMethod]
        public void Test_Terminal_DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year()
        {

            var fy = FinancialValuation.iTerminal;

            Assert.AreEqual(Math.Round((decimal)2.75000000, 6), Math.Round(fy.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year, 6));
        }

        [TestMethod]
        public void Test_Terminal_DiscountedCashFlowTerminalMultiple_DiscountFactor()
        {

            var fy = FinancialValuation.iTerminal;

            Assert.AreEqual(Math.Round((decimal)1.43127455, 6), Math.Round(fy.DiscountedCashFlowTerminalMultiple_DiscountFactor, 6));
        }

        [TestMethod]
        public void Test_Terminal_DiscountedCashFlowTerminalMultiple_DiscountFactorInverse()
        {

            var fy = FinancialValuation.iTerminal;

            Assert.AreEqual(Math.Round((decimal)0.69867797, 6), Math.Round(fy.DiscountedCashFlowTerminalMultiple_DiscountFactorInverse, 6));
        }

        [TestMethod]
        public void Test_Terminal_DiscountedCashFlowTerminalMultiple_FreeCashFlow()
        {

            var fy = FinancialValuation.iTerminal;

            Assert.AreEqual(Math.Round((decimal)64814542.09286880, 4), Math.Round(fy.DiscountedCashFlowTerminalMultiple_FreeCashFlow, 4));
        }

        [TestMethod]
        public void Test_Terminal_DiscountedCashFlowTerminalMultiple_DiscountedFreeCashFlow()
        {

            var fy = FinancialValuation.iTerminal;

            Assert.AreEqual(Math.Round((decimal)45284492.95431030, 6), Math.Round(fy.DiscountedCashFlowTerminalMultiple_DiscountedFreeCashFlow, 6));
        }


        [TestMethod]
        public void Test_EnterpriseValue_PostSurvivalAdjustment_VCMethod_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival()
        {
            Assert.AreEqual(Math.Round((decimal)24195739.89826070, 6), Math.Round(FinancialValuation.EnterpriseValue_PostSurvivalAdjustment_VCMethod_EnterpriseValuePost_IlliquidityDiscount_AdjustedForSurvival, 6));
        }

        #endregion



        #region LiquidationvalueRecoveryRate


        [TestMethod]
        public void Test_LiquidationvalueRecoveryRate_AccountsReceiveable()
        {
            Assert.AreEqual(Math.Round((decimal)80 / (decimal)100, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryRate_AccountsReceiveable, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryRate_AccountsReceiveable");
        }






        [TestMethod]
        public void Test_LiquidationvalueRecoveryRate_Inventory()
        {
            Assert.AreEqual(Math.Round((decimal)80 / (decimal)100, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryRate_Inventory, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryRate_Inventory");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryRate_OtherCurrentAssets()
        {
            Assert.AreEqual(Math.Round((decimal)0 / (decimal)100, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryRate_OtherCurrentAssets, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryRate_OtherCurrentAssets");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryRate_CashAndCashEquivalents()
        {
            Assert.AreEqual(Math.Round((decimal)100 / (decimal)100, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryRate_CashAndCashEquivalents, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryRate_CashAndCashEquivalents");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryRate_IntangibleAssets()
        {
            Assert.AreEqual(Math.Round((decimal)0 / (decimal)100, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryRate_IntangibleAssets, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryRate_IntangibleAssets");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryRate_FixedAssets()
        {
            Assert.AreEqual(Math.Round((decimal)50 / (decimal)100, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryRate_FixedAssets, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryRate_FixedAssets");

        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryRate_AccountsPayable()
        {
            Assert.AreEqual(Math.Round((decimal)100 / (decimal)100, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryRate_AccountsPayable, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryRate_AccountsPayable");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryRate_OtherCurrentLiabilities()
        {
            Assert.AreEqual(Math.Round((decimal)100 / (decimal)100, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryRate_OtherCurrentLiabilities, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryRate_OtherCurrentLiabilities");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryRate_3rdPartyInterest_BearingDebtLongTermShortTerm()
        {
            Assert.AreEqual(Math.Round((decimal)100 / (decimal)100, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryRate_3rdPartyInterest_BearingDebtLongTermShortTerm, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryRate_3rdPartyInterest_BearingDebtLongTermShortTerm");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryRate_OtherLong_TermLiabilities()
        {
            Assert.AreEqual(Math.Round((decimal)100 / (decimal)100, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryRate_OtherLong_TermLiabilities, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryRate_OtherLong_TermLiabilities");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryRate_ShareholderLoans()
        {
            Assert.AreEqual(Math.Round((decimal)0, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryRate_ShareholderLoans, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryRate_ShareholderLoans");
        }

        #endregion


        #region LiquidationvalueRecoveryAmount 

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_CurrentAssets()
        {
            Assert.AreEqual(Math.Round((decimal)3373560.0240000, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_CurrentAssets, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_CurrentAssets");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_AccountsReceiveable()
        {

            Assert.AreEqual(Math.Round((decimal)3373560.0240000, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_AccountsReceiveable, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_AccountsReceiveable");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_Inventory()
        {
            Assert.AreEqual(Math.Round((decimal)0, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_Inventory, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_Inventory");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_OtherCurrentAssets()
        {
            Assert.AreEqual(Math.Round((decimal)0, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_OtherCurrentAssets, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_OtherCurrentAssets");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_CashAndCashEquivalents()
        {
            Assert.AreEqual(Math.Round((decimal)0, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_CashAndCashEquivalents, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_CashAndCashEquivalents");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_IntangibleAssets()
        {
            Assert.AreEqual(Math.Round((decimal)0, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_IntangibleAssets, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_IntangibleAssets");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_FixedAssets()
        {
            var t1 = FinancialValuation.iActualCurrent.Liquidationvalue_FixedAssets;
            var t2 = FinancialValuation.LiquidationvalueRecoveryRate_FixedAssets;

            Assert.AreEqual(Math.Round((decimal)840283.5026875, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_FixedAssets, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_FixedAssets");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_TotalAssets()
        {
            Assert.AreEqual(Math.Round((decimal)4213843.5266875, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_TotalAssets, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_TotalAssets");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_CurrentLiabilities()
        {
            Assert.AreEqual(Math.Round((decimal)488399.2192188, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_CurrentLiabilities, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_CurrentLiabilities");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_AccountsPayable()
        {
            Assert.AreEqual(Math.Round((decimal)488399.2192188, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_AccountsPayable, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_AccountsPayable");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_OtherCurrentLiabilities()
        {
            Assert.AreEqual(Math.Round((decimal)0, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_OtherCurrentLiabilities, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_OtherCurrentLiabilities");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_3rdPartyInterest_BearingDebtLongTermShortTerm()
        {
            Assert.AreEqual(Math.Round((decimal)0, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_3rdPartyInterest_BearingDebtLongTermShortTerm, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_3rdPartyInterest_BearingDebtLongTermShortTerm");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_OtherLong_TermLiabilities()
        {
            Assert.AreEqual(Math.Round((decimal)597269.0000000, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_OtherLong_TermLiabilities, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_OtherLong_TermLiabilities");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_ShareholderLoans()
        {
            Assert.AreEqual(Math.Round((decimal)0, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_ShareholderLoans, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_ShareholderLoans");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_TotalLiabilities()
        {
            Assert.AreEqual(Math.Round((decimal)1085668.2192188, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_TotalLiabilities, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_TotalLiabilities");
        }

        [TestMethod]
        public void Test_LiquidationvalueRecoveryAmount_LiquidationValue()
        {
            Assert.AreEqual(Math.Round((decimal)3128175.3074687, 6), Math.Round(FinancialValuation.LiquidationvalueRecoveryAmount_LiquidationValue, 6), "{FinancialValuation.TypeOf()} LiquidationvalueRecoveryAmount_LiquidationValue");
        }
        #endregion




        #region Industry TRBC Values

        [TestMethod]
        public void Test_IndustryTRBCValues_Industry_EBITMargin()
        {
            Assert.AreEqual(Math.Round((decimal)0.02550735, 6), Math.Round(FinancialValuation.Industry_EBITMargin, 6));
        }

         
             [TestMethod]
        public void Test_IndustryTRBCValues_Industry_EBITDAMargin()
        {
            Assert.AreEqual(Math.Round((decimal)0.068852, 6), Math.Round(FinancialValuation.Industry_EBITDAMargin, 6));
        }
       
             [TestMethod]
        public void Test_IndustryTRBCValues_Industry_GrossProfitMargin()
        {
            Assert.AreEqual(Math.Round((decimal)0.329901, 6), Math.Round(FinancialValuation.Industry_GrossProfitMargin, 6));
        }
 
             [TestMethod]
        public void Test_IndustryTRBCValues_Industry_CurrentRatio()
        {
            Assert.AreEqual(Math.Round((decimal)1.40800250, 6), Math.Round(FinancialValuation.Industry_CurrentRatio, 6));
        }
 
          [TestMethod]
        public void Test_IndustryTRBCValues_Industry_InterestCover()
        {
            Assert.AreEqual(Math.Round((decimal)37.109729, 6), Math.Round(FinancialValuation.Industry_InterestCover, 6));
        }
 
          [TestMethod]
        public void Test_IndustryTRBCValues_Industry_TotalDebt_Over_Ebitda()
        {
            Assert.AreEqual(Math.Round((decimal)1.018831, 6), Math.Round(FinancialValuation.Industry_TotalDebt_Over_Ebitda, 6));
        }
 
          [TestMethod]
        public void Test_IndustryTRBCValues_Industry_AccountsPayableDaysAverage()
        {
            Assert.AreEqual(Math.Round((decimal)63.28528293, 6), Math.Round(FinancialValuation.Industry_AccountsPayableDaysAverage, 6));
        }
  
          [TestMethod]
        public void Test_IndustryTRBCValues_Industry_InventoryDaysAverage()
        {
            Assert.AreEqual(Math.Round((decimal)20.626834, 6), Math.Round(FinancialValuation.Industry_InventoryDaysAverage, 6));
        }
 
          [TestMethod]
        public void Test_IndustryTRBCValues_Industry_AccountsReceiveableDaysAverage()
        {
            Assert.AreEqual(Math.Round((decimal)88.579447, 6), Math.Round(FinancialValuation.Industry_AccountsReceiveableDaysAverage, 6));
        }

        #endregion



        #region Sensitivity Analysis Enterprise Value Pre Illiquidity Discount
        
        [TestMethod]
        public void Test_SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM()
        {
            var testSum = FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_1 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_2 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_3 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_4 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EBITDA_TTM_5;

            Assert.AreEqual(Math.Round((decimal)19233710.67450000, 6), Math.Round(testSum, 6));
        }
        [TestMethod]
        public void Test_SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A()
        {
            var testSum = FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_AMultiple + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A1+ FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A2 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A3 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A4 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_A5;

            Assert.AreEqual(Math.Round((decimal)188264676.18831000, 5), Math.Round(testSum, 5));
        }
        [TestMethod]
        public void Test_SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B()
        {
            var testSum = FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_BMultiple + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B1 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B2 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B3 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B4 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_B5;


            Assert.AreEqual(Math.Round((decimal)207498387.86280900, 5), Math.Round(testSum, 5));
        }
        [TestMethod]
        public void Test_SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C()
        {
            var testSum = FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_CMultiple + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C1 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C2 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C3 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C4 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_C5;

            Assert.AreEqual(Math.Round((decimal)226732099.53730900, 5), Math.Round(testSum, 5));
        }
        [TestMethod]
        public void Test_SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D()
        {
            var testSum = FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_DMultiple + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D1 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D2 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D3 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D4 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_D5;


            Assert.AreEqual(Math.Round((decimal)245965811.21180900, 5), Math.Round(testSum, 5));
        }

        [TestMethod]
        public void Test_SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E()
        {
            var testSum = FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_EMultiple + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E1 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E2 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E3 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E4 + FinancialValuation.SensitivityAnalysisEnterpriseValuePreIlliquidityDiscount_E5;


            Assert.AreEqual(Math.Round((decimal)265199522.88631000, 5), Math.Round(testSum, 5));

        }

        #endregion


        #region Sensitivity Analysis Equity Value Post Illiquidity Discount
        [TestMethod]
        public void Test_SensitivityAnalysisEquityValuePostIlliquidityDiscount_EBITDA_TTM()
        {

            var testSum = FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_EBITDA_TTM_1 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_EBITDA_TTM_2 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_EBITDA_TTM_3 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_EBITDA_TTM_4 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_EBITDA_TTM_5;
 
            Assert.AreEqual(Math.Round((decimal)19233710.67450000,5), Math.Round(testSum, 5));
        }

        [TestMethod]
        public void Test_SensitivityAnalysisEquityValuePostIlliquidityDiscount_A()
        {
            var testSum = FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_AMultiple + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_A1 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_A2 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_A3 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_A4 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_A5;
            Assert.AreEqual(Math.Round((decimal)125059514.91355700, 5), Math.Round(testSum, 5));
        }


        [TestMethod]
        public void Test_SensitivityAnalysisEquityValuePostIlliquidityDiscount_B()
        {
            var testSum = FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_BMultiple + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_B1 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_B2 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_B3 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_B4 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_B5;
            Assert.AreEqual(Math.Round((decimal)137561427.85198200, 5), Math.Round(testSum, 5));
             
        }
        [TestMethod]
        public void Test_SensitivityAnalysisEquityValuePostIlliquidityDiscount_C()
        {
            var testSum = FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_CMultiple + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_C1 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_C2 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_C3 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_C4 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_C5;
            Assert.AreEqual(Math.Round((decimal)150063340.79040700, 5), Math.Round(testSum, 5));

        }
        [TestMethod]
        public void Test_SensitivityAnalysisEquityValuePostIlliquidityDiscount_D()
        {
            var testSum = FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_DMultiple + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_D1 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_D2 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_D3 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_D4 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_D5;
            Assert.AreEqual(Math.Round((decimal)162565253.72883200, 5), Math.Round(testSum, 5));

        }
        [TestMethod]
        public void Test_SensitivityAnalysisEquityValuePostIlliquidityDiscount_E()
        {
            var testSum = FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_EMultiple + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_E1 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_E2 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_E3 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_E4 + FinancialValuation.SensitivityAnalysisEquityValuePostIlliquidityDiscount_E5;
            Assert.AreEqual(Math.Round((decimal)175067166.66725700, 5), Math.Round(testSum, 5));

        }

        #endregion



        #region Sensitivity Analysis Equity Value Surviva Adjusted Multiples

        
        [TestMethod]
        public void Test_SurvivalAdjustedMultiples_EBITDA_TTM()
        {

            var testSum = FinancialValuation.SurvivalAdjustedMultiples_EBITDA_TTM_1 + FinancialValuation.SurvivalAdjustedMultiples_EBITDA_TTM_2 + FinancialValuation.SurvivalAdjustedMultiples_EBITDA_TTM_3 + FinancialValuation.SurvivalAdjustedMultiples_EBITDA_TTM_4 + FinancialValuation.SurvivalAdjustedMultiples_EBITDA_TTM_5;

            Assert.AreEqual(Math.Round((decimal)19233710.67450000, 5), Math.Round(testSum, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedMultiples_A()
        {
            var testSum = FinancialValuation.SurvivalAdjustedMultiples_AMultiple + FinancialValuation.SurvivalAdjustedMultiples_A1 + FinancialValuation.SurvivalAdjustedMultiples_A2 + FinancialValuation.SurvivalAdjustedMultiples_A3 + FinancialValuation.SurvivalAdjustedMultiples_A4 + FinancialValuation.SurvivalAdjustedMultiples_A5;
            Assert.AreEqual(Math.Round((decimal)125059514.91355700, 5), Math.Round(testSum, 5));
        }


        [TestMethod]
        public void Test_SurvivalAdjustedMultiples_B()
        {
            var testSum = FinancialValuation.SurvivalAdjustedMultiples_BMultiple + FinancialValuation.SurvivalAdjustedMultiples_B1 + FinancialValuation.SurvivalAdjustedMultiples_B2 + FinancialValuation.SurvivalAdjustedMultiples_B3 + FinancialValuation.SurvivalAdjustedMultiples_B4 + FinancialValuation.SurvivalAdjustedMultiples_B5;
            Assert.AreEqual(Math.Round((decimal)137561427.85198200, 5), Math.Round(testSum, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedMultiples_C()
        {
            var testSum = FinancialValuation.SurvivalAdjustedMultiples_CMultiple + FinancialValuation.SurvivalAdjustedMultiples_C1 + FinancialValuation.SurvivalAdjustedMultiples_C2 + FinancialValuation.SurvivalAdjustedMultiples_C3 + FinancialValuation.SurvivalAdjustedMultiples_C4 + FinancialValuation.SurvivalAdjustedMultiples_C5;
            Assert.AreEqual(Math.Round((decimal)150063340.79040700, 5), Math.Round(testSum, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedMultiples_D()
        {
            var testSum = FinancialValuation.SurvivalAdjustedMultiples_DMultiple + FinancialValuation.SurvivalAdjustedMultiples_D1 + FinancialValuation.SurvivalAdjustedMultiples_D2 + FinancialValuation.SurvivalAdjustedMultiples_D3 + FinancialValuation.SurvivalAdjustedMultiples_D4 + FinancialValuation.SurvivalAdjustedMultiples_D5;
            Assert.AreEqual(Math.Round((decimal)162565253.72883200, 5), Math.Round(testSum, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedMultiples_E()
        {
            var testSum = FinancialValuation.SurvivalAdjustedMultiples_EMultiple + FinancialValuation.SurvivalAdjustedMultiples_E1 + FinancialValuation.SurvivalAdjustedMultiples_E2 + FinancialValuation.SurvivalAdjustedMultiples_E3 + FinancialValuation.SurvivalAdjustedMultiples_E4 + FinancialValuation.SurvivalAdjustedMultiples_E5;
            Assert.AreEqual(Math.Round((decimal)175067166.66725700, 5), Math.Round(testSum, 5));
        }


        #endregion




        #region Sensitivity Analysis Survival Adjusted DCF  Pre Illiquidity Discount
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc()
        {
            var testSum = FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1 + FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2 + FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3 + FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4 + FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5;

            Assert.AreEqual(Math.Round((decimal)0.69634823, 6), Math.Round(testSum, 6));
        }
      





        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A1()
        {
         
           

            //var T1 = (((decimal)Math.Pow((double)((decimal)1 / ((decimal)1 + FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (double)FinancialValuation.iBudget.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year)) * FinancialValuation.iBudget.DiscountedCashFlowTerminalMultiple_FreeCashFlow);

            //var T2 = ((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (double)FinancialValuation.iForecast1.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * FinancialValuation.iForecast1.DiscountedCashFlowTerminalMultiple_FreeCashFlow);

            //var T3 = ((Decimal)Math.Pow((Double)((decimal)1 / ((decimal)1 + FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1)), (Double)FinancialValuation.iForecast2.DiscountedCashFlowTerminalMultiple_DiscountPeriodMid_Year) * FinancialValuation.iForecast2.DiscountedCashFlowTerminalMultiple_FreeCashFlow);

            ////$I$64   FinancialValuation.iTerminal.DiscountedCashFlowTerminalMultiple_AdjustedFreeCashFlow
            ////$D$100   FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1
            ////$E$99  FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_AMultiple
            ////$I$69  FinancialValuation.iTerminal.DiscountedCashFlow_DiscountPeriodMidYear

            ////(($I$64 / ($D$100 -$E$99))/ (1 +$D$100)^$I$69)

            //var T4 = ((FinancialValuation.iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow / (FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1 - FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_AMultiple)) / (decimal)Math.Pow((double)((decimal)1 + FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1), (double)FinancialValuation.iTerminal.DiscountedCashFlow_DiscountPeriodMidYear));

            //var T41 = FinancialValuation.iTerminal.DiscountedCashFlow_AdjustedFreeCashFlow ;
            //var T42 = FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1;
            //var T43 = FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_AMultiple;
            //var T44 = FinancialValuation.iTerminal.DiscountedCashFlow_DiscountPeriodMidYear;
            //Assert.AreEqual(Math.Round((decimal)4101266.7046346, 5), Math.Round(T41, 5));
            //Assert.AreEqual(Math.Round((decimal)0.1292696, 5), Math.Round(T42, 5));
            //Assert.AreEqual(Math.Round((decimal)0.0516500, 5), Math.Round(T43, 5));
            //Assert.AreEqual(Math.Round((decimal)2.2500000, 5), Math.Round(T44, 5));

            





            //Assert.AreEqual(Math.Round((decimal)1342198.2861692, 5), Math.Round(T1, 5));
            //Assert.AreEqual(Math.Round((decimal)2486126.1368944, 5), Math.Round(T2, 5));
            //Assert.AreEqual(Math.Round((decimal)3257901.8941430, 5), Math.Round(T3, 5));
            //Assert.AreEqual(Math.Round((decimal)40193111.8131177, 5), Math.Round(T4, 5));
         
            
            Assert.AreEqual(Math.Round((decimal)47279338.130324, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A1, 5));
        }


        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A2()
        {
            Assert.AreEqual(Math.Round((decimal)44425293.1036844, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A2, 5));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A3()
        {
            Assert.AreEqual(Math.Round((decimal)41897101.8069010, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A3, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A4()
        {
            Assert.AreEqual(Math.Round((decimal)39641991.3323062, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A4, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A5()
        {
            Assert.AreEqual(Math.Round((decimal)37618000.6832739, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_A5, 5));
        }





        






        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B1()
        {
            Assert.AreEqual(Math.Round((decimal)50046709.9102910, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B1, 5));
        }


        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B2()
        {
            Assert.AreEqual(Math.Round((decimal)46833652.6907081, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B2, 5));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B3()
        {
            Assert.AreEqual(Math.Round((decimal)44009586.1939373, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B3, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B4()
        {
            Assert.AreEqual(Math.Round((decimal)41507917.9323860, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B4, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B5()
        {
            Assert.AreEqual(Math.Round((decimal)39276435.1470627, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_B5, 5));
        }









        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C1()
        {
            Assert.AreEqual(Math.Round((decimal)53223337.2754122, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C1, 5));
        }


        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C2()
        {
            Assert.AreEqual(Math.Round((decimal)49573652.5044518, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C2, 5));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C3()
        {
            Assert.AreEqual(Math.Round((decimal)46394229.0479815, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C3, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C4()
        {
            Assert.AreEqual(Math.Round((decimal)43599689.9203338, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C4, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C5()
        {
            Assert.AreEqual(Math.Round((decimal)41124146.1645845, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_C5, 5));
        }







        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D1()
        {
            Assert.AreEqual(Math.Round((decimal)56907253.8967090, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D1, 5));
        }


        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D2()
        {
            Assert.AreEqual(Math.Round((decimal)52718859.9723368, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D2, 5));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D3()
        {
            Assert.AreEqual(Math.Round((decimal)49107246.2448864, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D3, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D4()
        {
            Assert.AreEqual(Math.Round((decimal)45960951.9272674, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D4, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D5()
        {
            Assert.AreEqual(Math.Round((decimal)43195497.8180255, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_D5, 5));
        }







        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E1()
        {
            Assert.AreEqual(Math.Round((decimal)61230521.3081943, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E1, 5));
        }


        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E2()
        {
            Assert.AreEqual(Math.Round((decimal)56366339.1181716, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E2, 5));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E3()
        {
            Assert.AreEqual(Math.Round((decimal)52221480.7442483, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E3, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E4()
        {
            Assert.AreEqual(Math.Round((decimal)48647368.6459576, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E4, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E5()
        {
            Assert.AreEqual(Math.Round((decimal)45533708.6700925, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_E5, 5));
        }




        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1()
        {
            Assert.AreEqual(Math.Round((decimal)0.1292696, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_1, 5));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2()
        {
            Assert.AreEqual(Math.Round((decimal)0.1342696, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_2, 5));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3()
        {
            Assert.AreEqual(Math.Round((decimal)0.1392696, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_3, 5));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4()
        {
            Assert.AreEqual(Math.Round((decimal)0.1442696, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_4, 5));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5()
        {
            Assert.AreEqual(Math.Round((decimal)0.1492696, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_Wacc_5, 5));
        }


        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_AMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.051650, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_AMultiple, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_BMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.056650, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_BMultiple, 5));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_CMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.061650, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_CMultiple, 5));
 
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_DMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.066650, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_DMultiple, 5));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_EMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.071650, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEnterpriseValuePreIlliquidityDiscount_EMultiple, 5));
        }
        #endregion
        #region ensitivity Analysis Survival Adjusted DCF Post Illiquidity Discount

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_1()
        {
            Assert.AreEqual(Math.Round((decimal)0.1292696, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_1, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_2()
        {
            Assert.AreEqual(Math.Round((decimal)0.1342696, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_2, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_3()
        {
            Assert.AreEqual(Math.Round((decimal)0.1392696, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_3, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_4()
        {
            Assert.AreEqual(Math.Round((decimal)0.1442696, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_4, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_5()
        {
            Assert.AreEqual(Math.Round((decimal)0.1492696, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_Wacc_5, 6));
        }



        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_AMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.051650, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_AMultiple, 6));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_BMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.056650, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_BMultiple, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_CMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.061650, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_CMultiple, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_DMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.066650, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_DMultiple, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_EMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.071650, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_EMultiple, 6));
        }


        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A1()
        {
            Assert.AreEqual(Math.Round((decimal)31269064.1777633, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A1, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A2()
        {
            Assert.AreEqual(Math.Round((decimal)29413934.9104474, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A2, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A3()
        {
            Assert.AreEqual(Math.Round((decimal)27770610.5675382, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A3, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A4()
        {
            Assert.AreEqual(Math.Round((decimal)26304788.7590515, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A4, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A5()
        {
            Assert.AreEqual(Math.Round((decimal)24989194.8371806, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A5, 6));
        }





        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B1()
        {
            Assert.AreEqual(Math.Round((decimal)33067855.8347416, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B1, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B2()
        {
            Assert.AreEqual(Math.Round((decimal)30979368.6420128, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B2, 6));
        }
        





        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B3()
        {
            Assert.AreEqual(Math.Round((decimal)29143725.4191118, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B3, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B4()
        {
            Assert.AreEqual(Math.Round((decimal)27517641.0491034, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B4, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B5()
        {
            Assert.AreEqual(Math.Round((decimal)26067177.2386433, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_B5, 6));
        }





        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C1()
        {
            Assert.AreEqual(Math.Round((decimal)35132663.6220705, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C1, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C2()
        {
            Assert.AreEqual(Math.Round((decimal)32760368.5209462, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C2, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C3()
        {
            Assert.AreEqual(Math.Round((decimal)30693743.2742405, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C3, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C4()
        {
            Assert.AreEqual(Math.Round((decimal)28877292.8412694, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C4, 5));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C5()
        {
            Assert.AreEqual(Math.Round((decimal)27268189.4000324, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_C5, 6));
        }




        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D1()
        {
            Assert.AreEqual(Math.Round((decimal)37527209.4259133, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D1, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D2()
        {
            Assert.AreEqual(Math.Round((decimal)34804753.3750714, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D2, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D3()
        {
            Assert.AreEqual(Math.Round((decimal)32457204.4522287, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D3, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D4()
        {
            Assert.AreEqual(Math.Round((decimal)30412113.1457763, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D4, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D5()
        {
            Assert.AreEqual(Math.Round((decimal)28614567.9747691, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_D5, 6));
        }




        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E1()
        {
            Assert.AreEqual(Math.Round((decimal)40337333.2433788, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E1, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E2()
        {
            Assert.AreEqual(Math.Round((decimal)37175614.8198640, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E2, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E3()
        {
            Assert.AreEqual(Math.Round((decimal)34481456.8768139, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E3, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E4()
        {
            Assert.AreEqual(Math.Round((decimal)32158284.0129250, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E4, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E5()
        {
            Assert.AreEqual(Math.Round((decimal)30134405.0286127, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_E5, 6));
        }










        #endregion

        #region Sensitivity Analysis Survival Adjusted DCF 




        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_Wacc_1()
        {
            Assert.AreEqual(Math.Round((decimal)0.1292696, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_Wacc_1, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_Wacc_2()
        {
            Assert.AreEqual(Math.Round((decimal)0.1342696, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_Wacc_2, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_Wacc_3()
        {
            Assert.AreEqual(Math.Round((decimal)0.1392696, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_Wacc_3, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_Wacc_4()
        {
            Assert.AreEqual(Math.Round((decimal)0.1442696, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_Wacc_4, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_Wacc_5()
        {
            Assert.AreEqual(Math.Round((decimal)0.1492696, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_Wacc_5, 6));
        }



        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_AMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.051650, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_AMultiple, 6));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_BMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.056650, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_BMultiple, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_CMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.061650, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_CMultiple, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_DMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.066650, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_DMultiple, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_EMultiple()
        {
            Assert.AreEqual(Math.Round((decimal)0.071650, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_EMultiple, 6));
        }








        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_A1()
        {
            var t1 = FinancialValuation.SurvivalAdjustedDCFEquityValuePostIlliquidityDiscount_A1;
            var t2 = FinancialValuation.ProbabilityOfSurvival;
            var t3 = FinancialValuation.EnterpriseValue_PostSurvivalAdjustment_DCF_ProbabilityWeightedLiquidationValue;
            Assert.AreEqual(Math.Round((decimal)31269064.17776330, 6), Math.Round(t1, 6), "T1");
            Assert.AreEqual(Math.Round((decimal)1.00000000, 6), Math.Round(t2, 6), "T2");
            Assert.AreEqual(Math.Round((decimal)0.00000000, 6), Math.Round(t3, 6), "T3");



            




            Assert.AreEqual(Math.Round((decimal)31269064.1777633, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_A1, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_A2()
        {
            Assert.AreEqual(Math.Round((decimal)29413934.9104474, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_A2, 6));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_A3()
        {
            Assert.AreEqual(Math.Round((decimal)27770610.5675382, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_A3, 6));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_A4()
        {
            Assert.AreEqual(Math.Round((decimal)26304788.7590515, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_A4, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_A5()
        {
            Assert.AreEqual(Math.Round((decimal)24989194.8371806, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_A5, 6));
        }


        
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_B1()
        {
            Assert.AreEqual(Math.Round((decimal)33067855.8347416, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_B1, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_B2()
        {
            Assert.AreEqual(Math.Round((decimal)30979368.6420128, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_B2, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_B3()
        {
            Assert.AreEqual(Math.Round((decimal)29143725.4191118, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_B3, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_B4()
        {
            Assert.AreEqual(Math.Round((decimal)27517641.0491034, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_B4, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_B5()
        {
            Assert.AreEqual(Math.Round((decimal)26067177.2386433, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_B5, 6));
        }






        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_C1()
        {
            Assert.AreEqual(Math.Round((decimal)35132663.6220705, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_C1, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_C2()
        {
            Assert.AreEqual(Math.Round((decimal)32760368.5209462, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_C2, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_C3()
        {
            Assert.AreEqual(Math.Round((decimal)30693743.2742405, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_C3, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_C4()
        {
            Assert.AreEqual(Math.Round((decimal)28877292.8412694, 5), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_C4, 5));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_C5()
        {
            Assert.AreEqual(Math.Round((decimal)27268189.4000324, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_C5, 6));
        }






        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_D1()
        {
            Assert.AreEqual(Math.Round((decimal)37527209.4259133, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_D1, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_D2()
        {
            Assert.AreEqual(Math.Round((decimal)34804753.3750714, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_D2, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_D3()
        {
            Assert.AreEqual(Math.Round((decimal)32457204.4522287, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_D3, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_D4()
        {
            Assert.AreEqual(Math.Round((decimal)30412113.1457763, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_D4, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_D5()
        {
            Assert.AreEqual(Math.Round((decimal)28614567.9747691, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_D5, 6));
        }

        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_E1()
        {
            Assert.AreEqual(Math.Round((decimal)40337333.2433788, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_E1, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_E2()
        {
            Assert.AreEqual(Math.Round((decimal)37175614.8198640, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_E2, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_E3()
        {
            Assert.AreEqual(Math.Round((decimal)34481456.8768139, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_E3, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_E4()
        {
            Assert.AreEqual(Math.Round((decimal)32158284.0129250, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_E4, 6));
        }
        [TestMethod]
        public void Test_SurvivalAdjustedDCFEquity_E5()
        {
            Assert.AreEqual(Math.Round((decimal)30134405.0286127, 6), Math.Round(FinancialValuation.SurvivalAdjustedDCFEquity_E5, 6));
        }

        #endregion

    }
}
