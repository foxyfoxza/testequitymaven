﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EquityMaven.Data.Calculations;
using EquityMaven.Tests.UnitTests.TestData;
using EquityMaven.TestData;

namespace EquityMaven.Tests.UnitTests.Calculators

{
    [TestClass]
    public class UnitTestWACCCalculator
    {
        [TestInitialize]
        public virtual void InitializeTest()
        {
            FinancialValuation = TestDataHelper.GetFinancialValuation();
        }
        public FinancialValuation FinancialValuation { get; set; }


        [TestMethod]
        public void Test_WACC_WaccInputsSummary_Risk_FreeRate()
        {
            Assert.AreEqual((decimal)6.165 / 100, FinancialValuation.WaccInputsSummary_Risk_FreeRate, "WaccInputsSummary_Risk_FreeRate");
        }
        

        [TestMethod]
        public void Test_WACC_WaccInputsSummary_EquityRiskPremium()
        {
            Assert.AreEqual((decimal)7.086 / 100, FinancialValuation.WaccInputsSummary_EquityRiskPremium, "WaccInputsSummary_EquityRiskPremium");
        }
         
        [TestMethod]
        public void Test_WACC_WaccInputsSummary_CompanyRiskPremium()
        {
            Assert.AreEqual(Math.Round((decimal)8.29340559794381 / 100,6), Math.Round(FinancialValuation.WaccInputsSummary_CompanyRiskPremium,6), "WaccInputsSummary_CompanyRiskPremium");
        }



        
        [TestMethod]
        public void Test_WACC_WaccInputsSummary_CostOfDebtPre_Tax()
        {
            Assert.AreEqual(Math.Round((decimal)8.965 / 100, 6), Math.Round(FinancialValuation.WaccInputsSummary_CostOfDebtPre_Tax, 6), "WaccInputsSummary_CostOfDebtPre_Tax");
        }

        

        [TestMethod]
        public void Test_WACC_WaccInputsSummary_LeveredBeta()
        {
            Assert.AreEqual((decimal)Math.Round(1.170393112890740000, 6), Math.Round(FinancialValuation.WaccInputsSummary_LeveredBeta, 6));

        }

        

        [TestMethod]
        public void Test_WACC_WaccInputsSummary_TotalDebt_Over_EquityD_Over_E()
        {
            Assert.AreEqual(Math.Round((decimal)7.11227635232975 / 100, 6), Math.Round(FinancialValuation.WaccInputsSummary_TotalDebt_Over_EquityD_Over_E, 6), "WaccInputsSummary_TotalDebt_Over_EquityD_Over_E");
        }

        

        [TestMethod]
        public void Test_WACC_WaccInputsSummary_GearingD_Over_D_Plus_E()
        {
            Assert.AreEqual(Math.Round((decimal)6.64001979468254 / 100, 6), Math.Round(FinancialValuation.WaccInputsSummary_GearingD_Over_D_Plus_E, 6), "WaccInputsSummary_GearingD_Over_D_Plus_E");
        }


        [TestMethod]
        public void Test_WACC_WaccInputsSummary_TaxRate()
        {
            Assert.AreEqual(Math.Round((decimal)28 / 100, 6), Math.Round(FinancialValuation.WaccInputsSummary_TaxRate, 6), "WaccInputsSummary_TaxRate");
        }

         
        [TestMethod]
        public void Test_WACC_WaccInputsSummary_TerminalGrowthRate()
        {
            Assert.AreEqual(Math.Round((decimal)6.165 / 100, 6), Math.Round(FinancialValuation.WaccInputsSummary_TerminalGrowthRate, 6));
        }
         
        [TestMethod]
        public void Test_WACC_KE()
        {
            Assert.AreEqual(Math.Round((decimal)14.4584055979438 / 100, 6), Math.Round(FinancialValuation.WaccCalculation_Ke, 6));
        }

        
        [TestMethod]
        public void Test_WACC_WaccCalculation_E_Over_D_Plus_E()
        {
            Assert.AreEqual(Math.Round((decimal)93.3599802053175 / 100, 6), Math.Round(FinancialValuation.WaccCalculation_E_Over_D_Plus_E, 6));
        }
        
        [TestMethod]
        public void Test_WACC_WaccCalculation_Kd()
        {
            Assert.AreEqual(Math.Round((decimal)6.4548 / 100, 6), Math.Round(FinancialValuation.WaccCalculation_Kd, 6));
        }
       
        [TestMethod]
        public void Test_WACC_WaccCalculation_D_Over_D_Plus_E()
        {
            Assert.AreEqual(Math.Round((decimal)6.64001979468254 / 100, 6), Math.Round(FinancialValuation.WaccCalculation_D_Over_D_Plus_E, 6));
        }
         
        [TestMethod]
        public void Test_WACC_WaccCalculation_Wacc()
        {
            Assert.AreEqual(Math.Round((decimal)13.926964601952 / 100, 6), Math.Round(FinancialValuation.WaccCalculation_Wacc, 6));
        }



 
        [TestMethod]
        public void Test_WACC_Beta_UnleveredBeta()
        {
            Assert.AreEqual(Math.Round((decimal)1.1133787784778, 6), Math.Round(FinancialValuation.Beta_UnleveredBeta, 6));
        }


        [TestMethod]
        public void Test_WACC_Beta_MarginalTaxRate()
        {
            Assert.AreEqual(Math.Round((decimal)28.0   / 100, 6), Math.Round(FinancialValuation.Beta_MarginalTaxRate, 6));
        }

        [TestMethod]
        public void Test_WACC_Beta_TotalDebtToEquityMarketCap()
        {
            Assert.AreEqual(Math.Round((decimal)0.071122763523297500, 6), Math.Round(FinancialValuation.Beta_TotalDebtToEquityMarketCap, 6));
        }

        [TestMethod]
        public void Test_WACC_Beta_LeveredBeta()
        {
            Assert.AreEqual( (decimal)Math.Round(1.170393112890740000, 6), Math.Round(FinancialValuation.Beta_LeveredBeta, 6));
        }

        [TestMethod]
        public void Test_WACC_EquityRiskPremium_MatureMarketImpliedEquityRiskPremium()
        {
            Assert.AreEqual( (decimal)Math.Round(4.38000000000  / 100, 6), Math.Round(FinancialValuation.EquityRiskPremium_MatureMarketImpliedEquityRiskPremium , 6));
        }
        [TestMethod]
        public void Test_WACC_EquityRiskPremium_CountryRatingBasedDefaultSpread()
        {
            Assert.AreEqual( (decimal)220/10000, Math.Round(FinancialValuation.EquityRiskPremium_CountryRatingBasedDefaultSpread, 6));
        }

        [TestMethod]
        public void Test_WACC_EquityRiskPremium_RelativeVolatilityOfEquityVsBonds()
        {
            Assert.AreEqual((decimal)Math.Round(1.23, 6), Math.Round(FinancialValuation.EquityRiskPremium_RelativeVolatilityOfEquityVsBonds, 6));
        }
        [TestMethod]
        public void Test_WACC_EquityRiskPremium_EquityRiskPremium()
        {
            Assert.AreEqual((decimal)Math.Round(7.086/100, 6), Math.Round(FinancialValuation.EquityRiskPremium_EquityRiskPremium, 6));
        }





        [TestMethod]
        public void Test_WACC_CostOfDebt_RiskFreeRate()
        {
            Assert.AreEqual((decimal)Math.Round(6.165  / 100, 6), Math.Round(FinancialValuation.CostOfDebt_RiskFreeRate, 6));
        }

        [TestMethod]
        public void Test_WACC_CostOfDebt_InterestCoverageRatio()
        {
            Assert.AreEqual(Math.Round((decimal)37.1097294493, 6), Math.Round(FinancialValuation.CostOfDebt_InterestCoverageRatio, 6));
        }

        [TestMethod]
        public void Test_WACC_CostOfDebt_EstimatedCorporateDefaultSpread()
        {
            Assert.AreEqual((decimal)Math.Round(0.6  / 100, 6), Math.Round(FinancialValuation.CostOfDebt_EstimatedCorporateDefaultSpread, 6));
        }
        [TestMethod]
        public void Test_WACC_CostOfDebt_EstimatedCountryDefaultSpread()
        {
            Assert.AreEqual((decimal)Math.Round(2.2  / 100, 6), Math.Round(FinancialValuation.CostOfDebt_EstimatedCountryDefaultSpread, 6));
        }
        [TestMethod]
        public void Test_WACC_CostOfDebt_EstimatedCostOfDebt()
        {
            Assert.AreEqual((decimal)Math.Round(8.965 / 100, 6), Math.Round(FinancialValuation.CostOfDebt_EstimatedCostOfDebt, 6));
        }
        [TestMethod]
        public void Test_WACC_CostOfDebt_MarginalTaxRate()
        {
            Assert.AreEqual((decimal)Math.Round( 28.0 / 100, 6), Math.Round(FinancialValuation.CostOfDebt_MarginalTaxRate, 6));
        }
        [TestMethod]
        public void Test_WACC_CostOfDebt_After_TaxCostOfDebt()
        {
            Assert.AreEqual((decimal)Math.Round(6.4548 / 100, 6), Math.Round(FinancialValuation.CostOfDebt_After_TaxCostOfDebt, 6));
        }
        


    }
}
