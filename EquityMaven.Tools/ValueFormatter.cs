﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Tools
{
    public static class ValueFormatter
    {
        public static string ConvertZero(decimal value)
        {
            return value == 0 ? "-" : value.ToString();
        }

        public static string ConvertZero(decimal value, string suff)
        {
            return value == 0 ? "-" : (value.ToString() + suff);
        }

        
        public static string CommaSpacing(decimal value, int? roundto = null, string suff = "")
        {
            if (roundto.HasValue)
            {
                value = Math.Round(value, roundto.Value);
            }
            string format = "{0:0,0" + (roundto.HasValue && roundto.Value > 0 ? "." + new string('0', roundto.Value) : string.Empty) + "}";
            return string.Format(format, value) + (suff ?? string.Empty);
            //if (roundto.HasValue)
            //{
            //    value = Math.Round(value, roundto.Value);
            //}
            //return string.Format("{0:0,0}", value);

        }

        public static string SpaceSpacing(decimal value, int? roundto = null, string suff = "")
        {
            return CommaSpacing(value, roundto, suff).Replace(",", " ");
            //string format = "{0:0 0" + (roundto.HasValue ? "." + new string('0', roundto.Value) : string.Empty) + "}";
            //return string.Format(format, value) + (suff ?? string.Empty);
        }


    }
}
