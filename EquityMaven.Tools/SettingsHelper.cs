﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Tools
{
    public static class SettingsHelper
    {
        private static System.Configuration.AppSettingsReader SettingsReader = new System.Configuration.AppSettingsReader();
        private static ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static String Get(string settingName, string defaultValue)
        {
            return GetSetting<string>(settingName, defaultValue);
        }
        public static T GetSetting<T>(string settingName, T defaultValue)
        {
            try
            {
                return GetSetting<T>(settingName);
            }
            catch (Exception ex)
            {
                return defaultValue;
            }

        }

        public static T GetSetting<T>(string settingName)
        {
            if (typeof(T) == typeof(DateTime))
            {
                var result = GetDateTimeSetting(settingName);
                if (result.HasValue)
                    return (T)(object)(result.Value);
                else
                    throw new FormatException($"{settingName} has an invalid format or not found");
            }
            else
                return (T)SettingsReader.GetValue(settingName, typeof(T));
        }


        /// <summary>
        /// Gets a date and Time
        /// </summary>
        public static DateTime? GetDateTimeSetting(string settingName)
        {
            var _value = GetSetting<string>(settingName, null);
            DateTime? result = null;
            try
            {
                if (!string.IsNullOrWhiteSpace(_value))
                {
                    DateTime dateVal;
                    if (DateTime.TryParse(_value, out dateVal))
                    {
                        result = dateVal;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
    }
}
