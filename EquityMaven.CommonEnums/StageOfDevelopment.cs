﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.CommonEnums
{
    public enum StageOfDevelopment
    {
        [Description("Start-up")]
        Startup = 0,
        [Description("First Stage or \"early development\"")]
        FirstStage_EarlyDevelopment = 1,
        [Description("Second stage or \"expansion\"")]
        SecondStage_Expansion = 2,
        [Description("Bridge/IPO")]
        BridgeOrIPO = 3
    }

    public static class StageOfDevelopmentHelper
    {
        public static StageOfDevelopment GetStageOfDevelopment(string description)
        {
            int stageOfDevelopmentValue;
            if (int.TryParse(description, out stageOfDevelopmentValue))
            {
                return (StageOfDevelopment)stageOfDevelopmentValue;
            }

            switch (description)
            {
                case "Start-up":
                    return StageOfDevelopment.Startup;
                case "First Stage or \"early development\"":
                    return StageOfDevelopment.FirstStage_EarlyDevelopment;
                case "Second stage or \"expansion\"":
                    return StageOfDevelopment.SecondStage_Expansion;
                case "Bridge/IPO":
                    return StageOfDevelopment.BridgeOrIPO;
                default:
                    throw new ArgumentOutOfRangeException($"Could not convert {description} to StageOfDevelopment");


            }
        }
    }

}
