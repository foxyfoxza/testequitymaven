﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.CommonEnums
{
    public enum FinancialYearType
    {
        None = 0,
        ActualPrior = 1,
        ActualCurrent = 2,
        Budget = 3,
        Forecast1 = 4,
        Forecast2 = 5,
        Terminal = 6
    }

    public static class FinancialYearHelper
    {
        public static bool IsActual(this FinancialYearType fyt)
        {
            return fyt == FinancialYearType.ActualCurrent || fyt == FinancialYearType.ActualPrior;
        }
        public static bool IsForecast(this FinancialYearType fyt)
        {
            return fyt == FinancialYearType.Forecast1|| fyt == FinancialYearType.Forecast2;
        }
        public static FinancialYearType Prior(this FinancialYearType fyt)
        {
            switch(fyt)
            {
                case FinancialYearType.ActualCurrent:
                    return FinancialYearType.ActualPrior;
                case FinancialYearType.Budget:
                    return FinancialYearType.ActualCurrent;
                case FinancialYearType.Forecast1:
                    return FinancialYearType.Budget;
                case FinancialYearType.Forecast2:
                    return FinancialYearType.Forecast1;
                case FinancialYearType.Terminal:
                    return FinancialYearType.Forecast2;

                case FinancialYearType.ActualPrior:
                default:
                    return FinancialYearType.None;
            }
        }
        public static FinancialYearType Next(this FinancialYearType fyt)
        {
            switch (fyt)
            {
                case FinancialYearType.ActualPrior:
                    return FinancialYearType.ActualCurrent;
                case FinancialYearType.ActualCurrent:
                    return FinancialYearType.Budget;
                case FinancialYearType.Budget:
                    return FinancialYearType.Forecast1;
                case FinancialYearType.Forecast1:
                    return FinancialYearType.Forecast2;
                case FinancialYearType.Forecast2:
                case FinancialYearType.Terminal:
                default:
                    return FinancialYearType.None;
            }
        }
    }

}
