﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.CompilerServices;
using Umbraco.Core.Logging;
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Attributes;
using Umbraco.Forms.Core.Common;
using Umbraco.Forms.Core.Data.PrevalueSourceStorage;
using Umbraco.Forms.Data.Storage;
using System.Reflection;
using System.Linq;
using EquityMaven.Data.Entities;
using System.ComponentModel;
using EquityMaven.CommonEnums;

namespace EquityMaven.Umb.Custom.Prevalues
{


    public class GetValuesFromRestApi : FieldPreValueSourceType
    {
        private T GetData<T>(string url)
        {
            using (var httpClient = new HttpClient())
            {
                
                var response = httpClient.GetAsync(url).Result;
                //var response = client.PostAsJsonAsync(url, json).Result;
                if (!response.IsSuccessStatusCode)
                    throw new Exception(response.ReasonPhrase);
                var responseString = response.Content.ReadAsStringAsync().Result;
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseString);
            }
        }

        //[Setting("Url", description = "Webservice URL", view = "TextField")]
        public string ServiceUrl
        {
            get;
            set;
        }


        [Setting("Type of Value", description = "Choose the type you want to supply)", view = "pickers.EquityMavenPrelimEvalType")]
        public string PrelimEvaluationType
        {
            get;
            set;
        }

        [Setting("Key Column", description = "Column containing the keys", view = "TextField")]
        public string KeyColoumn
        {
            get;
            set;
        }

        [Setting("Table Name", description = "Table name", view = "TextField")]
        public string Table
        {
            get;
            set;
        }

        [Setting("Value Column", description = "Column containing the values", view = "TextField")]
        public string ValueColoumn
        {
            get;
            set;
        }

        public GetValuesFromRestApi()
        {
            base.Id = new Guid("C99E1910-7C27-4808-8766-EE21B39FA7F0");
            base.Name = "Rest Api";
            base.Description = "Connects to a Rest API and constructs a prevalue source from it";
        }


        public override List<PreValue> GetPreValues(Field field, Form form)
        {
            switch (PrelimEvaluationType)
            {         

                case "TRBC":
                    return GetPreValues<ThompsonReutersBusinessClassification>(field, form, "/umbraco/api/PrelimEvaluation/GetThompsonReutersBusinessClassifications");
                case "Currency":
                    return GetPreValues<Currency>(field, form, "/umbraco/api/PrelimEvaluation/GetCurrencies");
                case "Country":
                    return GetPreValues<Country>(field, form, "/umbraco/api/PrelimEvaluation/GetCountries");
                case "Stage of Development":
                    return GetPreValues<StageOfDevelopment> (field, form);
            }

            return new List<PreValue>();
        }


        //private string GetItemValue<T>(PropertyInfo valueField, PropertyInfo keyField,  T item)
        //{
        //    bool serializeItem = valueField == null;
        //    var value = serializeItem ? item : (object)valueField.GetValue(item);
        //    return serializeItem ? Newtonsoft.Json.JsonConvert.SerializeObject(value) : value?.ToString();
        //}
        //private void GetItemValue<T>(T item, out object _id, out string _value)
        //{
        //    Type t = typeof(T);
        //    PropertyInfo[] propInfos = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
        //    var keyField = propInfos.FirstOrDefault(o => o.Name == KeyColoumn.Split('.').FirstOrDefault());
        //    var valueField = string.IsNullOrEmpty(ValueColoumn) ? null : propInfos.FirstOrDefault(o => o.Name == ValueColoumn.Split('.').FirstOrDefault());
        //    bool serializeItem = valueField == null;
        //    var value = serializeItem ? item : (object)valueField.GetValue(item);
        //    _id = keyField.GetValue(item);
        //    _value = serializeItem ? Newtonsoft.Json.JsonConvert.SerializeObject(value) : value?.ToString();
        //}

        private void GetItemValue<T>(T item, PropertyInfo keyField, IEnumerable<PropertyInfo> valueFields, out object _id, out string _value)
        {
            _id = keyField.GetValue(item);
            if (valueFields == null)
            {
                _value = Newtonsoft.Json.JsonConvert.SerializeObject(item);
            }
            else 
            {
                _value = ValueColoumn.Replace(",", "");
                var values = new List<object>();
                foreach (var field in valueFields)
                {
                    var fieldValue = field.GetValue(item)?.ToString() ?? "";
                    _value = _value.Replace(field.Name, fieldValue);
                }
                 
                 
            }
        }

        private IEnumerable<PropertyInfo> GetValueProperties<T>()
        {
            var result = new List<PropertyInfo>();
            Type t = typeof(T);
            PropertyInfo[] propInfos = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            if (string.IsNullOrEmpty(ValueColoumn))
            {
                return null;
            }
            else
            {
                var values = new List<object>();
                foreach (var col in ValueColoumn.Split(','))
                {
                    var valueField = string.IsNullOrEmpty(col) ? null : propInfos.FirstOrDefault(o => o.Name == col.Split('.').FirstOrDefault());

                    if (valueField != null)
                        result.Add(valueField);
                }
            }
            return result;
        }

        public List<PreValue> GetPreValues<T>(Field field, Form form) where T : struct, IConvertible
        {
            List<PreValue> preValues = new List<PreValue>();

            
            try
            {
                try
                {
                    int num = 0;
                    Type t = typeof(T);
                    PropertyInfo[] propInfos = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                    var keyField = propInfos.FirstOrDefault(o => o.Name == KeyColoumn.Split('.').FirstOrDefault());
                    var enumValues = Enum.GetValues(typeof(T)).Cast<T>().ToList();

                    foreach (var item in enumValues)
                    {
                        FieldInfo fi = item.GetType().GetField(item.ToString());

                        DescriptionAttribute[] attributes =
                            (DescriptionAttribute[])fi.GetCustomAttributes(
                            typeof(DescriptionAttribute),
                            false);

                        string _value;
                        Object _Id = item;


                        if (attributes != null &&
                            attributes.Length > 0)
                            _value = attributes[0].Description;
                        else
                            _value = item.ToString();
                        
                        PreValue preValue = new PreValue()
                        {
                            Id = _Id,
                            Value = _value,
                            SortOrder = num
                        };

                        preValues.Add(preValue);
                        num++;
                    }



                }
                catch (Exception exception1)
                {
                    Exception exception = exception1;

                    throw;
                }
            }
            finally
            {

            }
            return preValues;
        }

        public List<PreValue> GetPreValues<T>(Field field, Form form, string urlAction = null)
        {
            List<PreValue> preValues = new List<PreValue>();

            var url = $"Http://{System.Web.HttpContext.Current.Request.Url.Host}{(!string.IsNullOrEmpty(urlAction) ? urlAction : ServiceUrl)}";

            var restValues = GetData<IEnumerable<T>>(url);

            try
            {
                try
                {
                    int num = 0;
                    Type t = typeof(T);
                    PropertyInfo[] propInfos = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                    var keyField = propInfos.FirstOrDefault(o => o.Name == KeyColoumn.Split('.').FirstOrDefault());
                    var valueFields = GetValueProperties<T>();

                    foreach (var item in restValues)
                    {
                        Object _Id;
                        string _value;

                        GetItemValue(item, keyField, valueFields, out _Id, out _value);
                        PreValue preValue = new PreValue()
                        {
                            Id = _Id,
                            Value = _value,
                            SortOrder = num
                        };

                        preValues.Add(preValue);
                        num++;
                    }



                }
                catch (Exception exception1)
                {
                    Exception exception = exception1;

                    throw;
                }
            }
            finally
            {

            }
            return preValues;
        }

        public override List<Exception> ValidateSettings()
        {
            List<Exception> exceptions = new List<Exception>();


            if (string.IsNullOrEmpty(this.PrelimEvaluationType) && string.IsNullOrEmpty(this.ServiceUrl))
            {
                exceptions.Add(new Exception("'Type of Value' setting has not been set"));
            }


            if (!exceptions.Any<Exception>())
            {

            }
            return exceptions;
        }
    }

}