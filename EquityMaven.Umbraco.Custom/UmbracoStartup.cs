﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;

namespace EquityMaven.Umb.Custom
{
    public class UmbracoStartup : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            Umbraco.Web.UI.JavaScript.ServerVariablesParser.Parsing += ServerVariablesParser_Parsing;
            //When Umbraco has started up - wire up the ServerVariablesParser event
            //ServerVariablesParser_Parsing.Parsing += ServerVariablesParser_Parsing;
        }

        
        private void ServerVariablesParser_Parsing(object sender, Dictionary<string, object> e)
        {
            e.Add("EMAdmin", new Dictionary<string, object>
        {
            { "emAdminBaseUrl", "Bob" },
            
        });
        }
    }
}
