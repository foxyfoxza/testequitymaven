﻿using EquityMaven.Data;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.Models;
using EquityMaven.Umb.Custom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using umbraco.BusinessLogic.Actions;
using Umbraco.Web;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;

namespace EquityMaven.Umb.Custom.Section
{
    [PluginController("EMAdmin")]
    [Umbraco.Web.Trees.Tree("EMAdmin", "EMAdminTree", "EM Admin", iconClosed: "icon-doc")]
    public class EMAdminSectionTreeController : TreeController
    {
        private T GetData<T>(string subpath)
        {
            using (var httpClient = new HttpClient())
            {
                var url = $"Http://{System.Web.HttpContext.Current.Request.Url.Host}/{subpath}";
                var response = httpClient.GetAsync(url).Result;
                //var response = client.PostAsJsonAsync(url, json).Result;
                if (!response.IsSuccessStatusCode)
                    throw new Exception(response.ReasonPhrase);
                var responseString = response.Content.ReadAsStringAsync().Result;
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseString);
            }
        }

        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            var nodes = new TreeNodeCollection();
            const string ID_ROOT = "-1";
            const string ID_ANNUAL_INPUTBATCH = "AnnualInputBatch";
            const string ID_MONTHLY_INPUTBATCH = "MonthlyInputBatch";
            const string ID_WACC_INPUTS_SUMMARY = "Wacc Inputs Summary";
            const string ID_CURRENCYRISKFREERATE = "Currency Risk Free Rate";// (monthly)
            const string ID_TRBC_MAP = "TRBC Map";
            const string ID_COUNTRYMOODYSRATING = "Country Moodys Rating"; //(monthly)
            const string ID_COUNTRYTAXRATE = "Country Tax Rate"; //(monthly)
            const string ID_MOODYSRATINGS = "Moodys Ratings"; //(CDS spreads - annually)
            const string ID_CORPORATEBONDSPREADS = "Corporate Bond Spreads"; //(annually)

            const string ID_PRELIM_VALUATIONS = "PRE-VALUATIONS";
            var idvalues = id.Split('_');
            var _id = idvalues.Count() == 1 ? id : id.Replace("_" + idvalues.LastOrDefault(), "");
            if (_id.StartsWith(ID_PRELIM_VALUATIONS))
                _id = ID_PRELIM_VALUATIONS;
            //check if we're rendering the root node's children
            switch (_id)
            {
                case ID_ROOT:
                    {
                        nodes.Add(CreateTreeNode(ID_ANNUAL_INPUTBATCH, id, queryStrings, "Annual Input Batches", "icon-calendar-alt", true, $"EMAdmin/EMAdminTree/InputBatches/1"));
                        nodes.Add(CreateTreeNode(ID_MONTHLY_INPUTBATCH, id, queryStrings, "Monthly Input Batches", "icon-calendar", true, $"EMAdmin/EMAdminTree/InputBatches/2"));

                        nodes.Add(CreateTreeNode(ID_PRELIM_VALUATIONS, id, queryStrings, "Pre-Valuations", "icon-thumbnail-list", true, $"EMAdmin/EMAdminTree/prevaluations/0"));


                        break;
                    }
                case ID_ANNUAL_INPUTBATCH:
                    {
                        if (idvalues.Count() == 1)
                        {
                            var inputBatches = GetData<IEnumerable<InputBatch>>($"umbraco/api/EMAdminBackoffice/GetInputBatches?includeInactive=true&batchTypeId={(_id == ID_ANNUAL_INPUTBATCH ? 1 : 2)}");
                            foreach (var ib in inputBatches)
                            {
                                nodes.Add(CreateTreeNode($"{id}_{ib.Id}", id, queryStrings, ib.Description, "icon-calendar-alt", true, $"EMAdmin/EMAdminTree/InputBatch/{ib.Id}"));
                            }
                        }
                        else
                        {
                            var inputBatchId = idvalues.LastOrDefault();
                            nodes.Add(CreateTreeNode(ID_MOODYSRATINGS, id, queryStrings, "Moody's ratings", "icon-company", false, $"EMAdmin/EMAdminTree/moodysratings/{id}_{inputBatchId}"));
                            nodes.Add(CreateTreeNode(ID_CORPORATEBONDSPREADS, id, queryStrings, "Corporate bond spreads", "icon-globe-alt", false, $"EMAdmin/EMAdminTree/corporatebondspreads/{id}_{inputBatchId}"));
                        }
                        break;
                    }
                case ID_MONTHLY_INPUTBATCH:
                    {
                        if (idvalues.Count() == 1)
                        {
                            var inputBatches = GetData<IEnumerable<InputBatch>>($"umbraco/api/EMAdminBackoffice/GetInputBatches?includeInactive=true&batchTypeId={(_id == ID_ANNUAL_INPUTBATCH ? 1 : 2)}");
                            foreach (var ib in inputBatches)
                            {
                                nodes.Add(CreateTreeNode($"{id}_{ib.Id}", id, queryStrings, ib.Description, "icon-calendar", true, $"EMAdmin/EMAdminTree/InputBatch/{ib.Id}"));
                            }
                        }
                        else
                        {
                            var inputBatchId = idvalues.LastOrDefault();
                            nodes.Add(CreateTreeNode(ID_WACC_INPUTS_SUMMARY, id, queryStrings, "Wacc inputs summary", "icon-globe-alt", false, $"EMAdmin/EMAdminTree/waccinputssummary/{id}_{inputBatchId}"));
                            nodes.Add(CreateTreeNode(ID_CURRENCYRISKFREERATE, id, queryStrings, "Currency risk-free rate", "icon-globe-alt", false, $"EMAdmin/EMAdminTree/currencyriskfreerate/{inputBatchId}"));
                            nodes.Add(CreateTreeNode(ID_TRBC_MAP, id, queryStrings, "TRBC map", "icon-thumbnail-list", false, $"EMAdmin/EMAdminTree/trbcmap/{id}_{inputBatchId}"));//todo - is this mothly or annually?
                            nodes.Add(CreateTreeNode(ID_COUNTRYMOODYSRATING, id, queryStrings, "Country moody's rating", "icon-globe-alt", false, $"EMAdmin/EMAdminTree/countrymoodysrating/{id}_{inputBatchId}"));
                            nodes.Add(CreateTreeNode(ID_COUNTRYTAXRATE, id, queryStrings, "Country tax rate", "icon-globe-alt", false, $"EMAdmin/EMAdminTree/countrytaxrate/{id}_{inputBatchId}"));
                        }
                        break;
                    }
                case ID_PRELIM_VALUATIONS:
                    {
                        var idGroupvalues = id.Split('_');
                        var _year = (idGroupvalues.Count() >= 2) ? idGroupvalues.Skip(1).FirstOrDefault() : null;
                        string _month = (idGroupvalues.Count() >= 3) ? idGroupvalues.Skip(2).FirstOrDefault() : null;
                        string _day = (idGroupvalues.Count() >= 4) ? idGroupvalues.Skip(3).FirstOrDefault() : null;

                        if (string.IsNullOrEmpty(_year))
                        {
                            var prelimEvaluationGroups = GetData<IEnumerable<PrelimEvaluationGroupModel>>($"umbraco/api/EMAdminBackoffice/GetPrelimEvaluationGroups");
                            foreach (var year in prelimEvaluationGroups.Select(o => o.Year).Distinct())
                            {
                                var yearNode = CreateTreeNode($"{id}_{year}", id, queryStrings, year.ToString(), "icon-bulleted-list", true, $"EMAdmin/EMAdminTree/EMAdmin/{year}");
                                nodes.Add(yearNode);
                                //foreach (var month in prelimEvaluationGroups.Where(o => o.Year == year).Select(o => o.Month).Distinct())
                                //{
                                //    var monthNode = CreateTreeNode($"{id}_{year}_{month}", yearNode.Id.ToString(), queryStrings, month.ToString(), "icon-books", true, $"EMAdmin/EMAdminTree/prevaluations/{year}_{month}");
                                //    nodes.Add(monthNode);
                                //}

                            }
                        }
                        else if (string.IsNullOrEmpty(_month))
                        {

                            var prelimEvaluationGroups = GetData<IEnumerable<PrelimEvaluationGroupModel>>($"umbraco/api/EMAdminBackoffice/GetPrelimEvaluationGroups?year={_year}");
                            foreach (var year in prelimEvaluationGroups.Select(o => o.Year).Distinct())
                            {
                                foreach (var month in prelimEvaluationGroups.Where(o => o.Year == year && o.Month.HasValue).Select(o => o.Month.Value).Distinct())
                                {
                                    var monthNode = CreateTreeNode($"{id}_{month}", id, queryStrings, new DateTime(year, month, 1).ToString("MMM"), "icon-books", true, $"EMAdmin/EMAdminTree/EMAdmin/{year}_{month}");
                                    nodes.Add(monthNode);
                                }
                            }


                        }

                        else if (string.IsNullOrEmpty(_day))
                        {
                            var prelimEvaluationGroups = GetData<IEnumerable<PrelimEvaluationGroupModel>>($"umbraco/api/EMAdminBackoffice/GetPrelimEvaluationGroups?year={_year}&month={_month}");
                            foreach (var year in prelimEvaluationGroups.Select(o => o.Year).Distinct())
                            {
                                foreach (var month in prelimEvaluationGroups.Where(o => o.Year == year).Select(o => o.Month).Distinct())
                                {
                                    foreach (var day in prelimEvaluationGroups.Where(o => o.Year == year && o.Month == month && o.Day.HasValue).Select(o => o.Day.Value).Distinct())
                                    {
                                        var dayNode = CreateTreeNode($"{id}_{day}", id, queryStrings, day.ToString(), "icon-books", true, $"EMAdmin/EMAdminTree/prevaluations/{year}_{month}_{day}");
                                        nodes.Add(dayNode);
                                    }
                                }
                            }


                        }

                    }
                    break;
                default:
                    break;
            }

            return nodes;
            //this tree doesn't suport rendering more than 1 level
            throw new NotSupportedException();

        }

        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            var menu = new MenuItemCollection();
            //menu.AddMenuItem(new MenuItem("create", "Create"));
            //menu.DefaultMenuAlias = ActionNew.Instance.Alias;
            //menu.Items.Add<ActionNew>("Create");
            return menu;
        }
    }
}
