﻿using EquityMaven.Data.Entities;
using HiQPdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Umb.Custom.Helpers
{
    public class ValuationReportRender
    {
        public static byte[] RenderReport(string razorViewFilePath, string hiQPDFSerialNumber = null)
        {

            string htmlTemplateMerged = string.Empty;
            PrelimEvaluation prelimEvaluation = null;
            byte[] pdfBuffer = null;
            try
            {
                htmlTemplateMerged = EquityMaven.Umb.Custom.Helpers.RazorHelper.ParseWithRazorView<PrelimEvaluation>(prelimEvaluation, razorViewFilePath);
                 
                {
                    {
                        HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
                        htmlToPdfConverter.SerialNumber = !string.IsNullOrEmpty(hiQPDFSerialNumber) ? hiQPDFSerialNumber : EquityMaven.Tools.SettingsHelper.GetSetting<string>("HiQPDFSerialNumber", null);
                        // render the HTML code as PDF in memory
                        RetryFromHere:
                        try
                        {
                            pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(htmlTemplateMerged, null);

                        }
                        catch (HiQPdfException exception)
                        {
                            //if the license is invalid, try to fall 

                            if (!string.IsNullOrEmpty(htmlToPdfConverter.SerialNumber) && EquityMaven.Tools.SettingsHelper.GetSetting<bool>("HiQPDFSerialErrorTryContinueWithEvaluation", false))
                            {
                                htmlToPdfConverter.SerialNumber = null;
                                goto RetryFromHere;

                            }
                            else
                            {
                                Umbraco.Core.Logging.LogHelper.Error<ValuationReportRender>(string.Format($"There was a problem rendering the rerport, invalid HiQPDFSerial"), exception);
                                return null;
                            }
                        }
                    }
                    
                    
                }
                 
            }
            catch (Exception exception2)
            {

                Umbraco.Core.Logging.LogHelper.Error<ValuationReportRender>(string.Format($"There was a problem rendering the rerport"), exception2);
                return null;
            }

            return pdfBuffer;
        }
    }
}
