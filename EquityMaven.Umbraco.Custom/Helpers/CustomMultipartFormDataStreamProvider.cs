﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Umb.Custom.Helpers
{
    //thanks to https://our.umbraco.org/member/133614
    //https://our.umbraco.org/forum/extending-umbraco-and-using-the-api/81065-file-upload-in-backoffice-custom-section
    public class CustomMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
    {
        public CustomMultipartFormDataStreamProvider(string path) : base(path) { }

        public override string GetLocalFileName(HttpContentHeaders headers)
        {
            return headers.ContentDisposition.FileName.Replace("\"", string.Empty);
        }
    }
}
