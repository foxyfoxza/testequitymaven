﻿using EquityMaven.Umb.Custom.Controllers;
using HiQPdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core.Logging;

namespace EquityMaven.Umb.Custom.Helpers
{
    
    public static class RazorHelper
    {
        public static void ParseWithRazorViewToPDF<T>(this T model, string razorViewFilePath)
        {
            var htmlTemplateMerged = ParseWithRazorView(model, razorViewFilePath);
            string HiQPDFSerialNumber = null;
            var AttachmentName = "test.pdf";
            try
            {

                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.Body = "message body";
                    mailMessage.From = new MailAddress("russell@reynardit.com");
                    mailMessage.Subject = "subject";
                    mailMessage.IsBodyHtml = true;

                    mailMessage.To.Add("russell@reynardit.com");
                    List<Stream> streams = new List<Stream>();

                    {
                        byte[] pdfBuffer = null;
                        HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
                        htmlToPdfConverter.SerialNumber = !string.IsNullOrEmpty(HiQPDFSerialNumber) ? HiQPDFSerialNumber : EquityMaven.Tools.SettingsHelper.GetSetting<string>("HiQPDFSerialNumber", null);
                        htmlToPdfConverter.TriggerMode = ConversionTriggerMode.WaitTime;
                        htmlToPdfConverter.WaitBeforeConvert = 2;
                        
                        htmlToPdfConverter.Document.PageSize= PdfPageSize.A4;
                        htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Landscape;
                        htmlToPdfConverter.Document.FitPageWidth = true;

                        // render the HTML code as PDF in memory
                        RetryFromHere:
                        try
                        {
                            var baseUrl = "http://testequitymaven.local.reynardit.com/";
                            pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(htmlTemplateMerged, baseUrl);

                        }
                        catch (HiQPdfException exception)
                        {
                            //if the license is invalid, try to fall 

                            if (!string.IsNullOrEmpty(htmlToPdfConverter.SerialNumber) && EquityMaven.Tools.SettingsHelper.GetSetting<bool>("HiQPDFSerialErrorTryContinueWithEvaluation", false))
                            {
                                htmlToPdfConverter.SerialNumber = null;
                                goto RetryFromHere;

                            }
                            else
                            {
                                Umbraco.Core.Logging.LogHelper.Error<T> (string.Format($"There was a problem sending an email to '{"abc"}' from Workflow -  invalid HiQPDFSerial"), exception);
                                 
                            }
                        }

                        try
                        {
                            var ms = new MemoryStream();
                            streams.Add(ms);
                            ms.Write(pdfBuffer, 0, pdfBuffer.Length);
                            ms.Seek(0, SeekOrigin.Begin);
                            System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Application.Pdf);
                            System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(ms, ct);
                            attach.ContentDisposition.FileName = AttachmentName;
                            mailMessage.Attachments.Add(attach);
                        }
                        catch (Exception exception)
                        {
                            Umbraco.Core.Logging.LogHelper.Error<T>(string.Format($"There was a problem sending an email to '{"abc"}'  - could not add attachment"), exception);
                        }
                    }
                    using (SmtpClient smtpClient = new SmtpClient())
                    {
                        smtpClient.Send(mailMessage);
                    }
                    foreach (Stream stream1 in streams)
                    {
                        stream1.Dispose();
                    }
                }
            }
            catch (Exception exception2)
            {
                Exception exception1 = exception2;
                LogHelper.Error<T>($"There was a problem sending an email", exception1);
            }
        }
        public static string ParseWithRazorView<T>(this T model, string razorViewFilePath)
        {
            //Umbraco.Web.Mvc.ControllerContextExtensions.GetUmbracoContext(razorViewFilePath);
            var umbracoContext = Umbraco.Core.ApplicationContext.Current;
            string str;
            if (string.IsNullOrWhiteSpace(razorViewFilePath))
            {
                ArgumentNullException argumentNullException = new ArgumentNullException("razorViewFilePath");
                LogHelper.Error<T>("razorFilePath cannot be null or empty", argumentNullException);
                throw argumentNullException;
            }

            //if (this.RecordFields.Values.Any<RecordField>((RecordField x) => string.IsNullOrWhiteSpace(x.Alias)))
            //{
            //    InvalidOperationException invalidOperationException = new InvalidOperationException("An alias without a value was in the recordfields. This should be fixed.");
            //    LogHelper.Error<Record>("razorFilePath cannot be null or empty", invalidOperationException);
            //    throw invalidOperationException;
            //}

            //FormFieldHtmlModel[] array = (
            //    from recordField in this.RecordFields
            //    select new FormFieldHtmlModel(recordField.Value.Alias)
            //    {
            //        Name = recordField.Value.Field.Caption,
            //        FieldType = recordField.Value.Field.FieldType.FieldTypeViewName,
            //        FieldValue = (recordField.Value == null || !recordField.Value.HasValue() ? new object[] { string.Empty } : recordField.Value.Values.ToArray())
            //    }).ToArray<FormFieldHtmlModel>();
            //FormsHtmlModel formsHtmlModel = new FormsHtmlModel(array);
            if (HttpContext.Current == null || umbracoContext == null)
            {
                NullReferenceException nullReferenceException = new NullReferenceException("Context is not available to parse the record");
                LogHelper.Error<T>("Context is not available to parse the record", nullReferenceException);
                throw nullReferenceException;
            }
            HttpContextWrapper httpContextWrapper = new HttpContextWrapper(HttpContext.Current);
            var current = umbracoContext;
            RouteData routeDatum = new RouteData();
            routeDatum.Values.Add("controller", "RazorTemplate");
            routeDatum.Values.Add("action", "Index");
            routeDatum.DataTokens.Add("umbraco-context", current);
            RequestContext requestContext = new RequestContext(httpContextWrapper, routeDatum);
            using (RazorTemplateController razorTemplateController = new RazorTemplateController())
            {
                razorTemplateController.ControllerContext = new ControllerContext(requestContext, razorTemplateController);
                try
                {
                    str = RenderViewToString(razorTemplateController, razorViewFilePath, model, true);
                }
                catch (Exception exception)
                {
                    LogHelper.Error<T>("Error parsing record with Razor view", exception);
                    throw;
                }
            }
            return str;
        }


        /// <summary>
        /// Renders the partial view to string.
        /// </summary>
        /// <param name="controller">The controller context.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="model">The model.</param>
        /// <param name="isPartial">true if it is a Partial view, otherwise false for a normal view </param>
        /// <returns></returns>
        internal static string RenderViewToString(this ControllerBase controller, string viewName, object model, bool isPartial = false)
        {
            string str;
            if (controller.ControllerContext == null)
            {
                throw new ArgumentException("The controller must have an assigned ControllerContext to execute this method.");
            }
            controller.ViewData.Model = model;
            using (StringWriter stringWriter = new StringWriter())
            {
                ViewEngineResult viewEngineResult = (!isPartial ? ViewEngines.Engines.FindView(controller.ControllerContext, viewName, null) : ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName));
                if (viewEngineResult.View == null)
                {
                    throw new InvalidOperationException(string.Concat("No view could be found by name ", viewName));
                }
                ViewContext viewContext = new ViewContext(controller.ControllerContext, viewEngineResult.View, controller.ViewData, controller.TempData, stringWriter);
                viewEngineResult.View.Render(viewContext, stringWriter);
                viewEngineResult.ViewEngine.ReleaseView(controller.ControllerContext, viewEngineResult.View);
                str = stringWriter.GetStringBuilder().ToString();
            }
            return str;
        }
    }
}
