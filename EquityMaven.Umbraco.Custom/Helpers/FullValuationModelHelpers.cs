﻿using EquityMaven.Data.HelperEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Forms.Mvc.Models;
using Umbraco.Forms.Core;
using EquityMaven.Data;
using EquityMaven.CommonEnums;
using EquityMaven.Data.Entities;

namespace EquityMaven.Umb.Custom.Helpers
{
    public static class FullValuationModelHelpers
    {

        public static FullValuationModel ReadFormFieldViewModel(this FormViewModel form, FieldViewModel field)
        {
            FullValuationModel result = null;
            try
            {
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<FullValuationModel>(field.Value);
            }
            catch (Exception ex)
            {
                result = new FullValuationModel();
            }

            result.ReadForm(form);
            return result;
        }
        public static FullValuationModel ReadFormFieldViewModel(this Record record)
        {

            FullValuationModel result = null;
            try
            {
                var incomeStatement = record.GetRecordFieldByAlias("incomeStatement").Values.FirstOrDefault(o => !string.IsNullOrEmpty(o.ToString()));
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<FullValuationModel>(incomeStatement?.ToString());
            }
            catch (Exception ex)
            {
                result = new FullValuationModel();
            }

            return result;


            return result;
        }


        private static void ReadForm(this FullValuationModel result, FormViewModel form)
        {
            FieldViewModel fldLastFinancialYearEnd = FindFieldWithCaption(form, "Last financial year end");
            FieldViewModel fldCurrentCashOnHand = FindFieldWithCaption(form, "Current cash on hand as at last month-end");

            FieldViewModel fldDateOfCommencement = FindFieldWithCaption(form, "Date of commencing business operations");
            FieldViewModel fldNumberOfEmployees = FindFieldWithCaption(form, "Number of employees");
            FieldViewModel fldCurrentInterestBearingDebt = FindFieldWithCaption(form, "All 3rd party interest-bearing debt as at last month-end (LT+ST)");
                   FieldViewModel fldStageOfDevelopment = FindFieldWithCaption(form, "Stage of Development");
       
            DateTime dateOfCommencement;
            if (DateTime.TryParse(fldDateOfCommencement?.Value, out dateOfCommencement))
            {
                result.DateOfCommencement = dateOfCommencement;
            }

            int numberOfEmployees;
            if (int.TryParse(fldNumberOfEmployees?.Value, out numberOfEmployees))
            {
                result.NumberOfEmployees = numberOfEmployees;
            }

            decimal currentInterestBearingDebt;
            if (decimal.TryParse(fldCurrentInterestBearingDebt?.Value, out currentInterestBearingDebt))
            {
                result.CurrentInterestBearingDebt = currentInterestBearingDebt;
            }

            result.ValuationDate = DateTime.UtcNow;
           
             
            result.StageOfDevelopment = StageOfDevelopmentHelper.GetStageOfDevelopment(fldStageOfDevelopment.Value);
 

            DateTime lastFinancialYearEnd;
            if (DateTime.TryParse(fldLastFinancialYearEnd.Value, out lastFinancialYearEnd))
            {
                result.LastFinancialYearEnd = lastFinancialYearEnd;

            }
            result.SetFullValuationClientInputDefaults();



            decimal currentCashOnHand;
            if (decimal.TryParse(fldCurrentCashOnHand.Value, out currentCashOnHand))
            {
                result.CurrentCashOnHand = currentCashOnHand;
 
            }
        }


        private static void SetFullValuationClientInputs(this FullValuationModel fvm)
        {
            fvm.InputsActualPrior =  fvm.InputsActualPrior ?? new FullValuationInputYear(FinancialYearType.ActualPrior)
            {
                
            };

            fvm.InputsActualCurrent = fvm.InputsActualCurrent ?? new FullValuationInputYear(FinancialYearType.ActualCurrent)
            {
                
            };

            fvm.InputsBudget  = fvm.InputsBudget ?? new FullValuationInputYear(FinancialYearType.Budget)
            {
               

            };
            fvm.InputsForecast1  = fvm.InputsForecast1 ?? new FullValuationInputYear(FinancialYearType.Forecast1)
            {
                

            };

            fvm.InputsForecast2  = fvm.InputsForecast2 ?? new FullValuationInputYear(FinancialYearType.Forecast2)
            {
            
                 


            };
        }
        private static void SetFullValuationClientInputDefaults(this FullValuationModel fvm)
        {
            fvm.InputsActualPrior = fvm.InputsActualPrior ?? new FullValuationInputYear(FinancialYearType.ActualPrior)
            {
                IncomeStatement_Revenue = 23636936,
                IncomeStatement_CostOfGoodsSold = 1305107,
                IncomeStatement_OperatingExpense = 19586949,
                IncomeStatement_DepreciationAndAmortisationExpense = 354554,
                IncomeStatement_InterestExpense = 33582,
                IncomeStatement_NonCashExpense = 1664284,
                BalanceSheetTotalAssets_AccountsReceiveable = 1711697,
                BalanceSheetTotalAssets_Inventory = 0,
                BalanceSheetTotalAssets_OtherCurrentAssets = 383503,
                BalanceSheetTotalAssets_CashAndCashEquivalents = 1317910,
                BalanceSheetTotalAssets_FixedAssets = 1060328,
                BalanceSheetEquityAndLiabilities_AccountsPayable = 606519,
            };

            fvm.InputsActualCurrent = fvm.InputsActualCurrent ?? new FullValuationInputYear(FinancialYearType.ActualCurrent)
            {
                IncomeStatement_Revenue = 29884199,
                IncomeStatement_CostOfGoodsSold = 1814682,
                IncomeStatement_OperatingExpense = 24339108,
                IncomeStatement_DepreciationAndAmortisationExpense = 448262,
                IncomeStatement_InterestExpense = 42598,
                IncomeStatement_NonCashExpense = 970513,

                BalanceSheetTotalAssets_AccountsReceiveable = 4045036,
                BalanceSheetTotalAssets_Inventory = 0,
                BalanceSheetTotalAssets_OtherCurrentAssets = 400897,
                BalanceSheetTotalAssets_CashAndCashEquivalents = 1089894,
                BalanceSheetTotalAssets_FixedAssets = 1748926,
                BalanceSheetEquityAndLiabilities_AccountsPayable = 437226,
                SustainableEBITDA_NonRecurringExpenses = 0,
                SustainableEBITDA_NonRecurringIncome = 0,
                //ExcessCash_RequiredPercentageOperatingCash = (decimal)0.02,
            };

            fvm.InputsBudget = fvm.InputsBudget ?? new FullValuationInputYear(FinancialYearType.Budget)
            {
                IncomeStatementAnalysis_PercentageRevenueGrowth = (decimal)0.17,
                IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal).94,
                IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal)0.82,
                IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = fvm.InputsActualPrior.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue,
                IncomeStatement_InterestExpense = 42598,
                IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = fvm.InputsActualPrior.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = fvm.InputsActualPrior.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = 0,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = fvm.InputsActualPrior.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = fvm.InputsActualPrior.
                    BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold,
                CapitalExpenditure_PercentageForecastCapitalExpenditure = (decimal).05,
                CapitalExpenditure_CapitalExpenditure = 1748226,
                SustainableEBITDA_NonRecurringExpenses = 0,
                SustainableEBITDA_NonRecurringIncome = 0,

            };
            fvm.InputsForecast1 = fvm.InputsForecast1 ?? new FullValuationInputYear(FinancialYearType.Forecast1)
            {
                IncomeStatementAnalysis_PercentageRevenueGrowth = (decimal)0.15,
                IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal).94,
                IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal)0.825,
                IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = fvm.InputsBudget.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue,
                IncomeStatement_InterestExpense = 42598,
                IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = fvm.InputsBudget.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = fvm.InputsBudget.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = 0,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = fvm.InputsBudget.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold =
                    fvm.InputsBudget.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold,
                CapitalExpenditure_PercentageForecastCapitalExpenditure = (decimal).03,
                CapitalExpenditure_CapitalExpenditure = 1206276,

            };

            fvm.InputsForecast2 = fvm.InputsForecast2 ?? new FullValuationInputYear(FinancialYearType.Forecast2)
            {

                IncomeStatementAnalysis_PercentageRevenueGrowth = (decimal)0.12,
                IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal).94,
                IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal)0.825,
                IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = fvm.InputsForecast1.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue,
                IncomeStatement_InterestExpense = 42598,
                IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = fvm.InputsForecast1.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = fvm.InputsForecast1.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = 0,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = fvm.InputsForecast1.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold =
                    fvm.InputsForecast1.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold,
                CapitalExpenditure_PercentageForecastCapitalExpenditure = (decimal).01,
                CapitalExpenditure_CapitalExpenditure = 450343,


            };
        }

        public static FullValuationModel ReadFormFieldViewModel(this FormViewModel form)
        {
            var result = new FullValuationModel();
            result.ReadForm(form);
            return result;
        }

        private static FieldViewModel FindFieldWithCaption(FormViewModel form, string caption)
        {
            foreach (var page in form.Pages)
            {
                foreach (var fs in page.Fieldsets)
                {
                    foreach (var con in fs.Containers)
                    {
                        foreach (var field in con.Fields)
                        {
                            if (field.Caption == caption)
                            {
                                return field;

                            }
                        }
                    }
                }
            }
            return null;
        }


    }
}