﻿using EquityMaven.Data;
using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Attributes;
using Umbraco.Forms.Core.Enums;
using Umbraco.Core.Logging;
using System.Net.Mail;
using Umbraco.Core.Configuration;
using Umbraco.Core.IO;
using HiQPdf;
using System.Web.Security;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using EquityMaven.Umb.Custom.Models;
using EquityMaven.Interfaces;
using EquityMaven.Interfaces.ClientInputs;

namespace EquityMaven.Umb.Custom
{
    public class EquityMavenPrelimValuationWorkFlow : WorkflowType
    {
        private static System.Configuration.AppSettingsReader SettingsReader = new System.Configuration.AppSettingsReader();
        const string ACTION_SUBMITTOAPI = "Post to API";
        const string ACTION_ATTACHTOEMAIL = "Email Template";
        const string ACTION_BOTH = "Both";

        [Setting("Action", prevalues = ACTION_SUBMITTOAPI + "," + ACTION_ATTACHTOEMAIL + "," + ACTION_BOTH, description = "What to with data", view = "Dropdownlist")]
        public string Action
        {
            get;
            set;
        }

        //[Setting("Url", description = "URL to Post data to", view = "TextField")]
        public string Url
        {
            get
            {
                switch (Action)
                {
                    case ACTION_SUBMITTOAPI:
                    case ACTION_BOTH:
                        return $"Http://{System.Web.HttpContext.Current.Request.Url.Host}{"/umbraco/api/PrelimEvaluation/SavePrelimEvaluation"}";
                    default:
                        return null;
                }
            }
        }
        [Setting("Email", description = "Enter the receiver email", view = "TextField")]
        public string Email
        {
            get;
            set;
        }

        [Setting("Template", description = "Enter the template URL", view = "TextField")]
        public string Template
        {
            get;
            set;
        }


        [Setting("HiQPDFSerialNumber", description = "Enter the HiQPDF Serial Number (if blank it will use the settings from web.config)", view = "TextField")]
        public string HiQPDFSerialNumber
        {
            get;
            set;
        }


        [Setting("SenderEmail", description = "Enter the sender email (if blank it will use the settings from /config/umbracosettings.config)", view = "TextField")]
        public string SenderEmail
        {
            get;
            set;
        }

        [Setting("Subject", description = "Enter the subject", view = "TextField")]
        public string Subject
        {
            get;
            set;
        }

        [Setting("AttachmentName", description = "Enter the name of the email attachment", view = "TextField")]
        public string AttachmentName
        {
            get;
            set;
        }

        [Setting("Message", description = "Enter the intro message", view = "TextArea")]
        public string Message
        {
            get;
            set;
        }


        public EquityMavenPrelimValuationWorkFlow()
        {
            base.Id = new Guid("1CA476C0-141C-4824-A7BD-8BB7716C799A");
            base.Name = "EM Preliminary Valuation";
            base.Description = "Submit Preliminary Evaluation Data";
            base.Icon = "icon-message";
            base.Group = "Email";
        }

         

        public override WorkflowExecutionStatus Execute(Record record, RecordEventArgs e)
        {
            WorkflowExecutionStatus workflowExecutionStatus;
            string htmlTemplateMerged = string.Empty;

            if (record == null)
            {
                LogHelper.Error<EquityMavenPrelimValuationWorkFlow>("Record is null", new ArgumentNullException("record"));
                return WorkflowExecutionStatus.Failed;
            }
            List<Exception> exceptions = this.ValidateSettings();
            if (exceptions != null && exceptions.Any<Exception>())
            {
                foreach (Exception exception in exceptions)
                {
                    LogHelper.Error<EquityMavenPrelimValuationWorkFlow>(exception.Message, exception);
                }
                return WorkflowExecutionStatus.Failed;
            }
              
            var emailAddress = record.GetValue<string>("emailAddress");
            var companyName = record.GetValue<string>("companyName");
            var currency = record.GetValue<string>("currency");

            
            PrelimEvaluation prelimEvaluation = null;
            try
            {
                if (!string.IsNullOrEmpty(Url))
                {
                    IPrelimEval prelimEval;
                    IPrelimExcessCash prelimExcessCash;
                    IPrelimIncomeStatement prelimIncomeStatement;

                    try
                    {
                        prelimEval = new PrelimEval(record);
                        prelimExcessCash = new PrelimExcessCash(record);
                        prelimIncomeStatement = new PrelimIncomeStatement(record);
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Error<EquityMavenPrelimValuationWorkFlow>("could not transfor record", ex);
                        return WorkflowExecutionStatus.Failed;
                    }
                    var value = new PrelimEvaluationModel(prelimEval, prelimIncomeStatement, prelimExcessCash );
                    value.Email = emailAddress;
                    value.CompanyName = companyName;
                    //value.SourceId = record.UniqueId;
                    value.ISOCurrencySymbol = currency;
                    PostData(value, this.Url, out prelimEvaluation);

                    try
                    {
                        var prelimEvaluationId = record.GetRecordFieldByAlias("prelimEvaluationId");
                        prelimEvaluationId.Values.Clear();
                        prelimEvaluationId.Values.Add(prelimEvaluation.Id.ToString());
                    }
                    catch
                    {
                        //dont do anyting
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error<EquityMavenPrelimValuationWorkFlow>($"could not Post Data to {this.Url}", ex);
                return WorkflowExecutionStatus.Failed;
            }

            try
            {
                htmlTemplateMerged = EquityMaven.Umb.Custom.Helpers.RazorHelper.ParseWithRazorView<PrelimEvaluation>(prelimEvaluation, "~/Views/Partials/Forms/Themes/EMPreValuation/Report.cshtml");
                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.Body = Message;
                    mailMessage.From = new MailAddress((string.IsNullOrEmpty(this.SenderEmail) ? UmbracoConfig.For.UmbracoSettings().Content.NotificationEmailAddress : this.SenderEmail));
                    mailMessage.Subject = this.Subject;
                    mailMessage.IsBodyHtml = true;
                    string[] strArrays = this.Email.Split(new char[] { ';' });
                    for (int i = 0; i < (int)strArrays.Length; i++)
                    {
                        string str1 = strArrays[i];
                        mailMessage.To.Add(str1.Trim());
                    }
                    List<Stream> streams = new List<Stream>();

                    {
                        byte[] pdfBuffer = null;


                        HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
                        htmlToPdfConverter.SerialNumber = !string.IsNullOrEmpty(HiQPDFSerialNumber) ? HiQPDFSerialNumber : EquityMaven.Tools.SettingsHelper.GetSetting<string>("HiQPDFSerialNumber", null);
                        // render the HTML code as PDF in memory
                        RetryFromHere:
                        try
                        {

                            pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(htmlTemplateMerged, null);

                        }
                        catch (HiQPdfException exception)
                        {
                            //if the license is invalid, try to fall 

                            if (!string.IsNullOrEmpty(htmlToPdfConverter.SerialNumber) && EquityMaven.Tools.SettingsHelper.GetSetting<bool>("HiQPDFSerialErrorTryContinueWithEvaluation", false))
                            {
                                htmlToPdfConverter.SerialNumber = null;
                                goto RetryFromHere;

                            }
                            else
                            {
                                Umbraco.Core.Logging.LogHelper.Error<EquityMavenPrelimValuationWorkFlow>(string.Format($"There was a problem sending an email to '{this.Email}' from Workflow for Form '{e.Form.Name}' with id '{e.Form.Id}' for Record with unique id '{record.UniqueId }' invalid HiQPDFSerial"), exception);
                                return WorkflowExecutionStatus.Failed;
                            }
                        }

                        try
                        {
                            var ms = new MemoryStream();
                            streams.Add(ms);
                            ms.Write(pdfBuffer, 0, pdfBuffer.Length);
                            ms.Seek(0, SeekOrigin.Begin);
                            System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Application.Pdf);
                            System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(ms, ct);
                            attach.ContentDisposition.FileName = AttachmentName;
                            mailMessage.Attachments.Add(attach);
                        }
                        catch (Exception exception)
                        {
                            Umbraco.Core.Logging.LogHelper.Error<EquityMavenPrelimValuationWorkFlow>(string.Format($"There was a problem sending an email to '{this.Email}' from Workflow for Form '{e.Form.Name}' with id '{e.Form.Id}' for Record with unique id '{record.UniqueId }' could not add attachment"), exception);
                            return WorkflowExecutionStatus.Failed;
                        }


                    }
                    using (SmtpClient smtpClient = new SmtpClient())
                    {
                        smtpClient.Send(mailMessage);
                    }
                    foreach (Stream stream1 in streams)
                    {
                        stream1.Dispose();
                    }
                }
                workflowExecutionStatus = WorkflowExecutionStatus.Completed;
            }
            catch (Exception exception2)
            {
                Exception exception1 = exception2;
                object[] email = new object[] { this.Email, e.Form.Name, e.Form.Id, record.UniqueId };
                LogHelper.Error<EquityMavenPrelimValuationWorkFlow>(string.Format("There was a problem sending an email to '{0}' from Workflow for Form '{1}' with id '{2}' for Record with unique id '{3}'", email), exception1);
                workflowExecutionStatus = WorkflowExecutionStatus.Failed;
            }



            workflowExecutionStatus = WorkflowExecutionStatus.Failed;
            return workflowExecutionStatus;
        }

        /// <summary>
        /// Converts the provided app-relative path into an absolute Url containing the 
        /// full host name
        /// </summary>
        /// <param name="relativeUrl">App-Relative path</param>
        /// <returns>Provided relativeUrl parameter as fully qualified Url</returns>
        /// <example>~/path/to/foo to http://www.web.com/path/to/foo</example>
        private static string ToAbsoluteUrl(string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (HttpContext.Current == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }

        public override List<Exception> ValidateSettings()
        {
            List<Exception> exceptions = new List<Exception>();



            //if (string.IsNullOrEmpty(this.Email) && string.IsNullOrEmpty(this.UserEmailField))
            //{
            //    exceptions.Add(new Exception("neither 'Email' nor 'User Email' setting has been set"));
            //}
            if (string.IsNullOrEmpty(this.Template))
            {
                exceptions.Add(new Exception("'Template' setting has not been set'"));
            }

            //if (string.IsNullOrEmpty(this.AttachmentName))
            //{
            //    exceptions.Add(new Exception("'AttachmentName' setting has not been set'"));
            //}
            //if (string.IsNullOrEmpty(this.Message))
            //{
            //    exceptions.Add(new Exception("'Message' setting has not been set'"));
            //}

            //string templatePath = "";
            //try
            //{
            //    templatePath = HttpContext.Current.Server.MapPath(this.Template);
            //    if (!System.IO.File.Exists(templatePath))
            //    {
            //        exceptions.Add(new Exception($"'Template' cannot be fould at {templatePath}'"));
            //    }
            //}
            //catch (Exception ex)
            //{
            //    exceptions.Add(ex);
            //} 
            return exceptions;


        }



        public void PostData<TResult, T>(T data, string url, out TResult returned)
        {
            using (var client = new HttpClient())
            {
                //string value = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                var response = client.PostAsJsonAsync(url, data).Result;
                if (!response.IsSuccessStatusCode)
                    throw new Exception(response.ReasonPhrase);
                var responseString = response.Content.ReadAsStringAsync().Result;
                returned = Newtonsoft.Json.JsonConvert.DeserializeObject<TResult>(responseString);
            }
        }

        public void TestPost(Dictionary<string, string> values, string url)
        {
            var client = new HttpClient();



            var content = new FormUrlEncodedContent(values);
            var response = client.PostAsJsonAsync(url, content).Result;
            if (!response.IsSuccessStatusCode)
                throw new Exception(response.ReasonPhrase);
            var responseString = response.Content.ReadAsStringAsync();
        }

    }
}
