﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Attributes;
using Umbraco.Forms.Core.Common;


namespace EquityMaven.Umb.Custom.Providers
{

//    [Serializable]
//    public class MyCustomField : FieldType
//{
//    public MyCustomField()
//    {
//        this.Id = new Guid("08b8057f-06c9-4ca5-8a42-fd1fc2a46eff"); // Replace this!
//        this.Name = "My Custom Field";
//        this.Description = "Render a custom text field.";
//        this.Icon = "icon-autofill";
//        this.DataType = FieldDataType.String;
//        this.SortOrder = 10;
//        this.SupportsRegex = true;
//    }

//    // You can do custom validation in here which will occur when the form is submitted.
//    // Any strings returned will cause the submit to be invalid!
//    // Where as returning an empty ienumerable of strings will say that it's okay.
//    public override IEnumerable<string> ValidateField(Form form, Field field, IEnumerable<object> postedValues, HttpContextBase context)
//    {
//        var returnStrings = new List<string>();
    
//        if (!postedValues.Any(value => value.ToString().ToLower().Contains("custom"))) {
//            returnStrings.Add("You need to include 'custom' in the field!");
//        }
        
//        // Also validate it against the original default method.
//        returnStrings.AddRange(base.ValidateField(form, field, postedValues, context));

//        return returnStrings;
//    }
//}
//    public class DropDownListFromRest : FieldType
//    {
//        [Setting("Default Value", description = "Enter a default value")]
//        public string DefaultValue
//        {
//            get;
//            set;
//        }

//        public override bool SupportsPrevalues
//        {
//            get
//            {
//                return true;
//            }
//        }

//        public DropDownListFromRest()
//        {
//            base.Id = new Guid("E4DA0677-8D17-47FB-AB4C-CE5903FE16EA");
//            base.Name = "Dropdown from Rest API";
//            base.Description = "Renders a list of values in a dropdown sourced from a Rest API";
//            this.Icon = "icon-indent";
//            this.DataType = FieldDataType.String;
//            this.Category = "List";
//            this.SortOrder = 80;
//            this.FieldTypeViewName = "FieldType.DropdownList.cshtml";
//        }
//    }
}
