﻿using EquityMaven.Data;
using EquityMaven.Data.Entities;
using EquityMaven.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Attributes;
using Umbraco.Forms.Core.Providers.FieldTypes;



namespace EquityMaven.Umb.Custom.Providers
{


    [Serializable]
    public class TRBCPicker : FieldType
    {

        [Setting("Default Value", description = "Enter a default value")]
        public string DefaultValue
        {
            get;
            set;
        }

        public override bool SupportsPrevalues
        {
            get
            {
                return true;
            }
        }

        public List<ThompsonReutersBusinessClassification> ThomsonReutersBusinessClassifications = new List<ThompsonReutersBusinessClassification>();

        public TRBCPicker()
        {
              

            base.Id = new Guid("F8AF0FE9-451C-460F-A550-CDB2E112C279");
            base.Name = "Picker for Thomson Reuters Codes";
            base.Description = "Select Thomson Reuters Code";
            this.Icon = "icon-indent";
            this.DataType = FieldDataType.String;
            this.Category = "List";
            this.SortOrder = 80;
            this.FieldTypeViewName = "FieldType.TRBCPicker.cshtml";
        }
         
    }
}
