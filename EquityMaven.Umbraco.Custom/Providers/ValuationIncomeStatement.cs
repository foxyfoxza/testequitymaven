﻿using EquityMaven.Data;
using EquityMaven.Data.Entities;
using EquityMaven.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Attributes;
using Umbraco.Forms.Core.Providers.FieldTypes;



namespace EquityMaven.Umb.Custom.Providers
{


    [Serializable]
    public class ValuationIncomeStatement : EMCustomFieldType
    {

        [Setting("Default Value", description = "Enter a default value")]
        public string DefaultValue
        {
            get;
            set;
        }

        public override bool SupportsPrevalues
        {
            get
            {
                return false;
            }
        }

        public List<ThompsonReutersBusinessClassification> ThomsonReutersBusinessClassifications = new List<ThompsonReutersBusinessClassification>();

        public ValuationIncomeStatement()
        {
            base.Id = new Guid("A9D610E3-C0B3-4CF9-82C7-189FCC229117");
            base.Name = "Valuation Income Statement";
            base.Description = "Valuation - Income Statement Values";
            this.Icon = "icon-indent";
            this.DataType = FieldDataType.LongString;
            this.Category = "List";
            this.SortOrder = 80;
            this.FieldTypeViewName = "FieldType.ValuationIncomeStatement.cshtml";
            UseFormFieldViewModel = true;
        }
         
    }
}
