﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Forms.Core.Attributes;
using Umbraco.Forms.Core.Providers.FieldTypes;

namespace EquityMaven.Umb.Custom.Providers
{
    //public class TextareaWithCount :  Textarea
    //{
    //    //Added a new setting when we add our field to the form
    //    [Setting("Max length", description = "Max length", view = "TextField")]
    //    public string MaxNumberOfChars { get; set; }

    //    public TextareaWithCount()
    //    {
    //        //Set a different view for this fieldtype
    //        this.FieldTypeViewName = "FieldType.TextareaWithCount.cshtml";

    //        //We can change the default name of 'Long answer' to something that suits us
    //        this.Name = "Long Answer with Limit";
    //    }

    //    public override IEnumerable<string> ValidateField(Form form, Field field, IEnumerable<object> postedValues, HttpContextBase context)
    //    {
    //        var baseValidation = base.ValidateField(form, field, postedValues, context);
    //        var value = postedValues.FirstOrDefault();

    //        if (value != null && value.ToString().Length < int.Parse(MaxNumberOfChars))
    //        {
    //            return baseValidation;
    //        }

    //        var custom = new List<string>();
    //        custom.AddRange(baseValidation);
    //        custom.Add("String is way way way too long!");

    //        return custom;
    //    }
    //}
}
