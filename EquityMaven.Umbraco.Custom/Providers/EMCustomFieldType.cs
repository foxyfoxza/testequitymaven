﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Forms.Core;

namespace EquityMaven.Umb.Custom.Providers
{
    public class EMCustomFieldType : FieldType
    {
        public bool UseFormFieldViewModel { get; set; }
    }
}
