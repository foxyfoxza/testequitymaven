﻿using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Forms.Core;

namespace EquityMaven.Umb.Custom.Models
{
    internal class PrelimEval : IPrelimEval
    {
        private Record record;

        public PrelimEval(Record record)
        {
             
            TRBC2012HierarchicalID = record.GetValue<string>("trbcCode");
            LastFinancialYearEnd = record.GetValue<DateTime>("lastFinancialYearEnd");
            CurrentCashOnHand = record.GetValue<Decimal>("currentCashOnHand");
            CurrentInterestBearingDebt = record.GetValue<Decimal>("currentInterestBearingDebt");
        }

        public decimal CurrentCashOnHand { get; set; }
        public decimal CurrentInterestBearingDebt { get; set; }

        public DateTime LastFinancialYearEnd { get; set; }
        public decimal PercentageOperatingCashRequired { get; set; }

        public string TRBC2012HierarchicalID { get; set; }
    }
}
