﻿using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Forms.Core;

namespace EquityMaven.Umb.Custom.Models
{
    internal class PrelimExcessCash : IPrelimExcessCash
    {
        private Record record;

        public PrelimExcessCash(Record record)
        {
            this.record = record;
            PercentageOperatingCashRequired = (decimal)((decimal)2 / (decimal)100);
        }

        public decimal EV_EBITDA_Valuation_EnterpriseValuePreIlliquidityDiscount { get; }
        
        public decimal EV_EBITDA_Valuation_EquityValuePostIlliquidityDiscount { get; }

        public decimal EV_EBITDA_Valuation_EquityValuePreIlliquidityDiscount { get; }

        public decimal EV_EBITDA_WorldMedian { get; }

        public decimal ExcessCash_CurrentCashOnHandAsAtLastMonthEnd { get; }

        public decimal ExcessCash_ExcessCash { get; }

        public decimal ExcessCash_PercentageAssumedOperatingCashRequiredOverRevenue { get; }

        public decimal PercentageOperatingCashRequired { get; set; }

        public decimal ValuationIlliquidityDiscount { get; }
    }
}
