﻿using EquityMaven.Interfaces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Umb.Custom.Models
{
    public class PrelimEvaluationGroupModel : IPrelimEvaluationGroupModel
    {
        public int Year { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public int Total { get; set; }
    }
}
