﻿using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Umb.Custom.Models
{
    internal class PrelimValuation : IPrelimValuation
    {
        public decimal EV_EBITDA_Valuation_EnterpriseValuePreIlliquidityDiscount { get; }

        public decimal EV_EBITDA_Valuation_EquityValuePostIlliquidityDiscount { get; }

        public decimal EV_EBITDA_Valuation_EquityValuePreIlliquidityDiscount { get; }

        public decimal EV_EBITDA_WorldMedian { get; }

        public decimal ValuationIlliquidityDiscount { get; }
    }
}
