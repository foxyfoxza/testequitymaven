﻿using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Forms.Core;

namespace EquityMaven.Umb.Custom.Models
{
    internal class PrelimIncomeStatement : IPrelimIncomeStatement
    {
        private Record record;

        public PrelimIncomeStatement(Record record)
        {
            this.record = record;
            IncomeStatement_Revenue = record.GetValue<Decimal>("incomeStatement_Revenue");
            IncomeStatement_CostOfGoodsSold = record.GetValue<Decimal>("incomeStatement_CostOfGoodsSold");
            IncomeStatement_OperatingExpense = record.GetValue<Decimal>("incomeStatement_OperatingExpense");

        }

        public decimal IncomeStatement_CostOfGoodsSold { get; set; }

        public decimal IncomeStatement_EBITDA { get;}

        public decimal IncomeStatement_GrossProfit { get;}

        public decimal IncomeStatement_OperatingExpense { get; set; }

        public decimal IncomeStatement_Revenue { get; set; }
    }
}
