﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Sandbox
{
    [DelimitedRecord("|")]
    public class RaRecord
    {
        [FieldCaption("Name a")]
        [FieldOrder(1)]
        public string Name;
        [FieldOrder(2)]
        public string Project;
        [FieldOrder(3)]
        [FieldConverter(ConverterKind.Decimal)]
        public decimal? Level;
        [FieldOrder(4)]
        [FieldConverter(ConverterKind.Date, "dd-MMM-yyyy")]
        public DateTime? Startdate;
        [FieldOrder(5)]
        public string ListOfIds;
    }
}
