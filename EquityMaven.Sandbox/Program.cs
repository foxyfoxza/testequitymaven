﻿using EquityMaven.ImportExport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
using FileHelpers.ExcelNPOIStorage;
using FileHelpers.DataLink;
using System.IO;
using NPOI.XSSF.UserModel;
using EquityMaven.ImportExport.Import;
using FileHelpers.Events;
using EquityMaven.Repository;
using System.Data.Entity;
using EquityMaven.Data;
using EquityMaven.Data.Entities;
using EquityMaven.ImportExport.Implementation;
using System.Diagnostics;
using System.Data;
using EquityMaven.Repository.DBInitialization;
using EquityMaven.CommonEnums;
using EquityMaven.Interfaces.ClientInputs;
using EquityMaven.Data.Calculations;

namespace EquityMaven.Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            //testVR();
            //return;
            testDB();
        }
        static void testDB()
        {
            Database.SetInitializer(new EquityMavenDropCreateDatabaseAlwaysInitializer());
            EquityMavenContext db = new EquityMavenContext();
             
            var tempEDB = new EntityDataManager(db);
            var inputBatchId = Guid.Empty;
            
          
            foreach (var item in db.InputsBatches)
            {
                Console.WriteLine($"{item.Description} {item.Id}");
                foreach(var item2 in db.TRBCValues)
                {
                    var trbc = db.ThompsonReutersBusinessClassifications.FirstOrDefault(o=> o.Id == item2.ThompsonReutersBusinessClassificationId);
                    Console.WriteLine($"TRBC ThompsonReutersBusinessClassification {(trbc.ActivityCode ?? "").PadRight(50)} World {item2.World}");
                }
            }
            Console.WriteLine("Done");
          


            Console.ReadLine();
        }
         static void testVR()
        {
             
            var DataManager = EquityMaven.Repository.EntityDataManagerService.DataManager;
            var fv = DataManager.GetFullValuations().FirstOrDefault();
            var finyears = new List<FullValuationInputYear>();
            finyears.AddRange(DataManager.GetFullValuationInputYears().Where(o => o.FullValuationInputId == fv.Id));

            var financialValuation = new FinancialValuation(fv, finyears, DataManager.GetThompsonReutersBusinessClassifications(), DataManager.GetTRBCValues(), DataManager.GetCurrencyRiskFreeRates(), DataManager.GetCurrencies(), DataManager.GetCountryTaxRates(), DataManager.GetGlobalDefaults(), DataManager.GetMoodysRatings(), DataManager.GetCorporateBondSpreads(), DataManager.GetCountries(), DataManager.GetCountryMoodysRatings());
            var fullValuationReport = new FullValuationReport();
            financialValuation.Apply(fullValuationReport);
            DataManager.SaveFullValuationReport(fullValuationReport);

        }

        static void TestExcelImport1()
        {
            var filename = @"C:\temp\BlueGrass\EquityMaven\test2.xlsx";
            var sheetNAme = "Sheet1";
            var provider = new ExcelNPOIStorage(typeof(TRBCImportMap))
            {
                FileName = filename,// Directory.GetCurrentDirectory() + @"\testBlankFields.xlsx",
                StartColumn = 0,
                StartRow = 1, SheetName = sheetNAme
            };

            var workbook = new XSSFWorkbook(provider.FileName);
            var row = workbook.GetSheet(sheetNAme).GetRow(0);
            var firstLineFields = row.Cells.Select(c => c.StringCellValue.Trim());

            var fieldFriendlyNames = provider.FieldFriendlyNames.ToList();

            provider.Progress += (sender, e) => 
                {
                    Console.WriteLine($"{e.CurrentRecord}");
                };

            foreach (var propertyName in fieldFriendlyNames)
            {
                Console.WriteLine(propertyName);
            }
            var test = provider.ExtractRecordsAsDT();

            foreach (var item in test.Rows)
            {

            }

            Console.ReadLine();
        }
         
        static void TestExcelImport()
        {
            var filename = @"C:\temp\BlueGrass\EquityMaven\test.xlsx";

            //var importer = new WaccInputsSummaryImporter();
            //var result = importer.Import(filename, "WACC INPUTS SUMMARY");
            
            Console.ReadLine();
        }


        static void TestExcelImport3()
        {
            var filename = @"C:\temp\BlueGrass\EquityMaven\test3.xlsx";
            var sheetNAme = "TRBC_MAP";
            var provider = new ExcelNPOIStorage(typeof(TRBCImportMap))
            {
                FileName = filename,// Directory.GetCurrentDirectory() + @"\testBlankFields.xlsx",
                StartColumn = 1,
                StartRow = 1,
                SheetName = sheetNAme
            };

            var workbook = new XSSFWorkbook(provider.FileName);
            var row = workbook.GetSheet(sheetNAme).GetRow(0);
            var firstLineFields = row.Cells.Select(c => c.StringCellValue.Trim());

            var fieldFriendlyNames = provider.FieldFriendlyNames.ToList();

            provider.Progress += (sender, e) =>
            {
                Console.WriteLine($"{e.CurrentRecord}");
            };

            foreach (var propertyName in fieldFriendlyNames)
            {
                Console.WriteLine(propertyName);
            }
            var test = provider.ExtractRecordsAsDT();

            foreach (var item in test.Rows)
            {
                var dr = item as DataRow;
                var i = 0;
                foreach (var propertyName in fieldFriendlyNames)
                {
                    Console.WriteLine($"{propertyName} {dr[i++]}");
                }

                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();

            }

            Console.ReadLine();
        }


        static bool TryGetCurrencySymbol(string ISOCurrencySymbol, out string symbol)
        {
            symbol = System.Globalization.CultureInfo
                .GetCultures(System.Globalization.CultureTypes.AllCultures)
                .Where(c => !c.IsNeutralCulture)
                .Select(culture =>
                {
                    try
                    {
                        return new System.Globalization.RegionInfo(culture.LCID);
                    }
                    catch
                    {
                        return null;
                    }
                })
                .Where(ri => ri != null && ri.ISOCurrencySymbol == ISOCurrencySymbol)
                .Select(ri => ri.CurrencySymbol)
                .FirstOrDefault();
            return symbol != null;
        }

    }
}
