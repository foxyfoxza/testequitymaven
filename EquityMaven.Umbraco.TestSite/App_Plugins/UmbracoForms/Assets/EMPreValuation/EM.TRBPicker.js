﻿var EMTRBPickerApp = angular.module('EMTRBPickerApp', []);




EMTRBPickerApp.controller('EMTRBPickerCtrl', ['$scope', '$http', '$location', function ($scope, $http, $location) {
    $scope.test = "abc";
    var emptyField = { "Id": "00000000-0000-0000-0000-000000000000", "FieldsetId": "00000000-0000-0000-0000-000000000000", "PageId": "00000000-0000-0000-0000-000000000000", "Name": "00000000-0000-0000-0000-000000000000", "Caption": "Revenue", "Mandatory": true, "RequiredErrorMessage": "", "Validate": true, "Regex": "^[0-9]*$", "InvalidErrorMessage": "", "FieldTypeName": "", "FieldType": { "icon": null, "id": "00000000-0000-0000-0000-000000000000", "name": "", "description": "", "group": null }, "HideLabel": false, "ShowIndicator": true, "ToolTip": null, "CssClass": "", "PreValues": [], "AdditionalSettings": { "DefaultValue": "", "Placeholder": "" }, "HasCondition": false, "PlaceholderText": "", "ConditionActionType": 0, "ConditionLogicType": 0, "ConditionRules": [], "ParentConditions": null, "Condition": { "id": "00000000-0000-0000-0000-000000000000", "enabled": false, "actionType": "Show", "logicType": "All", "rules": [] }, "Values": [], "Value": "", "ValueAsObject": null, "ValueAsHtmlString": {} };

    $scope.Data = { "Revenue": emptyField, "GrossProfit": emptyField, "Operatingexpenses": emptyField, "TRBCCode": {} }
    $scope.Data.TRBCCode.Activites = [];
    $scope.Data.TRBCCode.Industries = [];
    $scope.Data.TRBCCode.IndustryGroups = [];
    $scope.Data.TRBCCode.BusinessSectors = [];
    $scope.Data.TRBCCode.EconomicSectors = [];

    $scope.selectedActivityId = "";
    $scope.selectedIndustryId = "";
    $scope.selectedIndustryGroupId = "";
    $scope.selectedBusinessSectorId = "";
    $scope.selectedEconomicSectorId = "";

    $scope.counter = 0;

    $scope.GrossProfit = 0;
    $scope.Operatingexpenses = 0;


    $scope.change = function (item, value) {
        item.Value = value;

    };

    $scope.$watch('selectedActivityId', function () {
        var activity = getByField($scope.Data.TRBCCode.Activites, "Id", $scope.selectedActivityId);
        if (activity != null) {
            $scope.selectedIndustryId = activity.Industry.Id;
            $scope.selectedIndustryGroupId = activity.Industry.IndustryGroup.Id;
            $scope.selectedBusinessSectorId = activity.Industry.IndustryGroup.BusinessSector.Id;
            $scope.selectedEconomicSectorId = activity.Industry.IndustryGroup.BusinessSector.EconomicSector.Id;
        }
    });

    $scope.$watch('selectedIndustryId', function () {
        var changedItem = getByField($scope.Data.TRBCCode.Industries, "Id", $scope.selectedIndustryId);
        var subitem = getByField($scope.Data.TRBCCode.Activites, "Id", $scope.selectedActivityId);
        if (subitem != null && subitem.Industry.Id != changedItem.Id) {
            $scope.selectedActivityId = "";
        }
    });
    $scope.$watch('selectedIndustryGroupId', function () {
        var changedItem = getByField($scope.Data.TRBCCode.IndustryGroups, "Id", $scope.selectedIndustryGroupId);
        var subitem = getByField($scope.Data.TRBCCode.Industries, "Id", $scope.selectedIndustryId);
        if (subitem != null && subitem.IndustryGroup.Id != changedItem.Id) {


            $scope.selectedIndustryId = "";
            $scope.selectedActivityId = "";
        }
    });

    $scope.$watch('selectedBusinessSectorId', function () {
        var changedItem = getByField($scope.Data.TRBCCode.BusinessSectors, "Id", $scope.selectedBusinessSectorId);
        var subitem = getByField($scope.Data.TRBCCode.IndustryGroups, "Id", $scope.selectedIndustryGroupId);
        if (subitem != null && subitem.BusinessSector.Id != changedItem.Id) {

            $scope.selectedIndustryGroupId = "";
            $scope.selectedIndustryId = "";
            $scope.selectedActivityId = "";
        }

    });

    $scope.$watch('selectedEconomicSectorId', function () {
        var economicSector = getByField($scope.Data.TRBCCode.EconomicSectors, "Id", $scope.selectedEconomicSectorId);
        var businessSector = getByField($scope.Data.TRBCCode.BusinessSectors, "Id", $scope.selectedBusinessSectorId);
        if (businessSector != null && businessSector.EconomicSector.Id != economicSector.Id) {
            $scope.selectedBusinessSectorId = "";
            $scope.selectedIndustryGroupId = "";
            $scope.selectedIndustryId = "";
            $scope.selectedActivityId = "";
        }
    });



    $scope.init = function (data) {


        $scope.Data = { "Revenue": emptyField, "GrossProfit": emptyField, "Operatingexpenses": emptyField, "TRBCCode": {} }
        $scope.Data.TRBCCode.Activites = [];
        $scope.Data.TRBCCode.Industries = [];
        $scope.Data.TRBCCode.IndustryGroups = [];
        $scope.Data.TRBCCode.BusinessSectors = [];
        $scope.Data.TRBCCode.EconomicSectors = [];
        $scope.Data.Raw = data.preValues;


        _tempActivites = [];
        _tempIndustries = [];
        _tempIndustryGroups = [];
        _tempBusinessSectors = [];
        _tempEconomicSectors = [];

        $scope.selectedActivityId = data.selectedActivityId;
        for (var i = 0; i < $scope.Data.Raw.length; i++) {
            var item = $scope.Data.Raw[i];
            var trbc = JSON.parse(item.Value);
            var activity = { Id: trbc.ActivityCode, Description: trbc.ActivityName, Industry: { Id: trbc.IndustryCode, Description: trbc.IndustryName, IndustryGroup: { Id: trbc.IndustryGroupCode, Description: trbc.IndustryGroupName, BusinessSector: { Id: trbc.BusinessSectorCode, Description: trbc.BusinessSectorName, EconomicSector: { Id: trbc.EconomicSectorCode, Description: trbc.EconomicSectorName } } } } };
            var industry = activity.Industry;
            var industryGroup = industry.IndustryGroup;
            var businessSector = industryGroup.BusinessSector;
            var economicSector = businessSector.EconomicSector;

            _tempActivites.push(activity);
            _tempIndustries.push(industry);
            _tempIndustryGroups.push(industryGroup);
            _tempBusinessSectors.push(businessSector);
            _tempEconomicSectors.push(economicSector);

            if ($scope.selectedActivityId == activity.Id) {
                $scope.selectedIndustryId = industry.Id;
                $scope.selectedIndustryGroupId = industryGroup.Id;
                $scope.selectedBusinessSectorId = businessSector.Id;
                $scope.selectedEconomicSectorId = economicSector.Id;
            }

        }

        $scope.Data.TRBCCode.Activites = getUniqueBy(_tempActivites, "Description");
        $scope.Data.TRBCCode.Industries = getUniqueBy(_tempIndustries, "Description");
        $scope.Data.TRBCCode.IndustryGroups = getUniqueBy(_tempIndustryGroups, "Description");
        $scope.Data.TRBCCode.BusinessSectors = getUniqueBy(_tempBusinessSectors, "Description");
        $scope.Data.TRBCCode.EconomicSectors = getUniqueBy(_tempEconomicSectors, "Description");

    }

    var getUniqueBy = function (from, p) {
        var flags = [], to = [], l = from.length, i;

        for (i = 0; i < l; i++) {
            if (flags[from[i][p]]) continue;
            flags[from[i][p]] = true;
            to.push(from[i]);
        }
        return to;
    }

    var getByField = function (from, p, value) {
        var result = null, l = from.length, i;

        for (i = 0; i < l; i++) {
            if (from[i][p] != value) continue;
            result = from[i];
            break;
        }
        return result;
    }

}]);


