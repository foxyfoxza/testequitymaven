﻿function emAdminShared($q, $http, umbRequestHelper, filterFilter) {
    return {
        SetDefaults: function ($scope, callget, callSave, onNewRecord, onSearchRecord, emptyId, onGetData, onCancel) {
            if (!onCancel)
                onCancel = null;

            $scope.onCancel = onCancel;
            $scope.callget = callget;
            $scope.callSave = callSave;
            $scope.onNewRecord = onNewRecord;
            $scope.onSearchRecord = onSearchRecord;
            $scope.emptyId = emptyId;
            if (AddRecord)
                $scope.AddRecord = AddRecord;
            $scope.onGetData = onGetData;
            var vm = { results: [] };


            $scope.vm = vm;
            vm.search = search;
            vm.pagination = {
                pageIndex: 0,
                pageNumber: 1,
                totalPages: 1,
                pageSize: 10
            };
            $scope.load = {};
            $scope.ready = false;
            $scope.searchValue = "";
            vm.goToPage = goToPage;
            function goToPage(pageNumber) {
                vm.pagination.pageIndex = pageNumber - 1;
                vm.pagination.pageNumber = pageNumber;
                vm.search();
            }
            // $watch searchValue to update pagination
            $scope.$watch('searchValue', function (newVal, oldVal) {
                if ($scope.ready) {
                    search();
                }
            }, true);


            if (!$scope.data)
                $scope.data = []; //declare an empty array
            $scope.sortKey = "";
            $scope.reverse = "";
            $scope.sort = function (keyname) {
                $scope.sortKey = keyname;   //set the sortKey to the param passed
                $scope.reverse = !$scope.reverse; //if true make it false and vice versa
            }

            $scope.changed = function (item) {
                if (!item.changed)
                    item.changed = JSON.stringify(item);
            };
            $scope.Cancel = function () {
                if ($scope.onCancel != null)
                    $scope.onCancel();
                for (var i = $scope.data.length - 1; i >= 0; --i) {
                    var item = $scope.data[i];
                    if (item.changed) {
                        $scope.data.splice(i, 1);
                        if (item.changed.Id)
                            $scope.data.push(JSON.parse(item.changed));
                    }
                }
                vm.search();
            };

            $scope.reload = function () {
                $scope.ready = false;
                $scope.callget().then(function (result) {
                    $scope.onGetData(result);
                    $scope.ready = true;
                    vm.search();
                });
            };

            $scope.SaveRecord = function () {
                $scope.ready = false;
                var changed = [];

                AddChanged(changed, $scope.data);
                if (changed.length > 0) {
                    if ($scope.callSave != null) {
                        $scope.callSave(changed).then(function (result) {
                            for (var i = 0; i < result.length; i++) {
                                var found = false;
                                for (var j = 0; j < $scope.data.length; j++) {
                                    if ($scope.data[j].Id == result[i].Id) {
                                        $scope.data[j] = result[i];
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found) {
                                    $scope.data.push(result[i]);
                                }
                            }
                            //remove all 'new' records as they should have saved
                            for (var i = $scope.data.length - 1; i >= 0; --i) {
                                if ($scope.data[i].Id == $scope.emptyId) {
                                    $scope.data.splice(i, 1);
                                }
                            }
                            vm.search();
                        });
                    }
                }
                $scope.ready = true;
            };

            function AddRecord() {
                var newRec = onNewRecord();
                if (newRec != null) {
                    $scope.data.push(newRec);
                    vm.search();
                }
            };

            function search() {
                $scope.ready = false;
                vm.results = [];
                var tempResults = filterFilter($scope.data, onSearchRecord());

                AddChanged(tempResults, $scope.data);

                for (var i = (vm.pagination.pageNumber - 1) * vm.pagination.pageSize; i < (vm.pagination.pageNumber * vm.pagination.pageSize) && i < tempResults.length; i++) {
                    vm.results.push(tempResults[i]);
                }
                vm.pagination.totalPages = Math.ceil(tempResults.length / vm.pagination.pageSize);
                $scope.ready = true;
                // console.log(response.data);
            }

            AddChanged = function (existing, data) {
                if (!existing || existing.length == undefined)
                    existing = [];
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    if (item.changed) {
                        var found = false;
                        for (j = 0; j < existing.length; j++) {
                            if (existing[j] == item) {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                            existing.push(item);
                    }
                }
            };
            $scope.reload();
        },


    };
}
angular.module("umbraco.resources").factory("emAdminShared", emAdminShared);
