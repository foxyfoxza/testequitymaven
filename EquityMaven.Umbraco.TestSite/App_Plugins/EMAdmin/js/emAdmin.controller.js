﻿var app = angular.module("umbraco");


app.controller('EMAdmin.Blank', function ($scope, $rootScope, $http, $routeParams, $filter, emAdminResource, emAdminShared, filterFilter, navigationService, treeService) {
    emptyId = "00000000-0000-0000-0000-000000000000";
    $scope.id = $routeParams.id;

    $scope.vm = { Title: $routeParams.method };

});


app.controller("EMAdmin.delTRBCController",
    function ($scope, $location, $routeParams, $cookieStore, formResource, licensingResource, updatesResource, notificationsService, userService, utilityService, securityResource, emAdminResource, filterFilter) {

        $scope.items = [];
        $scope.id = $routeParams.id;
        $scope.overlay = {

            title: "TRBC",

        };
        $scope.reload = function () {
            $scope.ready = false;
            emAdminResource.getDashboard().then(function (result) {
                $scope.items = result;
                $scope.ready = true;
            });
        };
        //var packageInstall = $cookieStore.get("umbPackageInstallId");
        //if (packageInstall) {
        //    $scope.overlay.show = true;
        //    $cookieStore.put("umbPackageInstallId", "");
        //}


        //Get Current User - To Check if the user Type is Admin or Manager
        userService.getCurrentUser().then(function (response) {
            $scope.currentUser = response;
            $scope.isAdminUser = response.userType.toLowerCase() === "admin";
            $scope.isEMDMUser = $scope.isAdminUser || response.userType.toLowerCase() === "equityMavenDataManager";
            //securityResource.getByUserId($scope.currentUser.id).then(function (response) {
            //    $scope.userCanManageForms = response.data.userSecurity.manageForms;
            //});
        });

        $scope.configuration = { domain: window.location.hostname };
        $scope.reload();

        // create empty search model (object) to trigger $watch on update
        $scope.search = {};

        $scope.resetFilters = function () {
            // needs to be a function or it won't trigger a $watch
            $scope.search = {};
        };
        $scope.currentPage = 1;
        $scope.totalItems = $scope.items.length;
        $scope.entryLimit = 50; // items per page
        $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

        // $watch search to update pagination
        $scope.$watch('search', function (newVal, oldVal) {
            $scope.filtered = filterFilter($scope.items, newVal);
            $scope.totalItems = $scope.filtered.length;
            $scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
            $scope.currentPage = 1;
        }, true);
    });


app.controller('EMAdmin.TRBC', function ($scope, $rootScope, $http, $routeParams, $filter, emAdminResource, emAdminShared, filterFilter, navigationService, treeService) {
    emptyId = "00000000-0000-0000-0000-000000000000";

    var pieces = $routeParams.id.split('_');
    $scope.InputBatchId = pieces[pieces.length - 1];
    $scope.Description = "TRBC";
    $scope.data = [];
    $scope.TRBCCode = { Activities: [], Industries: [], IndustryGroups: [], BusinessSectors: [], EconomicSectors: [], }
    $scope.SelectedActivity = {};


    $scope.selectedActivityId = "";
    $scope.selectedIndustryId = "";
    $scope.selectedIndustryGroupId = "";
    $scope.selectedBusinessSectorId = "";
    $scope.selectedEconomicSectorId = "";

    function onGetData(result) {
        $scope.data = result.Values;

        $scope.selectedActivityId = "";

        $scope.TRBCCode.Original = { Activities: [], Industries: [], IndustryGroups: [], BusinessSectors: [], EconomicSectors: [], };
        $scope.TRBCCode.Activities = getUniqueBy(result.Activities, "Description");
        $scope.TRBCCode.Industries = getUniqueBy(result.Industries, "Description");
        $scope.TRBCCode.IndustryGroups = getUniqueBy(result.IndustryGroups, "Description");
        $scope.TRBCCode.BusinessSectors = getUniqueBy(result.BusinessSectors, "Description");
        $scope.TRBCCode.EconomicSectors = getUniqueBy(result.EconomicSectors, "Description");

        $scope.selectedActivityId = "";
        $scope.selectedIndustryId = "";
        $scope.selectedIndustryGroupId = "";
        $scope.selectedBusinessSectorId = "";
        $scope.selectedEconomicSectorId = "";
    }
    var getUniqueBy = function (from, p) {
        var flags = [], to = [], l = from.length, i;

        for (i = 0; i < l; i++) {
            if (flags[from[i][p]]) continue;
            flags[from[i][p]] = true;
            to.push(from[i]);
        }
        return to;
    }

    var getByField = function (from, p, value) {
        var result = null, l = from.length, i;

        for (i = 0; i < l; i++) {
            if (from[i][p] != value) continue;
            result = from[i];
            break;
        }
        return result;
    }
    var getAllByField = function (from, p, value) {
        var result = [], l = from.length, i;

        for (i = 0; i < l; i++) {
            if (from[i][p] != value) continue;
            result.push(from[i]);
        }
        return result;
    }
 
    function callget() { return emAdminResource.getTRBCDashboard($scope.InputBatchId); }
    function callSave(data) { return emAdminResource.updateTRBCDashboard(data); }
    function onNewRecord() {
        return null;
    }

    function onSearchRecord() {
        return $scope.searchValue;
    };

    $scope.$watch('selectedActivityId', function () {
        var activity = getByField($scope.TRBCCode.Activities, "Id", $scope.selectedActivityId);
        //if (!activity) { activity = { Industry: { Id: "", IndustryGroup: { Id: "", BusinessSector: { Id: "", EconomicSector: { Id: "" } } }, } };

        if (!activity) { activity = { "IndustryId": "", "Industry": { "IndustryGroupId": "", "IndustryGroup": { "BusinessSectorId": "", "BusinessSector": { "EconomicSectorId": "", "EconomicSector": { "BusinessSectors": null, "Id": "", "Description": "", "Created": "", "LastUpdated": "" }, "IndustryGroups": null, "Id": "", "Description": "", "Created": "", "LastUpdated": "" }, "Industries": null, "Id": "", "Description": "Coal", "Created": "", "LastUpdated": "" }, "Activities": null, "Id": "", "Description": "", "Created": "", "LastUpdated": "" }, "Values": null, "Id": "", "Description": "", "Created": "", "LastUpdated": "" } };
        $scope.SelectedActivity = activity;
        $scope.SelectedRecord = getByField($scope.data, "ActivityId", activity.Id);
        $scope.selectedIndustryId = (activity) ? activity.IndustryId : "";
        var _Industry = getByField($scope.TRBCCode.Industries, "Id", $scope.selectedIndustryId);
        $scope.selectedIndustryGroupId = _Industry ? _Industry.IndustryGroupId : "";
        var _IndustryGroup = getByField($scope.TRBCCode.IndustryGroups, "Id", $scope.selectedIndustryGroupId);
        $scope.selectedBusinessSectorId = (_IndustryGroup) ? _IndustryGroup.BusinessSectorId : "";
        var _BusinessSector = getByField($scope.TRBCCode.BusinessSectors, "Id", $scope.selectedBusinessSectorId);
        $scope.selectedEconomicSectorId = (_BusinessSector) ? _BusinessSector.EconomicSectorId : "";
        var _EconomicSector = getByField($scope.TRBCCode.EconomicSectors, "Id", $scope.selectedEconomicSectorId);
        

        $scope.searchValue = { ActivityId: $scope.selectedActivityId };

    });


    $scope.$watch('selectedIndustryId', function () {
        var changedItem = getByField($scope.TRBCCode.Industries, "Id", $scope.selectedIndustryId);
        var subitem = getByField($scope.TRBCCode.Activities, "Id", $scope.selectedActivityId);
        if (subitem != null && subitem.IndustryId != changedItem.Id) {
            $scope.selectedActivityId = "";
        }
    });
    $scope.$watch('selectedIndustryGroupId', function () {
        var changedItem = getByField($scope.TRBCCode.IndustryGroups, "Id", $scope.selectedIndustryGroupId);
        var subitem = getByField($scope.TRBCCode.Industries, "Id", $scope.selectedIndustryId);
        if (subitem != null && subitem.IndustryGroupId != changedItem.Id) {
            $scope.selectedIndustryId = "";
            $scope.selectedActivityId = "";
        }
    });

    $scope.$watch('selectedBusinessSectorId', function () {
        var changedItem = getByField($scope.TRBCCode.BusinessSectors, "Id", $scope.selectedBusinessSectorId);
        var subitem = getByField($scope.TRBCCode.IndustryGroups, "Id", $scope.selectedIndustryGroupId);
        if (subitem != null && subitem.BusinessSectorId != changedItem.Id) {

            $scope.selectedIndustryGroupId = "";
            $scope.selectedIndustryId = "";
            $scope.selectedActivityId = "";
        }

    });

    $scope.$watch('selectedEconomicSectorId', function () {
        var economicSector = getByField($scope.TRBCCode.EconomicSectors, "Id", $scope.selectedEconomicSectorId);
        var businessSector = getByField($scope.TRBCCode.BusinessSectors, "Id", $scope.selectedBusinessSectorId);
        if (businessSector != null && businessSector.EconomicSectorId != economicSector.Id) {
            $scope.selectedBusinessSectorId = "";
            $scope.selectedIndustryGroupId = "";
            $scope.selectedIndustryId = "";
            $scope.selectedActivityId = "";
        }
    });
    emAdminShared.SetDefaults($scope, callget, callSave, null, onSearchRecord, emptyId, onGetData);
    //emAdminShared.SetDefaults($scope, callget, callSave, onNewRecord, onSearchRecord, emptyId, onGetData);
});


app.controller('EMAdmin.Moodysratings', function ($scope, $rootScope, $http, $routeParams, $filter, emAdminResource, emAdminShared, filterFilter, navigationService, treeService) {
    emptyId = "00000000-0000-0000-0000-000000000000";
    var pieces = $routeParams.id.split('_');
    $scope.InputBatchId = pieces[pieces.length - 1];

    emAdminShared.SetDefaults($scope, callget, callSave, onNewRecord, onSearchRecord, emptyId, onGetData);
    function onGetData(result) { $scope.data = result; }
    function callget() { return emAdminResource.getMoodysRatings($scope.InputBatchId); }
    function callSave(data) { return emAdminResource.updateMoodysRatings(data); }
    function onNewRecord() {
        return {
            changed: {},
            Rating: "",
            DefaultSpread: "0",
            isActive: false,
            Id: emptyId,
            InputBatchId: $scope.InputBatchId
        }
    }

    function onSearchRecord() { return { "Rating": $scope.searchValue } };
});


app.controller('EMAdmin.InputBatch', function ($scope, $rootScope, $http, $routeParams, $filter, emAdminResource, emAdminShared, filterFilter, navigationService, treeService, notificationsService) {
    var pieces = $routeParams.id.split('_');
    //$scope.InputBatchId = pieces[pieces.length - 1];
    //$scope.inputbatchType = 1;//pieces[pieces.length - 1];
    //$scope.fileSelected = function (files) {
    //    // In this case, files is just a single path/filename
    //    $scope.file = files;
    //};


    //$scope.uploadFile = function () {
    //    if (!$scope.isUploading) {
    //        if ($scope.file) {
    //            $scope.isUploading = true;
    //            emAdminResource.importInputBatchData($scope.inputbatchType, $scope.file)
    //                .then(function (response) {
    //                    if (response) {
    //                        notificationsService.success("Success", "Saved to server with the filename " + response);
    //                    }
    //                    $scope.isUploading = false;
    //                }, function (reason) {
    //                    notificationsService.error("Error", "File import failed: " + reason.message);
    //                    $scope.isUploading = false;
    //                });
    //        } else {
    //            notificationsService.error("Error", "You must select a file to upload");
    //            $scope.isUploading = false;
    //        }
    //    }
    //};

    //$scope.file = false;
    //$scope.isUploading = false;


    //$scope.InputBatch = {
    //    ActiveFrom: moment(new Date()).format('YYYY-MM-DDTHH:mm:ss'),
    //    Description: "",
    //    isActive: false,
    //    Id: emptyId,
    //    BatchTypeId: $scope.inputbatchType
    //}

    


});

app.controller('EMAdmin.ListInputBatches', function ($scope, $rootScope, $http, $routeParams, $filter, emAdminResource, emAdminShared, filterFilter, navigationService, treeService, notificationsService) {
    emptyId = "00000000-0000-0000-0000-000000000000";
    if ($routeParams.id == 1)
        $scope.Description = "Annual Input Batches"
    else
        $scope.Description = "Monthly Input Batches"

    $scope.InputBatchId = $routeParams.id;
    
    function onGetData(result) { $scope.data = result; }


    function callget() { return emAdminResource.getInputBatches(true, $routeParams.id); }

    function callSave(data) { return emAdminResource.updateInputBatches(data); }


    function onNewRecord() {
        $scope.callSave = $scope.uploadFile;
        $scope.NewInputBatch = {
            ActiveFrom: moment(new Date()).format('YYYY-MM-DDTHH:mm:ss'),
            Description: "...",
            isActive: false,
            Id: emptyId,
            BatchTypeId: $routeParams.id
        }
        return $scope.NewInputBatch;
    }
     
    function onSearchRecord() { return { "Description": $scope.searchValue } };
     
    $scope.fileSelected = function (files) {
        // In this case, files is just a single path/filename
        $scope.file = files;
    };

    CancelNewInputBatch = function()
    {
        $scope.NewInputBatch = null;
        $scope.callSave = callSave;
    }

    $scope.uploadFile = function (data) {
        return new Promise( function (resolve, reject) {
            if (!$scope.isUploading) {
                if ($scope.file) {
                    $scope.isUploading = true;
                    emAdminResource.importInputBatchData($scope.NewInputBatch, $scope.file)
                        .then(function (response) {
                            if (response) {
                                CancelNewInputBatch();
                                notificationsService.success("Success", "Saved to server ");
                                resolve(response.data);
                            }
                            $scope.isUploading = false;
                        }, function (reason) {
                            notificationsService.error("Error", "File import failed: " + reason.message);
                            $scope.isUploading = false;
                            reject(reason);
                        });
                } else {
                    notificationsService.error("Error", "You must select a file to upload");
                    $scope.isUploading = false;
                    reject("You must select a file to upload");
                }
            }
        })};
    

    $scope.file = false;
    $scope.isUploading = false;
     
    emAdminShared.SetDefaults($scope, callget, callSave, onNewRecord, onSearchRecord, emptyId, onGetData, CancelNewInputBatch);
});




app.controller('EMAdmin.CorporateBondSpreads', function ($scope, $rootScope, $http, $routeParams, $filter, emAdminResource, emAdminShared, filterFilter, navigationService, treeService) {
    emptyId = "00000000-0000-0000-0000-000000000000";
    var pieces = $routeParams.id.split('_');
    $scope.InputBatchId = pieces[pieces.length - 1];
    emAdminShared.SetDefaults($scope, callget, callSave, onNewRecord, onSearchRecord, emptyId, onGetData);
    function onGetData(result) { $scope.data = result; }
    function callget() { return emAdminResource.getMoodysRatings($routeParams.id); }
    function callSave(data) { return emAdminResource.updateMoodysRatings(data); }
    function onNewRecord() {
        return {
            changed: {},
            Rating: "",
            DefaultSpread: "0",
            isActive: false,
            Id: emptyId,
            InputBatchId: $scope.InputBatchId
        }
    }

    function onSearchRecord() { return { "Rating": $scope.searchValue } };
});






