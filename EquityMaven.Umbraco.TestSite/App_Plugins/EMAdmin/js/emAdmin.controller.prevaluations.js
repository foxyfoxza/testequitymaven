﻿var app = angular.module("umbraco");


app.controller('EMAdmin.prevaluations', function ($scope, $rootScope, $http, $routeParams, $filter, emAdminClientPrelimEvaluationsResource, emAdminShared, filterFilter, navigationService, treeService, notificationsService) {
    emptyId = "00000000-0000-0000-0000-000000000000";
    //if ($routeParams.id == 1)
    //    $scope.Description = "Annual Input Batches"
    //else
    $scope.Description = "Pre-Valuations"
    var pieces = $routeParams.id.split('_');
    if (pieces.length >= 3)
    {
        $scope.year = pieces[pieces.length - 3];
        $scope.month = pieces[pieces.length - 2];
        $scope.day = pieces[pieces.length - 1];
    }
    else
    {
        $scope.year = 0;
        $scope.month = 0;
        $scope.day = 0;
    }
    //$scope.InputBatchId = $routeParams.id;

    function onGetData(result) { $scope.data = result; }

    $scope.Select = function (record) {
        $scope.selected = record;
    }

    function callget() { return emAdminClientPrelimEvaluationsResource.getAll($scope.year, $scope.month, $scope.day); }

    function callSave(data) { return emAdminResource.updateInputBatches(data); }


    function onNewRecord() {
        $scope.callSave = $scope.uploadFile;
        $scope.NewInputBatch = {
            ActiveFrom: moment(new Date()).format('YYYY-MM-DDTHH:mm:ss'),
            Description: "...",
            isActive: false,
            Id: emptyId,
            BatchTypeId: $routeParams.id
        }
        return $scope.NewInputBatch;
    }

    function onSearchRecord() { return { "Description": $scope.searchValue } };

    $scope.fileSelected = function (files) {
        // In this case, files is just a single path/filename
        $scope.file = files;
    };

    CancelNewInputBatch = function () {
        $scope.NewInputBatch = null;
        $scope.callSave = callSave;
    }

    $scope.uploadFile = function (data) {
        return new Promise(function (resolve, reject) {
            if (!$scope.isUploading) {
                if ($scope.file) {
                    $scope.isUploading = true;
                    emAdminResource.importInputBatchData($scope.NewInputBatch, $scope.file)
                        .then(function (response) {
                            if (response) {
                                CancelNewInputBatch();
                                notificationsService.success("Success", "Saved to server ");
                                resolve(response.data);
                            }
                            $scope.isUploading = false;
                        }, function (reason) {
                            notificationsService.error("Error", "File import failed: " + reason.message);
                            $scope.isUploading = false;
                            reject(reason);
                        });
                } else {
                    notificationsService.error("Error", "You must select a file to upload");
                    $scope.isUploading = false;
                    reject("You must select a file to upload");
                }
            }
        })
    };


    $scope.file = false;
    $scope.isUploading = false;

    emAdminShared.SetDefaults($scope, callget, callSave, onNewRecord, onSearchRecord, emptyId, onGetData, CancelNewInputBatch);
});
