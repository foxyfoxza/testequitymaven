﻿function emAdminResource($q, $http, umbRequestHelper) {

    return {
        
        getInputBatches: function (includeInactive, batchTypeId) {
            return umbRequestHelper.resourcePromise(
                $http.get("api/EMAdminBackoffice/GetInputBatches?includeInactive=" + includeInactive + "&batchTypeId=" + batchTypeId),
                "Failed to get Input Batches");
        },

        updateInputBatches: function (items) {
            return umbRequestHelper.resourcePromise(
                $http.post("api/EMAdminBackoffice/UpdateInputBatches", items),
                "Failed to update Input Batch");
        },

        importInputBatchData: function (inputBatch, file) {
            var request = {
                file: file
            };
            return $http({
                method: 'POST',

                url: "api/EMAdminBackoffice/ImportBatchData?batchTypeValue=" + inputBatch.BatchTypeId + "&Description=" + inputBatch.Description + "&ActiveFrom=" + inputBatch.ActiveFrom,
                headers: { 'Content-Type': undefined },
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("file", data.file);
                    return formData;
                },
                data: request
            }).then(function (response) {
                if (response) {
                    return response;
                    //var fileName = response.data;
                    //return fileName;
                } else {
                    return false;
                }
            });
        },
         


        getMoodysRatings: function (inputbatchId) {
            return umbRequestHelper.resourcePromise(
                $http.get("api/EMAdminBackoffice/GetMoodysRatings?inputbatchId=" + inputbatchId),
                "Failed to get Moodys Ratings");
        },

        updateMoodysRatings: function (items) {
            return umbRequestHelper.resourcePromise(
                $http.post("api/EMAdminBackoffice/UpdateMoodysRatings", items),
                "Failed to update Moodys Ratings");
        },
        getTRBCDashboard: function (inputbatchId) {
            return umbRequestHelper.resourcePromise(
                $http.get("api/EMAdminBackoffice/GetTRBCDashboard?inputbatchId=" + inputbatchId),
                "Failed to get TRBC Dashboard");
        },
        //getDashboard: function () {
        //    return umbRequestHelper.resourcePromise(
        //        $http.get("api/EMAdminBackoffice/GetThompsonReutersBusinessClassificationsPaged"),
        //        "Failed to get TRBC Activities");
        //}


    };
}
angular.module("umbraco.resources").factory("emAdminResource", emAdminResource);
