﻿using EquityMaven.Data;
using EquityMaven.Data.Entities;
using EquityMaven.Data.HelperEntities;
using EquityMaven.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EquityMaven.Umb.TestSite.Helpers
{
    public class tempHelper
    {
        public static FullValuationModel GetFullValuationModel(int memberId)
        {


            var DataManager = EntityDataManagerService.DataManager;
            var client = DataManager.GetClientByMemberId(memberId);
            PrelimEvaluation preval = null;
            if (client != null)
            {
                preval = DataManager.GetPrelimEvaluations(false).Where(o => o.Email == client.Email).OrderByDescending(o => o.Created).FirstOrDefault();
            }

             

            FullValuationModel result = null;
            try
            {
                //result = Newtonsoft.Json.JsonConvert.DeserializeObject<FullValuationModel>(json);
                result = new FullValuationModel
                {
                    ClientId = client?.Id ?? 0,
                    MemberId = memberId,
                    CompanyName = preval?.CompanyName,
                    //SelectedCurrencyId = preval?.CurrencyId,
                    CurrentCashOnHand = preval?.CurrentCashOnHand ?? 0,
                    CurrentInterestBearingDebt = preval?.CurrentInterestBearingDebt ?? 0,
                    TRBC2012HierarchicalID = preval?.TRBC2012HierarchicalID,
                    //IncomeStatement_Revenue = preval?.IncomeStatement_Revenue ?? 0,
                    //IncomeStatement_CostOfGoodsSold = preval?.IncomeStatement_CostOfGoodsSold ?? 0,
                    //IncomeStatement_OperatingExpense = preval?.IncomeStatement_OperatingExpense ?? 0
                    LastFinancialYearEnd = preval != null ? preval.LastFinancialYearEnd : new DateTime(),
                    ValuationDate = DateTime.UtcNow,

                };
                 
                result.PrePolulateDefaults();
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static PrelimEvaluationModel GetPrelimEvaluationModel(int memberId)
        {


            var DataManager = EntityDataManagerService.DataManager;
            var client = DataManager.GetClientByMemberId(memberId);
            PrelimEvaluation preval = null;
            if (client != null)
            {
                preval = DataManager.GetPrelimEvaluations(false).Where(o => o.Email == client.Email).OrderByDescending(o => o.Created).FirstOrDefault();
            }



            PrelimEvaluationModel result = null;
            try
            {
                var currencies = DataManager.GetCurrencies().ToList();
                var trbcs = DataManager.GetThompsonReutersBusinessClassifications().OrderBy(o => o.TRBC2012HierarchicalID).ToList();
                //result = Newtonsoft.Json.JsonConvert.DeserializeObject<FullValuationModel>(json);
                result = new PrelimEvaluationModel
                {
                    Email = client?.Email ,
                    CompanyName = preval?.CompanyName ,
                    ISOCurrencySymbol = preval?.ISOCurrencySymbol,
                    //SelectedCurrencyId = preval?.CurrencyId,
                    CurrentCashOnHand = preval?.CurrentCashOnHand ?? 0,
                    CurrentInterestBearingDebt = preval?.CurrentInterestBearingDebt ?? 0,
                    TRBC2012HierarchicalID = preval?.TRBC2012HierarchicalID ,
                    IncomeStatement_Revenue = preval?.IncomeStatement_Revenue ?? 0,
                    IncomeStatement_CostOfGoodsSold = preval?.IncomeStatement_CostOfGoodsSold ?? 0,
                    IncomeStatement_OperatingExpense = preval?.IncomeStatement_OperatingExpense ?? 0,
                    LastFinancialYearEnd =  preval?.LastFinancialYearEnd,
                    Currencies = currencies.Select(o=> new string[] { o.ISOCurrencySymbol, o.CurrencyEnglishName }).ToList(),
                    TRBCS = trbcs.Select(o => new string[] { o.TRBC2012HierarchicalID, o.EconomicSectorName, o.BusinessSectorName, o.IndustryGroupName, o.IndustryName, o.ActivityName }).ToList()

                };
                
            }
            catch (Exception ex)
            {

            }
            return result;
        }



    }
}