﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Forms.Mvc.Models;

namespace EquityMaven.Umb.TestSite.Models
{
    public class FormFieldViewModel
    {
        public FormFieldViewModel(FormViewModel Form, FieldViewModel Field/*, string valueField, bool isCalculated = false*/)
        {
            this.Form = Form;
            this.Field = Field;
            //this.ValueField = valueField;
            //this.isCalculated = isCalculated;
        }
        public FormViewModel Form { get; set; }
        public FieldViewModel Field { get; set; }
        //public string ValueField { get; private set; }
        //public bool isCalculated { get; private set; }
    }
}