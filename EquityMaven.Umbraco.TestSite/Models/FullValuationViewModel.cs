
using EquityMaven.Umb.TestSite.Models;
using System.Collections.Generic;
using EquityMaven.Data;
using System;
using Umbraco.Forms.Mvc.Models;
using EquityMaven.Data.HelperEntities;
using EquityMaven.Repository;
using System.Linq;
using EquityMaven.Data.Entities;

namespace EquityMaven.Umb.TestSite.Models
{
    public class FullValuationViewModel
    {

        public static FullValuationViewModel GetFullValuationViewModel(int memberId)
        {
            var DataManager = EntityDataManagerService.DataManager;
            var client = DataManager.GetClientByMemberId(memberId);
            PrelimEvaluation preval = null;
            if (client != null)
            {
                preval = DataManager.GetPrelimEvaluations(false).Where(o => o.Email == client.Email).OrderByDescending(o => o.Created).FirstOrDefault();
            }

            var result = new FullValuationViewModel
            {
                ClientId = client?.Id ?? 0,
                MemberId = memberId,
                CompanyName = preval?.CompanyName,
                ISOCurrencySymbol = preval?.ISOCurrencySymbol,
                CurrentCashOnHand = preval?.CurrentCashOnHand ?? 0,
                CurrentInterestBearingDebt = preval?.CurrentInterestBearingDebt ?? 0,
                TRBC2012HierarchicalID = preval?.TRBC2012HierarchicalID,
                IncomeStatement_Revenue = preval?.IncomeStatement_Revenue ?? 0,
                IncomeStatement_CostOfGoodsSold = preval?.IncomeStatement_CostOfGoodsSold ?? 0,
                IncomeStatement_OperatingExpense = preval?.IncomeStatement_OperatingExpense ?? 0
            };
            result.Currencies = DataManager.GetCurrencies().ToList();
            result.Countries = DataManager.GetCountries().ToList();
            return result;
        }

        public int MemberId { get; set; }
        public Int64 ClientId { get; set; }
        public string CompanyName { get; set; }
        public IEnumerable<Currency> Currencies { get; set; }
        public string ISOCurrencySymbol { get; set; }
        public IEnumerable<Country> Countries { get; set; }
        public Guid? SelectedCountryId { get; set; }
        public decimal CurrentCashOnHand { get; set; }
        public decimal CurrentInterestBearingDebt { get; set; }
        public string TRBC2012HierarchicalID { get; set; }
        public decimal IncomeStatement_Revenue { get; set; }
        public decimal IncomeStatement_CostOfGoodsSold { get; set; }
        public decimal IncomeStatement_OperatingExpense { get; set; }
    }
}