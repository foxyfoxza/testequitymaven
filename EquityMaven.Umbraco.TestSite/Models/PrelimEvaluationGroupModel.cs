﻿using EquityMaven.Interfaces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EquityMaven.Umb.TestSite.Models
{
    public class PrelimEvaluationGroupModel: IPrelimEvaluationGroupModel
    {
        public int Year { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public int Total { get; set; }
    }
}