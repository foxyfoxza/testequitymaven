﻿using EquityMaven.Data;
using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EquityMaven.Umb.TestSite.Models
{
    public class TRBCValueModel
    {
        public IEnumerable<TRBCValue> Values { get; set; }
        public IEnumerable<ThompsonReutersBusinessClassification> ThompsonReutersBusinessClassifications { get; set; }
    }
}