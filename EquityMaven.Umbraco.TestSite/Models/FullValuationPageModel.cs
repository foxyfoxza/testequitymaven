﻿using EquityMaven.Data.Entities;
using EquityMaven.Data.HelperEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EquityMaven.Umb.TestSite.Models
{
    public class FullValuationPageModel
    {
        public FullValuationReport FullValuationReport { get; set; }
        public int Page { get; set; }
        public int TotalPages { get; set; }

      
    }  
    

}