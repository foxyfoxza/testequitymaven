﻿using EquityMaven.CommonEnums;
using EquityMaven.Data.Entities;
using EquityMaven.Data.HelperEntities;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EquityMaven.Umb.TestSite.Models
{

    public class FullValuationInputYearModel
    {
        private IEnumerable<object[]> clientInputs;
        private decimal GetValueByAlias(string alias, int index)
        {
            var found = clientInputs.FirstOrDefault(o => ((string)o[0]).ToLower() == alias.ToLower());
            if (found == null)
                throw new ArgumentOutOfRangeException($"{alias} not found in ClientInputs");

            return decimal.Parse(found[index].ToString());
        }

        public FullValuationInputYearModel(IEnumerable<object[]> clientInputs, int index)
        {
            this.clientInputs = clientInputs;
            Calculated_BalanceSheetAnalysis_CurrentRatio = GetValueByAlias("BalanceSheetAnalysis_CurrentRatio", index);
            Calculated_BalanceSheetAnalysis_TotalEBITDA = GetValueByAlias("BalanceSheetAnalysis_TotalEBITDA", index);

            Calculated_BalanceSheetEquityAndLiabilities_ShareholdersEquity = GetValueByAlias("BalanceSheetEquityAndLiabilities_ShareholdersEquity", index);
            Calculated_BalanceSheetEquityAndLiabilities_TotalLiabilities = GetValueByAlias("BalanceSheetEquityAndLiabilities_TotalLiabilities", index);
            Calculated_BalanceSheetEquityAndLiabilitiesCurrentLiabilities = GetValueByAlias("BalanceSheetEquityAndLiabilitiesCurrentLiabilities", index);
            Calculated_BalanceSheetTotalAssets_Total = GetValueByAlias("BalanceSheetTotalAssets_Total", index);
            Calculated_CapitalExpenditure_TTM = GetValueByAlias("CapitalExpenditure_TTM", index);
            Calculated_ExcessCash_AssumedOperatingCashRequiredAsPercentageOfRevenue = GetValueByAlias("ExcessCash_AssumedOperatingCashRequiredAsPercentageOfRevenue", index);
            Calculated_ExcessCash_Total = GetValueByAlias("ExcessCash_Total", index);
            Calculated_SustainableEBITDA_Total = GetValueByAlias("SustainableEBITDA_Total", index);
            Calculated_SustainableEBITDA_TTM = GetValueByAlias("SustainableEBITDA_TTM", index);


            Data = new FullValuationInputYear
            {

                BalanceSheetAnalysis_AverageAccountsPayableDays = GetValueByAlias("BalanceSheetAnalysis_AverageAccountsPayableDays", index),
                BalanceSheetAnalysis_AverageAccountsReceiveableDays = GetValueByAlias("BalanceSheetAnalysis_AverageAccountsReceiveableDays", index),
                BalanceSheetAnalysis_AverageInventoryDays = GetValueByAlias("BalanceSheetAnalysis_AverageInventoryDays", index),

                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = GetValueByAlias("BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold", index),
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = GetValueByAlias("BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold", index),
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = GetValueByAlias("BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold", index),
                BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = GetValueByAlias("BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold", index),

                BalanceSheetEquityAndLiabilities_AccountsPayable = GetValueByAlias("BalanceSheetEquityAndLiabilities_AccountsPayable", index),
                BalanceSheetEquityAndLiabilities_LongTermDebt = GetValueByAlias("BalanceSheetEquityAndLiabilities_LongTermDebt", index),
                BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = GetValueByAlias("BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities", index),
                BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = GetValueByAlias("BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities", index),
                BalanceSheetEquityAndLiabilities_ShareholderLoans = GetValueByAlias("BalanceSheetEquityAndLiabilities_ShareholderLoans", index),

                BalanceSheetEquityAndLiabilities_ShortTermDebt = GetValueByAlias("BalanceSheetEquityAndLiabilities_ShortTermDebt", index),

                BalanceSheetTotalAssets_AccountsReceiveable = GetValueByAlias("BalanceSheetTotalAssets_AccountsReceiveable", index),
                BalanceSheetEquityAndLiabilities_CurrentLiabilities = GetValueByAlias("BalanceSheetTotalAssets_Calculated_CurrentLiabilities", index),
                BalanceSheetTotalAssets_CashAndCashEquivalents = GetValueByAlias("BalanceSheetTotalAssets_CashAndCashEquivalents", index),
                BalanceSheetTotalAssets_FixedAssets = GetValueByAlias("BalanceSheetTotalAssets_FixedAssets", index),
                BalanceSheetTotalAssets_IntangibleAssets = GetValueByAlias("BalanceSheetTotalAssets_IntangibleAssets", index),
                BalanceSheetTotalAssets_Inventory = GetValueByAlias("BalanceSheetTotalAssets_Inventory", index),
                BalanceSheetTotalAssets_OtherCurrentAssets = GetValueByAlias("BalanceSheetTotalAssets_OtherCurrentAssets", index),

                BalanceSheetTotalAssets_TotalAssets = GetValueByAlias("BalanceSheetTotalAssets_TotalAssets", index),
                CapitalExpenditure_CapitalExpenditure = GetValueByAlias("CapitalExpenditure_CapitalExpenditure", index),
                CapitalExpenditure_PercentageForecastCapitalExpenditure = GetValueByAlias("CapitalExpenditure_PercentageForecastCapitalExpenditure", index),

                ExcessCash_RevenueTTM = GetValueByAlias("ExcessCash_RevenueTTM", index),

                IncomeStatement_CostOfGoodsSold = GetValueByAlias("IncomeStatement_CostOfGoodsSold", index),
                IncomeStatement_DepreciationAndAmortisationExpense = GetValueByAlias("IncomeStatement_DepreciationAndAmortisationExpense", index),

                IncomeStatement_InterestExpense = GetValueByAlias("IncomeStatement_InterestExpense", index),
                IncomeStatement_NonCashExpense = GetValueByAlias("IncomeStatement_NonCashExpense", index),
                IncomeStatement_OperatingExpense = GetValueByAlias("IncomeStatement_OperatingExpense", index),
                IncomeStatement_Revenue = GetValueByAlias("IncomeStatement_Revenue", index),
                IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = GetValueByAlias("IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue", index),
                IncomeStatementAnalysis_PercentageGrossProfitMargin = GetValueByAlias("IncomeStatementAnalysis_PercentageGrossProfitMargin", index),
                IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = GetValueByAlias("IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue", index),
                IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = GetValueByAlias("IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue", index),
                IncomeStatementAnalysis_PercentageRevenueGrowth = GetValueByAlias("IncomeStatementAnalysis_PercentageRevenueGrowth", index),
                SustainableEBITDA_NonRecurringExpenses = GetValueByAlias("SustainableEBITDA_NonRecurringExpenses", index),
                SustainableEBITDA_NonRecurringIncome = GetValueByAlias("SustainableEBITDA_NonRecurringIncome", index),




            };

        }

        public FullValuationInputYear Data { get; set; }
        public decimal Calculated_BalanceSheetAnalysis_CurrentRatio { get; private set; }
        public decimal Calculated_BalanceSheetAnalysis_TotalEBITDA { get; private set; }
        public decimal Calculated_BalanceSheetEquityAndLiabilities_ShareholdersEquity { get; private set; }
        public decimal Calculated_BalanceSheetEquityAndLiabilities_TotalLiabilities { get; private set; }
        public decimal Calculated_BalanceSheetEquityAndLiabilitiesCurrentLiabilities { get; private set; }
        public decimal Calculated_BalanceSheetTotalAssets_Total { get; private set; }
        public decimal Calculated_CapitalExpenditure_TTM { get; private set; }
        public decimal Calculated_ExcessCash_AssumedOperatingCashRequiredAsPercentageOfRevenue { get; private set; }
        public decimal Calculated_ExcessCash_Total { get; private set; }
        public decimal Calculated_SustainableEBITDA_Total { get; private set; }
        public decimal Calculated_SustainableEBITDA_TTM { get; private set; }
    }
    public class FullValuationSaveModel
    {
        public IEnumerable<Object[]> clientInputs { get; set; }
        public Int64 ClientId { get; set; }
        public int MemberId { get; set; }
        public Int64 Id { get; set; }

        public DateTime LastFinancialYearEnd { get; set; }

        public DateTime ValuationDate { get; set; }

        public string ThreeLetterISORegionName { get; set; }
        public string ISOCurrencySymbol { get; set; }
        public FullValuationInputYearModel InputsActualPriorModel { get; private set; }
        public FullValuationInputYearModel InputsActualCurrentModel { get; private set; }
        public FullValuationInputYearModel InputsBudgetModel { get; private set; }
        public FullValuationInputYearModel InputsForecast1Model { get; private set; }
        public FullValuationInputYearModel InputsForecast2Model { get; private set; }

        private decimal GetValueByAlias(string alias, int index)
        {
            var found = clientInputs.FirstOrDefault(o => ((string)o[0]).ToLower() == alias.ToLower());
            if (found == null)
                throw new ArgumentOutOfRangeException($"{alias} not found in ClientInputs");

            return decimal.Parse(found[index].ToString());
        }

        public FullValuationInput AsFullValuation()
        {
            var model = AsFullValuationModel();

            var result = new FullValuationInput
            {
                //ActualCurrentId = Guid.Empty,
                //ActualPriorId = Guid.Empty,
                //BudgetId = Guid.Empty,
                //Forecast1Id = Guid.Empty,
                //Forecast2Id = Guid.Empty,
                //Client = null,
                ThreeLetterISORegionName = model.ThreeLetterISORegionName,

                Created = DateTime.UtcNow,

                ISOCurrencySymbol = model.ISOCurrencySymbol,

                Description = "",
                ClientId = model.ClientId,
                CompanyName = model.CompanyName,
                CurrentCashOnHand = model.CurrentCashOnHand,
                LastFinancialYearEnd = model.LastFinancialYearEnd,
                ValuationDate = model.ValuationDate,
                DateOfCommencement  = model.DateOfCommencement,
                Id = model.Id,
            };

            var _ActualCurrent = Read(result, model.InputsActualCurrent, FinancialYearType.ActualCurrent);
            var _ActualPrior = Read(result, model.InputsActualPrior, FinancialYearType.ActualPrior);
            var _Budget = Read(result, model.InputsBudget, FinancialYearType.Budget);
            var _Forecast1 = Read(result, model.InputsForecast1, FinancialYearType.Forecast1);
            var _Forecast2 = Read(result, model.InputsForecast2, FinancialYearType.Forecast2);

            //_ActualCurrent.Prior = _ActualPrior;
            //_ActualPrior.Next = _ActualCurrent;

            //_Budget.Prior = _ActualCurrent;
            //_ActualCurrent.Next = _Budget;

            //_Forecast1.Prior = _Budget;
            //_Budget.Next = _Forecast1;

            //_Forecast2.Prior = _Forecast1;
            //_Forecast1.Next = _Forecast2;


            result.FinancialYears.Add(_ActualCurrent);
            result.FinancialYears.Add(_ActualPrior);
            result.FinancialYears.Add(_Budget);
            result.FinancialYears.Add(_Forecast1);
            result.FinancialYears.Add(_Forecast2);

            return result;
        }

        private FullValuationInputYear Read(FullValuationInput fv, IFullValuationInputYear fvci, FinancialYearType fyt)
        {
            return new FullValuationInputYear
            {
                FullValuationInput = fv,
                FinancialYearType = fyt,
                //BalanceSheetAnalysis_AverageAccountsPayableDays = fvci.BalanceSheetAnalysis_AverageAccountsPayableDays,
                //BalanceSheetAnalysis_AverageAccountsReceiveableDays = fvci.BalanceSheetAnalysis_AverageAccountsReceiveableDays,
                //BalanceSheetAnalysis_AverageInventoryDays = fvci.BalanceSheetAnalysis_AverageInventoryDays,
                //BalanceSheetAnalysis_CurrentRatio = fvci.BalanceSheetAnalysis_CurrentRatio,
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = fvci.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = fvci.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = fvci.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = fvci.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold,
                BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = fvci.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold,
                //BalanceSheetAnalysis_TotalEBITDA = fvci.BalanceSheetAnalysis_TotalEBITDA,
                BalanceSheetEquityAndLiabilities_AccountsPayable = fvci.BalanceSheetEquityAndLiabilities_AccountsPayable,
                //BalanceSheetEquityAndLiabilities_LongTermDebt = fvci.BalanceSheetEquityAndLiabilities_LongTermDebt,
                //BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = fvci.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities,
                //BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = fvci.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities,
                //BalanceSheetEquityAndLiabilities_ShareholderLoans = fvci.BalanceSheetEquityAndLiabilities_ShareholderLoans,
                //BalanceSheetEquityAndLiabilities_ShareholdersEquity = fvci.BalanceSheetEquityAndLiabilities_ShareholdersEquity,
                //BalanceSheetEquityAndLiabilities_ShortTermDebt = fvci.BalanceSheetEquityAndLiabilities_ShortTermDebt,
                //BalanceSheetEquityAndLiabilities_TotalLiabilities = fvci.BalanceSheetEquityAndLiabilities_TotalLiabilities,
                //BalanceSheetEquityAndLiabilitiesCurrentLiabilities = fvci.BalanceSheetEquityAndLiabilitiesCurrentLiabilities,
                BalanceSheetTotalAssets_AccountsReceiveable = fvci.BalanceSheetTotalAssets_AccountsReceiveable,
                /*BalanceSheetTotalAssets_Calculated_CurrentLiabilities = fvci.BalanceSheetTotalAssets_Calculated_CurrentLiabilities*/
                BalanceSheetTotalAssets_CashAndCashEquivalents = fvci.BalanceSheetTotalAssets_CashAndCashEquivalents,
                BalanceSheetTotalAssets_FixedAssets = fvci.BalanceSheetTotalAssets_FixedAssets,
                BalanceSheetTotalAssets_IntangibleAssets = fvci.BalanceSheetTotalAssets_IntangibleAssets,
                BalanceSheetTotalAssets_Inventory = fvci.BalanceSheetTotalAssets_Inventory,
                BalanceSheetTotalAssets_OtherCurrentAssets = fvci.BalanceSheetTotalAssets_OtherCurrentAssets,
                //BalanceSheetTotalAssets_Total = fvci.BalanceSheetTotalAssets_Total,
                //BalanceSheetTotalAssets_TotalAssets = fvci.BalanceSheetTotalAssets_TotalAssets,
                CapitalExpenditure_CapitalExpenditure = fvci.CapitalExpenditure_CapitalExpenditure,
                CapitalExpenditure_PercentageForecastCapitalExpenditure = fvci.CapitalExpenditure_PercentageForecastCapitalExpenditure,
                //CapitalExpenditure_TTM = fvci.CapitalExpenditure_TTM,
                //ExcessCash_AssumedOperatingCashRequiredAsPercentageOfRevenue = fvci.ExcessCash_AssumedOperatingCashRequiredAsPercentageOfRevenue,
                //ExcessCash_RevenueTTM = fvci.ExcessCash_RevenueTTM,
                //ExcessCash_Total = fvci.ExcessCash_Total,
                IncomeStatement_CostOfGoodsSold = fvci.IncomeStatement_CostOfGoodsSold,
                IncomeStatement_DepreciationAndAmortisationExpense = fvci.IncomeStatement_DepreciationAndAmortisationExpense,
                IncomeStatement_InterestExpense = fvci.IncomeStatement_InterestExpense,
                IncomeStatement_NonCashExpense = fvci.IncomeStatement_NonCashExpense,
                IncomeStatement_OperatingExpense = fvci.IncomeStatement_OperatingExpense,
                IncomeStatement_Revenue = fvci.IncomeStatement_Revenue,
                IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = fvci.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue,
                IncomeStatementAnalysis_PercentageGrossProfitMargin = fvci.IncomeStatementAnalysis_PercentageGrossProfitMargin,
                IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = fvci.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue,
                IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = fvci.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue,
                IncomeStatementAnalysis_PercentageRevenueGrowth = fvci.IncomeStatementAnalysis_PercentageRevenueGrowth,
                SustainableEBITDA_NonRecurringExpenses = fvci.SustainableEBITDA_NonRecurringExpenses,
                SustainableEBITDA_NonRecurringIncome = fvci.SustainableEBITDA_NonRecurringIncome,
                //SustainableEBITDA_Total = fvci.SustainableEBITDA_Total,
                //SustainableEBITDA_TTM = fvci.SustainableEBITDA_TTM,
            };
        }


        private FullValuationModel AsFullValuationModel()
        {
            InputsActualPriorModel = Read(1);
            InputsActualCurrentModel = Read(2);
            InputsBudgetModel = Read(3);
            InputsForecast1Model = Read(4);
            InputsForecast2Model = Read(5);

            return new FullValuationModel
            {
                InputsActualPrior = InputsActualPriorModel.Data,
                InputsActualCurrent = InputsActualCurrentModel.Data,
                InputsBudget = InputsBudgetModel.Data,
                InputsForecast1 = InputsForecast1Model.Data,
                InputsForecast2 = InputsForecast2Model.Data,
                MemberId = MemberId,
                ClientId = ClientId,
                Id = Id,
                LastFinancialYearEnd = LastFinancialYearEnd,
                ValuationDate = ValuationDate,
                ThreeLetterISORegionName = ThreeLetterISORegionName,
                ISOCurrencySymbol = ISOCurrencySymbol,
            };
        }

        private FullValuationInputYearModel Read(int index)
        {
            return new FullValuationInputYearModel(clientInputs, index);
        }
    }
}