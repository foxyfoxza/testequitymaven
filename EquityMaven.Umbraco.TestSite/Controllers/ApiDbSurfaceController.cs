﻿using EquityMaven.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Mvc;

namespace EquityMaven.Umb.TestSite.Controllers
{
    public class ApiDbSurfaceController : SurfaceController
    {
        protected EntityDataManager DataManager;
        public ApiDbSurfaceController()
        {

            DataManager = EquityMaven.Repository.EntityDataManagerService.DataManager;
        }
    }
}