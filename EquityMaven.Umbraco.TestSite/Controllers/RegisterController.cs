﻿using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Web.Mvc;
using EquityMaven.Umb.TestSite.Models;
using System.Linq;
using EquityMaven.Data.Entities;
using System;
using Umbraco.Web.Models;
using umbraco.cms.businesslogic.member;
using EquityMaven.Umb.TestSite.Constants;

namespace EquityMaven.Umb.TestSite.Controllers
{
    public class RegisterController : ApiDbSurfaceController
    {

        
        public RegisterController()
        {
            

        }


        public ActionResult Register(RegisterModel registerModel)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();
            var test = Request.Url.ToString();

            MembershipCreateStatus status;
            registerModel.MemberTypeAlias = EMGeneral.DEFAULTMEMBERTYPEALIAS;
            var member = Members.RegisterMember(registerModel, out status, true);
            if (status == MembershipCreateStatus.Success)
            {
                var existingPreval = DataManager.GetPrelimEvaluation().Where(o => o.Email.ToLower() == member.Email).OrderByDescending(o => o.Created).FirstOrDefault();
                var client = DataManager.GetClientByEmail(registerModel.Email);
                if (client == null)
                {
                    client = new Client { CompanyName = existingPreval?.CompanyName, Email = member.Email, MemberId = 0 };
                    DataManager.SaveClient(client);
                }
                if (existingPreval != null)
                {
                    var clientProfile = new ClientProfile (client, existingPreval);
                    DataManager.SaveClientProfiles(clientProfile);
                }

                return Redirect("/");

            }
            else return CurrentUmbracoPage();

        }
    }
}