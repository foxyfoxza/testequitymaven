﻿using EquityMaven.Data.Entities;
using EquityMaven.Repository;
using EquityMaven.Umb.TestSite.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Umbraco.Web.UI.JavaScript;
using Umbraco.Web.WebApi;

namespace EquityMaven.Umb.TestSite.Api
{
    public class EntityMavenDataApiController : UmbracoApiController
    {
        protected EntityDataManager DataManager;
        public EntityMavenDataApiController()
        {
            DataManager = EquityMaven.Repository.EntityDataManagerService.DataManager;
            EnsureClientsCreated();
        }

        private void EnsureClientsCreated()
        {
            //todo - remove this , only here while testing and re-recreating db
            var members = Services.MemberService.GetMembersByMemberType(EMGeneral.DEFAULTMEMBERTYPEALIAS);
            var existing = DataManager.GetClients().ToList();
            var clients = new List<Client>();
            foreach (var member in members.Where(m => !existing.Any(c => c.Email.ToLower() == m.Email.ToLower())))
            {
                clients.Add(new Client { CompanyName = "", Email = member.Email, MemberId = member.Id });
            }
            if (clients.Any())
                DataManager.SaveClients(clients);
        }
    }
}