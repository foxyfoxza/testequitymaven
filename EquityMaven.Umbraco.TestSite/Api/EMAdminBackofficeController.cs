﻿using EquityMaven.Data;
using EquityMaven.Data.Entities;
using EquityMaven.ImportExport;
using EquityMaven.Umb.Custom.Helpers;
using EquityMaven.Umb.TestSite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace EquityMaven.Umb.TestSite.Api
{




    public class EMAdminBackofficeController : EntityMavenDataApiController
    {
        [HttpGet]
        public IHttpActionResult GetThompsonReutersBusinessClassifications(Int64 inputBatchId)
        {
            try
            {
                var result = DataManager.GetThompsonReutersBusinessClassifications(inputBatchId, true).ToList();


                return Json<IEnumerable<ThompsonReutersBusinessClassification>>(result);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }



        [HttpGet]
        public IHttpActionResult GetInputBatches(bool includeInactive = false, int batchTypeId = 0)
        {
            try
            {

                //var result = new List<object>();
                var records = DataManager.GetInputBatches(includeInactive, (BatchType)batchTypeId).ToList();
                //var result = records.Select(o => o.Activity).Distinct().Select(name => new TRBCActivity(records.FirstOrDefault(o => o.Activity == name))); 
                return Json<IEnumerable<InputBatch>>(records);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        [HttpPost]
        public IHttpActionResult UpdateInputBatches(IEnumerable<InputBatch> items)
        {

            try
            {
                //foreach(var item in items)
                //{
                //    DataManager.SaveInputBatch(item);

                //}
                DataManager.SaveInputBatches(items);
                //var updated = DataManager.AddUpdate(value, false);
                return Json<IEnumerable<InputBatch>>(items);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public async Task<HttpResponseMessage> ImportBatchData(int batchTypeValue, DateTime ActiveFrom, string Description = null)
        {
            var batchType = (BatchType)batchTypeValue;
            Description = string.IsNullOrEmpty(Description.Replace("*", "")) ? ActiveFrom.ToString("yyyyMMdd hhmmss") : Description;
            var inputBatch = new InputBatch { Description = Description, ActiveFrom = ActiveFrom, BatchType = batchType };

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            //TODO periodically clear this path
            //generate a new folder for the file 
            string uploadFolder = Path.Combine(HttpContext.Current.Server.MapPath("~/App_Data/TempFileUploads"), Guid.NewGuid().ToString());
            Directory.CreateDirectory(uploadFolder);
            var provider = new CustomMultipartFormDataStreamProvider(uploadFolder);
            var result = await Request.Content.ReadAsMultipartAsync(provider);
            List<InputBatch> results = new List<InputBatch>();
            foreach (var file in result.FileData)
            {
                var filename = file.LocalFileName;
                var import = new ImportConversion(filename, inputBatch, DataManager.GetCountries(), DataManager.GetMoodysRatings());
                DataManager.ImportData(import);

            }
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(results));
            return response;
             
        }


        [HttpGet]
        public IHttpActionResult GetTRBCDashboard(Int64 inputBatchId)
        {
            try
            {

                var values = DataManager.GetAllTRBCValues(inputBatchId).ToList();
                var result = new TRBCValueModel
                {
                    Values = values,
                    ThompsonReutersBusinessClassifications = DataManager.GetThompsonReutersBusinessClassifications(inputBatchId).ToList(),
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        [HttpGet]
        public IHttpActionResult GetMoodysRatings(Int64 inputBatchId)
        {
            try
            {

                //var result = new List<object>();
                var records = DataManager.GetMoodysRatings(inputBatchId).ToList();
                //var result = records.Select(o => o.Activity).Distinct().Select(name => new TRBCActivity(records.FirstOrDefault(o => o.Activity == name))); 
                return Json<IEnumerable<MoodysRating>>(records);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        [HttpPost]
        public IHttpActionResult UpdateMoodysRatings(IEnumerable<MoodysRating> items)
        {

            try
            {
                foreach (var item in items)
                {
                    DataManager.SaveMoodysRating(item);
                }
                //var updated = DataManager.AddUpdate(value, false);
                return Json<IEnumerable<MoodysRating>>(items);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }


        #region prevaluation

        [HttpGet]
        public IHttpActionResult GetPrelimEvaluations(int? year = null, int? month = null, int? day = null)
        {
            try
            {

                //var result = new List<object>();
                var records = DataManager.GetPrelimEvaluations(true, year, month, day).ToList();
                var trbcCodes = records.Select(o => o.TRBC2012HierarchicalID).Distinct();
                var trbcs = DataManager.GetThompsonReutersBusinessClassifications().Where(o => trbcCodes.Contains(o.ActivityCode)).ToList();
                foreach (var item in records)
                {
                    item.ThomsonReutersBusinessClassification= trbcs.FirstOrDefault(o => o.Id == item.Id);
                }
                return Json<IEnumerable<PrelimEvaluation>>(records);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        [HttpGet]
        public IHttpActionResult GetPrelimEvaluationGroups(int? year = null, int? month = null)
        {
            try
            {

                //var result = new List<object>();
                IEnumerable<int> years = year.HasValue ? new List<int> { year.Value } : DataManager.GetPrelimEvaluation().Select(o => o.Created.Year).Distinct().AsEnumerable();
                var groups = new List<PrelimEvaluationGroupModel>();
                foreach (var y in years)
                {
                    if (year.HasValue)
                    {

                        IEnumerable<int> months = month.HasValue ? new List<int> { month.Value } : DataManager.GetPrelimEvaluation().Where(o => o.Created.Year == y).Select(o => o.Created.Month).Distinct().ToList();
                        foreach (var m in months)
                        {
                            IEnumerable<int> days = DataManager.GetPrelimEvaluation().Where(o => o.Created.Year == y && o.Created.Month == m).Select(o => o.Created.Day).Distinct().ToList();
                            foreach (var d in days)
                            {
                                var total = DataManager.GetPrelimEvaluation().Count(o => o.Created.Year == y && o.Created.Month == m && o.Created.Day == d);
                                groups.Add(new PrelimEvaluationGroupModel { Year = y, Month = m, Day = d, Total = total });
                            }

                        }
                    }
                    else
                    {
                        var total = DataManager.GetPrelimEvaluation().Count(o => o.Created.Year == y);
                        groups.Add(new PrelimEvaluationGroupModel { Year = y, Total = total });
                    }
                }

                //var result = records.Select(o => o.Activity).Distinct().Select(name => new TRBCActivity(records.FirstOrDefault(o => o.Activity == name))); 
                return Json<IEnumerable<PrelimEvaluationGroupModel>>(groups);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #endregion

    }
}