﻿
using EquityMaven.Data.Entities;
using EquityMaven.Data.HelperEntities;
using EquityMaven.Umb.TestSite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using Umbraco.Web.WebApi;

namespace EquityMaven.Umb.TestSite.Api
{

    [Route("api/[controller]")]
    public class FullValuationController : EntityMavenDataApiController
    {


        [HttpGet]
        public string GetPublishReport(int fullValuationInputId)
        {
            var user = Membership.GetUser();
            int memberId = user != null ? Convert.ToInt32(user.ProviderUserKey) : 0;
            var model = DataManager.GetFullValuationModel(memberId, fullValuationInputId);

            return $"done valuationdate: {model.ValuationDate}";
        }

        [HttpPost]
        public FullValuationModel Save2()
        {
            HttpContent requestContent = Request.Content;
            string jsonContent = requestContent.ReadAsStringAsync().Result;

            try
            {
                var fvsm = Newtonsoft.Json.JsonConvert.DeserializeObject<FullValuationSaveModel>(jsonContent);
                var fv = fvsm.AsFullValuation();

                DataManager.SaveFullValuation(fv);
                //var result = GetFullValuationModel();
                //if (!ModelState.IsValid)
                var client = DataManager.GetClientByClientId(fv.ClientId);
                var fvfys = (fv.FinancialYears != null && fv.FinancialYears.Any()) ? fv.FinancialYears : DataManager.GetFullValuationInputYears(fv.Id).AsEnumerable();

                var fvm = FullValuationModel.ReadFullValuation(fv, client, fvfys);
                return fvm;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;

            }
        }
         
        [HttpGet]
        public string GetCurrencies(Int64? inputBatchId = null)
        {
            try
            {

                IEnumerable<Currency> result = EquityMaven.Repository.EntityDataManagerService.DataManager.GetCurrencies(inputBatchId).ToList();
                //return Json<IEnumerable<Currency>>(result);


                return Newtonsoft.Json.JsonConvert.SerializeObject(result);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public string GetReport(int fullValuationInputId)
        {
            var user = Membership.GetUser();
            int memberId = user != null ? Convert.ToInt32(user.ProviderUserKey) : 0;
            var model = DataManager.GetFullValuationModel(memberId, fullValuationInputId);
            //var report = Custom.Helpers.ValuationReportRender.RenderReport("~/Views/Partials/Forms/Themes/EMPreValuation/Report.cshtml", null);
            //var htmlTemplateMerged = EquityMaven.Umb.Custom.Helpers.RazorHelper.ParseWithRazorView<FullValuationModel>(model, "~/Views/Partials/Valuation/FullReport.cshtml");
            EquityMaven.Umb.Custom.Helpers.RazorHelper.ParseWithRazorViewToPDF<FullValuationModel>(model, $"~/Views/Partials/Valuation/RenderReportPages.cshtml");
            //"~/Views/Partials/Forms/Themes/EMPreValuation/Report.cshtml"
            return "done";
//E:\Work\BlueGrass\equity_maven\Source\EquityMaven.Umbraco.TestSite\Views\Partials\Valuation\RenderReportPages.cshtml
        }


    }
}