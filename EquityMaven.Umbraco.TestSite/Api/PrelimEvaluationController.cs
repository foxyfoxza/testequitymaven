﻿using EquityMaven.Data;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces;
using EquityMaven.Repository;
using EquityMaven.Umb.TestSite.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Web.WebApi;

namespace EquityMaven.Umb.TestSite.Api
{

    [Route("api/[controller]")]
    public class PrelimEvaluationController : EntityMavenDataApiController//UmbracoApiController
    {

     
        [HttpGet]
        public IHttpActionResult GetThomsonReutersBusinessClassifications()
        {
            try
            {
                IEnumerable<TRBCValue> result = DataManager.GetTRBCValues().ToList();
                return Ok(result);
                 
            }
            catch (Exception ex)
            {
                throw ex;
                //return new JsonNetResult() { Data = new { Success = false, Data = ex } };
                //return  JsonNetResult.From(null, false, $"Error getting ThomsonReutersBusinessClassifications");
            }
        }
         
        [HttpGet]
        public IHttpActionResult GetThompsonReutersBusinessClassifications(Int64? inputBatchId = null)
        {
            try
            {
               
                IEnumerable<ThompsonReutersBusinessClassification> result = DataManager.GetThompsonReutersBusinessClassifications(inputBatchId, true).ToList();
                return Json<IEnumerable<ThompsonReutersBusinessClassification>>(result);
                 
            }
            catch (Exception ex)
            {
                throw ex;
                 
            }
        }

        [HttpGet]
        public IHttpActionResult GetCurrencies(Int64? inputBatchId = null)
        {
            try
            {

                IEnumerable<Currency> result = DataManager.GetCurrencies(inputBatchId).ToList();
                //return Json<IEnumerable<Currency>>(result);
                return Json(result);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        [HttpGet]
        public IHttpActionResult GetCountries(Int64? inputBatchId = null)
        {
            try
            {

                IEnumerable<Country> result = DataManager.GetCountries().ToList();
                //return Json<IEnumerable<Currency>>(result);
                return Json(result);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        [HttpPost]
        public IHttpActionResult Add(object value)
        {
            try
            {
                return Ok(value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [HttpPost]
        public string Save2()
        {
            string _content = null;
            Request.Content.ReadAsStreamAsync().ContinueWith(x => {
                using (var sr = new StreamReader(x.Result))
                {
                    _content = sr.ReadToEnd();
                }
            }).Wait();
             
            var model = Newtonsoft.Json.JsonConvert.DeserializeObject<PrelimEvaluationModel>(_content);
            var pv = model.AsPrelimEvaluation();
            var gd = DataManager.GetGlobalDefaults();
            //var currency = DataManager.GetCurrencies().FirstOrDefault(o => o.ISOCurrencySymbol == model.ISOCurrencySymbol);
            pv.PercentageOperatingCashRequired = gd.PercentageOperatingCashRequired;
            
            DataManager.SavePrelimEvaluation(pv);


            //if (!ModelState.IsValid)
            return Newtonsoft.Json.JsonConvert.SerializeObject(pv) ;
        }


        [HttpPost]

        public IHttpActionResult SavePrelimEvaluation(PrelimEvaluationModel value)
        {
            try
            {
                var clientPreval = value.AsPrelimEvaluation();
                //var currency = DataManager.GetCurrencies().FirstOrDefault(o => o.ISOCurrencySymbol == value.ISOCurrencySymbol);
                clientPreval.ISOCurrencySymbol = value.ISOCurrencySymbol;
                TRBCValue trbc = DataManager.GetTRBCValue(clientPreval.TRBC2012HierarchicalID);
                //clientPreval.TRBCActivityId = trbc == null ? null : trbc.Activity.Id;
                decimal? _EV_EBITDA_WorldMedian = ((Decimal?)trbc?.World).HasValue ? trbc.World.Value : (decimal?)null;
                if (!_EV_EBITDA_WorldMedian.HasValue)
                    throw new Exception($"No World Median defined for {clientPreval.TRBC2012HierarchicalID}");
                clientPreval.EV_EBITDA_WorldMedian = _EV_EBITDA_WorldMedian.Value;
                var globalDefaults = DataManager.GetGlobalDefaults();
                if (globalDefaults == null)
                    throw new Exception("No Global Defaults defined");
                clientPreval.IlliquidityDiscount = globalDefaults.IlliquidityDiscount;
                clientPreval.PercentageOperatingCashRequired = globalDefaults.PercentageOperatingCashRequired;
                DataManager.SavePrelimEvaluation(clientPreval);


                //var test_IncomeStatement_EBITDA = value.IncomeStatement_EBITDA;// 7 000 001.00 
                //var test_EV_EBITDA_WorldMedian = value.EV_EBITDA_WorldMedian;// 11.71 
                //var test_EV_EBITDA_Valuation_EnterpriseValuePreIlliquidityDiscount = value.EV_EBITDA_Valuation_EnterpriseValuePreIlliquidityDiscount;// 81 945 624.38 
                //var test_ExcessCash_ExcessCash = value.ExcessCash_ExcessCash;//1 279 999.02
                //var test_CurrentInterestBearingDebt = value.CurrentInterestBearingDebt;//-999 999.00 
                //var test_EV_EBITDA_Valuation_EquityValuePreIlliquidityDiscount = value.EV_EBITDA_Valuation_EquityValuePreIlliquidityDiscount;// 82 225 624.40 



                //var test_IlliquidityDiscount = value.IlliquidityDiscount;//-28 778 968.54 
                //var test_EV_EBITDA_Valuation_EquityValuePostIlliquidityDiscount = value.EV_EBITDA_Valuation_EquityValuePostIlliquidityDiscount;// 53 446 655.86 


                return Json<PrelimEvaluation>(clientPreval);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public IHttpActionResult AddPrelimEvaluation(PrelimEvaluation value)
        {
            try
            {

                return Ok(value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}