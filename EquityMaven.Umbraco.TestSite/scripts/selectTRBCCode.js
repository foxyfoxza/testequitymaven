﻿ 
var selectTRBCCode = function (_EconomicSectorSelect, _BusinessSectorSelect, _IndustryGroupSelect, _IndustrySelect, _ActivitySelect, _Activity)
{
    var BusinessSectors = _BusinessSectorSelect.querySelectorAll('option');
    var IndustryGroup = _IndustryGroupSelect.querySelectorAll('option');
    var Industries = _IndustrySelect.querySelectorAll('option');
    var Activities = _ActivitySelect.querySelectorAll('option');

    if (jQuery(_EconomicSectorSelect).data('options') == undefined) {
        jQuery(_EconomicSectorSelect).data('options', jQuery(BusinessSectors).clone());
    }

    if (jQuery(_BusinessSectorSelect).data('options') == undefined) {
        jQuery(_BusinessSectorSelect).data('options', jQuery(IndustryGroup).clone());
    }
    if (jQuery(_IndustryGroupSelect).data('options') == undefined) {
        jQuery(_IndustryGroupSelect).data('options', jQuery(Industries).clone());
    }
    if (jQuery(_IndustrySelect).data('options') == undefined) {
        jQuery(_IndustrySelect).data('options', jQuery(Activities).clone());
    }
 
    _SelectChange = function(selectMaster,selectSlave)
    {
        var id = jQuery(selectMaster).val();
        var options2 = [];
        $.each(jQuery(selectMaster).data('options'), function (e, f) {
            var testfilter = f.getAttribute("data-filter");
            if (testfilter == id || testfilter == "")
                options2.push(f);
        });
        if (options2.length <= 1)
        {
            jQuery(selectSlave).html("");
        }
        else
        {
            jQuery(selectSlave).html(options2);
        }

        jQuery(selectSlave).change();
    }
    //defaults 
    _SelectChange(_EconomicSectorSelect, _BusinessSectorSelect);
    _SelectChange(_BusinessSectorSelect, _IndustryGroupSelect);
    _SelectChange(_IndustryGroupSelect, _IndustrySelect);
    _SelectChange(_IndustrySelect, _ActivitySelect);

    jQuery(_EconomicSectorSelect).change(function () {
        _SelectChange(this, _BusinessSectorSelect)
    });

    jQuery(_BusinessSectorSelect).change(function () {
        _SelectChange(this, _IndustryGroupSelect)
    });

    jQuery(_IndustryGroupSelect).change(function () {
        _SelectChange(this, _IndustrySelect)
    });

    jQuery(_IndustrySelect).change(function () {
        _SelectChange(this, _ActivitySelect);
    });

    jQuery(_ActivitySelect).change(function () {
        var testVal = jQuery(this).val();
        if (jQuery(_Activity).val() != testVal) {
            _Activity.value = testVal;
        }
    });

     
}
 