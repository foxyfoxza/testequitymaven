﻿

var EMValuationIncomeStatementApp = angular.module('EMValuationIncomeStatementApp', []);


function emValuationIncomeStatementResource($q, $http) {
    return {
        saveData: function (_data, callback) {
            $.ajax({
                url: "/umbraco/api/fullvaluation/Save2",
                type: 'post',
                dataType: 'json',
                async: false,
                cache: false,
                timeout: 30000,
                success: function (data) {
                    callback(data);
                },
                error: function (request, status, error) {
                    alert(request.responseText);
                },
                data: JSON.stringify(_data)
            });
        },


        GetFullValuation: function (callback) {
            return $.ajax({
                url: "/umbraco/api/fullvaluation/GetFullValuationModel",
                type: 'get',
                dataType: 'json',
                async: false,
                cache: false,
                timeout: 30000,
                success: function (data) {
                    callback(data);
                },
                error: function (request, status, error) {
                    alert(request.responseText);
                }
            });
        }


    };
};
angular.module("EMValuationIncomeStatementApp").factory("emValuationIncomeStatementResource", emValuationIncomeStatementResource);



EMValuationIncomeStatementApp.directive('ngcFocus', ['$parse', function ($parse) {
    return function (scope, element, attr) {
        var fn = $parse(attr['ngcFocus']);
        element.bind('focus', function (event) {
            scope.$apply(function () {
                fn(scope, { $event: event });
            });
        });
    }
}]);

EMValuationIncomeStatementApp.directive('ngcBlur', ['$parse', function ($parse) {
    return function (scope, element, attr) {
        var fn = $parse(attr['ngcBlur']);
        element.bind('blur', function (event) {
            scope.$apply(function () {
                fn(scope, { $event: event });
            });
        });
    }
}]);

EMValuationIncomeStatementApp.controller('EMValuationIncomeStatementCtrl', ['$scope', '$http', '$location', 'emValuationIncomeStatementResource', function ($scope, $http, $location, emValuationIncomeStatementResource) {





    $scope.LoadValuation = function (data) {
        $scope.FieldId = data.FieldId;
        $scope.Data = data;
        $scope.InputsActualPrior = data.InputsActualPrior;
        $scope.InputsActualCurrent = data.InputsActualCurrent;
        $scope.InputsBudget = data.InputsBudget;
        $scope.InputsForecast1 = data.InputsForecast1;
        $scope.InputsForecast2 = data.InputsForecast2;


        var currentLastFinancialYearEnd = new Date(data.LastFinancialYearEnd);

        var _year = currentLastFinancialYearEnd.getFullYear()

        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var _month = months[currentLastFinancialYearEnd.getMonth()];

        var _InputsActualPrior = "Actual " + _month + " " + (_year - 1);
        var _InputsActualCurrent = "Actual " + _month + " " + _year;
        var _InputsBudget = "Budget " + _month + " " + (_year + 1);
        var _InputsForecast1 = "Forecast " + _month + " " + (_year + 2);
        var _InputsForecast2 = "Forecast " + _month + " " + (_year + 3);

        $scope.Headings = { InputsActualPrior: _InputsActualPrior, InputsActualCurrent: _InputsActualCurrent, InputsBudget: _InputsBudget, InputsForecast1: _InputsForecast1, InputsForecast2: _InputsForecast2 };

        $scope.InputsActualCurrent.prior = $scope.InputsActualPrior;
        $scope.InputsBudget.prior = $scope.InputsActualCurrent;
        $scope.InputsForecast1.prior = $scope.InputsBudget;
        $scope.InputsForecast2.prior = $scope.InputsForecast1;

        $scope.MonthsSinceLastFinancialYearEnd = data.MonthsSinceLastFinancialYearEnd;
        $scope.ExcessCash_RequiredPercentageOperatingCash = data.ExcessCash_RequiredPercentageOperatingCash;

        $scope.InputsActualPrior.index = 0;
        $scope.InputsActualCurrent.index = 1;
        $scope.InputsBudget.index = 2;
        $scope.InputsForecast1.index = 3;
        $scope.InputsForecast2.index = 4;



        ////temp
        //$scope.InputsBudget.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = .135;
        //$scope.InputsForecast1.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = .135;
        //$scope.InputsForecast2.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = .135;



        //$scope.InputsBudget.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = .1;
        //$scope.InputsForecast1.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = .1;
        //$scope.InputsForecast2.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = .1;


        //$scope.InputsBudget.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = .1;
        //$scope.InputsForecast1.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = .1;
        //$scope.InputsForecast2.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = .1;

        //$scope.InputsBudget.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = 24.1;
        //$scope.InputsForecast1.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = 24.1;
        //$scope.InputsForecast2.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = 24.1;


        //$scope.InputsBudget.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = 0;
        //$scope.InputsForecast1.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = 0.0001;
        //$scope.InputsForecast2.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = 0;


        //Percentage($scope, 'ExcessCash_RequiredPercentageOperatingCash');


        Percentage('PercentagesHelper', 'InputsActualPrior', $scope.InputsActualPrior);
        Percentage('PercentagesHelper', 'InputsActualCurrent', $scope.InputsActualCurrent);
        Percentage('PercentagesHelper', 'InputsBudget', $scope.InputsBudget);
        Percentage('PercentagesHelper', 'InputsForecast1', $scope.InputsForecast1);
        Percentage('PercentagesHelper', 'InputsForecast2', $scope.InputsForecast2);

        $scope.Loading = false;
    };




    $scope.FullValuationModel = {};
    var submitBtn = document.getElementsByClassName("btn primary")[0];

    var ourForms = document.getElementsByTagName("form");
    if (ourForms.length) {
        ourForms[0].onsubmit = function () {
            var abc = document.getElementById($scope.FieldId);
            if (abc) {

                delete $scope.InputsActualCurrent.prior;
                delete $scope.InputsBudget.prior;
                delete $scope.InputsForecast1.prior;
                delete $scope.InputsForecast2.prior;

                delete $scope.InputsActualCurrent.PercentagesHelper;
                delete $scope.InputsBudget.PercentagesHelper;
                delete $scope.InputsForecast1.PercentagesHelper;
                delete $scope.InputsForecast2.PercentagesHelper;
                delete $scope.InputsActualPrior.PercentagesHelper;

                var fixValues = function (inputs) {

                    for (var key in inputs) {
                        if (isNaN(inputs[key]) || inputs[key] === Infinity || inputs[key] === null) {
                            inputs[key] = 0;
                        }
                    }
                }

                fixValues($scope.InputsActualPrior);
                fixValues($scope.InputsActualCurrent);
                fixValues($scope.InputsBudget);
                fixValues($scope.InputsForecast1);
                fixValues($scope.InputsForecast2);



                var test = "";
                try {
                    test = JSON.stringify($scope.Data);
                }
                catch (ex) {
                    test = "err"
                }
                abc.value = test;
            }

        }
    }
    $scope.Save = function () {
        var inputs = shallowCopy();
        var result = { clientInputs: inputs, Id: $scope.Data.Id, ClientId: $scope.Data.ClientId, MemberId: $scope.Data.MemberId, CurrentCashOnHand: $scope.Data.CurrentCashOnHand, CurrentInterestBearingDebt: $scope.Data.CurrentInterestBearingDebt, TRBC2012HierarchicalID: $scope.Data.TRBC2012HierarchicalID, LastFinancialYearEnd: $scope.Data.LastFinancialYearEnd, ValuationDate: $scope.Data.ValuationDate }


        result = {
            "clientInputs":
                [
              [
                "IncomeStatement_OperatingExpense",
                19586949,
                24339108,
                4191258.9097499996,
                605406.3932188387,
                72648.76718626064
              ],
              [
                "IncomeStatement_DepreciationAndAmortisationExpense",
                354554,
                448262,
                76204.54000000001,
                11007.3647803,
                1320.883773636
              ]
                ],
            "ClientId": 1,
            "MemberId": 1120,
            "Id": 0,
            "LastFinancialYearEnd": "2017-02-28T00:00:00",
            "ValuationDate": "0001-01-01T00:00:00.0002008",
            "ThreeLetterISORegionName": null,
            "ISOCurrencySymbol": null
        }


        result = {
            "clientInputs": inputs,
            "ClientId": $scope.Data.ClientId,
            "MemberId": $scope.Data.MemberId,
            "Id": $scope.Data.Id,
            "LastFinancialYearEnd": $scope.Data.LastFinancialYearEnd,
            "ValuationDate": $scope.Data.ValuationDate,
            "ThreeLetterISORegionName": "ZAF",
            "ISOCurrencySymbol": "ZAR"
        }



        //console.log(result);
        //console.log(JSON.stringify(result));

        emValuationIncomeStatementResource.saveData(result, $scope.LoadValuation);

    }
    var shallowCopy = function () {

        var copyValues = function (dest, inputs) {
            for (var i in dest) {
                var keyValueArray = dest[i]
                var key = keyValueArray[0];
                var value = null;
                if (!(isNaN(inputs[key]) || inputs[key] === Infinity || inputs[key] === null)) {
                    value = inputs[key];
                }
                keyValueArray.push(value);
            }
        }
        var copyProperties = function (inputs) {
            var propResult = [];
            for (var key in inputs) {
                if (key != 'prior' && key != 'PercentagesHelper') {
                    var keyValueArray = [];
                    keyValueArray.push(key);
                    propResult.push(keyValueArray);
                }
            }
            return propResult;
        }

        var result = copyProperties($scope.InputsActualCurrent);
        copyValues(result, $scope.InputsActualPrior)
        copyValues(result, $scope.InputsActualCurrent)
        copyValues(result, $scope.InputsBudget)
        copyValues(result, $scope.InputsForecast1)
        copyValues(result, $scope.InputsForecast2)

        return result;

    }


    $scope.change = function (item, value) {
        item.Value = value;
    };



    //$scope.init = function (data) {
    //    $scope.FieldId = data.FieldId;
    //    $scope.Data = data;
    //    $scope.InputsActualPrior = data.InputsActualPrior;
    //    $scope.InputsActualCurrent = data.InputsActualCurrent;
    //    $scope.InputsBudget = data.InputsBudget;
    //    $scope.InputsForecast1 = data.InputsForecast1;
    //    $scope.InputsForecast2 = data.InputsForecast2;


    //    var currentLastFinancialYearEnd = new Date(data.LastFinancialYearEnd);

    //    var _year = currentLastFinancialYearEnd.getFullYear()

    //    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    //    var _month = months[currentLastFinancialYearEnd.getMonth()];

    //    var _InputsActualPrior = "Actual " + _month + " " + (_year - 1);
    //    var _InputsActualCurrent = "Actual " + _month + " " + _year;
    //    var _InputsBudget = "Budget " + _month + " " + (_year + 1);
    //    var _InputsForecast1 = "Forecast " + _month + " " + (_year + 2);
    //    var _InputsForecast2 = "Forecast " + _month + " " + (_year + 3);

    //    $scope.Headings = { InputsActualPrior: _InputsActualPrior, InputsActualCurrent: _InputsActualCurrent, InputsBudget: _InputsBudget, InputsForecast1: _InputsForecast1, InputsForecast2: _InputsForecast2 };

    //    $scope.InputsActualCurrent.prior = $scope.InputsActualPrior;
    //    $scope.InputsBudget.prior = $scope.InputsActualCurrent;
    //    $scope.InputsForecast1.prior = $scope.InputsBudget;
    //    $scope.InputsForecast2.prior = $scope.InputsForecast1;

    //    $scope.MonthsSinceLastFinancialYearEnd = data.MonthsSinceLastFinancialYearEnd;
    //    $scope.ExcessCash_RequiredPercentageOperatingCash = data.ExcessCash_RequiredPercentageOperatingCash;

    //    $scope.InputsActualPrior.index = 0;
    //    $scope.InputsActualCurrent.index = 1;
    //    $scope.InputsBudget.index = 2;
    //    $scope.InputsForecast1.index = 3;
    //    $scope.InputsForecast2.index = 4;



    //    ////temp
    //    //$scope.InputsBudget.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = .135;
    //    //$scope.InputsForecast1.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = .135;
    //    //$scope.InputsForecast2.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = .135;



    //    //$scope.InputsBudget.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = .1;
    //    //$scope.InputsForecast1.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = .1;
    //    //$scope.InputsForecast2.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = .1;


    //    //$scope.InputsBudget.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = .1;
    //    //$scope.InputsForecast1.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = .1;
    //    //$scope.InputsForecast2.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = .1;

    //    //$scope.InputsBudget.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = 24.1;
    //    //$scope.InputsForecast1.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = 24.1;
    //    //$scope.InputsForecast2.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = 24.1;


    //    //$scope.InputsBudget.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = 0;
    //    //$scope.InputsForecast1.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = 0.0001;
    //    //$scope.InputsForecast2.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = 0;


    //    //Percentage($scope, 'ExcessCash_RequiredPercentageOperatingCash');


    //    Percentage('PercentagesHelper', 'InputsActualPrior', $scope.InputsActualPrior);
    //    Percentage('PercentagesHelper', 'InputsActualCurrent', $scope.InputsActualCurrent);
    //    Percentage('PercentagesHelper', 'InputsBudget', $scope.InputsBudget);
    //    Percentage('PercentagesHelper', 'InputsForecast1', $scope.InputsForecast1);
    //    Percentage('PercentagesHelper', 'InputsForecast2', $scope.InputsForecast2);


    //}

    $scope.overridden = function (m, f) {
        var fo = f + '_overridden';
        m[fo] = true;
    }
    function roundTo(n, digits) {
        try {
            var negative = false;
            if (digits === undefined) {
                digits = 0;
            }
            if (n < 0) {
                negative = true;
                n = n * -1;
            }
            var multiplicator = Math.pow(10, digits);
            n = parseFloat((n * multiplicator).toFixed(11));
            n = (Math.round(n) / multiplicator).toFixed(2);
            if (negative) {
                n = (n * -1).toFixed(2);
            }
            return Number(n);
        }
        catch (ex) {
            return 0;
        }
    }
    $scope.focusedPercentage = {};




    $scope.percentageFocus = function (_name, p) {
        var pNameEditing = _name + p;
        $scope.focusedPercentage[pNameEditing] = true;
    }

    $scope.percentageBlur = function (_name, p) {
        var pNameEditing = _name + p;
        $scope.focusedPercentage[pNameEditing] = false;
    }
    $scope.roundTo = function (n, digits) {
        try {
            var negative = false;
            if (digits === undefined) {
                digits = 0;
            }
            if (n < 0) {
                negative = true;
                n = n * -1;
            }
            var multiplicator = Math.pow(10, digits);
            n = parseFloat((n * multiplicator).toFixed(11));
            n = (Math.round(n) / multiplicator).toFixed(2);
            if (negative) {
                n = (n * -1).toFixed(2);
            }
            return Number(n);
        }
        catch (ex) {
            return 0;
        }
    }

    var Percentage = function (pName, _name, m) {
        m[pName] = {
            editing: false,
            name: _name,
            getFrom: function (p) {
                var result = this.primary[p] * 100.0;
                var pNameEditing = this.name + p;
                if ($scope.focusedPercentage[pNameEditing])
                    return result;
                else
                    return roundTo(result, 2);
            },
            setTo: function (p, x) {
                //this.primary[p] = Math.floor(x * 100) / 10000;
                this.primary[p] = x / 100;
            },
            primary: m,
            get IncomeStatementAnalysis_PercentageRevenueGrowth() { return this.getFrom('IncomeStatementAnalysis_PercentageRevenueGrowth'); },
            set IncomeStatementAnalysis_PercentageRevenueGrowth(x) { this.setTo('IncomeStatementAnalysis_PercentageRevenueGrowth', x); },

            get IncomeStatementAnalysis_PercentageGrossProfitMargin() {
                return this.getFrom('IncomeStatementAnalysis_PercentageGrossProfitMargin');
            },
            set IncomeStatementAnalysis_PercentageGrossProfitMargin(x) {
                this.setTo('IncomeStatementAnalysis_PercentageGrossProfitMargin', x);
            },

            get IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue() { return this.getFrom('IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue'); },
            set IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue(x) { this.setTo('IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue', x); },

            get IncomeStatementAnalysis_EBITDAMargin() { return this.getFrom('IncomeStatementAnalysis_EBITDAMargin'); },
            set IncomeStatementAnalysis_EBITDAMargin(x) { this.setTo('IncomeStatementAnalysis_EBITDAMargin', x); },

            get IncomeStatementAnalysis_EBITMargin() { return this.getFrom('IncomeStatementAnalysis_EBITMargin'); },
            set IncomeStatementAnalysis_EBITMargin(x) { this.setTo('IncomeStatementAnalysis_EBITMargin', x); },

            get IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue() { return this.getFrom('IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue'); },
            set IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue(x) { this.setTo('IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue', x); },

            get IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue() { return this.getFrom('IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue'); },
            set IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue(x) { this.setTo('IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue', x); },

            get IncomeStatementAnalysis_Calculated_InterestCover() { return this.getFrom('IncomeStatementAnalysis_Calculated_InterestCover'); },
            set IncomeStatementAnalysis_Calculated_InterestCover(x) { this.setTo('IncomeStatementAnalysis_Calculated_InterestCover', x); },

            get BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue() { return this.getFrom('BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue'); },
            set BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue(x) { this.setTo('BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue', x); },

            get BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold() { return this.getFrom('BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold'); },
            set BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold(x) { this.setTo('BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold', x); },

            get BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold() { return this.getFrom('BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold'); },
            set BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold(x) { this.setTo('BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold', x); },

            get BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold() { return this.getFrom('BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold'); },
            set BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold(x) { this.setTo('BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold', x); },

            get BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold() { return this.getFrom('BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold'); },
            set BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold(x) { this.setTo('BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold', x); },

            get CapitalExpenditure_PercentageForecastCapitalExpenditure() { return this.getFrom('CapitalExpenditure_PercentageForecastCapitalExpenditure'); },
            set CapitalExpenditure_PercentageForecastCapitalExpenditure(x) { this.setTo('CapitalExpenditure_PercentageForecastCapitalExpenditure', x); },


        };
    };

    //#region ******************************Income statement*************************

    //#region GrossProfit
    CalculateGrossProfitActual = function (record) {
        record.IncomeStatement_GrossProfit = record.IncomeStatement_Revenue - record.IncomeStatement_CostOfGoodsSold;
    }
    CalculateGrossProfitByMargin = function (record) {
        record.IncomeStatement_GrossProfit = record.IncomeStatement_Revenue * record.IncomeStatementAnalysis_PercentageGrossProfitMargin;
    }

    $scope.$watchCollection('[InputsActualPrior.IncomeStatement_Revenue, InputsActualPrior.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculateGrossProfitActual($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_Revenue, InputsActualCurrent.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculateGrossProfitActual($scope.InputsActualCurrent);
    });
    $scope.$watchCollection('[InputsBudget.IncomeStatement_Revenue, InputsBudget.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculateGrossProfitByMargin($scope.InputsBudget);
    });

    $scope.$watchCollection('[InputsForecast1.IncomeStatement_Revenue, InputsForecast1.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculateGrossProfitByMargin($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatement_Revenue, InputsForecast2.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculateGrossProfitByMargin($scope.InputsForecast2);
    });
    //#endregion GrossProfit


    //#region revenue
    CalculateRevenueByGrowthRate = function (record) {
        if (record.prior) {
            record.IncomeStatement_Revenue = record.prior.IncomeStatement_Revenue * record.IncomeStatementAnalysis_PercentageRevenueGrowth;
        }
    }

    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_Revenue, InputsBudget.IncomeStatementAnalysis_PercentageRevenueGrowth]', function (newValues) {
        CalculateRevenueByGrowthRate($scope.InputsBudget);
    });
    $scope.$watchCollection('[InputsBudget.IncomeStatement_Revenue, InputsForecast1.IncomeStatementAnalysis_PercentageRevenueGrowth]', function (newValues) {
        CalculateRevenueByGrowthRate($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast1.IncomeStatement_Revenue, InputsForecast2.IncomeStatementAnalysis_PercentageRevenueGrowth]', function (newValues) {
        CalculateRevenueByGrowthRate($scope.InputsForecast2);
    });
    //#endregion revenue

    //#region Cost of Goods Sold
    CalculateCostOfGoodsSold = function (record) {
        record.IncomeStatement_CostOfGoodsSold = record.IncomeStatement_Revenue - record.IncomeStatement_GrossProfit;
    }

    $scope.$watchCollection('[InputsBudget.IncomeStatement_Revenue, InputsBudget.IncomeStatement_GrossProfit]', function (newValues) {
        CalculateCostOfGoodsSold($scope.InputsBudget);
    });
    $scope.$watchCollection('[InputsForecast1.IncomeStatement_Revenue, InputsForecast1.IncomeStatement_GrossProfit]', function (newValues) {
        CalculateCostOfGoodsSold($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatement_Revenue, InputsForecast2.IncomeStatement_GrossProfit]', function (newValues) {
        CalculateCostOfGoodsSold($scope.InputsForecast2);
    });
    //#endregion Cost of Goods Sold

    //#region Operating expenses (excluding depreciation & amortisation)
    CalculateOperatingExpense = function (record) {
        record.IncomeStatement_OperatingExpense = record.IncomeStatement_Revenue * record.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue;
    }

    $scope.$watchCollection('[InputsBudget.IncomeStatement_Revenue, InputsBudget.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue]', function (newValues) {
        CalculateOperatingExpense($scope.InputsBudget);
    });
    $scope.$watchCollection('[InputsForecast1.IncomeStatement_Revenue, InputsForecast1.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue]', function (newValues) {
        CalculateOperatingExpense($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatement_Revenue, InputsForecast2.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue]', function (newValues) {
        CalculateOperatingExpense($scope.InputsForecast2);
    });
    //#endregion Operating expenses (excluding depreciation & amortisation)

    //#region Earnings before interest, tax, depreciation, amortisation ("EBITDA")
    CalculateEBITDA = function (record) {
        record.IncomeStatement_EBITDA = record.IncomeStatement_GrossProfit - record.IncomeStatement_OperatingExpense;
    }
    $scope.$watchCollection('[InputsActualPrior.IncomeStatement_GrossProfit, InputsActualPrior.IncomeStatement_OperatingExpense]', function (newValues) {
        CalculateEBITDA($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_GrossProfit, InputsActualCurrent.IncomeStatement_OperatingExpense]', function (newValues) {
        CalculateEBITDA($scope.InputsActualCurrent);
    });
    $scope.$watchCollection('[InputsBudget.IncomeStatement_GrossProfit, InputsBudget.IncomeStatement_OperatingExpense]', function (newValues) {
        CalculateEBITDA($scope.InputsBudget);
    });

    $scope.$watchCollection('[InputsForecast1.IncomeStatement_GrossProfit, InputsForecast1.IncomeStatement_OperatingExpense]', function (newValues) {
        CalculateEBITDA($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatement_GrossProfit, InputsForecast2.IncomeStatement_OperatingExpense]', function (newValues) {
        CalculateEBITDA($scope.InputsForecast2);
    });
    //#endregion Earnings before interest, tax, depreciation, amortisation ("EBITDA")

    //#region  DepreciationAndAmortisationExpense
    CalculateDepreciationAndAmortisationExpense = function (record) {
        record.IncomeStatement_DepreciationAndAmortisationExpense = record.IncomeStatement_Revenue * record.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue;
    }

    $scope.$watchCollection('[InputsBudget.IncomeStatement_Revenue, InputsBudget.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue]', function (newValues) {
        CalculateDepreciationAndAmortisationExpense($scope.InputsBudget);
    });
    $scope.$watchCollection('[InputsForecast1.IncomeStatement_Revenue, InputsForecast1.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue]', function (newValues) {
        CalculateDepreciationAndAmortisationExpense($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatement_Revenue, InputsForecast2.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue]', function (newValues) {
        CalculateDepreciationAndAmortisationExpense($scope.InputsForecast2);
    });
    //#endregion DepreciationAndAmortisationExpense

    //#region Earnings before interest and tax ("EBIT")

    CalculateEBIT = function (record) {
        record.IncomeStatement_EBIT = record.IncomeStatement_EBITDA - record.IncomeStatement_DepreciationAndAmortisationExpense;
    }
    $scope.$watchCollection('[InputsActualPrior.IncomeStatement_EBITDA, InputsActualPrior.IncomeStatement_DepreciationAndAmortisationExpense]', function (newValues) {
        CalculateEBIT($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_EBITDA, InputsActualCurrent.IncomeStatement_DepreciationAndAmortisationExpense]', function (newValues) {
        CalculateEBIT($scope.InputsActualCurrent);
    });
    $scope.$watchCollection('[InputsBudget.IncomeStatement_EBITDA, InputsBudget.IncomeStatement_DepreciationAndAmortisationExpense]', function (newValues) {
        CalculateEBIT($scope.InputsBudget);
    });

    $scope.$watchCollection('[InputsForecast1.IncomeStatement_EBITDA, InputsForecast1.IncomeStatement_DepreciationAndAmortisationExpense]', function (newValues) {
        CalculateEBIT($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatement_EBITDA, InputsForecast2.IncomeStatement_DepreciationAndAmortisationExpense]', function (newValues) {
        CalculateEBIT($scope.InputsForecast2);
    });
    //#endregion Earnings before interest and tax ("EBIT")

    //#region Interest expense
    CalculateInterestExpense = function (record) {
        if (record.prior) {
            record.IncomeStatement_InterestExpense = record.prior.IncomeStatement_InterestExpense;
        }
    }

    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_InterestExpense]', function (newValues) {
        CalculateInterestExpense($scope.InputsBudget);
        CalculateInterestExpense($scope.InputsForecast1);
        CalculateInterestExpense($scope.InputsForecast2);
    });
    //#endregion Interest expense

    //#region Earnings before tax
    CalculateEBT = function (record) {
        record.IncomeStatement_EarningsBeforeTax = record.IncomeStatement_EBIT - record.IncomeStatement_InterestExpense;
    }
    $scope.$watchCollection('[InputsActualPrior.IncomeStatement_EBIT, InputsActualPrior.IncomeStatement_InterestExpense]', function (newValues) {
        CalculateEBT($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_EBIT, InputsActualCurrent.IncomeStatement_InterestExpense]', function (newValues) {
        CalculateEBT($scope.InputsActualCurrent);
    });
    $scope.$watchCollection('[InputsBudget.IncomeStatement_EBIT, InputsBudget.IncomeStatement_InterestExpense]', function (newValues) {
        CalculateEBT($scope.InputsBudget);
    });

    $scope.$watchCollection('[InputsForecast1.IncomeStatement_EBIT, InputsForecast1.IncomeStatement_InterestExpense]', function (newValues) {
        CalculateEBT($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatement_EBIT, InputsForecast2.IncomeStatement_InterestExpense]', function (newValues) {
        CalculateEBT($scope.InputsForecast2);
    });
    //#endregion Earnings before tax

    //#region Non-cash expenses included in Operating expenses above
    CalculateNonCashExpense = function (record) {
        record.IncomeStatement_NonCashExpense = record.IncomeStatement_Revenue - record.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue;
    }

    $scope.$watchCollection('[InputsBudget.IncomeStatement_Revenue, InputsBudget.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue]', function (newValues) {
        CalculateNonCashExpense($scope.InputsBudget);
    });

    $scope.$watchCollection('[InputsForecast1.IncomeStatement_Revenue, InputsForecast1.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue]', function (newValues) {
        CalculateNonCashExpense($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatement_Revenue, InputsForecast2.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue]', function (newValues) {
        CalculateNonCashExpense($scope.InputsForecast2);
    });

    //#endregion Non-cash expenses included in Operating expenses above

    //#endregion ******************************Income statement*************************




    //#region  ******************************Income statement analysis*************************


    //#region Revenue growth rate



    CalculateRevenueGrowthRate = function (record) {
        if (record.prior) {
            record.IncomeStatementAnalysis_PercentageRevenueGrowth = (record.IncomeStatement_Revenue / record.prior.IncomeStatement_Revenue) - 1;

        }
    }

    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_Revenue, InputsActualPrior.IncomeStatement_Revenue]', function (newValues) {

        CalculateRevenueGrowthRate($scope.InputsActualCurrent);
    });




    //#endregion Revenue growth rate

    //#region Gross profit margin

    CalculatePercentageGrossProfitMargin = function (record) {
        record.IncomeStatementAnalysis_PercentageGrossProfitMargin = (record.IncomeStatement_Revenue / record.IncomeStatement_GrossProfit);
    }
    $scope.$watchCollection('[InputsActualPrior.IncomeStatement_Revenue, InputsActualPrior.IncomeStatement_GrossProfit]', function (newValues) {

        CalculatePercentageGrossProfitMargin($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_Revenue, InputsActualCurrent.IncomeStatement_GrossProfit]', function (newValues) {

        CalculatePercentageGrossProfitMargin($scope.InputsActualCurrent);
    });
    //#endregion Gross profit margin


    //#region  Operating expenses (excluding depreciation & amortisation) as % Revenue
    CalculatePercentageOperatingExpensesOfRevenue = function (record) {
        record.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (record.IncomeStatement_Revenue / record.IncomeStatement_OperatingExpense);
    }
    $scope.$watchCollection('[InputsActualPrior.IncomeStatement_Revenue, InputsActualPrior.IncomeStatement_OperatingExpense]', function (newValues) {

        CalculatePercentageOperatingExpensesOfRevenue($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_Revenue, InputsActualCurrent.IncomeStatement_OperatingExpense]', function (newValues) {

        CalculatePercentageOperatingExpensesOfRevenue($scope.InputsActualCurrent);
    });



    $scope.$watchCollection('[InputsBudget.IncomeStatementAnalysis_PercentageGrossProfitMargin]', function (newValues) {
        if (!$scope.InputsBudget.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue_overridden) {
            $scope.InputsBudget.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = $scope.InputsBudget.IncomeStatementAnalysis_PercentageGrossProfitMargin - .115;
        }
    });

    $scope.$watchCollection('[InputsForecast1.IncomeStatementAnalysis_PercentageGrossProfitMargin]', function (newValues) {
        if (!$scope.InputsForecast1.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue_overridden) {
            $scope.InputsForecast1.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = $scope.InputsForecast1.IncomeStatementAnalysis_PercentageGrossProfitMargin - .115;
        }
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatementAnalysis_PercentageGrossProfitMargin]', function (newValues) {
        if (!$scope.InputsForecast2.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue_overridden) {
            $scope.InputsForecast2.IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = $scope.InputsForecast2.IncomeStatementAnalysis_PercentageGrossProfitMargin - .115;
        }
    });
    //#endregion Operating expenses (excluding depreciation & amortisation) as % Revenue 

    //#region Gross EBITDA margin
    CalculateEBITDAMargin = function (record) {
        record.IncomeStatementAnalysis_EBITDAMargin = (record.IncomeStatement_Revenue / record.IncomeStatement_EBITDA);
    }
    $scope.$watchCollection('[InputsActualPrior.IncomeStatement_Revenue, InputsActualPrior.IncomeStatement_EBITDA]', function (newValues) {

        CalculateEBITDAMargin($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_Revenue, InputsActualCurrent.IncomeStatement_EBITDA]', function (newValues) {

        CalculateEBITDAMargin($scope.InputsActualCurrent);
    });
    $scope.$watchCollection('[InputsBudget.IncomeStatement_Revenue, InputsBudget.IncomeStatement_EBITDA]', function (newValues) {

        CalculateEBITDAMargin($scope.InputsBudget);
    });
    $scope.$watchCollection('[InputsForecast1.IncomeStatement_Revenue, InputsForecast1.IncomeStatement_EBITDA]', function (newValues) {

        CalculateEBITDAMargin($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatement_Revenue, InputsForecast2.IncomeStatement_EBITDA]', function (newValues) {

        CalculateEBITDAMargin($scope.InputsForecast2);
    });
    //#endregion EBITDA margin


    //#region EBIT margin
    CalculateEBITMargin = function (record) {
        record.IncomeStatementAnalysis_EBITMargin = (record.IncomeStatement_Revenue / record.IncomeStatement_EBIT);
    }
    $scope.$watchCollection('[InputsActualPrior.IncomeStatement_Revenue, InputsActualPrior.IncomeStatement_EBIT]', function (newValues) {

        CalculateEBITMargin($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_Revenue, InputsActualCurrent.IncomeStatement_EBIT]', function (newValues) {

        CalculateEBITMargin($scope.InputsActualCurrent);
    });
    $scope.$watchCollection('[InputsBudget.IncomeStatement_Revenue, InputsBudget.IncomeStatement_EBIT]', function (newValues) {

        CalculateEBITMargin($scope.InputsBudget);
    });
    $scope.$watchCollection('[InputsForecast1.IncomeStatement_Revenue, InputsForecast1.IncomeStatement_EBIT]', function (newValues) {

        CalculateEBITMargin($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatement_Revenue, InputsForecast2.IncomeStatement_EBIT]', function (newValues) {

        CalculateEBITMargin($scope.InputsForecast2);
    });

    //#endregion EBIT margin



    //#region Depreciation and amortisation as % of Revenue
    CalculatePercentageDepreciationAndAmortOfRevenue = function (record) {
        if (record.index == "0" || record.index == "1") {
            record.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = record.IncomeStatement_DepreciationAndAmortisationExpense / record.IncomeStatement_Revenue;
        }
        else if (record.prior) {
            record.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = record.prior.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue;
        }
    }

    $scope.$watchCollection('[InputsActualPrior.IncomeStatement_Revenue, InputsActualPrior.IncomeStatement_DepreciationAndAmortisationExpense]', function (newValues) {
        CalculatePercentageDepreciationAndAmortOfRevenue($scope.InputsActualPrior);
    });

    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_Revenue, InputsActualCurrent.IncomeStatement_DepreciationAndAmortisationExpense]', function (newValues) {
        CalculatePercentageDepreciationAndAmortOfRevenue($scope.InputsActualCurrent);
    });

    $scope.$watchCollection('[InputsActualCurrent.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue]', function (newValues) {
        CalculatePercentageDepreciationAndAmortOfRevenue($scope.InputsBudget);


    });
    $scope.$watchCollection('[InputsBudget.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue]', function (newValues) {
        CalculatePercentageDepreciationAndAmortOfRevenue($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast1.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue]', function (newValues) {
        CalculatePercentageDepreciationAndAmortOfRevenue($scope.InputsForecast2);

    });
    //#endregion Depreciation and amortisation as % of Revenue



    //#region Non-cash expenses (included in Operating expenses) as a % of Revenue

    CalculatePercentageNonCashExpensesOfRevenue = function (record) {
        if (record.index == "0" || record.index == "1") {
            record.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = record.IncomeStatement_NonCashExpense / record.IncomeStatement_Revenue;
        }

    }

    $scope.$watchCollection('[InputsActualPrior.IncomeStatement_Revenue, InputsActualPrior.IncomeStatement_DepreciationAndAmortisationExpense]', function (newValues) {
        CalculatePercentageNonCashExpensesOfRevenue($scope.InputsActualPrior);
    });

    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_Revenue, InputsActualCurrent.IncomeStatement_DepreciationAndAmortisationExpense]', function (newValues) {
        CalculatePercentageNonCashExpensesOfRevenue($scope.InputsActualCurrent);
    });


    $scope.$watchCollection('[InputsActualCurrent.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue]', function (newValues) {
        if (!$scope.InputsBudget.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue_overridden && $scope.InputsBudget.prior) {
            $scope.InputsBudget.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = $scope.InputsBudget.prior.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue;
        }
    });

    $scope.$watchCollection('[InputsBudget.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue]', function (newValues) {
        if (!$scope.InputsForecast1.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue_overridden && $scope.InputsForecast1.prior) {
            $scope.InputsForecast1.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = $scope.InputsForecast1.prior.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue;
        }
    });
    $scope.$watchCollection('[InputsForecast1.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue]', function (newValues) {
        if (!$scope.InputsForecast2.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue_overridden && $scope.InputsForecast2.prior) {
            $scope.InputsForecast2.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = $scope.InputsForecast2.prior.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue;
        }
    });
    //#endregionNon-cash expenses (included in Operating expenses) as a % of Revenue





    //#region Interest cover

    CalculateInterestCover = function (record) {
        if (record.index == "0" || record.index == "1" && record.IncomeStatement_InterestExpense > 0 && record.IncomeStatement_InterestExpense) {
            record.IncomeStatementAnalysis_Calculated_InterestCover = record.IncomeStatement_EBIT / record.IncomeStatement_InterestExpense;
        }
        //else if (record.prior) {
        //    record.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = record.prior.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue;
        //}
    }

    $scope.$watchCollection('[InputsActualPrior.IncomeStatement_EBIT, InputsActualPrior.IncomeStatement_InterestExpense]', function (newValues) {
        CalculateInterestCover($scope.InputsActualPrior);
    });

    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_EBIT, InputsActualCurrent.IncomeStatement_InterestExpense]', function (newValues) {
        CalculateInterestCover($scope.InputsActualCurrent);
    });

    //#endregion Interest cover



    //#endregion ******************************Income statement analysis*************************




    //#region ******************************Balance Sheet*************************


    //#region Current Assets
    CalculateTotalAssets_Total = function (record) {
        if (record.index == "0" || record.index == "1") {
            record.BalanceSheetTotalAssets_Total = record.BalanceSheetTotalAssets_AccountsReceiveable +
                                                    record.BalanceSheetTotalAssets_Inventory +
                                                    record.BalanceSheetTotalAssets_OtherCurrentAssets +
                                                    record.BalanceSheetTotalAssets_CashAndCashEquivalents;
        }

    }

    $scope.$watchCollection('[InputsActualPrior.BalanceSheetTotalAssets_AccountsReceiveable, InputsActualPrior.BalanceSheetTotalAssets_Inventory, InputsActualPrior.BalanceSheetTotalAssets_OtherCurrentAssets, InputsActualPrior.BalanceSheetTotalAssets_CashAndCashEquivalents]', function (newValues) {
        CalculateTotalAssets_Total($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.BalanceSheetTotalAssets_AccountsReceiveable, InputsActualCurrent.BalanceSheetTotalAssets_Inventory, InputsActualCurrent.BalanceSheetTotalAssets_OtherCurrentAssets, InputsActualCurrent.BalanceSheetTotalAssets_CashAndCashEquivalents]', function (newValues) {
        CalculateTotalAssets_Total($scope.InputsActualCurrent);
    });

    //#endregion Current Assets



    //#region Accounts Receiveable


    CalculateAccountsReceiveable = function (record) {
        record.BalanceSheetTotalAssets_AccountsReceiveable = record.IncomeStatement_Revenue *
                                                record.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue;
    }

    $scope.$watchCollection('[InputsBudget.IncomeStatement_Revenue, InputsBudget.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue]', function (newValues) {
        CalculateAccountsReceiveable($scope.InputsBudget);
    });

    $scope.$watchCollection('[InputsForecast1.IncomeStatement_Revenue, InputsForecast1.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue]', function (newValues) {
        CalculateAccountsReceiveable($scope.InputsForecast1);
    });

    $scope.$watchCollection('[InputsForecast2.IncomeStatement_Revenue, InputsForecast2.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue]', function (newValues) {
        CalculateAccountsReceiveable($scope.InputsForecast2);
    });

    //#endregion Accounts Receiveable



    //#region Inventory

    CalculateInventory = function (record) {
        record.BalanceSheetTotalAssets_Inventory = record.IncomeStatement_CostOfGoodsSold *
                                                record.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold;
    }

    $scope.$watchCollection('[InputsBudget.IncomeStatement_CostOfGoodsSold, InputsBudget.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold]', function (newValues) {
        CalculateInventory($scope.InputsBudget);
    });

    $scope.$watchCollection('[InputsForecast1.IncomeStatement_CostOfGoodsSold, InputsForecast1.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold]', function (newValues) {
        CalculateInventory($scope.InputsForecast1);
    });

    $scope.$watchCollection('[InputsForecast2.IncomeStatement_CostOfGoodsSold, InputsForecast2.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold]', function (newValues) {
        CalculateInventory($scope.InputsForecast2);
    });

    //#endregion Inventory




    //#region Other current assets




    CalculateOtherCurrentAssets = function (record) {
        record.BalanceSheetTotalAssets_OtherCurrentAssets = record.IncomeStatement_CostOfGoodsSold *
                                                record.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold;
    }

    $scope.$watchCollection('[InputsBudget.IncomeStatement_CostOfGoodsSold, InputsBudget.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold]', function (newValues) {
        CalculateOtherCurrentAssets($scope.InputsBudget);
    });

    $scope.$watchCollection('[InputsForecast1.IncomeStatement_CostOfGoodsSold, InputsForecast1.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold]', function (newValues) {
        CalculateOtherCurrentAssets($scope.InputsForecast1);
    });

    $scope.$watchCollection('[InputsForecast2.IncomeStatement_CostOfGoodsSold, InputsForecast2.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold]', function (newValues) {
        CalculateOtherCurrentAssets($scope.InputsForecast2);
    });

    //#endregion Other current assets


    ////#region Cash and cash equivalents


    //BalanceSheetTotalAssets_CashAndCashEquivalents
    ////#endregion Cash and cash equivalents

    //#region Fixed Assets

    //BalanceSheetTotalAssets_FixedAssets
    //#endregion Fixed Assets

    //#region Total Assets
    //



    CalculateTotalAssets = function (record) {

        record.BalanceSheetTotalAssets_TotalAssets = record.BalanceSheetTotalAssets_Total +
                                                record.BalanceSheetTotalAssets_FixedAssets +
                                                record.BalanceSheetTotalAssets_IntangibleAssets;


    }

    $scope.$watchCollection('[InputsActualPrior.BalanceSheetTotalAssets_Total, InputsActualPrior.BalanceSheetTotalAssets_FixedAssets, InputsActualPrior.BalanceSheetTotalAssets_IntangibleAssets]', function (newValues) {
        CalculateTotalAssets($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.BalanceSheetTotalAssets_Total, InputsActualCurrent.BalanceSheetTotalAssets_FixedAssets, InputsActualCurrent.BalanceSheetTotalAssets_IntangibleAssets]', function (newValues) {
        CalculateTotalAssets($scope.InputsActualCurrent);
    });

    //#endregion Total Assets



    //#region Current Liabilities

    CalculateCurrentLiabilities = function (record) {

        record.BalanceSheetEquityAndLiabilitiesCurrentLiabilities = record.BalanceSheetEquityAndLiabilities_AccountsPayable +
                                                record.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities +
                                                record.BalanceSheetEquityAndLiabilities_ShortTermDebt;
    }

    $scope.$watchCollection('[InputsActualCurrent.BalanceSheetEquityAndLiabilities_AccountsPayable, InputsActualCurrent.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, InputsActualCurrent.BalanceSheetEquityAndLiabilities_ShortTermDebt]', function (newValues) {
        CalculateCurrentLiabilities($scope.InputsActualCurrent);
    });

    $scope.$watchCollection('[InputsActualPrior.BalanceSheetEquityAndLiabilities_AccountsPayable, InputsActualPrior.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, InputsActualPrior.BalanceSheetEquityAndLiabilities_ShortTermDebt]', function (newValues) {
        CalculateCurrentLiabilities($scope.InputsActualPrior);
    });

    //#endregion Current Liabilities


    //#region Accounts Payable
    CalculateAccountsPayable = function (record) {

        record.BalanceSheetEquityAndLiabilities_AccountsPayable = record.IncomeStatement_CostOfGoodsSold *
                                                record.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold;
    }

    $scope.$watchCollection('[InputsBudget.IncomeStatement_CostOfGoodsSold, InputsBudget.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold]', function (newValues) {
        CalculateAccountsPayable($scope.InputsBudget);
    });

    $scope.$watchCollection('[InputsForecast1.IncomeStatement_CostOfGoodsSold, InputsForecast1.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold]', function (newValues) {
        CalculateAccountsPayable($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatement_CostOfGoodsSold, InputsForecast2.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold]', function (newValues) {
        CalculateAccountsPayable($scope.InputsForecast2);
    });
    //#endregion Accounts Payable

    //#region Other current liabilities

    CalculateOtherCurrentLiabilities = function (record) {

        record.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = record.IncomeStatement_CostOfGoodsSold *
                                                record.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold;
    }

    $scope.$watchCollection('[InputsBudget.IncomeStatement_CostOfGoodsSold, InputsBudget.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold]', function (newValues) {
        CalculateOtherCurrentLiabilities($scope.InputsBudget);
    });

    $scope.$watchCollection('[InputsForecast1.IncomeStatement_CostOfGoodsSold, InputsForecast1.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold]', function (newValues) {
        CalculateOtherCurrentLiabilities($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatement_CostOfGoodsSold, InputsForecast2.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold]', function (newValues) {
        CalculateOtherCurrentLiabilities($scope.InputsForecast2);
    });
    //#endregion Other current liabilities

    //#region Long-term debt

    //BalanceSheetEquityAndLiabilities_LongTermDebt

    //#endregionLong-term debt

    //BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities
    //BalanceSheetEquityAndLiabilities_ShareholderLoans

    //#region Short term debt

    //#endregion Short term debt


    //#region Total Liabilities

    CalculateTotalLiabilities = function (record) {

        record.BalanceSheetEquityAndLiabilities_TotalLiabilities = record.BalanceSheetEquityAndLiabilitiesCurrentLiabilities +
                                                record.BalanceSheetEquityAndLiabilities_LongTermDebt + record.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities + record.BalanceSheetEquityAndLiabilities_ShareholderLoans;
    }
    $scope.$watchCollection('[InputsActualPrior.BalanceSheetEquityAndLiabilitiesCurrentLiabilities, InputsActualPrior.BalanceSheetEquityAndLiabilities_LongTermDebt, InputsActualPrior.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities]', function (newValues) {
        CalculateTotalLiabilities($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.BalanceSheetEquityAndLiabilitiesCurrentLiabilities, InputsActualCurrent.BalanceSheetEquityAndLiabilities_LongTermDebt, InputsActualCurrent.BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities]', function (newValues) {
        CalculateTotalLiabilities($scope.InputsActualCurrent);
    });

    //#endregion Total Liabilities

    //#region Shareholders' Equity
    //
    CalculateShareholdersEquity = function (record) {

        record.BalanceSheetEquityAndLiabilities_ShareholdersEquity = record.BalanceSheetTotalAssets_Total -
                                                record.BalanceSheetEquityAndLiabilities_TotalLiabilities;
    }
    $scope.$watchCollection('[InputsActualPrior.BalanceSheetEquityAndLiabilities_TotalLiabilities, InputsActualPrior.BalanceSheetTotalAssets_Total]', function (newValues) {
        CalculateShareholdersEquity($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.BalanceSheetEquityAndLiabilities_TotalLiabilities, InputsActualCurrent.BalanceSheetTotalAssets_Total]', function (newValues) {
        CalculateShareholdersEquity($scope.InputsActualCurrent);
    });


    //#endregion Shareholders' Equity

    //#endregion ******************************Balance Sheet*************************




    //#region ******************************Working capital and leverage analysis*************************

    //#region Accounts Receiveable (as % Revenue)


    CalculatePercentageAccountReceivableOverRevenue = function (record) {
        record.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = record.BalanceSheetTotalAssets_AccountsReceiveable / record.IncomeStatement_Revenue;
    }
    $scope.$watchCollection('[InputsActualPrior.BalanceSheetTotalAssets_AccountsReceiveable, InputsActualPrior.IncomeStatement_Revenue]', function (newValues) {
        CalculatePercentageAccountReceivableOverRevenue($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.BalanceSheetTotalAssets_AccountsReceiveable, InputsActualCurrent.IncomeStatement_Revenue]', function (newValues) {
        CalculatePercentageAccountReceivableOverRevenue($scope.InputsActualCurrent);
    });

    //#endregion Accounts Receiveable (as % Revenue)


    //#region Inventory (as % Cost of Goods Sold)

    // BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold


    CalculatePercentageInventoryOverCostOfGoodsSold = function (record) {
        record.BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = record.BalanceSheetTotalAssets_Inventory / record.IncomeStatement_CostOfGoodsSold;
    }
    $scope.$watchCollection('[InputsActualPrior.BalanceSheetTotalAssets_Inventory, InputsActualPrior.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculatePercentageInventoryOverCostOfGoodsSold($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.BalanceSheetTotalAssets_Inventory, InputsActualCurrent.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculatePercentageInventoryOverCostOfGoodsSold($scope.InputsActualCurrent);
    });

    //#endregion Inventory (as % Cost of Goods Sold)



    //#region Other current assets (as % Cost of Goods Sold)

    CalculatePercentageOtherCurrentAssetsOverCostOfGoodsSold = function (record) {
        record.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = record.BalanceSheetTotalAssets_OtherCurrentAssets / record.IncomeStatement_CostOfGoodsSold;
    }
    $scope.$watchCollection('[InputsActualPrior.BalanceSheetTotalAssets_OtherCurrentAssets, InputsActualPrior.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculatePercentageOtherCurrentAssetsOverCostOfGoodsSold($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.BalanceSheetTotalAssets_OtherCurrentAssets, InputsActualCurrent.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculatePercentageOtherCurrentAssetsOverCostOfGoodsSold($scope.InputsActualCurrent);
    });

    //#endregion Other current assets (as % Cost of Goods Sold)



    //#region Accounts Payable (as % Cost of Goods Sold)
    //


    CalculatePercentageAccountsPayableOverCostOfGoodsSold = function (record) {
        record.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = record.BalanceSheetEquityAndLiabilities_AccountsPayable / record.IncomeStatement_CostOfGoodsSold;
    }
    $scope.$watchCollection('[InputsActualPrior.BalanceSheetEquityAndLiabilities_AccountsPayable, InputsActualPrior.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculatePercentageAccountsPayableOverCostOfGoodsSold($scope.InputsActualPrior);
    });
    $scope.$watchCollection('[InputsActualCurrent.BalanceSheetEquityAndLiabilities_AccountsPayable, InputsActualCurrent.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculatePercentageAccountsPayableOverCostOfGoodsSold($scope.InputsActualCurrent);
    });

    //#endregion Accounts Payable (as % Cost of Goods Sold)



    //#region Other current liabilities (as % Cost of Goods Sold)

    //

    CalculatePercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = function (record) {
        record.BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = record.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities / record.IncomeStatement_CostOfGoodsSold;
    }
    $scope.$watchCollection('[InputsBudget.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, InputsBudget.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculatePercentageOtherCurrentLiabilitiesOverCostOfGoodsSold($scope.InputsBudget);
    });
    $scope.$watchCollection('[InputsForecast1.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, InputsForecast1.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculatePercentageOtherCurrentLiabilitiesOverCostOfGoodsSold($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, InputsForecast2.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculatePercentageOtherCurrentLiabilitiesOverCostOfGoodsSold($scope.InputsForecast2);
    });


    //#endregion Other current liabilities (as % Cost of Goods Sold)



    //#region Accounts Receiveable Days (average)

    CalculateAverageAccountsReceiveableDays = function (record) {
        record.BalanceSheetAnalysis_AverageAccountsReceiveableDays = record.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities / record.IncomeStatement_CostOfGoodsSold;
    }

    $scope.$watchCollection('[InputsActualCurrent.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, InputsActualCurrent.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculateAverageAccountsReceiveableDays($scope.InputsActualCurrent);
    });

    $scope.$watchCollection('[InputsBudget.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, InputsBudget.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculateAverageAccountsReceiveableDays($scope.InputsBudget);
    });
    $scope.$watchCollection('[InputsForecast1.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, InputsForecast1.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculateAverageAccountsReceiveableDays($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities, InputsForecast2.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculateAverageAccountsReceiveableDays($scope.InputsForecast1);
    });

    //#endregion Accounts Receiveable Days (average)

    //#region Inventory Days (average)
    //
    CalculateAverageInventoryDays = function (record) {

        try {
            record.BalanceSheetAnalysis_AverageInventoryDays = ((record.BalanceSheetTotalAssets_AccountsReceiveable + record.prior.BalanceSheetTotalAssets_AccountsReceiveable) / 2) / record.IncomeStatement_Revenue * 365;
        }
        catch (err) {
            record.BalanceSheetAnalysis_AverageInventoryDays = null;
        }

    }
    $scope.$watchCollection('[InputsActualCurrent.BalanceSheetTotalAssets_AccountsReceiveable, InputsActualCurrent.prior.BalanceSheetTotalAssets_AccountsReceiveable, InputsActualCurrent.prior.IncomeStatement_Revenue]', function (newValues) {
        CalculateAverageInventoryDays($scope.InputsActualCurrent);
    });
    $scope.$watchCollection('[InputsBudget.BalanceSheetTotalAssets_AccountsReceiveable, InputsBudget.prior.BalanceSheetTotalAssets_AccountsReceiveable, InputsBudget.prior.IncomeStatement_Revenue]', function (newValues) {
        CalculateAverageInventoryDays($scope.InputsBudget);
    });

    $scope.$watchCollection('[InputsForecast1.BalanceSheetTotalAssets_AccountsReceiveable, InputsForecast1.prior.BalanceSheetTotalAssets_AccountsReceiveable, InputsForecast1.prior.IncomeStatement_Revenue]', function (newValues) {
        CalculateAverageInventoryDays($scope.InputsForecast1);
    });

    $scope.$watchCollection('[InputsForecast2.BalanceSheetTotalAssets_AccountsReceiveable, InputsForecast2.prior.BalanceSheetTotalAssets_AccountsReceiveable, InputsForecast2.prior.IncomeStatement_Revenue]', function (newValues) {
        CalculateAverageInventoryDays($scope.InputsForecast2);
    });
    //#endregion Inventory Days (average)


    //#region Accounts Payable Days (average)
    //

    CalculateAverageAccountsPayableDays = function (record) {
        try {
            record.BalanceSheetAnalysis_AverageAccountsPayableDays = ((record.BalanceSheetEquityAndLiabilities_AccountsPayable + record.prior.BalanceSheetEquityAndLiabilities_AccountsPayable) / 2) / record.IncomeStatement_CostOfGoodsSold * 365;
        }
        catch (err) {
            record.BalanceSheetAnalysis_AverageAccountsPayableDays = null;
        }
    }
    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_CostOfGoodsSold, InputsActualCurrent.prior.BalanceSheetEquityAndLiabilities_AccountsPayable, InputsActualCurrent.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculateAverageAccountsPayableDays($scope.InputsActualCurrent);
    });
    $scope.$watchCollection('[InputsBudget.IncomeStatement_CostOfGoodsSold, InputsBudget.prior.BalanceSheetEquityAndLiabilities_AccountsPayable, InputsBudget.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculateAverageAccountsPayableDays($scope.InputsBudget);
    });

    $scope.$watchCollection('[InputsForecast1.IncomeStatement_CostOfGoodsSold, InputsForecast1.prior.BalanceSheetEquityAndLiabilities_AccountsPayable, InputsForecast1.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculateAverageAccountsPayableDays($scope.InputsForecast1);
    });

    $scope.$watchCollection('[InputsForecast2.IncomeStatement_CostOfGoodsSold, InputsForecast2.prior.BalanceSheetEquityAndLiabilities_AccountsPayable, InputsForecast2.IncomeStatement_CostOfGoodsSold]', function (newValues) {
        CalculateAverageAccountsPayableDays($scope.InputsForecast2);
    });
    //#endregion Accounts Payable Days (average)

    //#region Current ratio


    CalculateCurrentRatio = function (record) {
        try {
            record.BalanceSheetAnalysis_CurrentRatio = record.BalanceSheetTotalAssets_Total / record.BalanceSheetEquityAndLiabilitiesCurrentLiabilities;
        }
        catch (err) {
            record.BalanceSheetAnalysis_CurrentRatio = null;
        }
    }

    $scope.$watchCollection('[InputsActualCurrent.BalanceSheetTotalAssets_Total, InputsActualCurrent.BalanceSheetEquityAndLiabilitiesCurrentLiabilities]', function (newValues) {
        CalculateCurrentRatio($scope.InputsActualCurrent);
    });

    $scope.$watchCollection('[InputsActualPrior.BalanceSheetTotalAssets_Total, InputsActualPrior.BalanceSheetEquityAndLiabilitiesCurrentLiabilities]', function (newValues) {
        CalculateCurrentRatio($scope.InputsActualPrior);
    });


    //#endregion Current ratio


    //#region Total Debt / EBITDA
    //BalanceSheetAnalysis_TotalEBITDA

    CalculateTotalEBITDA = function (record) {
        try {

            record.BalanceSheetAnalysis_TotalEBITDA = (record.BalanceSheetEquityAndLiabilities_ShortTermDebt + record.BalanceSheetEquityAndLiabilities_LongTermDebt) / record.IncomeStatement_EBITDA;
        }
        catch (err) {
            record.BalanceSheetAnalysis_TotalEBITDA = null;
        }
    }

    $scope.$watchCollection('[InputsActualCurrent.BalanceSheetEquityAndLiabilities_ShortTermDebt, InputsActualCurrent.BalanceSheetEquityAndLiabilities_LongTermDebt, InputsActualCurrent.IncomeStatement_EBITDA]', function (newValues) {
        CalculateTotalEBITDA($scope.InputsActualCurrent);
    });

    $scope.$watchCollection('[InputsActualPrior.BalanceSheetEquityAndLiabilities_ShortTermDebt, InputsActualPrior.BalanceSheetEquityAndLiabilities_LongTermDebt, InputsActualPrior.IncomeStatement_EBITDA]', function (newValues) {
        CalculateTotalEBITDA($scope.InputsActualPrior);
    });


    //#endregion Total Debt / EBITDA






    //#endregion ******************************Working capital and leverage analysis*************************





    //#region ******************************CAPITAL EXPENDITURE*************************

    //#region Capital expenditure



    CalculateCapitalExpenditure = function (record) {


        record.CapitalExpenditure_CapitalExpenditure = record.IncomeStatement_Revenue * record.CapitalExpenditure_PercentageForecastCapitalExpenditure;

    }

    $scope.$watchCollection('[InputsBudget.IncomeStatement_Revenue, InputsBudget.CapitalExpenditure_PercentageForecastCapitalExpenditure]', function (newValues) {
        CalculateCapitalExpenditure($scope.InputsBudget);
    });

    $scope.$watchCollection('[InputsForecast1.IncomeStatement_Revenue, InputsForecast1.CapitalExpenditure_PercentageForecastCapitalExpenditure]', function (newValues) {
        CalculateCapitalExpenditure($scope.InputsForecast1);
    });
    $scope.$watchCollection('[InputsForecast2.IncomeStatement_Revenue, InputsForecast2.CapitalExpenditure_PercentageForecastCapitalExpenditure]', function (newValues) {
        CalculateCapitalExpenditure($scope.InputsForecast2);
    });

    //#endregion Capital expenditure



    //#region Capital expenditure (as % Revenue)




    CalculateCapitalExpenditure = function (record) {


        record.CapitalExpenditure_PercentageForecastCapitalExpenditure = record.CapitalExpenditure_CapitalExpenditure / record.IncomeStatement_Revenue;

    }

    $scope.$watchCollection('[InputsActualPrior.IncomeStatement_Revenue, InputsActualPrior.CapitalExpenditure_CapitalExpenditure]', function (newValues) {
        CalculateCapitalExpenditure($scope.InputsActualPrior);
    });

    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_Revenue, InputsActualCurrent.CapitalExpenditure_CapitalExpenditure]', function (newValues) {
        CalculateCapitalExpenditure($scope.InputsActualCurrent);
    });


    //#endregion Capital expenditure (as % Revenue)


    //#region Capital expenditure (TTM)




    CalculateCapitalExpenditureTTM = function (record, MonthsSinceLastFinancialYearEnd) {


        record.prior.CapitalExpenditure_TTM = ((12 - MonthsSinceLastFinancialYearEnd) / 12 * record.prior.CapitalExpenditure_CapitalExpenditure) + ((MonthsSinceLastFinancialYearEnd) / 12 * record.CapitalExpenditure_CapitalExpenditure);

    }



    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_Revenue, InputsActualCurrent.CapitalExpenditure_CapitalExpenditure]', function (newValues) {
        CalculateCapitalExpenditureTTM($scope.InputsBudget, $scope.MonthsSinceLastFinancialYearEnd);
    });


    //#endregion Capital expenditure (TTM)



    //#endregion ******************************CAPITAL EXPENDITURE*************************




    //#region ******************************SUSTAINABLE EBITDA*************************

    //#region Sustainable EBITDA

    CalculateEBITDA_Total = function (record) {
        record.SustainableEBITDA_Total = record.IncomeStatement_EBITDA + record.SustainableEBITDA_NonRecurringExpenses - record.SustainableEBITDA_NonRecurringIncome;
    }

    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_EBITDA, InputsActualCurrent.SustainableEBITDA_NonRecurringExpenses, InputsActualCurrent.SustainableEBITDA_NonRecurringIncome]', function (newValues) {
        CalculateEBITDA_Total($scope.InputsActualCurrent);
    });

    $scope.$watchCollection('[InputsBudget.IncomeStatement_EBITDA, InputsBudget.SustainableEBITDA_NonRecurringExpenses, InputsBudget.SustainableEBITDA_NonRecurringIncome]', function (newValues) {
        CalculateEBITDA_Total($scope.InputsBudget);
    });

    //#endregion Sustainable EBITDA


    //#region Sustainable EBITDA (TTM)




    CalculateSustainableEBITDATTM = function (record, MonthsSinceLastFinancialYearEnd) {


        record.prior.SustainableEBITDA_TTM = ((12 - MonthsSinceLastFinancialYearEnd) / 12 * record.prior.SustainableEBITDA_Total) + ((MonthsSinceLastFinancialYearEnd) / 12 * record.SustainableEBITDA_Total);
    }



    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_Revenue, InputsActualCurrent.CapitalExpenditure_CapitalExpenditure]', function (newValues) {
        CalculateSustainableEBITDATTM($scope.InputsBudget, $scope.MonthsSinceLastFinancialYearEnd);
    });


    //#endregion Sustainable EBITDA (TTM)

    //#endregion ******************************SUSTAINABLE EBITDA*************************




    //#region ******************************EXCESS CASH*************************

    //#region Revenue (TTM)

    CalculateRevenueTTM = function (record, MonthsSinceLastFinancialYearEnd) {


        record.prior.ExcessCash_RevenueTTM = ((12 - MonthsSinceLastFinancialYearEnd) / 12 * record.prior.IncomeStatement_Revenue) + ((MonthsSinceLastFinancialYearEnd) / 12 * record.IncomeStatement_Revenue);
    }



    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_Revenue, InputsActualCurrent.CapitalExpenditure_CapitalExpenditure]', function (newValues) {
        CalculateRevenueTTM($scope.InputsBudget, $scope.MonthsSinceLastFinancialYearEnd);
    });
    //#endregion Revenue (TTM)




    //#region Operating cash required as % Revenue (assumed)
    CalculateAssumedOperatingCashRequiredAsPercentageOfRevenue = function (record, ExcessCash_RequiredPercentageOperatingCash) {
        record.ExcessCash_AssumedOperatingCashRequiredAsPercentageOfRevenue = record.ExcessCash_RevenueTTM * ExcessCash_RequiredPercentageOperatingCash;
    }

    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_EBITDA, InputsActualCurrent.SustainableEBITDA_NonRecurringExpenses, InputsActualCurrent.SustainableEBITDA_NonRecurringIncome]', function (newValues) {
        CalculateAssumedOperatingCashRequiredAsPercentageOfRevenue($scope.InputsActualCurrent, $scope.ExcessCash_RequiredPercentageOperatingCash);
    });
    //#endregion Operating cash required as % Revenue (assumed)



    //#region Excess cash

    CalculateAssumedOperatingCashRequiredAsPercentageOfRevenue = function (record) {
        record.ExcessCash_AssumedOperatingCashRequiredAsPercentageOfRevenue = record.IncomeStatement_EBITDA + record.SustainableEBITDA_NonRecurringExpenses - record.SustainableEBITDA_NonRecurringIncome;
    }

    $scope.$watchCollection('[InputsActualCurrent.IncomeStatement_EBITDA, InputsActualCurrent.SustainableEBITDA_NonRecurringExpenses, InputsActualCurrent.SustainableEBITDA_NonRecurringIncome]', function (newValues) {
        CalculateAssumedOperatingCashRequiredAsPercentageOfRevenue($scope.InputsActualCurrent);
    });



    //#endregion Excess cash

    //#endregion ******************************EXCESS CASH*************************








    $scope.Loading = true;

    
    emValuationIncomeStatementResource.GetFullValuation($scope.LoadValuation);



}]);


