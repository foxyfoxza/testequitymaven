﻿//$(document).ready(function() {
//$('#LastFinancialYearEnd').datepicker()
//    .on("input change", function (e) {
//        console.log("Date changed: ", e.target.value);

//    var dt = new Date();
//    $('#LastFinancialYearEndValue')[0].value = dt;


//    });
//});
 
var EMPreValuationApp = angular.module('EMPreValuationApp', []);

 
function EMPreValuationResource($q, $http) {
    return {


        saveData1: function (Id, ClientId, MemberId, data) {
            var request = {
                model: data
            };
            return $http({
                method: 'POST',

                url: "valuation/Save?Id=" + Id + "&ClientId=" + ClientId + "&MemberId=" + MemberId,
                headers: { 'Content-Type: ': 'application/json' },
                transformRequest: function (data) {
                    var formData = new FormData();
                    //formData.append("file", data.file);
                    return formData;
                },
                data: request
            }).then(function (response) {
                if (response) {
                    return response;

                } else {
                    return false;
                }
            });
        },
        saveData: function (_data){
            $.ajax({
                url: "/umbraco/api/PrelimEvaluation/Save2",
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    alert(JSON.stringify(data));
                },
                error: function (request, status, error) {
                    alert(request.responseText);
                },
                data: _data
            });
        }
    };
};
angular.module("EMPreValuationApp").factory("EMPreValuationResource", EMPreValuationResource);
 


EMPreValuationApp.directive('ngcFocus', ['$parse', function ($parse) {
    return function (scope, element, attr) {
        var fn = $parse(attr['ngcFocus']);
        element.bind('focus', function (event) {
            scope.$apply(function () {
                fn(scope, { $event: event });
            });
        });
    }
}]);

EMPreValuationApp.directive('ngcBlur', ['$parse', function ($parse) {
    return function (scope, element, attr) {
        var fn = $parse(attr['ngcBlur']);
        element.bind('blur', function (event) {
            scope.$apply(function () {
                fn(scope, { $event: event });
            });
        });
    }
}]);
 
EMPreValuationApp.controller('EMPreValuationCtrl', ['$scope', '$http', '$location', 'EMPreValuationResource', function ($scope, $http, $location, EMPreValuationResource) {
   
    $scope.testDate = '05-June-2018';
     
    $scope.Save = function () {
        var LastFinancialYearEnd = $('#LastFinancialYearEnd')[0];
        $scope.Data.LastFinancialYearEnd = LastFinancialYearEnd.value;
        var resultx = { "ISOCurrencySymbol": $scope.Data.ISOCurrencySymbol, "Email": $scope.Data.Email, "CompanyName": $scope.Data.CompanyName, "SourceId": $scope.Data.SourceId, "CurrentCashOnHand": $scope.Data.CurrentCashOnHand, "CurrentInterestBearingDebt": $scope.Data.CurrentInterestBearingDebt, "LastFinancialYearEnd": $scope.Data.LastFinancialYearEnd, "PercentageOperatingCashRequired": $scope.Data.PercentageOperatingCashRequired, "TRBC2012HierarchicalID": $scope.selectedActivityId, "IncomeStatement_CostOfGoodsSold": $scope.Data.IncomeStatement_CostOfGoodsSold, "IncomeStatement_OperatingExpense": $scope.Data.IncomeStatement_OperatingExpense, "IncomeStatement_Revenue": $scope.Data.IncomeStatement_Revenue, "ExcessCash_CurrentCashOnHandAsAtLastMonthEnd": $scope.Data.ExcessCash_CurrentCashOnHandAsAtLastMonthEnd, "SelectedCurrencyId": $scope.Data.SelectedCurrencyId };

        var result = { "ISOCurrencySymbol": $scope.Data.ISOCurrencySymbol, "Email": $scope.Data.Email, "CompanyName": $scope.Data.CompanyName, "SourceId": $scope.Data.SourceId, "CurrentCashOnHand": 1000.0, "CurrentInterestBearingDebt": 5000.0, "LastFinancialYearEnd": "2018-02-28T00:00:00", "PercentageOperatingCashRequired": 0.02, "TRBC2012HierarchicalID": "5002150202", "IncomeStatement_CostOfGoodsSold": 10.0, "IncomeStatement_OperatingExpense": 222.0, "IncomeStatement_Revenue": 50000.0, "ExcessCash_CurrentCashOnHandAsAtLastMonthEnd": 0.0, "Currencies": null, "TRBCS": null, "SelectedCurrencyId": null };

        var raw = JSON.stringify(resultx);
        console.log(raw);


        EMPreValuationResource.saveData(raw);

    } 
    
    $scope.change = function (item, value) {
        item.Value = value;
    };

 

    $scope.init = function (data) {
        
        $scope.Data = data;
 
        if ($scope.Data.LastFinancialYearEnd == null)
        {
            //dd - MM - yyyy
            var tDate = new Date();
            $scope.Data.LastFinancialYearEnd = tDate.toDateString();
        }
        $scope.Data.TRBCCode = { Activites: [], Industries: [], IndustryGroups: [], BusinessSectors: [], EconomicSectors: [] }
        var _tempActivites = [];
        var _tempIndustries = [];
        var _tempIndustryGroups = [];
        var _tempBusinessSectors = [];
        var _tempEconomicSectors = [];

        if (data.TRBC2012HierarchicalID != "")
            $scope.selectedActivityId = data.TRBC2012HierarchicalID;

        var industry = {Id: 0, Description: ""};
        var industryGroup = { Id: "", Description: "" };
        var businessSector = { Id: "", Description: "" };
        var economicSector = { Id: "", Description: "" };

        for (var i = 0; i < $scope.Data.TRBCS.length; i++) {
            var tmp = $scope.Data.TRBCS[i];
            if (economicSector.Id != tmp[0].substring(0,2))
            {
                economicSector = { Id: tmp[0].substring(0, 2), Description: tmp[1] };
                $scope.Data.TRBCCode.EconomicSectors.push(economicSector);
            }

            if (businessSector.Id != tmp[0].substring(0, 4)) {
                businessSector = { Id: tmp[0].substring(0, 4), Description: tmp[2], EconomicSector: economicSector };
                $scope.Data.TRBCCode.BusinessSectors.push(businessSector);
            }

            if (industryGroup.Id != tmp[0].substring(0, 6)) {
                industryGroup = { Id: tmp[0].substring(0, 6), Description: tmp[3], BusinessSector: businessSector };
                $scope.Data.TRBCCode.IndustryGroups.push(industryGroup);
            }

            if (industry.Id != tmp[0].substring(0, 8)) {
                industry = { Id: tmp[0].substring(0, 8), Description: tmp[4], IndustryGroup: industryGroup };
                $scope.Data.TRBCCode.Industries.push(industry);
            }

            activity = { Id: tmp[0].substring(0, 10), Description: tmp[5], Industry: industry };
            $scope.Data.TRBCCode.Activites.push(activity);

            if ($scope.selectedActivityId == activity.Id) {
                $scope.selectedIndustryId = industry.Id;
                $scope.selectedIndustryGroupId = industryGroup.Id;
                $scope.selectedBusinessSectorId = businessSector.Id;
                $scope.selectedEconomicSectorId = economicSector.Id;
            }

        }


        //transform Currencies
        var currencies = [];
        for (var i = 0; i < $scope.Data.Currencies.length; i++)
        {
            var tmp = $scope.Data.Currencies[i];
            currencies.push({ ISOCurrencySymbol: tmp[0], Description: tmp[1] })

        }
        $scope.Data.Currencies = currencies;
    }

    $scope.overridden = function (m, f) {
        var fo = f + '_overridden';
        m[fo] = true;
    }
    function roundTo(n, digits) {
        try
        {
            var negative = false;
            if (digits === undefined) {
                digits = 0;
            }
            if (n < 0) {
                negative = true;
                n = n * -1;
            }
            var multiplicator = Math.pow(10, digits);
            n = parseFloat((n * multiplicator).toFixed(11));
            n = (Math.round(n) / multiplicator).toFixed(2);
            if (negative) {
                n = (n * -1).toFixed(2);
            }
            return Number(n);
        }
        catch(ex)
        { 
            return 0;
        }
    }
    $scope.focusedPercentage = {};

















    $scope.$watch('selectedActivityId', function () {
        var activity = getByField($scope.Data.TRBCCode.Activites, "Id", $scope.selectedActivityId);
        if (activity != null) {
            $scope.selectedIndustryId = activity.Industry.Id;
            $scope.selectedIndustryGroupId = activity.Industry.IndustryGroup.Id;
            $scope.selectedBusinessSectorId = activity.Industry.IndustryGroup.BusinessSector.Id;
            $scope.selectedEconomicSectorId = activity.Industry.IndustryGroup.BusinessSector.EconomicSector.Id;
        }
    });

    $scope.$watch('selectedIndustryId', function () {
        var changedItem = getByField($scope.Data.TRBCCode.Industries, "Id", $scope.selectedIndustryId);
        var subitem = getByField($scope.Data.TRBCCode.Activites, "Id", $scope.selectedActivityId);
        if (subitem != null && subitem.Industry.Id != changedItem.Id) {
            $scope.selectedActivityId = "";
        }
    });
    $scope.$watch('selectedIndustryGroupId', function () {
        var changedItem = getByField($scope.Data.TRBCCode.IndustryGroups, "Id", $scope.selectedIndustryGroupId);
        var subitem = getByField($scope.Data.TRBCCode.Industries, "Id", $scope.selectedIndustryId);
        if (subitem != null && subitem.IndustryGroup.Id != changedItem.Id) {


            $scope.selectedIndustryId = "";
            $scope.selectedActivityId = "";
        }
    });

    $scope.$watch('selectedBusinessSectorId', function () {
        var changedItem = getByField($scope.Data.TRBCCode.BusinessSectors, "Id", $scope.selectedBusinessSectorId);
        var subitem = getByField($scope.Data.TRBCCode.IndustryGroups, "Id", $scope.selectedIndustryGroupId);
        if (subitem != null && subitem.BusinessSector.Id != changedItem.Id) {

            $scope.selectedIndustryGroupId = "";
            $scope.selectedIndustryId = "";
            $scope.selectedActivityId = "";
        }

    });

    $scope.$watch('selectedEconomicSectorId', function () {
        var economicSector = getByField($scope.Data.TRBCCode.EconomicSectors, "Id", $scope.selectedEconomicSectorId);
        var businessSector = getByField($scope.Data.TRBCCode.BusinessSectors, "Id", $scope.selectedBusinessSectorId);
        if (businessSector != null && businessSector.EconomicSector.Id != economicSector.Id) {
            $scope.selectedBusinessSectorId = "";
            $scope.selectedIndustryGroupId = "";
            $scope.selectedIndustryId = "";
            $scope.selectedActivityId = "";
        }
    });

     
    var getUniqueBy = function (from, p) {
        var flags = [], to = [], l = from.length, i;

        for (i = 0; i < l; i++) {
            if (flags[from[i][p]]) continue;
            flags[from[i][p]] = true;
            to.push(from[i]);
        }
        return to;
    }

    var getByField = function (from, p, value) {
        var result = null, l = from.length, i;

        for (i = 0; i < l; i++) {
            if (from[i][p] != value) continue;
            result = from[i];
            break;
        }
        return result;
    }
}]);


