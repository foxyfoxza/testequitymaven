﻿using EquityMaven.CommonEnums;
using EquityMaven.Data.Calculations;
using EquityMaven.Data.Entities;
using EquityMaven.ImportExport;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.Repository.DBInitialization
{
    internal static class PopulateDataOnInitialize
    {
        private static void Seed1 (EntityDataManager DataManager, string annualFromDesc, string monthlyFromDesc, DateTime annualFromDateTime, DateTime monthlyFromDateTime)
        {
            ImportConversion annualImport;
            ImportConversion monthlyImport;
            InputBatch inputBatchAnnual;
            InputBatch inputBatchMonthly;


            var id = Guid.Empty;

            inputBatchAnnual = new InputBatch { ActiveFrom = annualFromDateTime, Created = annualFromDateTime, LastUpdated = annualFromDateTime, Description = annualFromDesc, BatchType = BatchType.Annual };
            inputBatchMonthly = new InputBatch { ActiveFrom = monthlyFromDateTime, Created = monthlyFromDateTime, LastUpdated = monthlyFromDateTime, Description = monthlyFromDesc, BatchType = BatchType.Monthly };

            var TestFileName1 = System.IO.Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "TestData", "testfile1.xlsx");
            if (!System.IO.File.Exists(TestFileName1))
                throw new Exception($"Could not find the source data in {TestFileName1}");


            annualImport = new ImportConversion(TestFileName1, inputBatchAnnual, DataManager.GetCountries(), DataManager.GetMoodysRatings());
            monthlyImport = new ImportConversion(TestFileName1, inputBatchMonthly, DataManager.GetCountries(), DataManager.GetMoodysRatings());

            DataManager.ImportData(annualImport); ;
            DataManager.ImportData(monthlyImport); ;

            DataManager.SaveGlobalDefaults(new GlobalDefaults { InputBatch = inputBatchAnnual, PercentageOperatingCashRequired = (decimal)(35.0 / 100.0), IlliquidityDiscount = (decimal)(2.0 / 100.0) });

            DataManager.SaveGlobalDefaults(new GlobalDefaults { InputBatch = inputBatchAnnual, PercentageOperatingCashRequired = (decimal)(35.0 / 100.0), IlliquidityDiscount = (decimal)(2.0 / 100.0) });

            var client = new Client { MemberId = 1120, Email = "russell@bluegrassdigital.com" , Created = DateTime.UtcNow, LastUpdated = DateTime.UtcNow};
            DataManager.SaveClient(client);

            var prior = new FullValuationInputYear(FinancialYearType.ActualPrior)
            {
                BalanceSheetEquityAndLiabilities_AccountsPayable = 606519,
                BalanceSheetEquityAndLiabilities_LongTermDebt = 0,
                BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = 0,
                BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = 467259,
                BalanceSheetEquityAndLiabilities_ShareholderLoans = 0,
                BalanceSheetEquityAndLiabilities_ShortTermDebt = 0,
                BalanceSheetTotalAssets_AccountsReceiveable = 1711697,
                BalanceSheetTotalAssets_CashAndCashEquivalents = 1317910,
                BalanceSheetTotalAssets_FixedAssets = 1060328,
                BalanceSheetTotalAssets_IntangibleAssets = 0,
                BalanceSheetTotalAssets_Inventory = 0,
                BalanceSheetTotalAssets_OtherCurrentAssets = 383503,
                CapitalExpenditure_CapitalExpenditure = 0,
                IncomeStatement_CostOfGoodsSold = 1305107,
                IncomeStatement_DepreciationAndAmortisationExpense = 354554,
                IncomeStatement_InterestExpense = 33582,
                IncomeStatement_NonCashExpense = 1664284,
                IncomeStatement_OperatingExpense = 19586949,
                IncomeStatement_Revenue = 23636936,

                Created = DateTime.UtcNow,
                LastUpdated = DateTime.UtcNow
            };
            
            var actual = new FullValuationInputYear(FinancialYearType.ActualCurrent)
            {
                BalanceSheetEquityAndLiabilities_AccountsPayable = 437226,
                BalanceSheetEquityAndLiabilities_LongTermDebt = 0,
                BalanceSheetEquityAndLiabilities_OtherCurrentLiabilities = 0,
                BalanceSheetEquityAndLiabilities_OtherLongTermLiabilities = 597269,
                BalanceSheetEquityAndLiabilities_ShareholderLoans = 0,
                BalanceSheetEquityAndLiabilities_ShortTermDebt = 0,
                BalanceSheetTotalAssets_AccountsReceiveable = 4045036,
                BalanceSheetTotalAssets_CashAndCashEquivalents = 1089894,
                BalanceSheetTotalAssets_FixedAssets = 1748926,
                BalanceSheetTotalAssets_IntangibleAssets = 0,
                BalanceSheetTotalAssets_Inventory = 0,
                BalanceSheetTotalAssets_OtherCurrentAssets = 400897,
                CapitalExpenditure_CapitalExpenditure = 0,
                IncomeStatement_CostOfGoodsSold = 1814682,
                IncomeStatement_DepreciationAndAmortisationExpense = 448262,
                IncomeStatement_InterestExpense = 42598,
                IncomeStatement_NonCashExpense = 970513,
                IncomeStatement_OperatingExpense = 24339108,
                IncomeStatement_Revenue = 29884199,
                SustainableEBITDA_NonRecurringExpenses = 0,
                SustainableEBITDA_NonRecurringIncome = 0,

                Created = DateTime.UtcNow,
                LastUpdated = DateTime.UtcNow
            };

            var budget = new FullValuationInputYear(FinancialYearType.Budget)
            {
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = (decimal)0.135357015926711,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = (decimal)0.24093808171349,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = 0,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = (decimal)0.220918596205837,
                BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = 0,
                CapitalExpenditure_PercentageForecastCapitalExpenditure = (decimal)0.05,
                IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal) 0.94,
                IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = (decimal) 0.032475790969,
                IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal) 0.82,
                IncomeStatementAnalysis_PercentageRevenueGrowth = (decimal) 0.17,
                SustainableEBITDA_NonRecurringExpenses = (decimal) 0,
                SustainableEBITDA_NonRecurringIncome = (decimal) 0,

                Created = DateTime.UtcNow,
                LastUpdated = DateTime.UtcNow
            };

            var forecast1 = new FullValuationInputYear(FinancialYearType.Forecast1)
            {
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = (decimal) 0.135357015926711,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = (decimal) 0.24093808171349,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold =  0,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = (decimal)0.220918596205837,
                BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = (decimal) 0,
                CapitalExpenditure_PercentageForecastCapitalExpenditure = (decimal) 0.03,
                IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal) 0.94,
                IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = (decimal) 0.032475790969,
                IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal) 0.825,
                IncomeStatementAnalysis_PercentageRevenueGrowth = (decimal) 0.15,

                Created = DateTime.UtcNow,
                LastUpdated = DateTime.UtcNow
            };

            var forecast2 = new FullValuationInputYear(FinancialYearType.Forecast2)
            {
                BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = (decimal) 0.135357015926711,
                BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = (decimal) 0.24093808171349,
                BalanceSheetAnalysis_PercentageInventoryOverCostOfGoodsSold = (decimal) 0,
                BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = (decimal) 0.220918596205837,
                BalanceSheetAnalysis_PercentageOtherCurrentLiabilitiesOverCostOfGoodsSold = (decimal) 0,
                CapitalExpenditure_PercentageForecastCapitalExpenditure = (decimal) 0.01,
                IncomeStatementAnalysis_PercentageGrossProfitMargin = (decimal) 0.94,
                IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = (decimal) 0.032475790969,
                IncomeStatementAnalysis_PercentageOperatingExpensesOfRevenue = (decimal) 0.825,
                IncomeStatementAnalysis_PercentageRevenueGrowth = (decimal) 0.12,

                Created = DateTime.UtcNow,
                LastUpdated = DateTime.UtcNow
            };


        

            var fv = new FullValuationInput (){ Description = "autopopulate", CompanyName = "abc", CurrentCashOnHand = 1450000, LastFinancialYearEnd = new DateTime(2017, 02, 28), ValuationDate = new DateTime(2018 , 04 , 30), Client = client, ISOCurrencySymbol = "ZAR", ThreeLetterISORegionName = "ZAF", Created = DateTime.UtcNow, LastUpdated = DateTime.UtcNow, TRBC2012HierarchicalID = "5720101010", DateOfCommencement =  new DateTime (2009, 6, 30) , NumberOfEmployees = 20}; 



            var finyears = new List<FullValuationInputYear>();

            finyears.Add(prior);
            finyears.Add(actual);
            finyears.Add(budget);
            finyears.Add(forecast1);
            finyears.Add(forecast2);

            ApplyDefault(budget, actual);
            ApplyDefault(forecast1, budget);
            ApplyDefault(forecast2, forecast1);
            

            foreach (var item in finyears)
            {
                fv.FinancialYears.Add(item);
            }

            //FinancialValuation.Calculate(fv, finyears, DataManager.GetThompsonReutersBusinessClassifications(), DataManager.GetTRBCValues(), DataManager.GetCurrencyRiskFreeRates(), DataManager.GetCurrencies(), DataManager.GetCountryTaxRates(), DataManager.GetGlobalDefaults(), DataManager.GetMoodysRatings(), DataManager.GetCorporateBondSpreads(), DataManager.GetCountries(), DataManager.GetCountryMoodysRatings());
            var financialValuation = new FinancialValuation(fv, finyears, DataManager.GetThompsonReutersBusinessClassifications(), DataManager.GetTRBCValues(), DataManager.GetCurrencyRiskFreeRates(), DataManager.GetCurrencies(), DataManager.GetCountryTaxRates(), DataManager.GetGlobalDefaults(), DataManager.GetMoodysRatings(), DataManager.GetCorporateBondSpreads(), DataManager.GetCountries(), DataManager.GetCountryMoodysRatings());

            financialValuation.Calculate(fv);
            DataManager.SaveFullValuation(fv);

            foreach (var item in fv.FinancialYears)
            {
                Console.WriteLine($"{(FinancialYearType)item.FinancialYearTypeValue} {item.FullValuationInputId} {item.Id}");
            }

            var fullValuationReport = new FullValuationReport();
            financialValuation.Apply(fullValuationReport);
            DataManager.SaveFullValuationReport(fullValuationReport);




        }




        private static void Seed(EntityDataManager DataManager, string annualFromDesc, string monthlyFromDesc, DateTime annualFromDateTime, DateTime monthlyFromDateTime)
        {
            ImportConversion annualImport;
            ImportConversion monthlyImport;
            InputBatch inputBatchAnnual;
            InputBatch inputBatchMonthly;


            var id = Guid.Empty;

            inputBatchAnnual = new InputBatch { ActiveFrom = annualFromDateTime, Created = annualFromDateTime, LastUpdated = annualFromDateTime, Description = annualFromDesc, BatchType = BatchType.Annual };
            inputBatchMonthly = new InputBatch { ActiveFrom = monthlyFromDateTime, Created = monthlyFromDateTime, LastUpdated = monthlyFromDateTime, Description = monthlyFromDesc, BatchType = BatchType.Monthly };

            var TestFileName1 = System.IO.Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "TestData", "testfile1.xlsx");
            if (!System.IO.File.Exists(TestFileName1))
                throw new Exception($"Could not find the source data in {TestFileName1}");


            annualImport = new ImportConversion(TestFileName1, inputBatchAnnual, DataManager.GetCountries(), DataManager.GetMoodysRatings());
            monthlyImport = new ImportConversion(TestFileName1, inputBatchMonthly, DataManager.GetCountries(), DataManager.GetMoodysRatings());

            DataManager.ImportData(annualImport); ;
            DataManager.ImportData(monthlyImport); ;

            DataManager.SaveGlobalDefaults(new GlobalDefaults { InputBatch = inputBatchAnnual, PercentageOperatingCashRequired = (decimal)(35.0 / 100.0), IlliquidityDiscount = (decimal)(2.0 / 100.0) });

            DataManager.SaveGlobalDefaults(new GlobalDefaults { InputBatch = inputBatchAnnual, PercentageOperatingCashRequired = (decimal)(35.0 / 100.0), IlliquidityDiscount = (decimal)(2.0 / 100.0) });

            var client = new Client { MemberId = 1120, Email = "russell@bluegrassdigital.com", Created = DateTime.UtcNow, LastUpdated = DateTime.UtcNow };
            DataManager.SaveClient(client);
           
            FullValuationInput fullValuationInput;
            List<FullValuationInputYear> fullValuationInputYears;
            FinancialValuation financialValuation;
            TestData.TestDataHelper.PopulateFinancialValuation(out financialValuation, out fullValuationInput, out fullValuationInputYears);
            fullValuationInput.Client = client;

           
            financialValuation.Calculate(fullValuationInput);
            DataManager.SaveFullValuation(fullValuationInput);

            foreach (var item in fullValuationInput.FinancialYears)
            {
                Console.WriteLine($"{(FinancialYearType)item.FinancialYearTypeValue} {item.FullValuationInputId} {item.Id}");
            }

            var fullValuationReport = new FullValuationReport();
            financialValuation.Apply(fullValuationReport);
            DataManager.SaveFullValuationReport(fullValuationReport);




        }

        private static void ApplyDefault(FullValuationInputYear item, FullValuationInputYear _prior)
        {
            item.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue = item.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue != 0 ? item.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue : _prior?.IncomeStatementAnalysis_PercentageDepreciationAndAmortOfRevenue ?? 0;

            item.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue = item.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue != 0 ? item.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue : _prior?.IncomeStatementAnalysis_PercentageNonCashExpensesOfRevenue ?? 0;

            item.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue = item.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue != 0 ? item.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue : _prior?.BalanceSheetAnalysis_PercentageAccountReceivableOverRevenue ?? 0;

            item.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold = item.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold != 0 ? item.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold : _prior?.BalanceSheetAnalysis_PercentageOtherCurrentAssetsOverCostOfGoodsSold ?? 0;

            item.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold = item.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold != 0 ? item.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold : _prior?.BalanceSheetAnalysis_PercentageAccountsPayableOverCostOfGoodsSold ?? 0;
        }
        public static void Execute(EquityMavenContext context)
        {
            var DataManager = new EntityDataManager(context);
            Seed(DataManager, annualFromDesc: "Initial Annual Batch", monthlyFromDesc: "Initial Montly Batch", annualFromDateTime: new DateTime(2017, 1, 1), monthlyFromDateTime: new DateTime(2017, 1, 1));
            //Seed(DataManager, annualFromDesc: "second Annual Batch", monthlyFromDesc: "second Montly Batch", annualFromDateTime: new DateTime(2018, 1, 1), monthlyFromDateTime: new DateTime(2018, 1, 1));//try 2 of them
        }
    }
}
