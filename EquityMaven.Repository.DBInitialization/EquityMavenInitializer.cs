﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using EquityMaven.Data;
using EquityMaven.Interfaces;
using EquityMaven.Data.Entities;
using EquityMaven.Data.Extensions;

namespace EquityMaven.Repository.DBInitialization
{
    public class EquityMavenDropCreateDatabaseIfModelChangesInitializer : DropCreateDatabaseIfModelChanges<EquityMavenContext>
    {
        protected override void Seed(EquityMavenContext context)
        {
            PopulateDataOnInitialize.Execute(context);
        }
    }

    public class EquityMavenDropCreateDatabaseAlwaysInitializer : DropCreateDatabaseAlways<EquityMavenContext>
    {
        protected override void Seed(EquityMavenContext context)
        {
            PopulateDataOnInitialize.Execute(context);
        }
    }

}
