﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;
using EquityMaven.Repository;
using EquityMaven.Data;
using System.Linq;
using EquityMaven.Data.Entities;


namespace EquityMaven.Tests.Repository
{
    [TestClass]
    public class ClientDataTests : RepoTestBase
    {
        private TRBCValue _OilExplorationProductionOffshore;
        private TRBCValue _MultilineUtilities;


        private string _RegistrationMemberEmail;
        private PrelimEvaluation _ExistingPreval = null;
        private Client _RegistrationClient1 = null;
        private ClientProfile _RegistrationClientProfile = null;

        [TestInitialize]
        public override void InitializeTest()
        {
            _OilExplorationProductionOffshore = DataManager.GetTRBCValue("5010202012");

            _MultilineUtilities = DataManager.GetTRBCValue("5910401010");
            //default to AUD, but get any other if not active
            //var _Currency = DataManager.GetCurrencies().ToList().OrderByDescending(o => { switch (o.ISOCurrencySymbol) { case "AUD": return 9; case "ZAR": return 8; case "USD": return 7; default: return 0; } }).FirstOrDefault();
            try
            {
                _RegistrationMemberEmail = $"{Guid.NewGuid().ToString().Replace("-", "")}@{Guid.NewGuid().ToString().Replace("-", "")}.com";
                _ExistingPreval = new PrelimEvaluation {Email = _RegistrationMemberEmail, ISOCurrencySymbol = "AUD", CompanyName = "My Test Business", CurrentCashOnHand = 1000000, CurrentInterestBearingDebt = 1000000, ExcessCash_CurrentCashOnHandAsAtLastMonthEnd= 100000, IncomeStatement_CostOfGoodsSold = 500000, IncomeStatement_OperatingExpense = 200000, IncomeStatement_Revenue = 5000000, LastFinancialYearEnd = new DateTime (2017,2,28), PercentageOperatingCashRequired = (decimal)0.02, TRBC2012HierarchicalID = _OilExplorationProductionOffshore.ThompsonReutersBusinessClassification.ActivityCode, ThomsonReutersBusinessClassification = _OilExplorationProductionOffshore.ThompsonReutersBusinessClassification};

                DataManager.SavePrelimEvaluation(_ExistingPreval);

                _RegistrationClient1 = new Client { CompanyName = _ExistingPreval?.CompanyName, Email = _RegistrationMemberEmail, MemberId = 0 };
                DataManager.SaveClient(_RegistrationClient1);
                _RegistrationClientProfile = new ClientProfile(_RegistrationClient1, _ExistingPreval);
                DataManager.SaveClientProfiles(_RegistrationClientProfile);
            }
            catch (Exception ex)
            {

            }

        }

        [TestMethod]
        public void TestRepo_ClientRegistrationTestSave()
        {
            Assert.IsNotNull(_RegistrationClient1);
            Assert.AreNotEqual(_RegistrationClient1.Id, Guid.Empty);
        }

        [TestMethod]
        public void TestRepo_ClientExistingPrevalTestSave()
        {
            Assert.IsNotNull(_ExistingPreval);
            Assert.AreNotEqual(_ExistingPreval.Id, Guid.Empty);
        }

    }
}
