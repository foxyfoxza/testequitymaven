﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;
using EquityMaven.Repository;
using EquityMaven.Data;
using System.Linq;
using EquityMaven.Data.Entities;
using EquityMaven.Repository.DBInitialization;
using EquityMaven.CommonEnums;

namespace EquityMaven.Tests.Repository
{
    [TestClass]
    public class RepoTestBase
    {
         
        
        protected static String DeploymentDir
        {
            get
            {
                return AppDomain.CurrentDomain.GetData("TestDeploymentDir")?.ToString();
            }
            set
            {
                AppDomain.CurrentDomain.SetData("TestDeploymentDir", value);

            }
        }
        [AssemblyInitialize]
        public static void MyAssemblyInitialize(TestContext context)
        {
            DeploymentDir = System.IO.Path.Combine(context.TestDeploymentDir, string.Empty);
            Database.SetInitializer(new EquityMavenDropCreateDatabaseIfModelChangesInitializer());
            DataManager = EquityMaven.Repository.EntityDataManagerService.DataManager;
             
        }

        protected static EntityDataManager DataManager;
        protected ClientInputsGeneral ClientInputsGeneral;

        [TestInitialize]
        public virtual void InitializeTest()
        {
            
        }

    }
}