﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;
using EquityMaven.Repository;
using EquityMaven.Data;
using System.Linq;
using EquityMaven.Data.Entities;

namespace EquityMaven.Tests.Repository
{
    [TestClass]

    public class ThomsonReutersBusinessClassificationTests : RepoTestBase

    {
        private TRBCValue _OilExplorationProductionOffshore;
        private TRBCValue _MultilineUtilities;

        [TestInitialize]
        public override void InitializeTest()
        {
            _OilExplorationProductionOffshore = DataManager.GetTRBCValue("5010202012");
            _MultilineUtilities = DataManager.GetTRBCValue("5910401010");
        }
        [TestMethod]
        public void TestRepo_ThomsonReutersBusinessClassification_TotalDebtMarketCap()
        {
            Assert.AreEqual(Math.Round((decimal)0.275568916794078, 4), Math.Round(_OilExplorationProductionOffshore.TotalDebtMarketCap.Value, 4), "TotalDebtMarketCap");
        }

        [TestMethod]
        public void TestRepo_ThomsonReutersBusinessClassification_UnleveredBeta()
        {
            Assert.AreEqual(Math.Round((decimal)1.60687275307831, 4), Math.Round(_OilExplorationProductionOffshore.UnleveredBeta.Value, 4), "UnleveredBeta");
        }

        [TestMethod]
        public void TestRepo_ThomsonReutersBusinessClassification_InterestCoverAdjusted()
        {
            Assert.AreEqual(Math.Round((decimal)-11.4096374846, 4), Math.Round(_OilExplorationProductionOffshore.InterestCoverAdjusted.Value, 4), "InterestCoverAdjusted");
        }

        [TestMethod]
        public void TestRepo_ThomsonReutersBusinessClassification_TotalDebtEBITDA()
        {
            Assert.AreEqual(Math.Round((decimal)3.20733502942525, 4), Math.Round(_MultilineUtilities.TotalDebtEBITDA.Value, 4), "TotalDebtEBITDA");
        }

        [TestMethod]
        public void TestRepo_ThomsonReutersBusinessClassification_Africa()
        {
            Assert.IsTrue(_MultilineUtilities.Africa.HasValue, "Africa");
        }
        

        [TestMethod]
        public void TestRepo_ThomsonReutersBusinessClassification_AccRecDaysAve()
        {
            Assert.AreEqual(Math.Round((decimal)49.3486939070818, 4), Math.Round(_MultilineUtilities.AccRecDaysAve.Value, 4), "AccRecDaysAve");
        }
        [TestMethod]
        public void TestRepo_ThomsonReutersBusinessClassification_EBITMargin()
        {
            Assert.AreEqual(Math.Round((decimal)0.0648438375, 4), Math.Round(_MultilineUtilities.EBITMargin.Value, 4), "EBITMargin");
        }

    }
}
