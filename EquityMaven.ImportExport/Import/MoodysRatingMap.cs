﻿using EquityMaven.Data;
using EquityMaven.Data.Entities;
using FileHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.ImportExport.Import
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines]
    [IgnoreFirst(1)]
    public class MoodysRatingMap : ImportFileBase
    {
        [FieldOrder(0)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string Rating { get; set; }
        [FieldOrder(2)]
        [FieldConverter(typeof(PercentageConverter))]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public decimal? DefaultSpread { get; set; }

         



    }
}
