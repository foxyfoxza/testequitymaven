﻿using EquityMaven.Data;
using FileHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.ImportExport.Import
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines]
    [IgnoreFirst(1)]
    public class TRBCImportMap : ImportFileBase 
    {
        [FieldOrder(0)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string _X { get; set; }


        [FieldOrder(1)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string ECONOMICSECTOR { get; set; }

        [FieldOrder(2)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string BUSINESSSECTOR { get; set; }

        [FieldOrder(3)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string INDUSTRYGROUP { get; set; }

        [FieldOrder(4)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string INDUSTRY { get; set; }

        [FieldOrder(5)]        
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string ACTIVITY { get; set; }

        [FieldOrder(6)]        
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string TRBC2012HierarchicalID { get; set; }

        //[FieldOrder(6)]
        //[FieldTrim(TrimMode.Both)]
        //[FieldQuoted(QuoteMode.OptionalForBoth)]
        //public string Description { get; set; }
        [FieldOrder(7)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? UnleveredBeta { get; set; }

        [FieldOrder(8)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string PH_8 { get; set; }


        [FieldOrder(9)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? World { get; set; }
        [FieldOrder(10)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? Africa { get; set; }
        [FieldOrder(11)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? NorthAmerica { get; set; }
        [FieldOrder(12)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? SouthCentralAmerica { get; set; }
        [FieldOrder(13)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? Asia { get; set; }
        [FieldOrder(14)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? AustraliaNewZealand { get; set; }
        [FieldOrder(15)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? EasternEuropeRussia { get; set; }
        [FieldOrder(16)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? SouthernEurope { get; set; }
        [FieldOrder(17)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? WesternEurope { get; set; }
        [FieldOrder(18)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? MiddleEast { get; set; }

        [FieldOrder(19)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string Placeholder1 { get; set; }


        [FieldOrder(20)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? CurrentRatio { get; set; }
        [FieldOrder(21)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? InterestCover { get; set; }
        [FieldOrder(22)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? GrossMargin { get; set; }
        [FieldOrder(23)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? EBITDAMargin { get; set; }
        [FieldOrder(24)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? EBITMargin { get; set; }
        [FieldOrder(25)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? AccRecDaysave { get; set; }
        [FieldOrder(26)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? AccPayableDaysave { get; set; }
        [FieldOrder(27)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? InventoryDaysave { get; set; }
        [FieldOrder(28)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? TotalDebtMarketCap { get; set; }
        [FieldOrder(29)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? TotalDebtEBITDA { get; set; }

        [FieldOrder(30)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? Interestcoveradjusted { get; set; }

        [FieldOrder(31)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string PH31{ get; set; }

        [FieldOrder(32)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string PH32 { get; set; }

        [FieldOrder(33)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string PH33 { get; set; }

        [FieldOrder(34)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string PH34 { get; set; }

        [FieldOrder(35)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(YesNoConverter))]
        public bool? AutoSelect { get; set; }
        [FieldOrder(36)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string PH36 { get; set; }

        [FieldOrder(37)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(YesNoConverter))]
        public bool? ManualSelect { get; set; }
         
    }
}
