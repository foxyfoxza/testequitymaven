﻿using EquityMaven.Data;
using FileHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.ImportExport.Import
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines]
    [IgnoreFirst(1)]
    public class CorporateBondSpreadMap : ImportFileBase 
    {
        [FieldOrder(0)]
        [FieldConverter(typeof(PercentageConverter))]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public decimal? InterestCoverageFrom { get; set; }
        [FieldOrder(1)]
        [FieldConverter(typeof(PercentageConverter))]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public decimal? InterestCoverageTo { get; set; }

        [FieldOrder(2)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string Rating { get; set; }

        [FieldOrder(3)]
        [FieldConverter(typeof(PercentageConverter))]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public decimal? Spread { get; set; }

       

        //public override RawCorporateBondSpread As()
        //{
        //    return new RawCorporateBondSpread {InterestCoverageFrom = this.InterestCoverageFrom, InterestCoverageTo = this.InterestCoverageTo, Rating = this.Rating, Spread = this.Spread};
        //}
    }
}
