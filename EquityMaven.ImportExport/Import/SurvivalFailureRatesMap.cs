﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.ImportExport.Import
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines]
    [IgnoreFirst(1)]
    public class SurvivalFailureRatesMap : ImportFileBase
    {
        [FieldOrder(0)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        //[FieldConverter(typeof(PercentageConverter))]
        public int Year { get; set; }

        [FieldOrder(1)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? ProbabilityOfSurvival { get; set; }

        [FieldOrder(2)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? ProbabilityOfFailure { get; set; }
         
    }
}
