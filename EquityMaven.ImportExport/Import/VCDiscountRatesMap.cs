﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.ImportExport.Import
{

    //    Start-up
    //First Stage or "early development"
    //Second stage or "expansion"
    //Bridge/IPO

    [DelimitedRecord(",")]
    [IgnoreEmptyLines]
    [IgnoreFirst(1)]
    public class VCDiscountRatesMap : ImportFileBase
    {

        [FieldOrder(0)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
         
        public string StageOfDevelopment { get; set; }

        [FieldOrder(1)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? ScherlisMid { get; set; }

        [FieldOrder(2)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string PH2 { get; set; }

        [FieldOrder(3)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string PH3 { get; set; }

        [FieldOrder(4)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? Plummer { get; set; }

        [FieldOrder(5)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string PH5 { get; set; }

        [FieldOrder(6)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string PH6 { get; set; }

        [FieldOrder(7)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? ScherlisSahlman { get; set; }

        [FieldOrder(8)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string PH8 { get; set; }

        [FieldOrder(9)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? PlummerMid{ get; set; }





        


    }
}
