﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.ImportExport.Import
{
 
    [DelimitedRecord(",")]
    [IgnoreEmptyLines]
    [IgnoreFirst(1)]
    public class GlobalDefaultMap : ImportFileBase
    {
        [FieldOrder(0)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? IlliquidityDiscount { get; set; }

        [FieldOrder(1)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? PercentageOperatingCashRequired { get; set; }

        [FieldOrder(2)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? RelativeVolatility { get; set; }
        [FieldOrder(3)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(PercentageConverter))]
        public decimal? EquityRiskPremium { get; set; }
        


    }
}
