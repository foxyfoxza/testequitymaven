﻿using EquityMaven.Data;
using FileHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.ImportExport.Import
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines]
    [IgnoreFirst(1)]
    public class CountryMoodysRatingMap : ImportFileBase
    {
        [FieldOrder(0)]
         
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string Country { get; set; }

        [FieldOrder(1)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string MoodysRating { get; set; }
        
    }

     
}
