﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.ImportExport.Import
{
   
    public abstract class ImportFileBase
    {
        //public abstract String Headers { get; }

        //public abstract TDestination As();

       
       
    }

    public class GuidConverter : ConverterBase
    {
        public override object StringToField(string from)
        {
            Guid g;
            return Guid.TryParse(Convert.ToString(@from).Trim(), out g) ? g : Guid.Empty;
        }

        public override string FieldToString(object from)
        {
            return Convert.ToString(from);
        }
    }


    
    public class YesNoConverter : ConverterBase
    {
        public override object StringToField(string from)
        {
            var test = from?.Trim()?.ToLower();
            return test == "yes" || test == "true" || test == "1";
        }

        public override string FieldToString(object from)
        {
            return Convert.ToString(from);
        }
    }


    public class PercentageConverter : ConverterBase
    {
        public override object StringToField(string from)
        {
            if (string.IsNullOrWhiteSpace(from) || from == "#NUM!")
                return (decimal?)null;
            decimal result;
            result = decimal.TryParse(Convert.ToString(@from).Replace("%", "").Trim(), out result) ? result : 0;

            return result / (from.Contains("%") ? 100 : 1);
            //return Guid.TryParse(Convert.ToString(@from).Trim(), out g) ? g : Guid.Empty;
        }

        public override string FieldToString(object from)
        {
            return Convert.ToString(from);
        }
    }

    


}
