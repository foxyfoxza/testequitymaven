﻿using EquityMaven.Data;
using FileHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.ImportExport.Import
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines]
    [IgnoreFirst(1)]
    public class CurrencyRiskFreeRateImportMap : ImportFileBase 
    {
        [FieldOrder(0)]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public string Value { get; set; }

        [FieldOrder(1)]
        
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public String Currency { get; set; }

        [FieldOrder(2)]
        [FieldConverter(typeof(PercentageConverter))]
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted(QuoteMode.OptionalForBoth)]
        public decimal? RiskFreeRate { get; set; }    
    }
}
