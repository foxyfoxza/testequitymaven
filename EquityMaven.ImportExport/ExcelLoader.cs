﻿using EquityMaven.Data;
using EquityMaven.ImportExport.Import;
using FileHelpers;
using FileHelpers.ExcelNPOIStorage;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquityMaven.ImportExport
{
    public abstract class ExcelLoader<TImport> where TImport : ImportFileBase 
    {
        private readonly object _locker = new object();
        protected readonly FileHelperEngine<TImport> _engine = new FileHelperEngine<TImport>();
        protected List<TImport> ImportRecords = new List<TImport>();
        public IEnumerable<TImport> Raw { get { return ImportRecords; } }
        protected string _sheetName;
        public string SheetName { get { return _sheetName ?? GetDefaultSheetName(); } set { _sheetName = value; } }
        protected abstract string GetDefaultSheetName();

        public ExcelLoader()
        {
            _engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
        }


        protected virtual int StartColumn { get; set; } = 0;
        protected virtual int StartRow { get; set; } = 1;
        protected virtual int ExcelReadStopAfterEmptyRows { get; set; } = 5;
       


        protected void Import(string fileName, string sheetName)
        {
            lock (_locker)
            {
                var provider = new ExcelNPOIStorage(typeof(TImport))
                {
                    FileName = fileName,// Directory.GetCurrentDirectory() + @"\testBlankFields.xlsx",
                    StartColumn = StartColumn,
                    StartRow = StartRow,
                    SheetName = sheetName,
                    ExcelReadStopAfterEmptyRows = ExcelReadStopAfterEmptyRows
                };
                provider.Progress += (sender, e) =>
                {
                    System.Diagnostics.Debug.WriteLine($"{sheetName} {e.CurrentRecord}");
                };

                var workbook = new XSSFWorkbook(provider.FileName);
                var row = workbook.GetSheet(sheetName).GetRow(0);
                var firstLineFields = row.Cells.Select(c => c.StringCellValue.Trim());

                ImportRecords.AddRange(((TImport[])provider.ExtractRecords()));
            }
        }
        public void Import(string fileName)
        {
            Import(fileName, SheetName);
        }

        public IEnumerable<string> ImportErrors
        {
            get
            {
                return _engine
                    .ErrorManager
                    .Errors
                    .Select(x => x.ExceptionInfo.Message);
            }
        }
    }
}
