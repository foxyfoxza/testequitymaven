﻿using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.ImportExport.Import;
namespace EquityMaven.ImportExport.Implementation
{
 
    public class CountryTaxRateImport : ExcelLoader<CountryTaxRateMap>
    {
        protected override string GetDefaultSheetName()
        {
            return EquityMaven.Tools.SettingsHelper.GetSetting<string>("CountryTaxRateImportSheetName", "CountryTaxRate_2_Annual");
        }
        public BatchType BatchType = BatchType.Monthly;

        //public void Convert(InputBatch inputBatch, IEnumerable<Country> Countries , out IEnumerable<CountryTaxRate> _CountryTaxRates)
        //{
        //    if (inputBatch.BatchType != BatchType)
        //    {
        //        _CountryTaxRates = Enumerable.Empty<CountryTaxRate>();
        //        return;
        //    }

        //    var CountryTaxRates = new List<CountryTaxRate>();
        //    _CountryTaxRates = CountryTaxRates;

        //    var _countries = ImportRecords.Where(o => (!string.IsNullOrEmpty(o.Country?.Trim()) && !Countries.Any(c => c.CountryEnglishName.ToLower().Trim() == o.Country?.Trim()?.ToLower()))).Select(o => Country.GetCountryByCountryEnglishName(o.Country.Trim())).ToList();
        //    _countries.AddRange(Countries);

        //    foreach (var item in ImportRecords)
        //    {
        //        var country = _countries.FirstOrDefault(o => o.CountryEnglishName.ToLower() == item.Country.ToLower());
        //        CountryTaxRates.Add(new CountryTaxRate { Country = country, TaxRate = !item.TaxRate.HasValue ? 0 : item.TaxRate.Value });
        //    }

        //} 

        public static CountryTaxRate Convert(CountryTaxRateMap from, IEnumerable<Country> countries, InputBatch inputBatch)
        {
            var country = countries.FirstOrDefault(o => o.CountryEnglishName.ToLower() == from.Country.ToLower());
            if (country == null)
            {
                country = Country.GetCountryByCountryEnglishName(from.Country);
            }
            return new CountryTaxRate { Country = country, TaxRate = from.TaxRate.Value, InputBatch = inputBatch };
        }


    }
}
