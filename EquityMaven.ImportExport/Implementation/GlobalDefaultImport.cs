﻿using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.ImportExport.Import;
namespace EquityMaven.ImportExport.Implementation
{

    public class GlobalDefaultImport : ExcelLoader<GlobalDefaultMap>
    {
        protected override string GetDefaultSheetName()
        {
            return EquityMaven.Tools.SettingsHelper.GetSetting<string>("GlobalDefaultImportSheetName", "GlobalDefault");
        }
        public BatchType BatchType = BatchType.Monthly;
        public void Convert(InputBatch inputBatch, out IEnumerable<GlobalDefaults> _globalDefaults)
        {
            if (inputBatch.BatchType != BatchType)
            {
                _globalDefaults= Enumerable.Empty<GlobalDefaults>();
                return;
            }
            var GlobalDefaultsRecs = new List<GlobalDefaults>();
            _globalDefaults = GlobalDefaultsRecs;

            //todo what about relative volatility and equity risk premium
            GlobalDefaultsRecs.AddRange(ImportRecords.Select(rec =>
            {
                return new GlobalDefaults{ EquityRiskPremium = rec.EquityRiskPremium.Value, IlliquidityDiscount= rec.IlliquidityDiscount.Value, PercentageOperatingCashRequired = rec.PercentageOperatingCashRequired.Value, RelativeVolatility = rec.RelativeVolatility.Value, InputBatch = inputBatch };
            })
                );
        }

    }

}
