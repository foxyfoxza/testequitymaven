﻿using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.ImportExport.Import;

namespace EquityMaven.ImportExport.Implementation
{
 
     public class CorporateBondSpreadImport : ExcelLoader<CorporateBondSpreadMap>
    {
        protected override string GetDefaultSheetName()
        {
            return EquityMaven.Tools.SettingsHelper.GetSetting<string>("CorporateBondSpreadsImportSheetName", "CorporateBondSpreads_4_Annual");
        }
        public BatchType BatchType = BatchType.Annual;
        //public void Convert(InputBatch inputBatch, out IEnumerable<MoodysRating> _MoodysRatings)
        //{
        //    if (inputBatch.BatchType != BatchType)
        //    {
        //        _MoodysRatings = Enumerable.Empty<MoodysRating>();
        //        return;
        //    }
        //    var MoodysRatings = new List<MoodysRating>();
        //    _MoodysRatings = MoodysRatings;

        //    //todo what about relative volatility and equity risk premium
        //    MoodysRatings.AddRange(ImportRecords.Select(o => o.Rating).Distinct().Where(o => o.Length <= 10).Select(o =>
        //    {
        //        var rec = ImportRecords.FirstOrDefault(r => r.Rating == o);
        //        return new MoodysRating { Rating = rec.Rating, DefaultSpread = rec.DefaultSpread, InputBatch = inputBatch };
        //    })
        //        );
        //}
        public static CorporateBondSpread Convert(CorporateBondSpreadMap from, InputBatch inputBatch)
        {
            return new CorporateBondSpread { InterestCoverageFrom = from.InterestCoverageFrom.Value, InterestCoverageTo= from.InterestCoverageTo.Value, Rating = from.Rating, Spread = from.Spread.Value, InputBatch = inputBatch};
        }
    }
}
