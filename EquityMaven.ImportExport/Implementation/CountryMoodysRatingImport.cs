﻿using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.ImportExport.Import;

namespace EquityMaven.ImportExport.Implementation
{

    public class CountryMoodysRatingImport : ExcelLoader<CountryMoodysRatingMap>
    {
        protected override string GetDefaultSheetName()
        {
            return EquityMaven.Tools.SettingsHelper.GetSetting<string>("CountryMoodysRatingImportSheetName", "CountryMoodysRating_2_Monthly");
        }
        public BatchType BatchType = BatchType.Monthly;
        //public void Convert(InputBatch inputBatch,  IQueryable<Country> Countries, IQueryable<MoodysRating> MoodysRatings, out IEnumerable<CountryMoodysRating> _CountryMoodysRatings)
        //{
        //    if (inputBatch.BatchType != BatchType)
        //    {
        //        _CountryMoodysRatings = Enumerable.Empty<CountryMoodysRating>();

        //        return;
        //    }

        //    var CountryMoodysRatings = new List<CountryMoodysRating>();
        //    _CountryMoodysRatings = CountryMoodysRatings;

        //    //Countries = ImportRecords.Where(o=> !string.IsNullOrEmpty(o.Country?.Trim())).Select(o => Country.GetCountryByCountryEnglishName(o.Country.Trim()));

        //    //MoodysRatings = ImportRecords.Where(o => !string.IsNullOrEmpty(o.MOODYSRATING?.Trim()) ).Select(o => new MoodysRating {Rating = o.MOODYSRATING.Trim()});

        //    foreach (var item in ImportRecords)
        //    {
        //        var country = Countries.FirstOrDefault(o => o.CountryEnglishName.ToLower() == item.Country.ToLower());
        //        var moodysRating = MoodysRatings.FirstOrDefault(o => o.Rating.ToLower() == item.MOODYSRATING.ToLower());
        //        if ()
        //        CountryMoodysRatings.Add(new CountryMoodysRating { Country = country, MoodysRating = moodysRating });
        //    }

        //}

        public static CountryMoodysRating Convert(CountryMoodysRatingMap from, IEnumerable<Country> countries, InputBatch inputBatch)
        {
            var country = countries.FirstOrDefault(o => o.CountryEnglishName.ToLower() == from.Country.ToLower());
            if (country == null)
            {
                country = Country.GetCountryByCountryEnglishName(from.Country);
            }
            return new CountryMoodysRating {Country = country, Rating = from.MoodysRating, InputBatch = inputBatch };
        }

    }
}
