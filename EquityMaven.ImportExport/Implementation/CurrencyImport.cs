﻿using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.ImportExport.Import;
namespace EquityMaven.ImportExport.Implementation
{
    public class CurrencyImport: ExcelLoader<CurrencyRiskFreeRateImportMap>
    {
        public BatchType BatchType = BatchType.Monthly;

        

        protected override string GetDefaultSheetName()
        {
            return  EquityMaven.Tools.SettingsHelper.GetSetting<string>("CurrencyImportSheetName", "CurrencyRiskFreeRate_1_Monthly");
        }

        public void Convert(InputBatch inputBatch, out IEnumerable<Currency> _Currencies, out IEnumerable<CurrencyRiskFreeRate> _CurrencyRiskFreeRates)
        {
            if (inputBatch.BatchType != BatchType || ImportRecords.Count() == 0)
            {
                _Currencies = Enumerable.Empty<Currency>();
                _CurrencyRiskFreeRates = Enumerable.Empty<CurrencyRiskFreeRate>();
                return;
            }

            var Currencies = new List<Currency>();
            var CurrencyRiskFreeRates = new List<CurrencyRiskFreeRate>();
            _Currencies = Currencies;
            _CurrencyRiskFreeRates = CurrencyRiskFreeRates;

            Currencies.AddRange(ImportRecords.Select(o => o.Currency.Substring(0, 3)).Distinct().Select(o => new Currency { InputBatch = inputBatch, ISOCurrencySymbol = o }));

            var _cny = Currencies.FirstOrDefault(o => o.ISOCurrencySymbol == "CNY");
            var _usd = Currencies.FirstOrDefault(o => o.ISOCurrencySymbol == "USD");
            var _gbp = Currencies.FirstOrDefault(o => o.ISOCurrencySymbol == "GBP");

            if (_cny != null)
                _cny.CurrencyEnglishName = "Chinese Yuan Renminbi";
            if (_usd != null)
                _usd.CurrencyEnglishName = "United States Dollars";
            if (_gbp != null)
                _gbp.CurrencyEnglishName = "Great British Pound";


            foreach (var currency in Currencies)
            {
                var rec = ImportRecords.FirstOrDefault(o => o.Currency.Substring(0, 3) == currency.ISOCurrencySymbol);
                if (rec != null && rec.RiskFreeRate.HasValue)
                {
                    CurrencyRiskFreeRates.Add(new CurrencyRiskFreeRate { Currency = currency, RiskFreeRate = rec.RiskFreeRate.Value, InputBatch = inputBatch });
                }
            }


        }

        public static CurrencyRiskFreeRate Convert(CurrencyRiskFreeRateImportMap from, IEnumerable<Currency> currencies, InputBatch inputBatch)
        {
            var currency = currencies.FirstOrDefault(o => o.CurrencyEnglishName.ToLower() == from.Currency.ToLower()) ?? new Currency { InputBatch = inputBatch, ISOCurrencySymbol = from.Currency.Substring(0, 3) };

            return new CurrencyRiskFreeRate { Currency = currency, RiskFreeRate= from.RiskFreeRate.Value, InputBatch = inputBatch };
        }


    }
}
