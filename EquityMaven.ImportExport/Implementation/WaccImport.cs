﻿using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data;
using EquityMaven.ImportExport.Import;
namespace EquityMaven.ImportExport.Implementation
{
    public class WaccImport
    {


        public ClientInputsGeneral GeneralClientInputs { get; private set; }
        public IEnumerable<CurrencyRiskFreeRateImportMap> CurrencyRiskFreeRateImportMapRecords { get; private set; }
        public IEnumerable<CountryMoodysRatingMap> CountryMoodysRatingMapRecords { get; private set; }
        public IEnumerable<CountryTaxRateMap> CountryTaxRateMapRecords { get; private set; }

        public IEnumerable<MoodysRatingMap> MoodysRatingMapRecords { get; private set; }
        public IEnumerable<CorporateBondSpreadMap> CorporateBondSpreadsMapRecords { get; private set; }
        public IEnumerable<TRBCImportMap> TRBCImportMapRecords { get; private set; }
        public IEnumerable<GlobalDefaultMap> GlobalDefaultsRecords { get; private set; }
        public IEnumerable<VCDiscountRatesMap> VCDiscountRatesImportRecords { get; private set; }
        public IEnumerable<SurvivalFailureRatesMap> SurvivalFailureRatesRecords { get; private set; }

        public WaccImport() { }
        public WaccImport(ClientInputsGeneral clientInputsGeneral, IEnumerable<CurrencyRiskFreeRateImportMap> _currencyRiskFreeRateImportMap, IEnumerable<CountryMoodysRatingMap> _countryMoodysRatingMapRecords, IEnumerable<MoodysRatingMap> _moodysRatingMapRecords, IEnumerable<CorporateBondSpreadMap> _corporateBondSpreadsMapRecords, IEnumerable<TRBCImportMap> _trbcImportMapRecords, IEnumerable<CountryTaxRateMap>  _countryTaxRateMapRecords, IEnumerable<GlobalDefaultMap> _globalDefaultsRecords, IEnumerable<VCDiscountRatesMap> _vcDiscountRatesImportRecords, IEnumerable<SurvivalFailureRatesMap> _survivalFailureRatesRecords)
        {
            this.SurvivalFailureRatesRecords = _survivalFailureRatesRecords;
            this.VCDiscountRatesImportRecords = _vcDiscountRatesImportRecords;
            this.GlobalDefaultsRecords = _globalDefaultsRecords;
            this.GeneralClientInputs = clientInputsGeneral;
            this.CurrencyRiskFreeRateImportMapRecords = _currencyRiskFreeRateImportMap;
            this.CountryMoodysRatingMapRecords = _countryMoodysRatingMapRecords;
            this.MoodysRatingMapRecords = _moodysRatingMapRecords;
            this.CorporateBondSpreadsMapRecords = _corporateBondSpreadsMapRecords;
            this.TRBCImportMapRecords = _trbcImportMapRecords;
            this.CountryTaxRateMapRecords = _countryTaxRateMapRecords;
        }


        public Wacc Import()
        {
            var result = new Wacc();

            var table1CurrencyValue = CurrencyRiskFreeRateImportMapRecords.FirstOrDefault(o => o.Currency == GeneralClientInputs.Currency);
            var c68_Table2CountryMoodysRatingValue = CountryMoodysRatingMapRecords.FirstOrDefault(o => o.Country == GeneralClientInputs.Country);
            var c68_Table2CountryTaxRateValue = CountryTaxRateMapRecords.FirstOrDefault(o => o.Country == GeneralClientInputs.Country);

            result.RiskFreeRate = table1CurrencyValue.RiskFreeRate.Value;
            var c67 = GlobalDefaultsRecords.FirstOrDefault()?.EquityRiskPremium;
            var c69 = c68_Table2CountryMoodysRatingValue.MoodysRating;
            var c70_Table3MoodysRatingValue = MoodysRatingMapRecords.FirstOrDefault(o => o.Rating == c68_Table2CountryMoodysRatingValue.MoodysRating);
            var c71 = GlobalDefaultsRecords.FirstOrDefault()?.RelativeVolatility;

            MatureMarketImpliedEquityRiskPremium = c67.Value;
            CountryRatingBasedDefaultSpread = c70_Table3MoodysRatingValue.DefaultSpread.Value / 10000;
            RelativeVolatilityOfEquityVSBonds = c71.Value;
            EquityRiskPremium = MatureMarketImpliedEquityRiskPremium + (CountryRatingBasedDefaultSpread * RelativeVolatilityOfEquityVSBonds);//=C67+(C70*C71)

            result.EquityRiskPremium = EquityRiskPremium;
            var trbcMap = TRBCImportMapRecords.FirstOrDefault(o => o.TRBC2012HierarchicalID == GeneralClientInputs.TRBC2012HierarchicalID);

            UnleveredBeta = trbcMap.UnleveredBeta.Value;
            TotalDebtToEquity = trbcMap.TotalDebtMarketCap.Value;
            MarginalTaxRate = c68_Table2CountryTaxRateValue.TaxRate.Value;//(decimal).28;
            RiskFreeRate = table1CurrencyValue.RiskFreeRate.Value;// (decimal)0.06165;
            InterestCoverageRatio = trbcMap.Interestcoveradjusted.Value;//(decimal)0.371097294493;

            var table3 = MoodysRatingMapRecords.FirstOrDefault(o => o.Rating == c68_Table2CountryMoodysRatingValue.MoodysRating); //  

            var table4 = CorporateBondSpreadsMapRecords.FirstOrDefault(o => o.InterestCoverageFrom.Value < InterestCoverageRatio && InterestCoverageRatio <= o.InterestCoverageTo.Value);
            EstimatedCorporateDefaultSpread = table4.Spread.Value;// (decimal)0.006;
            EstimatedCountryDefaultSpread = c70_Table3MoodysRatingValue.DefaultSpread.Value / 10000;//(decimal)0.022;
            result.LeveredBeta = UnleveredBeta * (1 + TotalDebtToEquity * (1 - MarginalTaxRate));//C60*(1+C63*(1-C62))
            result.PreTaxCostOfDebt = RiskFreeRate + EstimatedCorporateDefaultSpread + EstimatedCountryDefaultSpread;
            result.TotalDebtEquity = TotalDebtToEquity;
            result.Taxrate = MarginalTaxRate;
            return result;
        }
        public Decimal MatureMarketImpliedEquityRiskPremium { get; private set; }
        public Decimal CountryRatingBasedDefaultSpread { get; private set; }
        public Decimal RelativeVolatilityOfEquityVSBonds { get; private set; }
        public Decimal EquityRiskPremium { get; private set; }
        public Decimal UnleveredBeta { get; private set; }
        public Decimal TotalDebtToEquity { get; private set; }
        public Decimal MarginalTaxRate { get; private set; }

        public Decimal RiskFreeRate { get; private set; }
        public Decimal InterestCoverageRatio { get; private set; }
        public Decimal EstimatedCorporateDefaultSpread { get; private set; }
        public Decimal EstimatedCountryDefaultSpread { get; private set; }
         
    }
}
