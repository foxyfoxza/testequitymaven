﻿using EquityMaven.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.ImportExport.Import;
namespace EquityMaven.ImportExport.Implementation
{

    public class TRBCImport : ExcelLoader<TRBCImportMap>
    {

        protected override int StartColumn { get; set; } = 0;
        protected override int StartRow { get; set; } = 1;
        protected override int ExcelReadStopAfterEmptyRows { get; set; } = 10;



        protected override string GetDefaultSheetName()
        {
            return EquityMaven.Tools.SettingsHelper.GetSetting<string>("TRBCImportSheetName", "TRBC_MAP");
        }
        public BatchType BatchType = BatchType.Monthly;

        public void Convert(InputBatch inputBatch, out IEnumerable<ThompsonReutersBusinessClassification> _ThompsonReutersBusinessClassifications, out IEnumerable<TRBCValue> _TRBCValues)
        {
            if (inputBatch.BatchType != BatchType)
            {
                _ThompsonReutersBusinessClassifications = Enumerable.Empty<ThompsonReutersBusinessClassification>();
                _TRBCValues = Enumerable.Empty<TRBCValue>();
                return;
            }
            var ThompsonReutersBusinessClassifications = new List<ThompsonReutersBusinessClassification>();
            
            var TRBCValues = new List<TRBCValue>();

            _ThompsonReutersBusinessClassifications = ThompsonReutersBusinessClassifications;
            _TRBCValues = TRBCValues;


            foreach(var item in ImportRecords.Select(o => o.TRBC2012HierarchicalID).Distinct().Select(_TRBC2012HierarchicalID => ImportRecords.FirstOrDefault(r => r.TRBC2012HierarchicalID == _TRBC2012HierarchicalID)))
            {
                ThompsonReutersBusinessClassification t1;
                TRBCValue t2;
                TRBCImport.Convert(item, inputBatch, out t1, out t2);
                ThompsonReutersBusinessClassifications.Add(t1);
                TRBCValues.Add(t2);
            }


            //ThompsonReutersBusinessClassifications.AddRange(
                
            //    ImportRecords.Select(o => o.TRBC2012HierarchicalID).Distinct().Select(_TRBC2012HierarchicalID =>
            //{
            //    var rec = ImportRecords.FirstOrDefault(r => r.TRBC2012HierarchicalID == _TRBC2012HierarchicalID);
            //    return new ThompsonReutersBusinessClassification { TRBC2012HierarchicalID  = rec.TRBC2012HierarchicalID, EconomicSectorName = rec.ECONOMICSECTOR, BusinessSectorName = rec.BUSINESSSECTOR, IndustryGroupName = rec.INDUSTRYGROUP, IndustryName = rec.INDUSTRY, ActivityName = rec.ACTIVITY, InputBatch = inputBatch };
            //}));

            //TRBCValues.AddRange(ThompsonReutersBusinessClassifications.Select(trbc =>
            //{
            //    var result = new TRBCValue { ThompsonReutersBusinessClassification= trbc, InputBatch = inputBatch, Created = DateTime.UtcNow, LastUpdated = DateTime.UtcNow};
            //    var raw = ImportRecords.FirstOrDefault(o => o.TRBC2012HierarchicalID == trbc.TRBC2012HierarchicalID);
            //    result.isActive = raw.ManualSelect.HasValue && raw.ManualSelect.Value;
            //    result.AccPayableDaySave = raw.AccPayableDaysave;
            //    result.AccRecDaysAve = raw.AccRecDaysave;
            //    result.Africa = raw.Africa;
            //    result.Asia = raw.Asia;
            //    result.AustraliaNewZealand = raw.AustraliaNewZealand;
            //    result.CurrentRatio = raw.CurrentRatio;
            //    result.EasternEuropeRussia = raw.EasternEuropeRussia;
            //    result.EBITDAMargin = raw.EBITDAMargin;
            //    result.EBITMargin = raw.EBITMargin;
            //    result.GrossMargin = raw.GrossMargin;
            //    result.InterestCover = raw.InterestCover;
            //    result.InterestCoverAdjusted = raw.Interestcoveradjusted;
            //    result.InventoryDaysAve = raw.InventoryDaysave;
            //    result.MiddleEast = raw.MiddleEast;
            //    result.NorthAmerica = raw.NorthAmerica;
            //    result.SouthCentralAmerica = raw.SouthCentralAmerica;
            //    result.SouthernEurope = raw.SouthernEurope;
            //    result.TotalDebtEBITDA= raw.TotalDebtEBITDA;
            //    result.TotalDebtMarketCap = raw.TotalDebtMarketCap;
            //    result.UnleveredBeta = raw.UnleveredBeta;
            //    result.WesternEurope = raw.WesternEurope;
            //    result.World = raw.World;
            //    return result;
            //}));


        }


        public static void Convert(TRBCImportMap from, InputBatch inputBatch, out ThompsonReutersBusinessClassification thompsonReutersBusinessClassification, out TRBCValue trbcValue)
        {
            thompsonReutersBusinessClassification = new ThompsonReutersBusinessClassification { TRBC2012HierarchicalID = from.TRBC2012HierarchicalID, EconomicSectorName = from.ECONOMICSECTOR, BusinessSectorName = from.BUSINESSSECTOR, IndustryGroupName = from.INDUSTRYGROUP, IndustryName = from.INDUSTRY, ActivityName = from.ACTIVITY, InputBatch = inputBatch };

            trbcValue = new TRBCValue { ThompsonReutersBusinessClassification = thompsonReutersBusinessClassification, InputBatch = inputBatch, Created = DateTime.UtcNow, LastUpdated = DateTime.UtcNow };

            trbcValue.isActive = from.ManualSelect.HasValue && from.ManualSelect.Value;
            trbcValue.AccPayableDaysAve = from.AccPayableDaysave;
            trbcValue.AccRecDaysAve = from.AccRecDaysave;
            trbcValue.Africa = from.Africa;
            trbcValue.Asia = from.Asia;
            trbcValue.AustraliaNewZealand = from.AustraliaNewZealand;
            trbcValue.CurrentRatio = from.CurrentRatio;
            trbcValue.EasternEuropeRussia = from.EasternEuropeRussia;
            trbcValue.EBITDAMargin = from.EBITDAMargin;
            trbcValue.EBITMargin = from.EBITMargin;
            trbcValue.GrossMargin = from.GrossMargin;
            trbcValue.InterestCover = from.InterestCover;
            trbcValue.InterestCoverAdjusted = from.Interestcoveradjusted;
            trbcValue.InventoryDaysAve = from.InventoryDaysave;
            trbcValue.MiddleEast = from.MiddleEast;
            trbcValue.NorthAmerica = from.NorthAmerica;
            trbcValue.SouthCentralAmerica = from.SouthCentralAmerica;
            trbcValue.SouthernEurope = from.SouthernEurope;
            trbcValue.TotalDebtEBITDA = from.TotalDebtEBITDA;
            trbcValue.TotalDebtMarketCap = from.TotalDebtMarketCap;
            trbcValue.UnleveredBeta = from.UnleveredBeta;
            trbcValue.WesternEurope = from.WesternEurope;
            trbcValue.World = from.World;

        }

    }
}
