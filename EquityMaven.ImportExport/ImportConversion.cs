﻿using EquityMaven.Data;
using EquityMaven.Data.Entities;
using EquityMaven.Interfaces.ClientInputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquityMaven.Data.Extensions;
using EquityMaven.ImportExport.Implementation;
using EquityMaven.ImportExport.Import;

namespace EquityMaven.ImportExport
{
    public class ImportConversion
    {
        public ImportConversion() { }
         
        public IEnumerable<CountryMoodysRatingMap> CountryMoodysRatings = Enumerable.Empty<CountryMoodysRatingMap>();
        public IEnumerable<CountryTaxRateMap> CountryTaxRates = Enumerable.Empty<CountryTaxRateMap>();
        public IEnumerable<CorporateBondSpreadMap> CorporateBondSpreads = Enumerable.Empty<CorporateBondSpreadMap>();
        

        public IEnumerable<Country> Countries = Enumerable.Empty<Country>();
        public IEnumerable<Currency> Currencies = Enumerable.Empty<Currency>();
        public IEnumerable<CurrencyRiskFreeRate> CurrencyRiskFreeRates = Enumerable.Empty<CurrencyRiskFreeRate>();
        public IEnumerable<MoodysRating> MoodysRatings = Enumerable.Empty<MoodysRating>();
        public IEnumerable<ThompsonReutersBusinessClassification> ThompsonReutersBusinessClassifications = Enumerable.Empty<ThompsonReutersBusinessClassification>();
        public IEnumerable<TRBCValue> TRBCValues = Enumerable.Empty<TRBCValue>();
        public InputBatch InputBatch { get; set; }
        public ImportConversion(String filename, InputBatch inputBatch, IQueryable<Country> countries, IQueryable<MoodysRating> moodysRatings)
        {
            InputBatch = inputBatch;
            var batchType = inputBatch.BatchType;

            if (batchType == BatchType.Monthly)
            {
                var currencyImport = new CurrencyImport();
                currencyImport.Import(filename);
                currencyImport.Convert(inputBatch, out Currencies, out CurrencyRiskFreeRates);

                var countryMoodysRatingImport = new CountryMoodysRatingImport();
                countryMoodysRatingImport.Import(filename);
                CountryMoodysRatings = countryMoodysRatingImport.Raw.ToList();
                //countryMoodysRatingImport.Convert(inputBatch, countries, moodysRatings, out CountryMoodysRatings);


                var trbcImport = new TRBCImport();
                if (EquityMaven.Tools.SettingsHelper.GetSetting<bool>("readfromjson", false) && System.IO.File.Exists("trbcImport.json"))
                {
                    Console.WriteLine("file exists");
                    var _raw = System.IO.File.ReadAllText("trbcImport.json");
                    trbcImport = Newtonsoft.Json.JsonConvert.DeserializeObject<TRBCImport>(_raw);
                }
                else
                {
                    trbcImport.Import(filename);
                    System.IO.File.WriteAllText("trbcImport.json", Newtonsoft.Json.JsonConvert.SerializeObject(trbcImport));
                }

                trbcImport.Convert(inputBatch, out ThompsonReutersBusinessClassifications, out TRBCValues);

                Console.WriteLine($"ThompsonReutersBusinessClassifications {ThompsonReutersBusinessClassifications.Count()} TRBCValues {TRBCValues.Count()}");
            }
            else if (batchType == BatchType.Annual)
            {
                var moodysRatingImport = batchType == BatchType.Monthly ? null : new MoodysRatingImport();
                moodysRatingImport?.Import(filename);
                moodysRatingImport.Convert(inputBatch, out MoodysRatings);


                var countryTaxRateImport = new CountryTaxRateImport();
                countryTaxRateImport.Import(filename);
                CountryTaxRates = countryTaxRateImport.Raw.ToList();
                //countryTaxRateImport.Convert(inputBatch, countries.ToList(), out CountryTaxRates);

                var corporateBondSpreadImport = new CorporateBondSpreadImport();
                corporateBondSpreadImport.Import(filename);
                CorporateBondSpreads = corporateBondSpreadImport.Raw.ToList();

                Console.WriteLine($"MoodysRatings {MoodysRatings.Count()} ");

            }

            //var LoadWaccUpdateTable2File = batchType == BatchType.Annual ? null : new ExcelLoader<WaccUpdateTable2File, RawCountryLookup>();
            //var LoadWaccUpdateTable4File = batchType == BatchType.Monthly ? null : new ExcelLoader<WaccUpdateTable4File, RawCorporateBondSpread>();

            //_RawCountryLookupResults = LoadWaccUpdateTable2File?.Import(filename, "UPDATE TABLE 2") ?? Enumerable.Empty<RawCountryLookup>();
            //_RawCorporateBondSpreadResults = LoadWaccUpdateTable4File?.Import(filename, "UPDATE TABLE 4") ?? Enumerable.Empty<RawCorporateBondSpread>();
 
        }

    }
     
}
